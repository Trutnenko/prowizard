﻿using System;

namespace WebApplicationProWizard
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {

        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            string CurrentUrl = Request.Url.ToString().ToLower();
                      
            if (CurrentUrl == "http://homga.ru/content/")
            {
                Response.Redirect("http://homga.ru/Index.aspx");
            }
            if (CurrentUrl == "http://homga.ru/dealdocuments/")
            {
                Response.Redirect("http://homga.ru/Index.aspx");
            }
            if (CurrentUrl == "http://homga.ru/metadata/")
            {
                Response.Redirect("http://homga.ru/Index.aspx");
            }
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            //Response.Redirect("http://homga.ru/Index.aspx");
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {

        }
    }
}