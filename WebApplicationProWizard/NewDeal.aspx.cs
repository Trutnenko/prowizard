﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Web;

namespace WebApplicationProWizard.WebForms
{
    public partial class WebFormNewDeal : System.Web.UI.Page
    {
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (PanelReminder.Visible)
            {
                PanelForward.Visible = false;
            }
            else
            {
                PanelForward.Visible = true;
            }

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            Conn.Open();

            SqlCommand CommandFindIdUser = Conn.CreateCommand();
            CommandFindIdUser.CommandText = "SELECT IdUser From dbo.TableUser Where PassKey = @PassKey";

            SqlParameter ParamFindUser = new SqlParameter();
            ParamFindUser.ParameterName = "@PassKey";
            HttpCookie CookieProWizardResult = new HttpCookie("CookieProWizard");
            ParamFindUser.Value = Request.Cookies["CookieProWizard"]["PassKey"].ToString();
            CommandFindIdUser.Parameters.Add(ParamFindUser);
            string IdUserBuyer = Convert.ToString(CommandFindIdUser.ExecuteScalar());

            SqlCommand CommandStatusDeal = Conn.CreateCommand();
            CommandStatusDeal.CommandText = "SELECT StatusDeal"
               + " From dbo.TableDeal Where IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandStatusDeal.Parameters.Add(ParamIdDeal);

            string StatusDeal = Convert.ToString(CommandStatusDeal.ExecuteScalar());

            if (StatusDeal == "Подбор")
            {
                TextBoxLinkObjectONE.Visible = true;

                CheckBox3YearOwner.Visible = true;
                CheckBoxContractForOriginal.Visible = true;
                CheckBoxExtractOwner.Visible = true;
                CheckBoxPriceContract.Visible = true;

                LinkButtonRedirectObjectONE.Visible = true;

                SqlCommand CommandObject = Conn.CreateCommand();
                CommandObject.CommandText = "SELECT"
                    + " YearOwner,"
                    + " PriceContract,"
                    + " ContractForOriginal,"
                    + " ExtractOwner,"
                    + " LinkObject"
                    + " From dbo.TableDeal"
                    + " Where IdUserBuyer = @ParamIdUserBuyer and IdDeal = @ParamIdDealONE";

                SqlParameter ParamIdUserBuyer = new SqlParameter();
                ParamIdUserBuyer.ParameterName = "@ParamIdUserBuyer";
                ParamIdUserBuyer.Value = IdUserBuyer;
                CommandObject.Parameters.Add(ParamIdUserBuyer);

                SqlParameter ParamIdDealONE = new SqlParameter();
                ParamIdDealONE.ParameterName = "@ParamIdDealONE";
                ParamIdDealONE.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandObject.Parameters.Add(ParamIdDealONE);

                SqlDataReader dReaderONE;

                dReaderONE = CommandObject.ExecuteReader();

                if (dReaderONE.Read())

                {
                    string YearOwner = dReaderONE["YearOwner"].ToString();
                    if (YearOwner == "True") { CheckBox3YearOwner.Checked = true; }

                    string PriceContract = dReaderONE["PriceContract"].ToString();
                    if (PriceContract == "True") { CheckBoxPriceContract.Checked = true; }

                    string ContractForOriginal = dReaderONE["ContractForOriginal"].ToString();
                    if (ContractForOriginal == "True") { CheckBoxContractForOriginal.Checked = true; }

                    string ExtractOwner = dReaderONE["ExtractOwner"].ToString();
                    if (ExtractOwner == "True") { CheckBoxExtractOwner.Checked = true; }

                    string LinkObjectOne = dReaderONE["LinkObject"].ToString();
                    TextBoxLinkObjectONE.Text = LinkObjectOne;
                }
                dReaderONE.Close();
            }

            else
            {
                PanelForward.Visible = false;
                PanelReminder.Visible = false;
                Label2.Text = "Этап завершен";

                TextBoxLinkObjectONE.Visible = true;
                LinkButtonRedirectObjectONE.Visible = true;

                CheckBox3YearOwner.Visible = true;
                CheckBoxContractForOriginal.Visible = true;
                CheckBoxExtractOwner.Visible = true;
                CheckBoxPriceContract.Visible = true;

                CheckBox3YearOwner.Enabled = false;
                CheckBoxContractForOriginal.Enabled = false;
                CheckBoxExtractOwner.Enabled = false;
                CheckBoxPriceContract.Enabled = false;
                TextBoxLinkObjectONE.Enabled = false;

                SqlCommand CommandObject = Conn.CreateCommand();
                CommandObject.CommandText = "SELECT"
                    + " YearOwner,"
                    + " PriceContract,"
                    + " ContractForOriginal,"
                    + " ExtractOwner,"
                    + " LinkObject"
                    + " From dbo.TableDeal"
                    + " Where IdUserBuyer = @ParamIdUserBuyer and"
                    + " IdDeal = @ParamIdDealONE";

                SqlParameter ParamIdUserBuyer = new SqlParameter();
                ParamIdUserBuyer.ParameterName = "@ParamIdUserBuyer";
                ParamIdUserBuyer.Value = IdUserBuyer;
                CommandObject.Parameters.Add(ParamIdUserBuyer);

                SqlParameter ParamIdDealONE = new SqlParameter();
                ParamIdDealONE.ParameterName = "@ParamIdDealONE";
                ParamIdDealONE.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandObject.Parameters.Add(ParamIdDealONE);

                SqlDataReader dReaderONE;

                dReaderONE = CommandObject.ExecuteReader();

                if (dReaderONE.Read())

                {
                    string YearOwner = dReaderONE["YearOwner"].ToString();
                    if (YearOwner == "True") { CheckBox3YearOwner.Checked = true; }

                    string PriceContract = dReaderONE["PriceContract"].ToString();
                    if (PriceContract == "True") { CheckBoxPriceContract.Checked = true; }

                    string ContractForOriginal = dReaderONE["ContractForOriginal"].ToString();
                    if (ContractForOriginal == "True") { CheckBoxContractForOriginal.Checked = true; }

                    string ExtractOwner = dReaderONE["ExtractOwner"].ToString();
                    if (ExtractOwner == "True") { CheckBoxExtractOwner.Checked = true; }

                    string LinkObjectOne = dReaderONE["LinkObject"].ToString();
                    TextBoxLinkObjectONE.Text = LinkObjectOne;
                }
                dReaderONE.Close();
            }
            Conn.Close();
        }

        protected void LinkButtonRedirectObjectOne_Click(object sender, EventArgs e)
        {
            string queryString = TextBoxLinkObjectONE.Text.ToString();
            string newWin = "window.open('" + queryString + "');";
            ClientScript.RegisterStartupScript(this.GetType(), "pop", newWin, true);
        }

        protected void ButtonForce_Click(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandForUpdateObject = new SqlCommand("UPDATE dbo.TableDeal SET"
             + " YearOwner = @ParamYearOwner,"
             + " PriceContract = @ParamPriceContract,"
             + " ContractForOriginal = @ParamContractForOriginal,"
             + " ExtractOwner = @ParamExtractOwner,"
             + " StatusDeal = @ParamStatusDeal, "
             + " LinkObject = @ParamLinkObject WHERE"
             + " IdDeal = @ParamIdDeal", Conn);

            SqlParameter Param3YearOwner = new SqlParameter();
            Param3YearOwner.ParameterName = "@ParamYearOwner";
            Param3YearOwner.Value = CheckBox3YearOwner.Checked.ToString();
            CommandForUpdateObject.Parameters.Add(Param3YearOwner);

            SqlParameter ParamPriceContract = new SqlParameter();
            ParamPriceContract.ParameterName = "@ParamPriceContract";
            ParamPriceContract.Value = CheckBoxPriceContract.Checked.ToString();
            CommandForUpdateObject.Parameters.Add(ParamPriceContract);

            SqlParameter ParamContractForOriginal = new SqlParameter();
            ParamContractForOriginal.ParameterName = "@ParamContractForOriginal";
            ParamContractForOriginal.Value = CheckBoxContractForOriginal.Checked.ToString();
            CommandForUpdateObject.Parameters.Add(ParamContractForOriginal);

            SqlParameter ParamTypeExtractOwner = new SqlParameter();
            ParamTypeExtractOwner.ParameterName = "@ParamExtractOwner";
            ParamTypeExtractOwner.Value = CheckBoxExtractOwner.Checked.ToString();
            CommandForUpdateObject.Parameters.Add(ParamTypeExtractOwner);

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandForUpdateObject.Parameters.Add(ParamIdDeal);

            SqlParameter ParamStatusDeal = new SqlParameter();
            ParamStatusDeal.ParameterName = "@ParamStatusDeal";
            ParamStatusDeal.Value = "Подключение продавца";
            CommandForUpdateObject.Parameters.Add(ParamStatusDeal);

            SqlParameter ParamLinkObject = new SqlParameter();
            ParamLinkObject.ParameterName = "@ParamLinkObject";
            ParamLinkObject.Value = TextBoxLinkObjectONE.Text.ToString();
            CommandForUpdateObject.Parameters.Add(ParamLinkObject);

            Conn.Open();
            CommandForUpdateObject.ExecuteNonQuery();
            Conn.Close();

            Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormCallSeller"));
        }

        protected void ButtonSaveDeal_Click(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandForUpdateObject = new SqlCommand("UPDATE dbo.TableDeal SET"
             + " YearOwner = @ParamYearOwner,"
             + " PriceContract = @ParamPriceContract,"
             + " ContractForOriginal = @ParamContractForOriginal,"
             + " ExtractOwner = @ParamExtractOwner,"
             + " LinkObject = @ParamLinkObject WHERE"
             + " IdDeal = @ParamIdDeal", Conn);

            SqlParameter Param3YearOwner = new SqlParameter();
            Param3YearOwner.ParameterName = "@ParamYearOwner";
            Param3YearOwner.Value = CheckBox3YearOwner.Checked.ToString();
            CommandForUpdateObject.Parameters.Add(Param3YearOwner);

            SqlParameter ParamPriceContract = new SqlParameter();
            ParamPriceContract.ParameterName = "@ParamPriceContract";
            ParamPriceContract.Value = CheckBoxPriceContract.Checked.ToString();
            CommandForUpdateObject.Parameters.Add(ParamPriceContract);

            SqlParameter ParamContractForOriginal = new SqlParameter();
            ParamContractForOriginal.ParameterName = "@ParamContractForOriginal";
            ParamContractForOriginal.Value = CheckBoxContractForOriginal.Checked.ToString();
            CommandForUpdateObject.Parameters.Add(ParamContractForOriginal);

            SqlParameter ParamTypeExtractOwner = new SqlParameter();
            ParamTypeExtractOwner.ParameterName = "@ParamExtractOwner";
            ParamTypeExtractOwner.Value = CheckBoxExtractOwner.Checked.ToString();
            CommandForUpdateObject.Parameters.Add(ParamTypeExtractOwner);

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandForUpdateObject.Parameters.Add(ParamIdDeal);

            SqlParameter ParamLinkObject = new SqlParameter();
            ParamLinkObject.ParameterName = "@ParamLinkObject";
            ParamLinkObject.Value = TextBoxLinkObjectONE.Text.ToString();
            CommandForUpdateObject.Parameters.Add(ParamLinkObject);

            Conn.Open();
            CommandForUpdateObject.ExecuteNonQuery();
            Conn.Close();
        }

        protected void ButtonForward_Click(object sender, EventArgs e)
        {
            if (
                CheckBox3YearOwner.Checked 
                & CheckBoxPriceContract.Checked
                & CheckBoxContractForOriginal.Checked
                & CheckBoxExtractOwner.Checked 
                )

            {
                SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
                SqlCommand CommandForUpdateObject = new SqlCommand("UPDATE dbo.TableDeal SET"
                 + " YearOwner = @ParamYearOwner,"
                 + " PriceContract = @ParamPriceContract,"
                 + " ContractForOriginal = @ParamContractForOriginal,"
                 + " ExtractOwner = @ParamExtractOwner,"
                 + " StatusDeal = @ParamStatusDeal,"
                 + " LinkObject = @ParamLinkObject"
                 + " WHERE IdDeal = @ParamIdDeal", Conn);

                SqlParameter Param3YearOwner = new SqlParameter();
                Param3YearOwner.ParameterName = "@ParamYearOwner";
                Param3YearOwner.Value = CheckBox3YearOwner.Checked.ToString();
                CommandForUpdateObject.Parameters.Add(Param3YearOwner);

                SqlParameter ParamPriceContract = new SqlParameter();
                ParamPriceContract.ParameterName = "@ParamPriceContract";
                ParamPriceContract.Value = CheckBoxPriceContract.Checked.ToString();
                CommandForUpdateObject.Parameters.Add(ParamPriceContract);

                SqlParameter ParamContractForOriginal = new SqlParameter();
                ParamContractForOriginal.ParameterName = "@ParamContractForOriginal";
                ParamContractForOriginal.Value = CheckBoxContractForOriginal.Checked.ToString();
                CommandForUpdateObject.Parameters.Add(ParamContractForOriginal);

                SqlParameter ParamTypeExtractOwner = new SqlParameter();
                ParamTypeExtractOwner.ParameterName = "@ParamExtractOwner";
                ParamTypeExtractOwner.Value = CheckBoxExtractOwner.Checked.ToString();
                CommandForUpdateObject.Parameters.Add(ParamTypeExtractOwner);

                SqlParameter ParamIdDeal = new SqlParameter();
                ParamIdDeal.ParameterName = "@ParamIdDeal";
                ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandForUpdateObject.Parameters.Add(ParamIdDeal);

                SqlParameter ParamStatusDeal = new SqlParameter();
                ParamStatusDeal.ParameterName = "@ParamStatusDeal";
                ParamStatusDeal.Value = "Подключение продавца";
                CommandForUpdateObject.Parameters.Add(ParamStatusDeal);

                SqlParameter ParamLinkObject = new SqlParameter();
                ParamLinkObject.ParameterName = "@ParamLinkObject";
                ParamLinkObject.Value = TextBoxLinkObjectONE.Text.ToString();
                CommandForUpdateObject.Parameters.Add(ParamLinkObject);

                Conn.Open();
                CommandForUpdateObject.ExecuteNonQuery();
                Conn.Close();

                Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormCallSeller"));
            }

            else
            {
                PanelForward.Visible = false;
                PanelReminder.Visible = true;
            }
        }

        protected void ButtonToBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);
        }
    }
}