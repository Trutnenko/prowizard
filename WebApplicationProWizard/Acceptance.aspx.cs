﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using EasyDox;
using System.Net;
using System.IO;
using System.Web;

namespace WebApplicationProWizard
{
    public partial class WebFormGettingDocument : System.Web.UI.Page
    {
        protected void Page_PreRender(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

            SqlCommand CommandCountContract = Conn.CreateCommand();
            CommandCountContract.CommandText = "SELECT Count (*) FROM dbo.TableAcceptance"
              + " WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandCountContract.Parameters.Add(ParamIdDeal);

            Conn.Open();
            string CountAcceptance = Convert.ToString(CommandCountContract.ExecuteScalar());
            Conn.Close();

            if (CountAcceptance == "0")
            {
                SqlCommand CommandCountPreContract = Conn.CreateCommand();
                CommandCountPreContract.CommandText = "SELECT Count (*) FROM dbo.TableContract"
                  + " WHERE IdDeal = @ParamIdDealTWO";

                SqlParameter ParamIdDealTWO = new SqlParameter();
                ParamIdDealTWO.ParameterName = "@ParamIdDealTWO";
                ParamIdDealTWO.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandCountPreContract.Parameters.Add(ParamIdDealTWO);

                Conn.Open();
                string CountPreContract = Convert.ToString(CommandCountPreContract.ExecuteScalar());
                Conn.Close();

                if (CountPreContract == "1")
                {
                    SqlCommand CommandPreContract = Conn.CreateCommand();
                    CommandPreContract.CommandText = "SELECT" +
                        " Buyer," +
                        " BuyerBirthDate," +

                        " Seller," +
                        " SellerBirthDate," +

                        " Address," +
                        " Floor," +
                        " Room," +
                        " Area" +

                        " FROM dbo.TableContract WHERE IdDeal = @ParamIdDealTHREE";

                    SqlParameter ParamIdDealTHREE = new SqlParameter();
                    ParamIdDealTHREE.ParameterName = "@ParamIdDealTHREE";
                    ParamIdDealTHREE.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                    CommandPreContract.Parameters.Add(ParamIdDealTHREE);

                    SqlDataReader dReader;

                    Conn.Open();
                    dReader = CommandPreContract.ExecuteReader();

                    if (dReader.Read())

                    {
                        string Buyer = dReader["Buyer"].ToString();
                        string BuyerBirthDate = dReader["BuyerBirthDate"].ToString();

                        string Seller = dReader["Seller"].ToString();
                        string SellerBirthDate = dReader["SellerBirthDate"].ToString();

                        string ContractAddress = dReader["Address"].ToString();
                        string ContractFloor = dReader["Floor"].ToString();
                        string ContractRoom = dReader["Room"].ToString();
                        string ContractArea = dReader["Area"].ToString();

                        dReader.Close();

                        SqlCommand CommandCreateContract =
                            new SqlCommand("INSERT INTO dbo.TableAcceptance" +
                 " (IdDeal," +

                 " Buyer," +
                 " BuyerBirthDate," +

                 " Seller," +
                 " SellerBirthDate," +

                 " Address," +
                 " Floor," +
                 " Room," +
                 " Area)" +

                 " Values" +

                 " (@ParamIdDeal4," +

                 " @ParamBuyer," +
                 " @ParamBuyerBirthDate," +

                 " @ParamSeller," +
                 " @ParamSellerBirthDate," +

                 " @ParamAddress," +
                 " @ParamFloor," +
                 " @ParamRoom," +
                 " @ParamArea)",

                 Conn);

                        SqlParameter ParamIdDeal4 = new SqlParameter();
                        ParamIdDeal4.ParameterName = "@ParamIdDeal4";
                        ParamIdDeal4.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                        CommandCreateContract.Parameters.Add(ParamIdDeal4);

                        SqlParameter ParamBuyer = new SqlParameter();
                        ParamBuyer.ParameterName = "@ParamBuyer";
                        ParamBuyer.Value = Buyer;
                        CommandCreateContract.Parameters.Add(ParamBuyer);

                        SqlParameter ParamBuyerBirthDate = new SqlParameter();
                        ParamBuyerBirthDate.ParameterName = "@ParamBuyerBirthDate";
                        ParamBuyerBirthDate.Value = BuyerBirthDate;
                        CommandCreateContract.Parameters.Add(ParamBuyerBirthDate);

                        SqlParameter ParamSeller = new SqlParameter();
                        ParamSeller.ParameterName = "@ParamSeller";
                        ParamSeller.Value = Seller;
                        CommandCreateContract.Parameters.Add(ParamSeller);

                        SqlParameter ParamSellerBirthDate = new SqlParameter();
                        ParamSellerBirthDate.ParameterName = "@ParamSellerBirthDate";
                        ParamSellerBirthDate.Value = SellerBirthDate;
                        CommandCreateContract.Parameters.Add(ParamSellerBirthDate);

                        SqlParameter ParamAddress = new SqlParameter();
                        ParamAddress.ParameterName = "@ParamAddress";
                        ParamAddress.Value = ContractAddress;
                        CommandCreateContract.Parameters.Add(ParamAddress);

                        SqlParameter ParamFloor = new SqlParameter();
                        ParamFloor.ParameterName = "@ParamFloor";
                        ParamFloor.Value = ContractFloor;
                        CommandCreateContract.Parameters.Add(ParamFloor);

                        SqlParameter ParamRoom = new SqlParameter();
                        ParamRoom.ParameterName = "@ParamRoom";
                        ParamRoom.Value = ContractRoom;
                        CommandCreateContract.Parameters.Add(ParamRoom);

                        SqlParameter ParamArea = new SqlParameter();
                        ParamArea.ParameterName = "@ParamArea";
                        ParamArea.Value = ContractArea;
                        CommandCreateContract.Parameters.Add(ParamArea);

                        CommandCreateContract.ExecuteNonQuery();
                        Conn.Close();

                        TextBoxContractAddress.Text = ContractAddress;
                        TextBoxContractArea.Text = ContractArea;
                        TextBoxContractFloor.Text = ContractFloor;
                        TextBoxContractRoom.Text = ContractRoom;

                        TextBoxContractBuyer.Text = Buyer;
                        TextBoxContractBuyerBirthDate.Text = BuyerBirthDate;

                        TextBoxContractSeller.Text = Seller;
                        TextBoxContractSellerBirthDate.Text = SellerBirthDate;
                    }
                }
            }

            if (CountAcceptance == "1")
            {
                SqlCommand CommandContract = Conn.CreateCommand();
                CommandContract.CommandText = "SELECT" +

                    " Buyer," +
                    " BuyerBirthDate," +

                    " Seller," +
                    " SellerBirthDate," +

                    " Address," +
                    " Floor," +
                    " Room," +
                    " Area" +

                    " FROM dbo.TableAcceptance WHERE IdDeal = @ParamIdDeal5";

                SqlParameter ParamIdDeal5 = new SqlParameter();
                ParamIdDeal5.ParameterName = "@ParamIdDeal5";
                ParamIdDeal5.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandContract.Parameters.Add(ParamIdDeal5);

                SqlDataReader dReader;

                Conn.Open();
                dReader = CommandContract.ExecuteReader();

                if (dReader.Read())

                {
                    TextBoxContractBuyer.Text = dReader["Buyer"].ToString();
                    TextBoxContractBuyerBirthDate.Text = dReader["BuyerBirthDate"].ToString();

                    TextBoxContractSeller.Text = dReader["Seller"].ToString();
                    TextBoxContractSellerBirthDate.Text = dReader["SellerBirthDate"].ToString();

                    TextBoxContractAddress.Text = dReader["Address"].ToString();
                    TextBoxContractFloor.Text = dReader["Floor"].ToString();
                    TextBoxContractRoom.Text = dReader["Room"].ToString();
                    TextBoxContractArea.Text = dReader["Area"].ToString();

                    dReader.Close();
                }
                Conn.Close();
            }
        }

        protected void ButtonAcceptance_Click(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

            SqlCommand CommandCountContract = Conn.CreateCommand();
            CommandCountContract.CommandText = "SELECT Count (*) FROM dbo.TableAcceptance"
              + " WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandCountContract.Parameters.Add(ParamIdDeal);

            Conn.Open();
            string CountAcceptance = Convert.ToString(CommandCountContract.ExecuteScalar());
            Conn.Close();

            if (CountAcceptance == "0")
            {
                var FieldValues = new Dictionary<string, string>
            {
                { "Адрес квартиры", TextBoxContractAddress.Text },
                { "Общая площадь", TextBoxContractArea.Text },
                { "Этаж", TextBoxContractFloor.Text },
                { "Количество комнат", TextBoxContractRoom.Text },

                { "ФИО покупателя", TextBoxContractBuyer.Text },
                { "Дата рождения покупателя", TextBoxContractBuyerBirthDate.Text },

                { "ФИО продавца", TextBoxContractSeller.Text },
                { "Дата рождения продавца", TextBoxContractSellerBirthDate.Text },

            };

                var Engine = new Engine();

                Engine.Merge(Server.MapPath("Content/Acceptance.docx"),
                FieldValues,
                Server.MapPath("DealDocuments/"
              + Request.Cookies["Deal"]["IdDeal"].ToString() +
                "/Acceptance.docx"));

                SqlCommand CommandCreateContract = new SqlCommand("INSERT into dbo.TableAcceptance" +
                    " (IdDeal," +

                    " Buyer," +
                    " BuyerBirthDate," +

                    " Seller," +
                    " SellerBirthDate," +

                    " Room," +
                    " Area," +
                    " Floor," +
                    " Address)" +

                    " Values" +

                    " (@ParamIdDealTWO," +

                    " @ParamBuyer," +
                    " @ParamBuyerBirthDate," +

                    " @ParamSeller," +
                    " @ParamSellerBirthDate," +

                    " @ParamRoom," +
                    " @ParamArea," +
                    " @ParamFloor," +
                    " @ParamAddress)"

                    , Conn);

                SqlParameter ParamRoom = new SqlParameter();
                ParamRoom.ParameterName = "@ParamRoom";
                ParamRoom.Value = TextBoxContractRoom.Text;
                CommandCreateContract.Parameters.Add(ParamRoom);

                SqlParameter ParamArea = new SqlParameter();
                ParamArea.ParameterName = "@ParamArea";
                ParamArea.Value = TextBoxContractArea.Text;
                CommandCreateContract.Parameters.Add(ParamArea);

                SqlParameter ParamFloor = new SqlParameter();
                ParamFloor.ParameterName = "@ParamFloor";
                ParamFloor.Value = TextBoxContractFloor.Text;
                CommandCreateContract.Parameters.Add(ParamFloor);

                SqlParameter ParamAddress = new SqlParameter();
                ParamAddress.ParameterName = "@ParamAddress";
                ParamAddress.Value = TextBoxContractAddress.Text;
                CommandCreateContract.Parameters.Add(ParamAddress);

                SqlParameter ParamIdDealTWO = new SqlParameter();
                ParamIdDealTWO.ParameterName = "@ParamIdDealTWO";
                ParamIdDealTWO.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandCreateContract.Parameters.Add(ParamIdDealTWO);

                SqlParameter ParamBuyer = new SqlParameter();
                ParamBuyer.ParameterName = "@ParamBuyer";
                ParamBuyer.Value = TextBoxContractBuyer.Text;
                CommandCreateContract.Parameters.Add(ParamBuyer);

                SqlParameter ParamBuyerBirthDate = new SqlParameter();
                ParamBuyerBirthDate.ParameterName = "@ParamBuyerBirthDate";
                ParamBuyerBirthDate.Value = TextBoxContractBuyerBirthDate.Text;
                CommandCreateContract.Parameters.Add(ParamBuyerBirthDate);

                SqlParameter ParamSeller = new SqlParameter();
                ParamSeller.ParameterName = "@ParamSeller";
                ParamSeller.Value = TextBoxContractSeller.Text;
                CommandCreateContract.Parameters.Add(ParamSeller);

                SqlParameter ParamSellerBirthDate = new SqlParameter();
                ParamSellerBirthDate.ParameterName = "@ParamSellerBirthDate";
                ParamSellerBirthDate.Value = TextBoxContractSellerBirthDate.Text;
                CommandCreateContract.Parameters.Add(ParamSellerBirthDate);

                Conn.Open();
                CommandCreateContract.ExecuteNonQuery();
                Conn.Close();

                string PassKey = Request.Cookies["CookieProWizard"]["PassKey"].ToString();
                string IdCurrentUser;
                string IdUserSeller;
                string IdUserBuyer;
                string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();

                SqlCommand CommandFindIdUser = Conn.CreateCommand();
                CommandFindIdUser.CommandText = "SELECT" +
                    " IdUser FROM dbo.TableUser" +
                    " WHERE PassKey = @ParamPassKey";

                SqlParameter ParamPassKey = new SqlParameter();
                ParamPassKey.ParameterName = "@ParamPassKey";
                ParamPassKey.Value = PassKey;
                CommandFindIdUser.Parameters.Add(ParamPassKey);

                Conn.Open();
                IdCurrentUser = CommandFindIdUser.ExecuteScalar().ToString();

                SqlCommand CommandDefineSeller = Conn.CreateCommand();
                CommandDefineSeller.CommandText = "SELECT"
                + " IdUserSeller FROM dbo.TableDeal"
                + " WHERE IdDeal = @ParamIdDealELSE";

                SqlParameter ParamIdDealELSE = new SqlParameter();
                ParamIdDealELSE.ParameterName = "@ParamIdDealELSE";
                ParamIdDealELSE.Value = IdDeal;
                CommandDefineSeller.Parameters.Add(ParamIdDealELSE);

                IdUserSeller = CommandDefineSeller.ExecuteScalar().ToString();

                SqlCommand CommandDefineBuyer = Conn.CreateCommand();
                CommandDefineBuyer.CommandText = "SELECT"
                + " IdUserBuyer FROM dbo.TableDeal"
                + " WHERE IdDeal = @ParamIdDealTWOTWO";

                SqlParameter ParamIdDealTWOTWO = new SqlParameter();
                ParamIdDealTWOTWO.ParameterName = "@ParamIdDealTWOTWO";
                ParamIdDealTWOTWO.Value = IdDeal;
                CommandDefineBuyer.Parameters.Add(ParamIdDealTWOTWO);

                IdUserBuyer = CommandDefineBuyer.ExecuteScalar().ToString();

                Conn.Close();

                if (IdCurrentUser == IdUserBuyer)
                {
                    Response.Redirect("http://homga.ru/GettingBuyer.aspx");
                }

                if (IdCurrentUser == IdUserSeller)
                {
                    Response.Redirect("http://homga.ru/GettingSeller.aspx");
                }
        }

            else
            {
                var FieldValues = new Dictionary<string, string>
            {
                { "Адрес квартиры", TextBoxContractAddress.Text },
                { "Общая площадь", TextBoxContractArea.Text },
                { "Этаж", TextBoxContractFloor.Text },
                { "Количество комнат", TextBoxContractRoom.Text },

                { "ФИО покупателя", TextBoxContractBuyer.Text },
                { "Дата рождения покупателя", TextBoxContractBuyerBirthDate.Text },

                { "ФИО продавца", TextBoxContractSeller.Text },
                { "Дата рождения продавца", TextBoxContractSellerBirthDate.Text },

            };

                var Engine = new Engine();

                Engine.Merge(Server.MapPath("Content/Acceptance.docx"),
                FieldValues,
                Server.MapPath("DealDocuments/"
              + Request.Cookies["Deal"]["IdDeal"].ToString() +
                "/Acceptance.docx"));

                SqlCommand CommandUpdateContract = new SqlCommand("UPDATE dbo.TableAcceptance" +
                    " Set" +

                    " Buyer = @ParamBuyer," +
                    " BuyerBirthDate = @ParamBuyerBirthDate," +

                    " Seller = @ParamSeller," +
                    " SellerBirthDate = @ParamSellerBirthDate," +

                    " Room = @ParamRoom," +
                    " Area = @ParamArea," +
                    " Floor = @ParamFloor," +
                    " Address = @ParamAddress" +

                    " WHERE IdDeal = @ParamIdDealTWO", Conn);

                SqlParameter ParamRoom = new SqlParameter();
                ParamRoom.ParameterName = "@ParamRoom";
                ParamRoom.Value = TextBoxContractRoom.Text;
                CommandUpdateContract.Parameters.Add(ParamRoom);

                SqlParameter ParamArea = new SqlParameter();
                ParamArea.ParameterName = "@ParamArea";
                ParamArea.Value = TextBoxContractArea.Text;
                CommandUpdateContract.Parameters.Add(ParamArea);

                SqlParameter ParamFloor = new SqlParameter();
                ParamFloor.ParameterName = "@ParamFloor";
                ParamFloor.Value = TextBoxContractFloor.Text;
                CommandUpdateContract.Parameters.Add(ParamFloor);

                SqlParameter ParamAddress = new SqlParameter();
                ParamAddress.ParameterName = "@ParamAddress";
                ParamAddress.Value = TextBoxContractAddress.Text;
                CommandUpdateContract.Parameters.Add(ParamAddress);

                SqlParameter ParamIdDealTWO = new SqlParameter();
                ParamIdDealTWO.ParameterName = "@ParamIdDealTWO";
                ParamIdDealTWO.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandUpdateContract.Parameters.Add(ParamIdDealTWO);

                SqlParameter ParamBuyer = new SqlParameter();
                ParamBuyer.ParameterName = "@ParamBuyer";
                ParamBuyer.Value = TextBoxContractBuyer.Text;
                CommandUpdateContract.Parameters.Add(ParamBuyer);

                SqlParameter ParamBuyerBirthDate = new SqlParameter();
                ParamBuyerBirthDate.ParameterName = "@ParamBuyerBirthDate";
                ParamBuyerBirthDate.Value = TextBoxContractBuyerBirthDate.Text;
                CommandUpdateContract.Parameters.Add(ParamBuyerBirthDate);

                SqlParameter ParamSeller = new SqlParameter();
                ParamSeller.ParameterName = "@ParamSeller";
                ParamSeller.Value = TextBoxContractSeller.Text;
                CommandUpdateContract.Parameters.Add(ParamSeller);

                SqlParameter ParamSellerBirthDate = new SqlParameter();
                ParamSellerBirthDate.ParameterName = "@ParamSellerBirthDate";
                ParamSellerBirthDate.Value = TextBoxContractSellerBirthDate.Text;
                CommandUpdateContract.Parameters.Add(ParamSellerBirthDate);

                Conn.Open();
                CommandUpdateContract.ExecuteNonQuery();
                Conn.Close();

                string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();
                string FileName = "Acceptance.docx";
                string Ftp = "ftp://waws-prod-db3-051.ftp.azurewebsites.windows.net/site/wwwroot/DealDocuments/" + IdDeal + "/";

                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(Ftp + FileName);
                request.Method = WebRequestMethods.Ftp.DownloadFile;

                request.Credentials = new NetworkCredential(@"ProWizard\ProWizard", "100Million");
                request.UsePassive = true;
                request.UseBinary = true;
                request.EnableSsl = false;

                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                using (MemoryStream stream = new MemoryStream())
                {
                    response.GetResponseStream().CopyTo(stream);
                    Response.AddHeader("content-disposition", "attachment;filename=" + FileName);
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.BinaryWrite(stream.ToArray());
                    Response.End();
                }
            }
        }

        protected void ButtonSave_Click(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

            SqlCommand CommandCountContract = Conn.CreateCommand();
            CommandCountContract.CommandText = "SELECT Count (*) FROM dbo.TableAcceptance"
              + " WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandCountContract.Parameters.Add(ParamIdDeal);

            Conn.Open();
            string CountAcceptance = Convert.ToString(CommandCountContract.ExecuteScalar());
            Conn.Close();

            if (CountAcceptance == "0")
            {
                SqlCommand CommandCreateContract = new SqlCommand("INSERT INTO dbo.TableAcceptance" +
                    " (IdDeal," +

                    " Buyer," +
                    " BuyerBirthDate," +

                    " Seller," +
                    " SellerBirthDate," +

                    " Room," +
                    " Area," +
                    " Floor," +
                    " Address)" +

                    " Values" +

                    " (@ParamIdDealTWO," +

                    " @ParamBuyer," +
                    " @ParamBuyerBirthDate," +

                    " @ParamSeller," +
                    " @ParamSellerBirthDate," +

                    " @ParamRoom," +
                    " @ParamArea," +
                    " @ParamFloor," +
                    " @ParamAddress)"

                    , Conn);

                SqlParameter ParamRoom = new SqlParameter();
                ParamRoom.ParameterName = "@ParamRoom";
                ParamRoom.Value = TextBoxContractRoom.Text;
                CommandCreateContract.Parameters.Add(ParamRoom);

                SqlParameter ParamArea = new SqlParameter();
                ParamArea.ParameterName = "@ParamArea";
                ParamArea.Value = TextBoxContractArea.Text;
                CommandCreateContract.Parameters.Add(ParamArea);

                SqlParameter ParamFloor = new SqlParameter();
                ParamFloor.ParameterName = "@ParamFloor";
                ParamFloor.Value = TextBoxContractFloor.Text;
                CommandCreateContract.Parameters.Add(ParamFloor);

                SqlParameter ParamAddress = new SqlParameter();
                ParamAddress.ParameterName = "@ParamAddress";
                ParamAddress.Value = TextBoxContractAddress.Text;
                CommandCreateContract.Parameters.Add(ParamAddress);

                SqlParameter ParamIdDealTWO = new SqlParameter();
                ParamIdDealTWO.ParameterName = "@ParamIdDealTWO";
                ParamIdDealTWO.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandCreateContract.Parameters.Add(ParamIdDealTWO);

                SqlParameter ParamBuyer = new SqlParameter();
                ParamBuyer.ParameterName = "@ParamBuyer";
                ParamBuyer.Value = TextBoxContractBuyer.Text;
                CommandCreateContract.Parameters.Add(ParamBuyer);

                SqlParameter ParamBuyerBirthDate = new SqlParameter();
                ParamBuyerBirthDate.ParameterName = "@ParamBuyerBirthDate";
                ParamBuyerBirthDate.Value = TextBoxContractBuyerBirthDate.Text;
                CommandCreateContract.Parameters.Add(ParamBuyerBirthDate);

                SqlParameter ParamSeller = new SqlParameter();
                ParamSeller.ParameterName = "@ParamSeller";
                ParamSeller.Value = TextBoxContractSeller.Text;
                CommandCreateContract.Parameters.Add(ParamSeller);

                SqlParameter ParamSellerBirthDate = new SqlParameter();
                ParamSellerBirthDate.ParameterName = "@ParamSellerBirthDate";
                ParamSellerBirthDate.Value = TextBoxContractSellerBirthDate.Text;
                CommandCreateContract.Parameters.Add(ParamSellerBirthDate);

                Conn.Open();
                CommandCreateContract.ExecuteNonQuery();
                Conn.Close();
            }

            else
            {
                SqlCommand CommandUpdateContract = new SqlCommand("UPDATE dbo.TableAcceptance" +
                    " SET" +

                    " Buyer = @ParamBuyer," +
                    " BuyerBirthDate = @ParamBuyerBirthDate," +

                    " Seller = @ParamSeller," +
                    " SellerBirthDate = @ParamSellerBirthDate," +

                    " Room = @ParamRoom," +
                    " Area = @ParamArea," +
                    " Floor = @ParamFloor," +
                    " Address = @ParamAddress" +

                    " WHERE IdDeal = @ParamIdDealTWO", Conn);

                SqlParameter ParamRoom = new SqlParameter();
                ParamRoom.ParameterName = "@ParamRoom";
                ParamRoom.Value = TextBoxContractRoom.Text;
                CommandUpdateContract.Parameters.Add(ParamRoom);

                SqlParameter ParamArea = new SqlParameter();
                ParamArea.ParameterName = "@ParamArea";
                ParamArea.Value = TextBoxContractArea.Text;
                CommandUpdateContract.Parameters.Add(ParamArea);

                SqlParameter ParamFloor = new SqlParameter();
                ParamFloor.ParameterName = "@ParamFloor";
                ParamFloor.Value = TextBoxContractFloor.Text;
                CommandUpdateContract.Parameters.Add(ParamFloor);

                SqlParameter ParamAddress = new SqlParameter();
                ParamAddress.ParameterName = "@ParamAddress";
                ParamAddress.Value = TextBoxContractAddress.Text;
                CommandUpdateContract.Parameters.Add(ParamAddress);

                SqlParameter ParamIdDealTWO = new SqlParameter();
                ParamIdDealTWO.ParameterName = "@ParamIdDealTWO";
                ParamIdDealTWO.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandUpdateContract.Parameters.Add(ParamIdDealTWO);

                SqlParameter ParamBuyer = new SqlParameter();
                ParamBuyer.ParameterName = "@ParamBuyer";
                ParamBuyer.Value = TextBoxContractBuyer.Text;
                CommandUpdateContract.Parameters.Add(ParamBuyer);

                SqlParameter ParamBuyerBirthDate = new SqlParameter();
                ParamBuyerBirthDate.ParameterName = "@ParamBuyerBirthDate";
                ParamBuyerBirthDate.Value = TextBoxContractBuyerBirthDate.Text;
                CommandUpdateContract.Parameters.Add(ParamBuyerBirthDate);

                SqlParameter ParamSeller = new SqlParameter();
                ParamSeller.ParameterName = "@ParamSeller";
                ParamSeller.Value = TextBoxContractSeller.Text;
                CommandUpdateContract.Parameters.Add(ParamSeller);

                SqlParameter ParamSellerBirthDate = new SqlParameter();
                ParamSellerBirthDate.ParameterName = "@ParamSellerBirthDate";
                ParamSellerBirthDate.Value = TextBoxContractSellerBirthDate.Text;
                CommandUpdateContract.Parameters.Add(ParamSellerBirthDate);

                Conn.Open();
                CommandUpdateContract.ExecuteNonQuery();
                Conn.Close();
            }
        }

        protected void LinkButtonToHome_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            string PassKey = Request.Cookies["CookieProWizard"]["PassKey"].ToString();
            string IdCurrentUser;
            string IdUserSeller;
            string IdUserBuyer;
            string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

            SqlCommand CommandFindIdUser = Conn.CreateCommand();
            CommandFindIdUser.CommandText = "SELECT" +
                " IdUser FROM dbo.TableUser" +
                " WHERE PassKey = @ParamPassKey";

            SqlParameter ParamPassKey = new SqlParameter();
            ParamPassKey.ParameterName = "@ParamPassKey";
            ParamPassKey.Value = PassKey;
            CommandFindIdUser.Parameters.Add(ParamPassKey);

            Conn.Open();
            IdCurrentUser = CommandFindIdUser.ExecuteScalar().ToString();

            SqlCommand CommandDefineSeller = Conn.CreateCommand();
            CommandDefineSeller.CommandText = "SELECT"
            + " IdUserSeller FROM dbo.TableDeal"
            + " WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDealELSE = new SqlParameter();
            ParamIdDealELSE.ParameterName = "@ParamIdDeal";
            ParamIdDealELSE.Value = IdDeal;
            CommandDefineSeller.Parameters.Add(ParamIdDealELSE);

            IdUserSeller = CommandDefineSeller.ExecuteScalar().ToString();

            SqlCommand CommandDefineBuyer = Conn.CreateCommand();
            CommandDefineBuyer.CommandText = "SELECT"
            + " IdUserBuyer FROM dbo.TableDeal"
            + " WHERE IdDeal = @ParamIdDealTWO";

            SqlParameter ParamIdDealTWO = new SqlParameter();
            ParamIdDealTWO.ParameterName = "@ParamIdDealTWO";
            ParamIdDealTWO.Value = IdDeal;
            CommandDefineBuyer.Parameters.Add(ParamIdDealTWO);

            IdUserBuyer = CommandDefineBuyer.ExecuteScalar().ToString();

            Conn.Close();

            if (IdCurrentUser == IdUserBuyer)
            {
                Response.Redirect("http://homga.ru/GettingBuyer.aspx");
            }

            if (IdCurrentUser == IdUserSeller)
            {
                Response.Redirect("http://homga.ru/GettingSeller.aspx");
            }
        }
    }
}