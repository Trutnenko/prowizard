﻿//------------------------------------------------------------------------------
// <автоматически создаваемое>
//     Этот код создан программой.
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода. 
// </автоматически создаваемое>
//------------------------------------------------------------------------------

namespace WebApplicationProWizard {
    
    
    public partial class WebForm1 {
        
        /// <summary>
        /// form1 элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm form1;
        
        /// <summary>
        /// Label1 элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label1;
        
        /// <summary>
        /// LinkButtonPanelSignS элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton LinkButtonPanelSignS;
        
        /// <summary>
        /// LabelHomga элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label LabelHomga;
        
        /// <summary>
        /// LinkButtonPanelSign элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton LinkButtonPanelSign;
        
        /// <summary>
        /// LabelDetail элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label LabelDetail;
        
        /// <summary>
        /// LabelBenefit элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label LabelBenefit;
        
        /// <summary>
        /// LinkButtonPanelRegistration элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton LinkButtonPanelRegistration;
        
        /// <summary>
        /// PanelRegistration элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel PanelRegistration;
        
        /// <summary>
        /// TextBoxEmailRegistration элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox TextBoxEmailRegistration;
        
        /// <summary>
        /// TextBoxPasswortRegistration элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox TextBoxPasswortRegistration;
        
        /// <summary>
        /// LabelWhat элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label LabelWhat;
        
        /// <summary>
        /// LabelQuestion элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label LabelQuestion;
        
        /// <summary>
        /// TextBoxAnswer элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox TextBoxAnswer;
        
        /// <summary>
        /// LinkButtonRegistration элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton LinkButtonRegistration;
        
        /// <summary>
        /// LabelInfoRegistration элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label LabelInfoRegistration;
        
        /// <summary>
        /// LinkButtonOpenStepBuyer элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton LinkButtonOpenStepBuyer;
        
        /// <summary>
        /// LinkButtonOpenStepSeller элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton LinkButtonOpenStepSeller;
        
        /// <summary>
        /// PanelBuyerAboutStep элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel PanelBuyerAboutStep;
        
        /// <summary>
        /// PanelSellerAboutStep элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel PanelSellerAboutStep;
        
        /// <summary>
        /// LinkButtonCompanyAbout элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton LinkButtonCompanyAbout;
        
        /// <summary>
        /// LinkButtonVacancy элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton LinkButtonVacancy;
        
        /// <summary>
        /// LinkButtonPubOffer элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton LinkButtonPubOffer;
    }
}
