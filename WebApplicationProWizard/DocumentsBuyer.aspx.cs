﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Web;

namespace WebApplicationProWizard.WebForms.MainDocuments
{
    public partial class WebFormMainDocumentsBuyer : System.Web.UI.Page
    {
        protected void Page_PreRender(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandDefineStatusMainDocuments = Conn.CreateCommand();
            CommandDefineStatusMainDocuments.CommandText = "SELECT StatusDeal,"
                + " StatusPassport,"
                + " StatusSertificateOfOwnerShip,"
                + " StatusDocumentFoundation,"
                + " StatusExtractFromRegister,"
                + " StatusExtendedExtractFromRegister,"
                + " StatusTechnicalPassport,"
                + " StatusConsentOfSpouse,"
                + " StatusResolutionOfGuardianship,"
                + " StatusDebtForUtility,"
                + " StatusCertificateFromTaxInspection,"
                + " SertificateOfOwnerShipBurden, "
                + " PassportOwner,"
                + " PassportActual,"
                + " PassportSpouse,"
                + " SertificateOfOwnerShipPassport,"
                + " SertificateOfOwnerShipAddress,"
                + " SertificateOfOwnerShipBurden,"
                + " DocumentFoundationPassport,"
                + " DocumentFoundationAddress,"
                + " DocumentFoundationBurden,"
                + " ExtractFromRegisterPassport,"
                + " ExtractFromRegisterCharacteristic,"
                + " ExtractFromRegisterBurden,"
                + " ExtendedExtractFromRegisterPeople,"
                + " ExtendedExtractFromRegisterMinors,"
                + " ExtendedExtractFromRegisterTemporarilyВrawn,"
                + " TechnicalPassportCharacteristic,"
                + " ConsentOfSpouseCharacteristic,"
                + " ResolutionOfGuardianshipCharacteristic,"
                + " DebtForUtilityCharacteristic,"
                + " CertificateFromTaxInspectionCharacteristic"
                + " FROM dbo.TableDeal WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandDefineStatusMainDocuments.Parameters.Add(ParamIdDeal);

            SqlDataReader dReader;

            Conn.Open();
            dReader = CommandDefineStatusMainDocuments.ExecuteReader();

            if (dReader.Read())

            {
                string StatusDeal = dReader["StatusDeal"].ToString();
                string StatusPassport = dReader["StatusPassport"].ToString();
                string StatusSertificateOfOwnerShip = dReader["StatusSertificateOfOwnerShip"].ToString();
                string StatusDocumentFoundation = dReader["StatusDocumentFoundation"].ToString();
                string StatusExtractFromRegister = dReader["StatusExtractFromRegister"].ToString();
                string StatusExtendedExtractFromRegister = dReader["StatusExtendedExtractFromRegister"].ToString();
                string StatusTechnicalPassport = dReader["StatusTechnicalPassport"].ToString();
                string StatusConsentOfSpouse = dReader["StatusConsentOfSpouse"].ToString();
                string StatusResolutionOfGuardianship = dReader["StatusResolutionOfGuardianship"].ToString();
                string StatusDebtForUtility = dReader["StatusDebtForUtility"].ToString();
                string StatusCertificateFromTaxInspection = dReader["StatusCertificateFromTaxInspection"].ToString();

                if (StatusDeal == "Проверка документов")

                {
                    if (StatusPassport == "Подготовка")
                    {
                        LabelPassport.Text = "Ожидается";
                        LinkButtonDownLoadPassport.Visible = false;
                    }

                    if (StatusPassport == "На согласовании")
                    {
                        LabelPassport.Text = "На согласовании";
                        LinkButtonDownLoadPassport.Visible = true;
                        LinkButtonDenidePassport.Visible = true;
                        CheckBoxPassportOwner.Visible = true;
                        CheckBoxPassportActual.Visible = true;
                        CheckBoxPassportSpouse.Visible = true;
                        LinkButtonSavePassport.Visible = true;
                    }

                    if (StatusPassport == "Проверен")
                    {
                        LabelPassport.Text = "Проверен";
                        LinkButtonDownLoadPassport.Visible = true;
                        LinkButtonDownLoadPassport.Visible = true;
                        LinkButtonDenidePassport.Visible = true;
                        CheckBoxPassportOwner.Visible = true;
                        CheckBoxPassportActual.Visible = true;
                        CheckBoxPassportSpouse.Visible = true;
                        LinkButtonSavePassport.Visible = true;
                    }

                    if (StatusPassport == "Отклонен")
                    {
                        LabelPassport.Text = "Отклонен";
                        LinkButtonDownLoadPassport.Visible = false;
                    }

                    if (StatusSertificateOfOwnerShip == "Подготовка")
                    {
                        LabelSertificateOfOwnerShip.Text = "Ожидается";
                        LinkButtonDownLoadSertificateOfOwnerShip.Visible = false;
                    }
                    if (StatusSertificateOfOwnerShip == "На согласовании")
                    {
                        LabelSertificateOfOwnerShip.Text = "На согласовании";
                        LinkButtonDownLoadSertificateOfOwnerShip.Visible = true;
                        LinkButtonDenideSertificateOfOwnerShip.Visible = true;
                        CheckBoxSertificateOfOwnerShipPassport.Visible = true;
                        CheckBoxSertificateOfOwnerShipAddress.Visible = true;
                        CheckBoxSertificateOfOwnerShipBurden.Visible = true;
                        LinkButtonSaveSertificateOfOwnerShip.Visible = true;
                    }
                    if (StatusSertificateOfOwnerShip == "Проверен")
                    {
                        LabelSertificateOfOwnerShip.Text = "Проверен";
                        LinkButtonDownLoadSertificateOfOwnerShip.Visible = true;
                        LinkButtonDenideSertificateOfOwnerShip.Visible = true;
                        CheckBoxSertificateOfOwnerShipPassport.Visible = true;
                        CheckBoxSertificateOfOwnerShipAddress.Visible = true;
                        CheckBoxSertificateOfOwnerShipBurden.Visible = true;
                        LinkButtonSaveSertificateOfOwnerShip.Visible = true;
                    }
                    if (StatusSertificateOfOwnerShip == "Отклонен")
                    {
                        LabelSertificateOfOwnerShip.Text = "Отклонен";
                        LinkButtonDownLoadSertificateOfOwnerShip.Visible = false;
                    }

                    if (StatusDocumentFoundation == "Подготовка")
                    {
                        LabelDocumentFoundation.Text = "Ожидается";
                        LinkButtonDownLoadDocumentFoundation.Visible = false;
                    }
                    if (StatusDocumentFoundation == "На согласовании")
                    {
                        LabelDocumentFoundation.Text = "На согласовании";
                        LinkButtonDownLoadDocumentFoundation.Visible = true;
                        LinkButtonDenideDocumentFoundation.Visible = true;
                        CheckBoxDocumentFoundationPassport.Visible = true;
                        CheckBoxDocumentFoundationAddress.Visible = true;
                        CheckBoxDocumentFoundationBurden.Visible = true;
                        LinkButtonSaveDocumentFoundation.Visible = true;
                    }
                    if (StatusDocumentFoundation == "Проверен")
                    {
                        LabelDocumentFoundation.Text = "Проверен";
                        LinkButtonDownLoadDocumentFoundation.Visible = true;
                        LinkButtonDenideDocumentFoundation.Visible = true;
                        CheckBoxDocumentFoundationPassport.Visible = true;
                        CheckBoxDocumentFoundationAddress.Visible = true;
                        CheckBoxDocumentFoundationBurden.Visible = true;
                        LinkButtonSaveDocumentFoundation.Visible = true;
                    }
                    if (StatusDocumentFoundation == "Отклонен")
                    {
                        LabelDocumentFoundation.Text = "Отклонен";
                        LinkButtonDownLoadDocumentFoundation.Visible = false;
                    }


                    if (StatusExtractFromRegister == "Подготовка")
                    {
                        LabelExtractFromRegister.Text = "Ожидается";
                        LinkButtonDownLoadExtractFromRegister.Visible = false;
                    }
                    if (StatusExtractFromRegister == "На согласовании")
                    {
                        LabelExtractFromRegister.Text = "На согласовании";
                        LinkButtonDownLoadExtractFromRegister.Visible = true;
                        LinkButtonDenideExtractFromRegister.Visible = true;
                        CheckBoxExtractFromRegisterPassport.Visible = true;
                        CheckBoxExtractFromRegisterCharacteristic.Visible = true;
                        CheckBoxExtractFromRegisterBurden.Visible = true;
                        LinkButtonSaveExtractFromRegister.Visible = true;
                    }
                    if (StatusExtractFromRegister == "Проверен")
                    {
                        LabelExtractFromRegister.Text = "Проверен";
                        LinkButtonDownLoadExtractFromRegister.Visible = true;
                        LinkButtonDenideExtractFromRegister.Visible = true;
                        CheckBoxExtractFromRegisterPassport.Visible = true;
                        CheckBoxExtractFromRegisterCharacteristic.Visible = true;
                        CheckBoxExtractFromRegisterBurden.Visible = true;
                        LinkButtonSaveExtractFromRegister.Visible = true;
                    }
                    if (StatusExtractFromRegister == "Отклонен")
                    {
                        LabelExtractFromRegister.Text = "Отклонен";
                        LinkButtonDownLoadExtractFromRegister.Visible = false;
                    }

                    if (StatusExtendedExtractFromRegister == "Подготовка")
                    {
                        LabelExtendedExtractFromRegister.Text = "Ожидается";
                        LinkButtonDownLoadExtendedExtractFromRegister.Visible = false;
                    }
                    if (StatusExtendedExtractFromRegister == "На согласовании")
                    {
                        LabelExtendedExtractFromRegister.Text = "На согласовании";
                        LinkButtonDownLoadExtendedExtractFromRegister.Visible = true;
                        LinkButtonDenideExtendedExtractFromRegister.Visible = true;
                        CheckBoxExtendedExtractFromRegisterPeople.Visible = true;
                        CheckBoxExtendedExtractFromRegisterMinors.Visible = true;
                        CheckBoxExtendedExtractFromRegisterTemporarilyВrawn.Visible = true;
                        LinkButtonSaveExtendedExtractFromRegister.Visible = true;
                    }
                    if (StatusExtendedExtractFromRegister == "Проверен")
                    {
                        LabelExtendedExtractFromRegister.Text = "Проверен";
                        LinkButtonDownLoadExtendedExtractFromRegister.Visible = true;
                        LinkButtonDenideExtendedExtractFromRegister.Visible = true;
                        CheckBoxExtendedExtractFromRegisterPeople.Visible = true;
                        CheckBoxExtendedExtractFromRegisterMinors.Visible = true;
                        CheckBoxExtendedExtractFromRegisterTemporarilyВrawn.Visible = true;
                        LinkButtonSaveExtendedExtractFromRegister.Visible = true;
                    }
                    if (StatusExtendedExtractFromRegister == "Отклонен")
                    {
                        LabelExtendedExtractFromRegister.Text = "Отклонен";
                        LinkButtonDownLoadExtendedExtractFromRegister.Visible = false;
                    }

                    if (StatusTechnicalPassport == "Подготовка")
                    {
                        LabelTechnicalPassport.Text = "Ожидается";
                        LinkButtonDownLoadTechnicalPassport.Visible = false;
                    }
                    if (StatusTechnicalPassport == "На согласовании")
                    {
                        LabelTechnicalPassport.Text = "На согласовании";
                        LinkButtonDownLoadTechnicalPassport.Visible = true;
                        LinkButtonDenideTechnicalPassport.Visible = true;
                        CheckBoxTechnicalPassportCharacteristic.Visible = true;
                        LinkButtonSaveTechnicalPassport.Visible = true;
                    }
                    if (StatusTechnicalPassport == "Проверен")
                    {
                        LabelTechnicalPassport.Text = "Проверен";
                        LinkButtonDownLoadTechnicalPassport.Visible = true;
                        LinkButtonDenideTechnicalPassport.Visible = true;
                        CheckBoxTechnicalPassportCharacteristic.Visible = true;
                        LinkButtonSaveTechnicalPassport.Visible = true;
                    }
                    if (StatusTechnicalPassport == "Отклонен")
                    {
                        LabelTechnicalPassport.Text = "Отклонен";
                        LinkButtonDownLoadTechnicalPassport.Visible = false;
                    }

                    if (StatusConsentOfSpouse == "Подготовка")
                    {
                        LabelConsentOfSpouse.Text = "Ожидается";
                        LinkButtonDownLoadConsentOfSpouse.Visible = false;
                    }
                    if (StatusConsentOfSpouse == "На согласовании")
                    {
                        LabelConsentOfSpouse.Text = "На согласовании";
                        LinkButtonDownLoadConsentOfSpouse.Visible = true;
                        LinkButtonDenideConsentOfSpouse.Visible = true;
                        CheckBoxConsentOfSpouseCharacteristic.Visible = true;
                        LinkButtonSaveConsentOfSpouse.Visible = true;
                    }
                    if (StatusConsentOfSpouse == "Проверен")
                    {
                        LabelConsentOfSpouse.Text = "Проверен";
                        LinkButtonDownLoadConsentOfSpouse.Visible = true;
                        LinkButtonDenideConsentOfSpouse.Visible = true;
                        CheckBoxConsentOfSpouseCharacteristic.Visible = true;
                        LinkButtonSaveConsentOfSpouse.Visible = true;
                    }
                    if (StatusConsentOfSpouse == "Отклонен")
                    {
                        LabelConsentOfSpouse.Text = "Отклонен";
                        LinkButtonDownLoadConsentOfSpouse.Visible = false;
                    }

                    if (StatusResolutionOfGuardianship == "Подготовка")
                    {
                        LabelResolutionOfGuardianship.Text = "Ожидается";
                        LinkButtonDownLoadResolutionOfGuardianship.Visible = false;
                    }
                    if (StatusResolutionOfGuardianship == "На согласовании")
                    {
                        LabelResolutionOfGuardianship.Text = "На согласовании";
                        LinkButtonDownLoadResolutionOfGuardianship.Visible = true;
                        LinkButtonDenideResolutionOfGuardianship.Visible = true;
                        CheckBoxResolutionOfGuardianshipCharacteristic.Visible = true;
                        LinkButtonSaveResolutionOfGuardianship.Visible = true;
                    }
                    if (StatusResolutionOfGuardianship == "Проверен")
                    {
                        LabelResolutionOfGuardianship.Text = "Проверен";
                        LinkButtonDownLoadResolutionOfGuardianship.Visible = true;
                        LinkButtonDenideResolutionOfGuardianship.Visible = true;
                        CheckBoxResolutionOfGuardianshipCharacteristic.Visible = true;
                        LinkButtonSaveResolutionOfGuardianship.Visible = true;
                    }
                    if (StatusResolutionOfGuardianship == "Отклонен")
                    {
                        LabelResolutionOfGuardianship.Text = "Отклонен";
                        LinkButtonDownLoadResolutionOfGuardianship.Visible = false;
                    }

                    if (StatusDebtForUtility == "Подготовка")
                    {
                        LabelDebtForUtility.Text = "Ожидается";
                        LinkButtonDownLoadDebtForUtility.Visible = false;
                    }
                    if (StatusDebtForUtility == "На согласовании")
                    {
                        LabelDebtForUtility.Text = "На согласовании";
                        LinkButtonDownLoadDebtForUtility.Visible = true;
                        LinkButtonDenideDebtForUtility.Visible = true;
                        CheckBoxDebtForUtilityCharacteristic.Visible = true;
                        LinkButtonSaveDebtForUtility.Visible = true;
                    }
                    if (StatusDebtForUtility == "Проверен")
                    {
                        LabelDebtForUtility.Text = "Проверен";
                        LinkButtonDownLoadDebtForUtility.Visible = true;
                        LinkButtonDenideDebtForUtility.Visible = true;
                        CheckBoxDebtForUtilityCharacteristic.Visible = true;
                        LinkButtonSaveDebtForUtility.Visible = true;
                    }
                    if (StatusDebtForUtility == "Отклонен")
                    {
                        LabelDebtForUtility.Text = "Отклонен";
                        LinkButtonDownLoadDebtForUtility.Visible = false;
                    }

                    if (StatusCertificateFromTaxInspection == "Подготовка")
                    {
                        LabelCertificateFromTaxInspection.Text = "Ожидается";
                        LinkButtonDownLoadCertificateFromTaxInspection.Visible = false;
                    }
                    if (StatusCertificateFromTaxInspection == "На согласовании")
                    {
                        LabelCertificateFromTaxInspection.Text = "На согласовании";
                        LinkButtonDownLoadCertificateFromTaxInspection.Visible = true;
                        LinkButtonDenideCertificateFromTaxInspection.Visible = true;
                        CheckBoxCertificateFromTaxInspectionCharacteristic.Visible = true;
                        LinkButtonSaveCertificateFromTaxInspection.Visible = true;
                    }
                    if (StatusCertificateFromTaxInspection == "Проверен")
                    {
                        LabelCertificateFromTaxInspection.Text = "Проверен";
                        LinkButtonDownLoadCertificateFromTaxInspection.Visible = true;
                        LinkButtonDenideCertificateFromTaxInspection.Visible = true;
                        CheckBoxCertificateFromTaxInspectionCharacteristic.Visible = true;
                        LinkButtonSaveCertificateFromTaxInspection.Visible = true;
                    }
                    if (StatusCertificateFromTaxInspection == "Отклонен")
                    {
                        LabelCertificateFromTaxInspection.Text = "Отклонен";
                        LinkButtonDownLoadCertificateFromTaxInspection.Visible = false;
                    }

                    string PassportOwner = dReader["PassportOwner"].ToString();
                    if (PassportOwner == "True") { CheckBoxPassportOwner.Checked = true; }
                    else { CheckBoxPassportOwner.Checked = false; }

                    string PassportActual = dReader["PassportActual"].ToString();
                    if (PassportActual == "True") { CheckBoxPassportActual.Checked = true; }
                    else { CheckBoxPassportActual.Checked = false; }

                    string PassportSpouse = dReader["PassportSpouse"].ToString();
                    if (PassportSpouse == "True") { CheckBoxPassportSpouse.Checked = true; }
                    else { CheckBoxPassportSpouse.Checked = false; }

                    string SertificateOfOwnerShipPassport = dReader["SertificateOfOwnerShipPassport"].ToString();
                    if (SertificateOfOwnerShipPassport == "True") { CheckBoxSertificateOfOwnerShipPassport.Checked = true; }
                    else { CheckBoxSertificateOfOwnerShipPassport.Checked = false; }

                    string SertificateOfOwnerShipAddress = dReader["SertificateOfOwnerShipAddress"].ToString();
                    if (SertificateOfOwnerShipAddress == "True") { CheckBoxSertificateOfOwnerShipAddress.Checked = true; }
                    else { CheckBoxSertificateOfOwnerShipAddress.Checked = false; }

                    string SertificateOfOwnerShipBurden = dReader["SertificateOfOwnerShipBurden"].ToString();
                    if (SertificateOfOwnerShipBurden == "True") { CheckBoxSertificateOfOwnerShipBurden.Checked = true; }
                    else { CheckBoxSertificateOfOwnerShipBurden.Checked = false; }

                    string DocumentFoundationPassport = dReader["DocumentFoundationPassport"].ToString();
                    if (DocumentFoundationPassport == "True") { CheckBoxDocumentFoundationPassport.Checked = true; }
                    else { CheckBoxDocumentFoundationPassport.Checked = false; }

                    string DocumentFoundationAddress = dReader["DocumentFoundationAddress"].ToString();
                    if (DocumentFoundationAddress == "True") { CheckBoxDocumentFoundationAddress.Checked = true; }
                    else { CheckBoxDocumentFoundationAddress.Checked = false; }

                    string DocumentFoundationBurden = dReader["DocumentFoundationBurden"].ToString();
                    if (DocumentFoundationBurden == "True") { CheckBoxDocumentFoundationBurden.Checked = true; }
                    else { CheckBoxDocumentFoundationBurden.Checked = false; }

                    string ExtractFromRegisterPassport = dReader["ExtractFromRegisterPassport"].ToString();
                    if (ExtractFromRegisterPassport == "True") { CheckBoxExtractFromRegisterPassport.Checked = true; }
                    else { CheckBoxExtractFromRegisterPassport.Checked = false; }

                    string ExtractFromRegisterCharacteristic = dReader["ExtractFromRegisterCharacteristic"].ToString();
                    if (ExtractFromRegisterCharacteristic == "True") { CheckBoxExtractFromRegisterCharacteristic.Checked = true; }
                    else { CheckBoxExtractFromRegisterCharacteristic.Checked = false; }

                    string ExtractFromRegisterBurden = dReader["ExtractFromRegisterBurden"].ToString();
                    if (ExtractFromRegisterBurden == "True") { CheckBoxExtractFromRegisterBurden.Checked = true; }
                    else { CheckBoxExtractFromRegisterBurden.Checked = false; }

                    string ExtendedExtractFromRegisterPeople = dReader["ExtendedExtractFromRegisterPeople"].ToString();
                    if (ExtendedExtractFromRegisterPeople == "True") { CheckBoxExtendedExtractFromRegisterPeople.Checked = true; }
                    else { CheckBoxExtendedExtractFromRegisterPeople.Checked = false; }

                    string ExtendedExtractFromRegisterMinors = dReader["ExtendedExtractFromRegisterMinors"].ToString();
                    if (ExtendedExtractFromRegisterMinors == "True") { CheckBoxExtendedExtractFromRegisterMinors.Checked = true; }
                    else { CheckBoxExtendedExtractFromRegisterMinors.Checked = false; }

                    string ExtendedExtractFromRegisterTemporarilyВrawn = dReader["ExtendedExtractFromRegisterTemporarilyВrawn"].ToString();
                    if (ExtendedExtractFromRegisterTemporarilyВrawn == "True") { CheckBoxExtendedExtractFromRegisterTemporarilyВrawn.Checked = true; }
                    else { CheckBoxExtendedExtractFromRegisterTemporarilyВrawn.Checked = false; }

                    string TechnicalPassportCharacteristic = dReader["TechnicalPassportCharacteristic"].ToString();
                    if (TechnicalPassportCharacteristic == "True") { CheckBoxTechnicalPassportCharacteristic.Checked = true; }
                    else { CheckBoxTechnicalPassportCharacteristic.Checked = false; }

                    string ConsentOfSpouseCharacteristic = dReader["ConsentOfSpouseCharacteristic"].ToString();
                    if (ConsentOfSpouseCharacteristic == "True") { CheckBoxConsentOfSpouseCharacteristic.Checked = true; }
                    else { CheckBoxConsentOfSpouseCharacteristic.Checked = false; }

                    string ResolutionOfGuardianshipCharacteristic = dReader["ResolutionOfGuardianshipCharacteristic"].ToString();
                    if (ResolutionOfGuardianshipCharacteristic == "True") { CheckBoxResolutionOfGuardianshipCharacteristic.Checked = true; }
                    else { CheckBoxResolutionOfGuardianshipCharacteristic.Checked = false; }

                    string DebtForUtilityCharacteristic = dReader["DebtForUtilityCharacteristic"].ToString();
                    if (DebtForUtilityCharacteristic == "True") { CheckBoxDebtForUtilityCharacteristic.Checked = true; }
                    else { CheckBoxDebtForUtilityCharacteristic.Checked = false; }

                    string CertificateFromTaxInspectionCharacteristic = dReader["CertificateFromTaxInspectionCharacteristic"].ToString();
                    if (CertificateFromTaxInspectionCharacteristic == "True") { CheckBoxCertificateFromTaxInspectionCharacteristic.Checked = true; }
                    else { CheckBoxCertificateFromTaxInspectionCharacteristic.Checked = false; }

                }

                if (StatusDeal != "Проверка документов")

                {
                    PanelCommonInformation.Visible = false;
                    PanelCloseForm.Visible = true;
                    PanelForward.Visible = false;

                    LinkButtonPassport.Enabled = false;
                    LinkButtonSertificateOfOwnerShip.Enabled = false;
                    LinkButtonDocumentFoundation.Enabled = false;
                    LinkButtonExtractFromRegister.Enabled = false;
                    LinkButtonExtendedExtractFromRegister.Enabled = false;
                    LinkButtonTechnicalPassport.Enabled = false;
                    LinkButtonConsentOfSpouse.Enabled = false;
                    LinkButtonResolutionOfGuardianship.Enabled = false;
                    LinkButtonDebtForUtility.Enabled = false;
                    LinkButtonCertificateFromTaxInspection.Enabled = false;

                    if (StatusPassport == "Подготовка")
                    {
                        LinkButtonDownLoadPassport.Visible = false;
                        LabelPassport.Text = "Документ не был прислан";
                    }
                    if (StatusPassport == "На согласовании")
                    {
                        LinkButtonDownLoadPassport.Visible = true;
                        LabelPassport.Text = "Документ не был согласован";
                    }
                    if (StatusPassport == "Проверен")
                    { LinkButtonDownLoadPassport.Visible = true; LabelPassport.Text = "Документ был проверен"; }
                    if (StatusPassport == "Отклонен")
                    {
                        LinkButtonDownLoadPassport.Visible = false; LabelPassport.Text = "Документ был отклонен";
                    }

                    if (StatusSertificateOfOwnerShip == "Подготовка")
                    { LinkButtonDownLoadSertificateOfOwnerShip.Visible = false; LabelSertificateOfOwnerShip.Text = "Документ не был прислан"; }
                    if (StatusSertificateOfOwnerShip == "На согласовании")
                    { LinkButtonDownLoadSertificateOfOwnerShip.Visible = true; LabelSertificateOfOwnerShip.Text = "Документ не был согласован"; }
                    if (StatusSertificateOfOwnerShip == "Проверен")
                    { LinkButtonDownLoadSertificateOfOwnerShip.Visible = true; LabelSertificateOfOwnerShip.Text = "Документ был проверен"; }
                    if (StatusSertificateOfOwnerShip == "Отклонен")
                    { LinkButtonDownLoadSertificateOfOwnerShip.Visible = false; LabelSertificateOfOwnerShip.Text = "Документ был отклонен"; }

                    if (StatusDocumentFoundation == "Подготовка")
                    { LinkButtonDownLoadDocumentFoundation.Visible = false; LabelDocumentFoundation.Text = "Документ не был прислан"; }
                    if (StatusDocumentFoundation == "На согласовании")
                    { LinkButtonDownLoadDocumentFoundation.Visible = true; LabelDocumentFoundation.Text = "Документ не был согласован"; }
                    if (StatusDocumentFoundation == "Проверен")
                    { LinkButtonDownLoadDocumentFoundation.Visible = true; LabelDocumentFoundation.Text = "Документ был проверен"; }
                    if (StatusDocumentFoundation == "Отклонен")
                    { LinkButtonDownLoadDocumentFoundation.Visible = false; LabelDocumentFoundation.Text = "Документ был отклонен"; }

                    if (StatusExtractFromRegister == "Подготовка")
                    { LinkButtonDownLoadExtractFromRegister.Visible = false; LabelExtractFromRegister.Text = "Документ не был прислан"; }
                    if (StatusExtractFromRegister == "На согласовании")
                    { LinkButtonDownLoadExtractFromRegister.Visible = true; LabelExtractFromRegister.Text = "Документ не был согласован"; }
                    if (StatusExtractFromRegister == "Проверен")
                    { LinkButtonDownLoadExtractFromRegister.Visible = true; LabelExtractFromRegister.Text = "Документ был проверен"; }
                    if (StatusExtractFromRegister == "Отклонен")
                    { LinkButtonDownLoadExtractFromRegister.Visible = false; LabelExtractFromRegister.Text = "Документ был отклонен"; }

                    if (StatusExtendedExtractFromRegister == "Подготовка")
                    { LinkButtonDownLoadExtendedExtractFromRegister.Visible = false; LabelExtendedExtractFromRegister.Text = "Документ не был прислан"; }
                    if (StatusExtendedExtractFromRegister == "На согласовании")
                    { LinkButtonDownLoadExtendedExtractFromRegister.Visible = true; LabelExtendedExtractFromRegister.Text = "Документ не был согласован"; }
                    if (StatusExtendedExtractFromRegister == "Проверен")
                    { LinkButtonDownLoadExtendedExtractFromRegister.Visible = true; LabelExtendedExtractFromRegister.Text = "Документ был проверен"; }
                    if (StatusExtendedExtractFromRegister == "Отклонен")
                    { LinkButtonDownLoadExtendedExtractFromRegister.Visible = false; LabelExtendedExtractFromRegister.Text = "Документ был отклонен"; }

                    if (StatusTechnicalPassport == "Подготовка")
                    { LinkButtonDownLoadTechnicalPassport.Visible = false; LabelTechnicalPassport.Text = "Документ не был прислан"; }
                    if (StatusTechnicalPassport == "На согласовании")
                    { LinkButtonDownLoadTechnicalPassport.Visible = true; LabelTechnicalPassport.Text = "Документ не был согласован"; }
                    if (StatusTechnicalPassport == "Проверен")
                    { LinkButtonDownLoadTechnicalPassport.Visible = true; LabelTechnicalPassport.Text = "Документ был проверен"; }
                    if (StatusTechnicalPassport == "Отклонен")
                    { LinkButtonDownLoadTechnicalPassport.Visible = false; LabelTechnicalPassport.Text = "Документ был отклонен"; }

                    if (StatusConsentOfSpouse == "Подготовка")
                    { LinkButtonDownLoadConsentOfSpouse.Visible = false; LabelConsentOfSpouse.Text = "Документ не был прислан"; }
                    if (StatusConsentOfSpouse == "На согласовании")
                    { LinkButtonDownLoadConsentOfSpouse.Visible = true; LabelConsentOfSpouse.Text = "Документ не был согласован"; }
                    if (StatusConsentOfSpouse == "Проверен")
                    { LinkButtonDownLoadConsentOfSpouse.Visible = true; LabelConsentOfSpouse.Text = "Документ был проверен"; }
                    if (StatusConsentOfSpouse == "Отклонен")
                    { LinkButtonDownLoadConsentOfSpouse.Visible = false; LabelConsentOfSpouse.Text = "Документ был отклонен"; }

                    if (StatusResolutionOfGuardianship == "Подготовка")
                    { LinkButtonDownLoadResolutionOfGuardianship.Visible = false; LabelResolutionOfGuardianship.Text = "Документ не был прислан"; }
                    if (StatusResolutionOfGuardianship == "На согласовании")
                    { LinkButtonDownLoadResolutionOfGuardianship.Visible = true; LabelResolutionOfGuardianship.Text = "Документ не был согласован"; }
                    if (StatusResolutionOfGuardianship == "Проверен")
                    { LinkButtonDownLoadResolutionOfGuardianship.Visible = true; LabelResolutionOfGuardianship.Text = "Документ был проверен"; }
                    if (StatusResolutionOfGuardianship == "Отклонен")
                    { LinkButtonDownLoadResolutionOfGuardianship.Visible = false; LabelResolutionOfGuardianship.Text = "Документ был отклонен"; }

                    if (StatusDebtForUtility == "Подготовка")
                    { LinkButtonDownLoadDebtForUtility.Visible = false; LabelDebtForUtility.Text = "Документ не был прислан"; }
                    if (StatusDebtForUtility == "На согласовании")
                    { LinkButtonDownLoadDebtForUtility.Visible = true; LabelDebtForUtility.Text = "Документ не был согласован"; }
                    if (StatusDebtForUtility == "Проверен")
                    { LinkButtonDownLoadDebtForUtility.Visible = true; LabelDebtForUtility.Text = "Документ был проверен"; }
                    if (StatusDebtForUtility == "Отклонен")
                    { LinkButtonDownLoadDebtForUtility.Visible = false; LabelDebtForUtility.Text = "Документ был отклонен"; }

                    if (StatusCertificateFromTaxInspection == "Подготовка")
                    { LinkButtonDownLoadCertificateFromTaxInspection.Visible = false; LabelCertificateFromTaxInspection.Text = "Документ не был прислан"; }
                    if (StatusCertificateFromTaxInspection == "На согласовании")
                    { LinkButtonDownLoadCertificateFromTaxInspection.Visible = true; LabelCertificateFromTaxInspection.Text = "Документ не был согласован"; }
                    if (StatusCertificateFromTaxInspection == "Проверен")
                    { LinkButtonDownLoadCertificateFromTaxInspection.Visible = true; LabelCertificateFromTaxInspection.Text = "Документ был проверен"; }
                    if (StatusCertificateFromTaxInspection == "Отклонен")
                    { LinkButtonDownLoadCertificateFromTaxInspection.Visible = false; LabelCertificateFromTaxInspection.Text = "Документ был отклонен"; }
                }
            }
            Conn.Close();
        }

        protected void LinkButtonSaveResult_Click(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

            SqlCommand CommandSetStatusPassportCheck = new SqlCommand("UPDATE dbo.TableDeal"
               + " SET PassportOwner = @ParamPassportOwner, PassportActual = @ParamPassportActual,"
               + " PassportSpouse = @ParamPassportSpouse" 
               + " WHERE IdDeal = @ParamIdDeal", Conn);

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandSetStatusPassportCheck.Parameters.Add(ParamIdDeal);

            SqlParameter ParamPassportOwner = new SqlParameter();
            ParamPassportOwner.ParameterName = "@ParamPassportOwner";
            ParamPassportOwner.Value = CheckBoxPassportOwner.Checked;
            CommandSetStatusPassportCheck.Parameters.Add(ParamPassportOwner);

            SqlParameter ParamPassportActual = new SqlParameter();
            ParamPassportActual.ParameterName = "@ParamPassportActual";
            ParamPassportActual.Value = CheckBoxPassportActual.Checked;
            CommandSetStatusPassportCheck.Parameters.Add(ParamPassportActual);

            SqlParameter ParamPassportSpouse = new SqlParameter();
            ParamPassportSpouse.ParameterName = "@ParamPassportSpouse";
            ParamPassportSpouse.Value = CheckBoxPassportActual.Checked;
            CommandSetStatusPassportCheck.Parameters.Add(ParamPassportSpouse);

            Conn.Open();
            CommandSetStatusPassportCheck.ExecuteNonQuery();
            Conn.Close();
        }

        protected void LinkButtonDownLoadPassport_Click(object sender, EventArgs e)
        {
            string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();
            string NameDirectory = Server.MapPath("DealDocuments/"
                 + Request.Cookies["Deal"]["IdDeal"].ToString());
            string[] FileNameArray = Directory.GetFiles(NameDirectory, "Passport*");
            string FileNameWithoutPath = System.IO.Path.GetFileName(FileNameArray[0]);

            string Ftp = "ftp://waws-prod-db3-051.ftp.azurewebsites.windows.net/site/wwwroot/DealDocuments/" +
                "/" + IdDeal + "/";

            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(Ftp + FileNameWithoutPath);
            request.Method = WebRequestMethods.Ftp.DownloadFile;

            request.Credentials = new NetworkCredential(@"ProWizard\ProWizard", "100Million");
            request.UsePassive = true;
            request.UseBinary = true;
            request.EnableSsl = false;

            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            using (MemoryStream stream = new MemoryStream())
            {
                response.GetResponseStream().CopyTo(stream);
                Response.AddHeader("content-disposition", "attachment;filename=" + FileNameWithoutPath);
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
        }
        protected void LinkPassport_Click(object sender, EventArgs e)
        {
            LinkButtonPassport.ForeColor = System.Drawing.ColorTranslator.FromHtml("#AA4825");
            LinkButtonSertificateOfOwnerShip.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonDocumentFoundation.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonExtractFromRegister.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonExtendedExtractFromRegister.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonTechnicalPassport.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonConsentOfSpouse.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonResolutionOfGuardianship.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonDebtForUtility.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonCertificateFromTaxInspection.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandStatus = Conn.CreateCommand();
            CommandStatus.CommandText = "SELECT StatusDeal, StatusPassport FROM" 
                + " dbo.TableDeal WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandStatus.Parameters.Add(ParamIdDeal);

            SqlDataReader dReader;

            Conn.Open();
            dReader = CommandStatus.ExecuteReader();

            if (dReader.Read())

            {
                string StatusDeal = dReader["StatusDeal"].ToString();
                string StatusPassport = dReader["StatusPassport"].ToString();

                if (StatusDeal == "Проверка документов")

                {
                    if (StatusPassport == "Подготовка")
                    {
                        PanelWaiting.Visible = true;

                        PanelCertificateFromTaxInspection.Visible = false;
                        PanelConsentOfSpouse.Visible = false;
                        PanelDebtForUtility.Visible = false;
                        PanelDocumentFoundation.Visible = false;
                        PanelExtendedExtractFromRegister.Visible = false;
                        PanelExtractFromRegister.Visible = false;
                        PanelResolutionOfGuardianship.Visible = false;
                        PanelSertificateOfOwnerShip.Visible = false;
                        PanelTechnicalPassport.Visible = false;
                        PanelPassport.Visible = false;
                    }

                    if (StatusPassport == "Отклонен")
                    {
                        PanelDenide.Visible = true;

                        PanelCertificateFromTaxInspection.Visible = false;
                        PanelConsentOfSpouse.Visible = false;
                        PanelDebtForUtility.Visible = false;
                        PanelDocumentFoundation.Visible = false;
                        PanelExtendedExtractFromRegister.Visible = false;
                        PanelExtractFromRegister.Visible = false;
                        PanelResolutionOfGuardianship.Visible = false;
                        PanelSertificateOfOwnerShip.Visible = false;
                        PanelTechnicalPassport.Visible = false;
                        PanelPassport.Visible = false;
                    }

                    if (StatusPassport == "На согласовании")
                    {
                        PanelPassport.Visible = true;
                        PanelCertificateFromTaxInspection.Visible = false;
                        PanelConsentOfSpouse.Visible = false;
                        PanelDebtForUtility.Visible = false;
                        PanelDocumentFoundation.Visible = false;
                        PanelExtendedExtractFromRegister.Visible = false;
                        PanelExtractFromRegister.Visible = false;
                        PanelResolutionOfGuardianship.Visible = false;
                        PanelSertificateOfOwnerShip.Visible = false;
                        PanelTechnicalPassport.Visible = false;
                    }

                    if (StatusPassport == "Проверен")
                    {
                        PanelPassport.Visible = true;
                        PanelCertificateFromTaxInspection.Visible = false;
                        PanelConsentOfSpouse.Visible = false;
                        PanelDebtForUtility.Visible = false;
                        PanelDocumentFoundation.Visible = false;
                        PanelExtendedExtractFromRegister.Visible = false;
                        PanelExtractFromRegister.Visible = false;
                        PanelResolutionOfGuardianship.Visible = false;
                        PanelSertificateOfOwnerShip.Visible = false;
                        PanelTechnicalPassport.Visible = false;
                    }
                }
            }
            Conn.Close();
        }
        protected void LinkButtonDenidePassport_Click(object sender, EventArgs e)
        {
            string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();
            string NameDirectory = Server.MapPath("DealDocuments/"
                 + Request.Cookies["Deal"]["IdDeal"].ToString());
            string[] FileName = Directory.GetFiles(NameDirectory, "Passport*");
            string FileNameWithoutPath = System.IO.Path.GetFileName(FileName[0]);

            File.Delete(Server.MapPath("DealDocuments/" + IdDeal + "/") + FileNameWithoutPath);

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandSetStatusMainDocuments = new SqlCommand("UPDATE dbo.TableDeal"
                + " SET StatusPassport = @ParamStatusPassport" 
                + " WHERE IdDeal = @ParamIdDeal", Conn);

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandSetStatusMainDocuments.Parameters.Add(ParamIdDeal);

            SqlParameter ParamStatusPassport = new SqlParameter();
            ParamStatusPassport.ParameterName = "@ParamStatusPassport";
            ParamStatusPassport.Value = "Отклонен";
            CommandSetStatusMainDocuments.Parameters.Add(ParamStatusPassport);

            Conn.Open();
            CommandSetStatusMainDocuments.ExecuteNonQuery();
            Conn.Close();

            PanelPassport.Visible = false;
            PanelDenide.Visible = true;
        }
        protected void LinkButtonSavePassport_Click(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

            if (CheckBoxPassportOwner.Checked && CheckBoxPassportActual.Checked && CheckBoxPassportSpouse.Checked)

            {
                SqlCommand CommandSetStatusPassport = new SqlCommand("UPDATE dbo.TableDeal"
                    + " SET StatusPassport = @ParamStatusPassport,"
                    + " PassportOwner = @ParamPassportOwner,"
                    + " PassportActual = @ParamPassportActual,"
                    + " PassportSpouse = @ParamPassportSpouse"
                    + " WHERE IdDeal = @ParamIdDealSetStatusPassport", Conn);

                SqlParameter ParamIdDealSetStatusPassport = new SqlParameter();
                ParamIdDealSetStatusPassport.ParameterName = "@ParamIdDealSetStatusPassport";
                ParamIdDealSetStatusPassport.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandSetStatusPassport.Parameters.Add(ParamIdDealSetStatusPassport);

                SqlParameter ParamPassportOwner = new SqlParameter();
                ParamPassportOwner.ParameterName = "@ParamPassportOwner";
                ParamPassportOwner.Value = CheckBoxPassportOwner.Checked.ToString();
                CommandSetStatusPassport.Parameters.Add(ParamPassportOwner);

                SqlParameter ParamPassportActual = new SqlParameter();
                ParamPassportActual.ParameterName = "@ParamPassportActual";
                ParamPassportActual.Value = CheckBoxPassportActual.Checked.ToString();
                CommandSetStatusPassport.Parameters.Add(ParamPassportActual);

                SqlParameter ParamPassportSpouse = new SqlParameter();
                ParamPassportSpouse.ParameterName = "@ParamPassportSpouse";
                ParamPassportSpouse.Value = CheckBoxPassportSpouse.Checked.ToString();
                CommandSetStatusPassport.Parameters.Add(ParamPassportSpouse);

                SqlParameter ParamStatusPassport = new SqlParameter();
                ParamStatusPassport.ParameterName = "@ParamStatusPassport";
                ParamStatusPassport.Value = "Проверен";
                CommandSetStatusPassport.Parameters.Add(ParamStatusPassport);

                Conn.Open();
                CommandSetStatusPassport.ExecuteNonQuery();
                Conn.Close();
            }

            else

            {
                SqlCommand CommandSetStatusPassport = new SqlCommand("UPDATE dbo.TableDeal"
                    + " SET StatusPassport = @ParamStatusPassport,"
                    + " PassportOwner = @ParamPassportOwner,"
                    + " PassportActual = @ParamPassportActual,"
                    + " PassportSpouse = @ParamPassportSpouse"
                    + " WHERE IdDeal = @ParamIdDealSetStatusPassport", Conn);

                SqlParameter ParamIdDealSetStatusPassport = new SqlParameter();
                ParamIdDealSetStatusPassport.ParameterName = "@ParamIdDealSetStatusPassport";
                ParamIdDealSetStatusPassport.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandSetStatusPassport.Parameters.Add(ParamIdDealSetStatusPassport);

                SqlParameter ParamPassportOwner = new SqlParameter();
                ParamPassportOwner.ParameterName = "@ParamPassportOwner";
                ParamPassportOwner.Value = CheckBoxPassportOwner.Checked.ToString();
                CommandSetStatusPassport.Parameters.Add(ParamPassportOwner);

                SqlParameter ParamPassportActual = new SqlParameter();
                ParamPassportActual.ParameterName = "@ParamPassportActual";
                ParamPassportActual.Value = CheckBoxPassportActual.Checked.ToString();
                CommandSetStatusPassport.Parameters.Add(ParamPassportActual);

                SqlParameter ParamPassportSpouse = new SqlParameter();
                ParamPassportSpouse.ParameterName = "@ParamPassportSpouse";
                ParamPassportSpouse.Value = CheckBoxPassportSpouse.Checked.ToString();
                CommandSetStatusPassport.Parameters.Add(ParamPassportSpouse);

                SqlParameter ParamStatusPassport = new SqlParameter();
                ParamStatusPassport.ParameterName = "@ParamStatusPassport";
                ParamStatusPassport.Value = "На согласовании";
                CommandSetStatusPassport.Parameters.Add(ParamStatusPassport);

                Conn.Open();
                CommandSetStatusPassport.ExecuteNonQuery();
                Conn.Close();
            }
        }

        protected void LinkButtonSertificateOfOwnerShip_Click(object sender, EventArgs e)
        {
            LinkButtonPassport.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonSertificateOfOwnerShip.ForeColor = System.Drawing.ColorTranslator.FromHtml("#AA4825");
            LinkButtonDocumentFoundation.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonExtractFromRegister.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonExtendedExtractFromRegister.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonTechnicalPassport.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonConsentOfSpouse.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonResolutionOfGuardianship.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonDebtForUtility.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonCertificateFromTaxInspection.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandStatus = Conn.CreateCommand();
            CommandStatus.CommandText = "SELECT StatusDeal, StatusSertificateOfOwnerShip"
                + " FROM dbo.TableDeal WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandStatus.Parameters.Add(ParamIdDeal);

            SqlDataReader dReader;

            Conn.Open();
            dReader = CommandStatus.ExecuteReader();

            if (dReader.Read())

            {
                string StatusDeal = dReader["StatusDeal"].ToString();
                string StatusSertificateOfOwnerShip = dReader["StatusSertificateOfOwnerShip"].ToString();

                if (StatusDeal == "Проверка документов")

                {
                    if (StatusSertificateOfOwnerShip == "На согласовании")
                    {
                        PanelSertificateOfOwnerShip.Visible = true;

                        PanelPassport.Visible = false;
                        PanelCertificateFromTaxInspection.Visible = false;
                        PanelConsentOfSpouse.Visible = false;
                        PanelDebtForUtility.Visible = false;
                        PanelDocumentFoundation.Visible = false;
                        PanelExtendedExtractFromRegister.Visible = false;
                        PanelExtractFromRegister.Visible = false;
                        PanelResolutionOfGuardianship.Visible = false;
                        PanelTechnicalPassport.Visible = false;
                    }

                    if (StatusSertificateOfOwnerShip == "Проверен")
                    {
                        PanelSertificateOfOwnerShip.Visible = true;

                        PanelCertificateFromTaxInspection.Visible = false;
                        PanelConsentOfSpouse.Visible = false;
                        PanelDebtForUtility.Visible = false;
                        PanelDocumentFoundation.Visible = false;
                        PanelExtendedExtractFromRegister.Visible = false;
                        PanelExtractFromRegister.Visible = false;
                        PanelResolutionOfGuardianship.Visible = false;
                        PanelTechnicalPassport.Visible = false;
                        PanelPassport.Visible = false;

                    }

                    if (StatusSertificateOfOwnerShip == "Подготовка")
                    {
                        PanelWaiting.Visible = true;

                        PanelPassport.Visible = false;
                        PanelCertificateFromTaxInspection.Visible = false;
                        PanelConsentOfSpouse.Visible = false;
                        PanelDebtForUtility.Visible = false;
                        PanelDocumentFoundation.Visible = false;
                        PanelExtendedExtractFromRegister.Visible = false;
                        PanelExtractFromRegister.Visible = false;
                        PanelResolutionOfGuardianship.Visible = false;
                        PanelSertificateOfOwnerShip.Visible = false;
                        PanelTechnicalPassport.Visible = false;
                    }

                    if (StatusSertificateOfOwnerShip == "Отклонен")
                    {
                        PanelDenide.Visible = true;

                        PanelPassport.Visible = false;
                        PanelCertificateFromTaxInspection.Visible = false;
                        PanelConsentOfSpouse.Visible = false;
                        PanelDebtForUtility.Visible = false;
                        PanelDocumentFoundation.Visible = false;
                        PanelExtendedExtractFromRegister.Visible = false;
                        PanelExtractFromRegister.Visible = false;
                        PanelResolutionOfGuardianship.Visible = false;
                        PanelSertificateOfOwnerShip.Visible = false;
                        PanelTechnicalPassport.Visible = false;
                    }
                }
            }
            Conn.Close();
        }
        protected void LinkButtonDownLoadSertificateOfOwnerShip_Click(object sender, EventArgs e)
        {
            string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();
            string NameDirectory = Server.MapPath("DealDocuments/"
                 + Request.Cookies["Deal"]["IdDeal"].ToString());
            string[] FileNameArray = Directory.GetFiles(NameDirectory, "SertificateOfOwnerShip*");
            string FileNameWithoutPath = System.IO.Path.GetFileName(FileNameArray[0]);

            string Ftp = "ftp://waws-prod-db3-051.ftp.azurewebsites.windows.net/site/wwwroot/DealDocuments/" +
                "/" + IdDeal + "/";

            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(Ftp + FileNameWithoutPath);
            request.Method = WebRequestMethods.Ftp.DownloadFile;

            request.Credentials = new NetworkCredential(@"ProWizard\ProWizard", "100Million");
            request.UsePassive = true;
            request.UseBinary = true;
            request.EnableSsl = false;

            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            using (MemoryStream stream = new MemoryStream())
            {
                response.GetResponseStream().CopyTo(stream);
                Response.AddHeader("content-disposition", "attachment;filename=" + FileNameWithoutPath);
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
        }
        protected void LinkButtonDenideSertificateOfOwnerShip_Click(object sender, EventArgs e)
        {
            string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();
            string NameDirectory = Server.MapPath("DealDocuments/"
                 + Request.Cookies["Deal"]["IdDeal"].ToString());
            string[] FileName = Directory.GetFiles(NameDirectory, "SertificateOfOwnerShip*");
            string FileNameWithoutPath = System.IO.Path.GetFileName(FileName[0]);

            File.Delete(Server.MapPath("DealDocuments/" + IdDeal + "/") + FileNameWithoutPath);

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

            SqlCommand CommandSetStatusMainDocuments = new SqlCommand("UPDATE dbo.TableDeal SET"
                + " StatusSertificateOfOwnerShip = @ParamSertificateOfOwnerShip" 
                + " WHERE IdDeal = @ParamIdDeal", Conn);

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandSetStatusMainDocuments.Parameters.Add(ParamIdDeal);

            SqlParameter ParamSertificateOfOwnerShip = new SqlParameter();
            ParamSertificateOfOwnerShip.ParameterName = "@ParamSertificateOfOwnerShip";
            ParamSertificateOfOwnerShip.Value = "Отклонен";
            CommandSetStatusMainDocuments.Parameters.Add(ParamSertificateOfOwnerShip);

            Conn.Open();
            CommandSetStatusMainDocuments.ExecuteNonQuery();
            Conn.Close();

            PanelSertificateOfOwnerShip.Visible = false;
            PanelDenide.Visible = true;
        }
        protected void LinkButtonSaveSertificateOfOwnerShip_Click(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

            if (CheckBoxSertificateOfOwnerShipPassport.Checked
                && CheckBoxSertificateOfOwnerShipAddress.Checked
                && CheckBoxSertificateOfOwnerShipBurden.Checked)

            {
                SqlCommand CommandSaveSertificateOfOwnerShip = new SqlCommand("UPDATE dbo.TableDeal"
                    + " SET StatusSertificateOfOwnerShip = @ParamStatusSertificateOfOwnerShip,"
                    + " SertificateOfOwnerShipPassport = @ParamSertificateOfOwnerShipPassport,"
                    + " SertificateOfOwnerShipAddress = @ParamSertificateOfOwnerShipAddress,"
                    + " SertificateOfOwnerShipBurden = @ParamSertificateOfOwnerShipBurden"
                    + " WHERE IdDeal = @ParamIdDeal", Conn);

                SqlParameter ParamIdDeal = new SqlParameter();
                ParamIdDeal.ParameterName = "@ParamIdDeal";
                ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandSaveSertificateOfOwnerShip.Parameters.Add(ParamIdDeal);

                SqlParameter ParamSertificateOfOwnerShipPassport = new SqlParameter();
                ParamSertificateOfOwnerShipPassport.ParameterName = "@ParamSertificateOfOwnerShipPassport";
                ParamSertificateOfOwnerShipPassport.Value = CheckBoxSertificateOfOwnerShipPassport.Checked.ToString();
                CommandSaveSertificateOfOwnerShip.Parameters.Add(ParamSertificateOfOwnerShipPassport);

                SqlParameter SertificateOfOwnerShipAddress = new SqlParameter();
                SertificateOfOwnerShipAddress.ParameterName = "@ParamSertificateOfOwnerShipAddress";
                SertificateOfOwnerShipAddress.Value = CheckBoxSertificateOfOwnerShipAddress.Checked.ToString();
                CommandSaveSertificateOfOwnerShip.Parameters.Add(SertificateOfOwnerShipAddress);

                SqlParameter ParamSertificateOfOwnerShipBurden = new SqlParameter();
                ParamSertificateOfOwnerShipBurden.ParameterName = "@ParamSertificateOfOwnerShipBurden";
                ParamSertificateOfOwnerShipBurden.Value = CheckBoxSertificateOfOwnerShipBurden.Checked.ToString();
                CommandSaveSertificateOfOwnerShip.Parameters.Add(ParamSertificateOfOwnerShipBurden);

                SqlParameter ParamStatusSertificateOfOwnerShip = new SqlParameter();
                ParamStatusSertificateOfOwnerShip.ParameterName = "@ParamStatusSertificateOfOwnerShip";
                ParamStatusSertificateOfOwnerShip.Value = "Проверен";
                CommandSaveSertificateOfOwnerShip.Parameters.Add(ParamStatusSertificateOfOwnerShip);

                Conn.Open();
                CommandSaveSertificateOfOwnerShip.ExecuteNonQuery();
                Conn.Close();
            }

            else

            {
                SqlCommand CommandSaveSertificateOfOwnerShip = new SqlCommand("UPDATE dbo.TableDeal"
                + " SET StatusSertificateOfOwnerShip = @ParamStatusSertificateOfOwnerShip,"
                + " SertificateOfOwnerShipPassport = @ParamSertificateOfOwnerShipPassport,"
                + " SertificateOfOwnerShipAddress = @ParamSertificateOfOwnerShipAddress,"
                + " SertificateOfOwnerShipBurden = @ParamSertificateOfOwnerShipBurden"
                + " WHERE IdDeal = @ParamIdDeal", Conn);

                SqlParameter ParamIdDeal = new SqlParameter();
                ParamIdDeal.ParameterName = "@ParamIdDeal";
                ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandSaveSertificateOfOwnerShip.Parameters.Add(ParamIdDeal);

                SqlParameter ParamSertificateOfOwnerShipPassport = new SqlParameter();
                ParamSertificateOfOwnerShipPassport.ParameterName = "@ParamSertificateOfOwnerShipPassport";
                ParamSertificateOfOwnerShipPassport.Value = CheckBoxSertificateOfOwnerShipPassport.Checked.ToString();
                CommandSaveSertificateOfOwnerShip.Parameters.Add(ParamSertificateOfOwnerShipPassport);

                SqlParameter SertificateOfOwnerShipAddress = new SqlParameter();
                SertificateOfOwnerShipAddress.ParameterName = "@ParamSertificateOfOwnerShipAddress";
                SertificateOfOwnerShipAddress.Value = CheckBoxSertificateOfOwnerShipAddress.Checked.ToString();
                CommandSaveSertificateOfOwnerShip.Parameters.Add(SertificateOfOwnerShipAddress);

                SqlParameter ParamSertificateOfOwnerShipBurden = new SqlParameter();
                ParamSertificateOfOwnerShipBurden.ParameterName = "@ParamSertificateOfOwnerShipBurden";
                ParamSertificateOfOwnerShipBurden.Value = CheckBoxSertificateOfOwnerShipBurden.Checked.ToString();
                CommandSaveSertificateOfOwnerShip.Parameters.Add(ParamSertificateOfOwnerShipBurden);

                SqlParameter ParamStatusSertificateOfOwnerShip = new SqlParameter();
                ParamStatusSertificateOfOwnerShip.ParameterName = "@ParamStatusSertificateOfOwnerShip";
                ParamStatusSertificateOfOwnerShip.Value = "На согласовании";
                CommandSaveSertificateOfOwnerShip.Parameters.Add(ParamStatusSertificateOfOwnerShip);

                Conn.Open();
                CommandSaveSertificateOfOwnerShip.ExecuteNonQuery();
                Conn.Close();
            }
        }

        protected void LinkButtonDownLoadDocumentFoundation_Click(object sender, EventArgs e)
        {
            string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();
            string NameDirectory = Server.MapPath("DealDocuments/"
                 + Request.Cookies["Deal"]["IdDeal"].ToString());
            string[] FileNameArray = Directory.GetFiles(NameDirectory, "DocumentFoundation*");
            string FileNameWithoutPath = System.IO.Path.GetFileName(FileNameArray[0]);

            string Ftp = "ftp://waws-prod-db3-051.ftp.azurewebsites.windows.net/site/wwwroot/DealDocuments/" +
                "/" + IdDeal + "/";

            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(Ftp + FileNameWithoutPath);
            request.Method = WebRequestMethods.Ftp.DownloadFile;

            request.Credentials = new NetworkCredential(@"ProWizard\ProWizard", "100Million");
            request.UsePassive = true;
            request.UseBinary = true;
            request.EnableSsl = false;

            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            using (MemoryStream stream = new MemoryStream())
            {
                response.GetResponseStream().CopyTo(stream);
                Response.AddHeader("content-disposition", "attachment;filename=" + FileNameWithoutPath);
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
        }
        protected void LinkButtonDocumentFoundation_Click(object sender, EventArgs e)
        {
            LinkButtonPassport.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonSertificateOfOwnerShip.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonDocumentFoundation.ForeColor = System.Drawing.ColorTranslator.FromHtml("#AA4825");
            LinkButtonExtractFromRegister.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonExtendedExtractFromRegister.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonTechnicalPassport.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonConsentOfSpouse.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonResolutionOfGuardianship.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonDebtForUtility.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonCertificateFromTaxInspection.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandStatus = Conn.CreateCommand();
            CommandStatus.CommandText = "SELECT StatusDeal, StatusDocumentFoundation FROM dbo.TableDeal"
                + " WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandStatus.Parameters.Add(ParamIdDeal);

            SqlDataReader dReader;

            Conn.Open();
            dReader = CommandStatus.ExecuteReader();

            if (dReader.Read())

            {
                string StatusDeal = dReader["StatusDeal"].ToString();
                string StatusDocumentFoundation = dReader["StatusDocumentFoundation"].ToString();

                if (StatusDeal == "Проверка документов")
                {
                    if (StatusDocumentFoundation == "На согласовании")
                    {
                        PanelDocumentFoundation.Visible = true;

                        PanelPassport.Visible = false;
                        PanelCertificateFromTaxInspection.Visible = false;
                        PanelConsentOfSpouse.Visible = false;
                        PanelDebtForUtility.Visible = false;
                        PanelExtendedExtractFromRegister.Visible = false;
                        PanelExtractFromRegister.Visible = false;
                        PanelResolutionOfGuardianship.Visible = false;
                        PanelSertificateOfOwnerShip.Visible = false;
                        PanelTechnicalPassport.Visible = false;

                    }

                    if (StatusDocumentFoundation == "Проверен")
                    {
                        PanelDocumentFoundation.Visible = true;

                        PanelPassport.Visible = false;
                        PanelCertificateFromTaxInspection.Visible = false;
                        PanelConsentOfSpouse.Visible = false;
                        PanelDebtForUtility.Visible = false;
                        PanelExtendedExtractFromRegister.Visible = false;
                        PanelExtractFromRegister.Visible = false;
                        PanelResolutionOfGuardianship.Visible = false;
                        PanelSertificateOfOwnerShip.Visible = false;
                        PanelTechnicalPassport.Visible = false;
                    }

                    if (StatusDocumentFoundation == "Подготовка")
                    {
                        PanelWaiting.Visible = true;

                        PanelPassport.Visible = false;
                        PanelCertificateFromTaxInspection.Visible = false;
                        PanelConsentOfSpouse.Visible = false;
                        PanelDebtForUtility.Visible = false;
                        PanelDocumentFoundation.Visible = false;
                        PanelExtendedExtractFromRegister.Visible = false;
                        PanelExtractFromRegister.Visible = false;
                        PanelResolutionOfGuardianship.Visible = false;
                        PanelSertificateOfOwnerShip.Visible = false;
                        PanelTechnicalPassport.Visible = false;
                    }

                    if (StatusDocumentFoundation == "Отклонен")
                    {
                        PanelDenide.Visible = true;

                        PanelPassport.Visible = false;
                        PanelCertificateFromTaxInspection.Visible = false;
                        PanelConsentOfSpouse.Visible = false;
                        PanelDebtForUtility.Visible = false;
                        PanelDocumentFoundation.Visible = false;
                        PanelExtendedExtractFromRegister.Visible = false;
                        PanelExtractFromRegister.Visible = false;
                        PanelResolutionOfGuardianship.Visible = false;
                        PanelSertificateOfOwnerShip.Visible = false;
                        PanelTechnicalPassport.Visible = false;
                    }
                }
            }
            Conn.Close();
        }
        protected void LinkButtonDenideDocumentFoundation_Click(object sender, EventArgs e)
        {
            string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();
            string NameDirectory = Server.MapPath("DealDocuments/"
                 + Request.Cookies["Deal"]["IdDeal"].ToString());
            string[] FileName = Directory.GetFiles(NameDirectory, "DocumentFoundation*");
            string FileNameWithoutPath = System.IO.Path.GetFileName(FileName[0]);

            File.Delete(Server.MapPath("DealDocuments/" + IdDeal + "/") + FileNameWithoutPath);

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

            SqlCommand CommandSetStatusMainDocuments = new SqlCommand("UPDATE dbo.TableDeal SET"
                + " StatusDocumentFoundation = @ParamStatusDocumentFoundation" 
                + " WHERE IdDeal = @ParamIdDeal", Conn);

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandSetStatusMainDocuments.Parameters.Add(ParamIdDeal);

            SqlParameter ParamStatusDocumentFoundation = new SqlParameter();
            ParamStatusDocumentFoundation.ParameterName = "@ParamStatusDocumentFoundation";
            ParamStatusDocumentFoundation.Value = "Отклонен";
            CommandSetStatusMainDocuments.Parameters.Add(ParamStatusDocumentFoundation);

            Conn.Open();
            CommandSetStatusMainDocuments.ExecuteNonQuery();
            Conn.Close();

            PanelDocumentFoundation.Visible = false;
            PanelDenide.Visible = true;
        }
        protected void LinkButtonSaveDocumentFoundation_Click(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

            if (CheckBoxDocumentFoundationPassport.Checked
                && CheckBoxDocumentFoundationAddress.Checked
                && CheckBoxDocumentFoundationBurden.Checked)

            {
                SqlCommand CommandSaveDocumentFoundation = new SqlCommand("UPDATE dbo.TableDeal"
                    + " SET StatusDocumentFoundation = @ParamStatusDocumentFoundation,"
                    + " DocumentFoundationPassport = @ParamDocumentFoundationPassport,"
                    + " DocumentFoundationAddress = @ParamDocumentFoundationAddress,"
                    + " DocumentFoundationBurden = @ParamDocumentFoundationBurden"
                    + " WHERE IdDeal = @ParamIdDeal", Conn);

                SqlParameter ParamIdDeal = new SqlParameter();
                ParamIdDeal.ParameterName = "@ParamIdDeal";
                ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandSaveDocumentFoundation.Parameters.Add(ParamIdDeal);

                SqlParameter ParamStatusDocumentFoundation = new SqlParameter();
                ParamStatusDocumentFoundation.ParameterName = "@ParamStatusDocumentFoundation";
                ParamStatusDocumentFoundation.Value = "Проверен";
                CommandSaveDocumentFoundation.Parameters.Add(ParamStatusDocumentFoundation);

                SqlParameter ParamDocumentFoundationPassport = new SqlParameter();
                ParamDocumentFoundationPassport.ParameterName = "@ParamDocumentFoundationPassport";
                ParamDocumentFoundationPassport.Value = CheckBoxDocumentFoundationPassport.Checked.ToString();
                CommandSaveDocumentFoundation.Parameters.Add(ParamDocumentFoundationPassport);

                SqlParameter ParamDocumentFoundationAddress = new SqlParameter();
                ParamDocumentFoundationAddress.ParameterName = "@ParamDocumentFoundationAddress";
                ParamDocumentFoundationAddress.Value = CheckBoxDocumentFoundationAddress.Checked.ToString();
                CommandSaveDocumentFoundation.Parameters.Add(ParamDocumentFoundationAddress);

                SqlParameter ParamDocumentFoundationBurden = new SqlParameter();
                ParamDocumentFoundationBurden.ParameterName = "@ParamDocumentFoundationBurden";
                ParamDocumentFoundationBurden.Value = CheckBoxDocumentFoundationBurden.Checked.ToString();
                CommandSaveDocumentFoundation.Parameters.Add(ParamDocumentFoundationBurden);

                Conn.Open();
                CommandSaveDocumentFoundation.ExecuteNonQuery();
                Conn.Close();
            }

            else

            {
                SqlCommand CommandSaveDocumentFoundation = new SqlCommand("UPDATE dbo.TableDeal"
                  + " SET StatusDocumentFoundation = @ParamStatusDocumentFoundation,"
                  + " DocumentFoundationPassport = @ParamDocumentFoundationPassport,"
                  + " DocumentFoundationAddress = @ParamDocumentFoundationAddress,"
                  + " DocumentFoundationBurden = @ParamDocumentFoundationBurden"
                  + " WHERE IdDeal = @ParamIdDeal", Conn);

                SqlParameter ParamIdDeal = new SqlParameter();
                ParamIdDeal.ParameterName = "@ParamIdDeal";
                ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandSaveDocumentFoundation.Parameters.Add(ParamIdDeal);

                SqlParameter ParamStatusDocumentFoundation = new SqlParameter();
                ParamStatusDocumentFoundation.ParameterName = "@ParamStatusDocumentFoundation";
                ParamStatusDocumentFoundation.Value = "На согласовании";
                CommandSaveDocumentFoundation.Parameters.Add(ParamStatusDocumentFoundation);

                SqlParameter ParamDocumentFoundationPassport = new SqlParameter();
                ParamDocumentFoundationPassport.ParameterName = "@ParamDocumentFoundationPassport";
                ParamDocumentFoundationPassport.Value = CheckBoxDocumentFoundationPassport.Checked.ToString();
                CommandSaveDocumentFoundation.Parameters.Add(ParamDocumentFoundationPassport);

                SqlParameter ParamDocumentFoundationAddress = new SqlParameter();
                ParamDocumentFoundationAddress.ParameterName = "@ParamDocumentFoundationAddress";
                ParamDocumentFoundationAddress.Value = CheckBoxDocumentFoundationAddress.Checked.ToString();
                CommandSaveDocumentFoundation.Parameters.Add(ParamDocumentFoundationAddress);

                SqlParameter ParamDocumentFoundationBurden = new SqlParameter();
                ParamDocumentFoundationBurden.ParameterName = "@ParamDocumentFoundationBurden";
                ParamDocumentFoundationBurden.Value = CheckBoxDocumentFoundationBurden.Checked.ToString();
                CommandSaveDocumentFoundation.Parameters.Add(ParamDocumentFoundationBurden);

                Conn.Open();
                CommandSaveDocumentFoundation.ExecuteNonQuery();
                Conn.Close();
            }
        }

        protected void LinkButtonDownLoadExtractFromRegister_Click(object sender, EventArgs e)
        {
            string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();
            string NameDirectory = Server.MapPath("DealDocuments/"
                 + Request.Cookies["Deal"]["IdDeal"].ToString());
            string[] FileNameArray = Directory.GetFiles(NameDirectory, "ExtractFromRegister*");
            string FileNameWithoutPath = System.IO.Path.GetFileName(FileNameArray[0]);

            string Ftp = "ftp://waws-prod-db3-051.ftp.azurewebsites.windows.net/site/wwwroot/DealDocuments/" +
                "/" + IdDeal + "/";

            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(Ftp + FileNameWithoutPath);
            request.Method = WebRequestMethods.Ftp.DownloadFile;

            request.Credentials = new NetworkCredential(@"ProWizard\ProWizard", "100Million");
            request.UsePassive = true;
            request.UseBinary = true;
            request.EnableSsl = false;

            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            using (MemoryStream stream = new MemoryStream())
            {
                response.GetResponseStream().CopyTo(stream);
                Response.AddHeader("content-disposition", "attachment;filename=" + FileNameWithoutPath);
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
        }
        protected void LinkButtonExtractFromRegister_Click(object sender, EventArgs e)
        {
            LinkButtonPassport.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonSertificateOfOwnerShip.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonDocumentFoundation.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonExtractFromRegister.ForeColor = System.Drawing.ColorTranslator.FromHtml("#AA4825");
            LinkButtonExtendedExtractFromRegister.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonTechnicalPassport.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonConsentOfSpouse.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonResolutionOfGuardianship.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonDebtForUtility.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonCertificateFromTaxInspection.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

            SqlCommand CommandStatus = Conn.CreateCommand();
            CommandStatus.CommandText = "SELECT StatusDeal, StatusExtractFromRegister"
                + " FROM dbo.TableDeal WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandStatus.Parameters.Add(ParamIdDeal);

            SqlDataReader dReader;

            Conn.Open();
            dReader = CommandStatus.ExecuteReader();

            if (dReader.Read())

            {
                string StatusDeal = dReader["StatusDeal"].ToString();
                string StatusExtractFromRegister = dReader["StatusExtractFromRegister"].ToString();

                if (StatusDeal == "Проверка документов")
                {
                    if (StatusExtractFromRegister == "На согласовании")
                    {
                        PanelExtractFromRegister.Visible = true;

                        PanelPassport.Visible = false;
                        PanelCertificateFromTaxInspection.Visible = false;
                        PanelConsentOfSpouse.Visible = false;
                        PanelDebtForUtility.Visible = false;
                        PanelDocumentFoundation.Visible = false;
                        PanelExtendedExtractFromRegister.Visible = false;
                        PanelResolutionOfGuardianship.Visible = false;
                        PanelSertificateOfOwnerShip.Visible = false;
                        PanelTechnicalPassport.Visible = false;
                    }

                    if (StatusExtractFromRegister == "Проверен")
                    {
                        PanelExtractFromRegister.Visible = true;

                        PanelPassport.Visible = false;
                        PanelCertificateFromTaxInspection.Visible = false;
                        PanelConsentOfSpouse.Visible = false;
                        PanelDebtForUtility.Visible = false;
                        PanelDocumentFoundation.Visible = false;
                        PanelExtendedExtractFromRegister.Visible = false;
                        PanelResolutionOfGuardianship.Visible = false;
                        PanelSertificateOfOwnerShip.Visible = false;
                        PanelTechnicalPassport.Visible = false;
                    }

                    if (StatusExtractFromRegister == "Подготовка")
                    {
                        PanelWaiting.Visible = true;

                        PanelPassport.Visible = false;
                        PanelCertificateFromTaxInspection.Visible = false;
                        PanelConsentOfSpouse.Visible = false;
                        PanelDebtForUtility.Visible = false;
                        PanelDocumentFoundation.Visible = false;
                        PanelExtendedExtractFromRegister.Visible = false;
                        PanelExtractFromRegister.Visible = false;
                        PanelResolutionOfGuardianship.Visible = false;
                        PanelSertificateOfOwnerShip.Visible = false;
                        PanelTechnicalPassport.Visible = false;
                    }

                    if (StatusExtractFromRegister == "Отклонен")
                    {
                        PanelDenide.Visible = true;

                        PanelPassport.Visible = false;
                        PanelCertificateFromTaxInspection.Visible = false;
                        PanelConsentOfSpouse.Visible = false;
                        PanelDebtForUtility.Visible = false;
                        PanelDocumentFoundation.Visible = false;
                        PanelExtendedExtractFromRegister.Visible = false;
                        PanelExtractFromRegister.Visible = false;
                        PanelResolutionOfGuardianship.Visible = false;
                        PanelSertificateOfOwnerShip.Visible = false;
                        PanelTechnicalPassport.Visible = false;
                    }
                }
            }
            Conn.Close();
        }
        protected void LinkButtonDenideExtractFromRegister_Click(object sender, EventArgs e)
        {
            string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();
            string NameDirectory = Server.MapPath("DealDocuments/"
                 + Request.Cookies["Deal"]["IdDeal"].ToString());
            string[] FileName = Directory.GetFiles(NameDirectory, "ExtractFromRegister*");
            string FileNameWithoutPath = System.IO.Path.GetFileName(FileName[0]);

            File.Delete(Server.MapPath("DealDocuments/" + IdDeal + "/") + FileNameWithoutPath);

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

            SqlCommand CommandSetStatusMainDocuments = new SqlCommand("UPDATE dbo.TableDeal SET"
                + " StatusExtractFromRegister = @ParamStatusExtractFromRegister"
                + " WHERE IdDeal = @ParamIdDeal", Conn);

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandSetStatusMainDocuments.Parameters.Add(ParamIdDeal);

            SqlParameter ParamStatusExtractFromRegister = new SqlParameter();
            ParamStatusExtractFromRegister.ParameterName = "@ParamStatusExtractFromRegister";
            ParamStatusExtractFromRegister.Value = "Отклонен";
            CommandSetStatusMainDocuments.Parameters.Add(ParamStatusExtractFromRegister);

            Conn.Open();
            CommandSetStatusMainDocuments.ExecuteNonQuery();
            Conn.Close();

            PanelExtractFromRegister.Visible = false;
            PanelDenide.Visible = true;
        }
        protected void LinkButtonSaveExtractFromRegister_Click(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

            if (CheckBoxExtractFromRegisterPassport.Checked
                && CheckBoxExtractFromRegisterCharacteristic.Checked
                && CheckBoxExtractFromRegisterBurden.Checked)

            {
                SqlCommand CommandSaveExtractFromRegister = new SqlCommand("UPDATE dbo.TableDeal"
                               + " SET StatusExtractFromRegister = @ParamStatusExtractFromRegister,"
                               + " ExtractFromRegisterPassport = @ParamExtractFromRegisterPassport,"
                               + " ExtractFromRegisterCharacteristic = @ParamExtractFromRegisterCharacteristic,"
                               + " ExtractFromRegisterBurden = @ParamExtractFromRegisterBurden"
                               + " WHERE IdDeal = @ParamIdDeal", Conn);

                SqlParameter ParamIdDeal = new SqlParameter();
                ParamIdDeal.ParameterName = "@ParamIdDeal";
                ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandSaveExtractFromRegister.Parameters.Add(ParamIdDeal);

                SqlParameter ParamStatusExtractFromRegister = new SqlParameter();
                ParamStatusExtractFromRegister.ParameterName = "@ParamStatusExtractFromRegister";
                ParamStatusExtractFromRegister.Value = "Проверен";
                CommandSaveExtractFromRegister.Parameters.Add(ParamStatusExtractFromRegister);

                SqlParameter ParamExtractFromRegisterPassport = new SqlParameter();
                ParamExtractFromRegisterPassport.ParameterName = "@ParamExtractFromRegisterPassport";
                ParamExtractFromRegisterPassport.Value = CheckBoxExtractFromRegisterPassport.Checked.ToString();
                CommandSaveExtractFromRegister.Parameters.Add(ParamExtractFromRegisterPassport);

                SqlParameter ParamExtractFromRegisterCharacteristic = new SqlParameter();
                ParamExtractFromRegisterCharacteristic.ParameterName = "@ParamExtractFromRegisterCharacteristic";
                ParamExtractFromRegisterCharacteristic.Value = CheckBoxExtractFromRegisterCharacteristic.Checked.ToString();
                CommandSaveExtractFromRegister.Parameters.Add(ParamExtractFromRegisterCharacteristic);

                SqlParameter ParamExtractFromRegisterBurden = new SqlParameter();
                ParamExtractFromRegisterBurden.ParameterName = "@ParamExtractFromRegisterBurden";
                ParamExtractFromRegisterBurden.Value = CheckBoxExtractFromRegisterBurden.Checked.ToString();
                CommandSaveExtractFromRegister.Parameters.Add(ParamExtractFromRegisterBurden);

                Conn.Open();
                CommandSaveExtractFromRegister.ExecuteNonQuery();
                Conn.Close();
            }

            else

            {
                SqlCommand CommandSaveExtractFromRegister = new SqlCommand("UPDATE dbo.TableDeal"
                              + " SET StatusExtractFromRegister = @ParamStatusExtractFromRegister,"
                              + " ExtractFromRegisterPassport = @ParamExtractFromRegisterPassport,"
                              + " ExtractFromRegisterCharacteristic = @ParamExtractFromRegisterCharacteristic,"
                              + " ExtractFromRegisterBurden = @ParamExtractFromRegisterBurden"
                              + " WHERE IdDeal = @ParamIdDeal", Conn);

                SqlParameter ParamIdDeal = new SqlParameter();
                ParamIdDeal.ParameterName = "@ParamIdDeal";
                ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandSaveExtractFromRegister.Parameters.Add(ParamIdDeal);

                SqlParameter ParamStatusExtractFromRegister = new SqlParameter();
                ParamStatusExtractFromRegister.ParameterName = "@ParamStatusExtractFromRegister";
                ParamStatusExtractFromRegister.Value = "На согласовании";
                CommandSaveExtractFromRegister.Parameters.Add(ParamStatusExtractFromRegister);

                SqlParameter ParamExtractFromRegisterPassport = new SqlParameter();
                ParamExtractFromRegisterPassport.ParameterName = "@ParamExtractFromRegisterPassport";
                ParamExtractFromRegisterPassport.Value = CheckBoxExtractFromRegisterPassport.Checked.ToString();
                CommandSaveExtractFromRegister.Parameters.Add(ParamExtractFromRegisterPassport);

                SqlParameter ParamExtractFromRegisterCharacteristic = new SqlParameter();
                ParamExtractFromRegisterCharacteristic.ParameterName = "@ParamExtractFromRegisterCharacteristic";
                ParamExtractFromRegisterCharacteristic.Value = CheckBoxExtractFromRegisterCharacteristic.Checked.ToString();
                CommandSaveExtractFromRegister.Parameters.Add(ParamExtractFromRegisterCharacteristic);

                SqlParameter ParamExtractFromRegisterBurden = new SqlParameter();
                ParamExtractFromRegisterBurden.ParameterName = "@ParamExtractFromRegisterBurden";
                ParamExtractFromRegisterBurden.Value = CheckBoxExtractFromRegisterBurden.Checked.ToString();
                CommandSaveExtractFromRegister.Parameters.Add(ParamExtractFromRegisterBurden);

                Conn.Open();
                CommandSaveExtractFromRegister.ExecuteNonQuery();
                Conn.Close();
            }
        }

        protected void LinkButtonDownLoadExtendedExtractFromRegister_Click(object sender, EventArgs e)
        {
            string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();
            string NameDirectory = Server.MapPath("DealDocuments/"
                 + Request.Cookies["Deal"]["IdDeal"].ToString());
            string[] FileNameArray = Directory.GetFiles(NameDirectory, "ExtendedExtractFromRegister*");
            string FileNameWithoutPath = System.IO.Path.GetFileName(FileNameArray[0]);

            string Ftp = "ftp://waws-prod-db3-051.ftp.azurewebsites.windows.net/site/wwwroot/DealDocuments/" +
                "/" + IdDeal + "/";

            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(Ftp + FileNameWithoutPath);
            request.Method = WebRequestMethods.Ftp.DownloadFile;

            request.Credentials = new NetworkCredential(@"ProWizard\ProWizard", "100Million");
            request.UsePassive = true;
            request.UseBinary = true;
            request.EnableSsl = false;

            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            using (MemoryStream stream = new MemoryStream())
            {
                response.GetResponseStream().CopyTo(stream);
                Response.AddHeader("content-disposition", "attachment;filename=" + FileNameWithoutPath);
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
        }
        protected void LinkButtonExtendedExtractFromRegister_Click(object sender, EventArgs e)
        {
            LinkButtonPassport.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonSertificateOfOwnerShip.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonDocumentFoundation.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonExtractFromRegister.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonExtendedExtractFromRegister.ForeColor = System.Drawing.ColorTranslator.FromHtml("#AA4825");
            LinkButtonTechnicalPassport.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonConsentOfSpouse.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonResolutionOfGuardianship.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonDebtForUtility.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonCertificateFromTaxInspection.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandStatus = Conn.CreateCommand();
            CommandStatus.CommandText = "SELECT StatusDeal, StatusExtendedExtractFromRegister"
                + " FROM dbo.TableDeal WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandStatus.Parameters.Add(ParamIdDeal);

            SqlDataReader dReader;

            Conn.Open();
            dReader = CommandStatus.ExecuteReader();

            if (dReader.Read())

            {
                string StatusDeal = dReader["StatusDeal"].ToString();
                string StatusExtendedExtractFromRegister = dReader["StatusExtendedExtractFromRegister"].ToString();

                if (StatusDeal == "Проверка документов")
                {
                    if (StatusExtendedExtractFromRegister == "Подготовка")
                    {
                        PanelWaiting.Visible = true;

                        PanelPassport.Visible = false;
                        PanelCertificateFromTaxInspection.Visible = false;
                        PanelConsentOfSpouse.Visible = false;
                        PanelDebtForUtility.Visible = false;
                        PanelDocumentFoundation.Visible = false;
                        PanelExtendedExtractFromRegister.Visible = false;
                        PanelExtractFromRegister.Visible = false;
                        PanelResolutionOfGuardianship.Visible = false;
                        PanelSertificateOfOwnerShip.Visible = false;
                        PanelTechnicalPassport.Visible = false;
                    }

                    if (StatusExtendedExtractFromRegister == "Отклонен")
                    {
                        PanelDenide.Visible = true;

                        PanelPassport.Visible = false;
                        PanelCertificateFromTaxInspection.Visible = false;
                        PanelConsentOfSpouse.Visible = false;
                        PanelDebtForUtility.Visible = false;
                        PanelDocumentFoundation.Visible = false;
                        PanelExtendedExtractFromRegister.Visible = false;
                        PanelExtractFromRegister.Visible = false;
                        PanelResolutionOfGuardianship.Visible = false;
                        PanelSertificateOfOwnerShip.Visible = false;
                        PanelTechnicalPassport.Visible = false;
                    }

                    if (StatusExtendedExtractFromRegister == "На согласовании")
                    {
                        PanelExtendedExtractFromRegister.Visible = true;

                        PanelPassport.Visible = false;
                        PanelCertificateFromTaxInspection.Visible = false;
                        PanelConsentOfSpouse.Visible = false;
                        PanelDebtForUtility.Visible = false;
                        PanelDocumentFoundation.Visible = false;
                        PanelExtractFromRegister.Visible = false;
                        PanelResolutionOfGuardianship.Visible = false;
                        PanelSertificateOfOwnerShip.Visible = false;
                        PanelTechnicalPassport.Visible = false;
                    }

                    if (StatusExtendedExtractFromRegister == "Проверен")
                    {
                        PanelExtendedExtractFromRegister.Visible = true;

                        PanelPassport.Visible = false;
                        PanelCertificateFromTaxInspection.Visible = false;
                        PanelConsentOfSpouse.Visible = false;
                        PanelDebtForUtility.Visible = false;
                        PanelDocumentFoundation.Visible = false;
                        PanelExtractFromRegister.Visible = false;
                        PanelResolutionOfGuardianship.Visible = false;
                        PanelSertificateOfOwnerShip.Visible = false;
                        PanelTechnicalPassport.Visible = false;
                    }
                }
            }
            Conn.Close();
        }
        protected void LinkButtonDenideExtendedExtractFromRegister_Click(object sender, EventArgs e)
        {
            string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();
            string NameDirectory = Server.MapPath("DealDocuments/"
                 + Request.Cookies["Deal"]["IdDeal"].ToString());
            string[] FileName = Directory.GetFiles(NameDirectory, "ExtendedExtractFromRegister*");
            string FileNameWithoutPath = System.IO.Path.GetFileName(FileName[0]);

            File.Delete(Server.MapPath("DealDocuments/" + IdDeal + "/") + FileNameWithoutPath);

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandSetStatusMainDocuments = new SqlCommand("UPDATE dbo.TableDeal SET"
                + " StatusExtendedExtractFromRegister = @ParamStatusExtendedExtractFromRegister"
                + " WHERE IdDeal = @ParamIdDeal", Conn);

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandSetStatusMainDocuments.Parameters.Add(ParamIdDeal);

            SqlParameter ParamStatusExtendedExtractFromRegister = new SqlParameter();
            ParamStatusExtendedExtractFromRegister.ParameterName = "@ParamStatusExtendedExtractFromRegister";
            ParamStatusExtendedExtractFromRegister.Value = "Отклонен";
            CommandSetStatusMainDocuments.Parameters.Add(ParamStatusExtendedExtractFromRegister);

            Conn.Open();
            CommandSetStatusMainDocuments.ExecuteNonQuery();
            Conn.Close();

            PanelExtendedExtractFromRegister.Visible = false;
            PanelDenide.Visible = true;
        }
        protected void LinkButtonSaveExtendedExtractFromRegister_Click(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

            if (CheckBoxExtendedExtractFromRegisterPeople.Checked
                && CheckBoxExtendedExtractFromRegisterMinors.Checked
                && CheckBoxExtendedExtractFromRegisterTemporarilyВrawn.Checked)

            {
                SqlCommand CommandSaveExtendedExtractFromRegister = new SqlCommand("UPDATE dbo.TableDeal"
                             + " SET StatusExtendedExtractFromRegister = @ParamStatusExtendedExtractFromRegister,"
                             + " ExtendedExtractFromRegisterPeople = @ParamExtendedExtractFromRegisterPeople,"
                             + " ExtendedExtractFromRegisterMinors = @ParamExtendedExtractFromRegisterMinors,"
                             + " ExtendedExtractFromRegisterTemporarilyВrawn = @ParamExtendedExtractFromRegisterTemporarilyВrawn"
                             + " WHERE IdDeal = @ParamIdDeal", Conn);

                SqlParameter ParamIdDeal = new SqlParameter();
                ParamIdDeal.ParameterName = "@ParamIdDeal";
                ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandSaveExtendedExtractFromRegister.Parameters.Add(ParamIdDeal);

                SqlParameter ParamStatusExtendedExtractFromRegister = new SqlParameter();
                ParamStatusExtendedExtractFromRegister.ParameterName = "@ParamStatusExtendedExtractFromRegister";
                ParamStatusExtendedExtractFromRegister.Value = "Проверен";
                CommandSaveExtendedExtractFromRegister.Parameters.Add(ParamStatusExtendedExtractFromRegister);

                SqlParameter ParamExtendedExtractFromRegisterPeople = new SqlParameter();
                ParamExtendedExtractFromRegisterPeople.ParameterName = "@ParamExtendedExtractFromRegisterPeople";
                ParamExtendedExtractFromRegisterPeople.Value = CheckBoxExtendedExtractFromRegisterPeople.Checked.ToString();
                CommandSaveExtendedExtractFromRegister.Parameters.Add(ParamExtendedExtractFromRegisterPeople);

                SqlParameter ParamExtendedExtractFromRegisterMinors = new SqlParameter();
                ParamExtendedExtractFromRegisterMinors.ParameterName = "@ParamExtendedExtractFromRegisterMinors";
                ParamExtendedExtractFromRegisterMinors.Value = CheckBoxExtendedExtractFromRegisterMinors.Checked.ToString();
                CommandSaveExtendedExtractFromRegister.Parameters.Add(ParamExtendedExtractFromRegisterMinors);

                SqlParameter ParamExtendedExtractFromRegisterTemporarilyВrawn = new SqlParameter();
                ParamExtendedExtractFromRegisterTemporarilyВrawn.ParameterName = "@ParamExtendedExtractFromRegisterTemporarilyВrawn";
                ParamExtendedExtractFromRegisterTemporarilyВrawn.Value = CheckBoxExtendedExtractFromRegisterTemporarilyВrawn.Checked.ToString();
                CommandSaveExtendedExtractFromRegister.Parameters.Add(ParamExtendedExtractFromRegisterTemporarilyВrawn);

                Conn.Open();
                CommandSaveExtendedExtractFromRegister.ExecuteNonQuery();
                Conn.Close();
            }

            else

            {
                SqlCommand CommandSaveExtendedExtractFromRegister = new SqlCommand("UPDATE dbo.TableDeal"
                             + " SET StatusExtendedExtractFromRegister = @ParamStatusExtendedExtractFromRegister,"
                             + " ExtendedExtractFromRegisterPeople = @ParamExtendedExtractFromRegisterPeople,"
                             + " ExtendedExtractFromRegisterMinors = @ParamExtendedExtractFromRegisterMinors,"
                             + " ExtendedExtractFromRegisterTemporarilyВrawn = @ParamExtendedExtractFromRegisterTemporarilyВrawn"
                             + " WHERE IdDeal = @ParamIdDeal", Conn);

                SqlParameter ParamIdDeal = new SqlParameter();
                ParamIdDeal.ParameterName = "@ParamIdDeal";
                ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandSaveExtendedExtractFromRegister.Parameters.Add(ParamIdDeal);

                SqlParameter ParamStatusExtendedExtractFromRegister = new SqlParameter();
                ParamStatusExtendedExtractFromRegister.ParameterName = "@ParamStatusExtendedExtractFromRegister";
                ParamStatusExtendedExtractFromRegister.Value = "На согласовании";
                CommandSaveExtendedExtractFromRegister.Parameters.Add(ParamStatusExtendedExtractFromRegister);

                SqlParameter ParamExtendedExtractFromRegisterPeople = new SqlParameter();
                ParamExtendedExtractFromRegisterPeople.ParameterName = "@ParamExtendedExtractFromRegisterPeople";
                ParamExtendedExtractFromRegisterPeople.Value = CheckBoxExtendedExtractFromRegisterPeople.Checked.ToString();
                CommandSaveExtendedExtractFromRegister.Parameters.Add(ParamExtendedExtractFromRegisterPeople);

                SqlParameter ParamExtendedExtractFromRegisterMinors = new SqlParameter();
                ParamExtendedExtractFromRegisterMinors.ParameterName = "@ParamExtendedExtractFromRegisterMinors";
                ParamExtendedExtractFromRegisterMinors.Value = CheckBoxExtendedExtractFromRegisterMinors.Checked.ToString();
                CommandSaveExtendedExtractFromRegister.Parameters.Add(ParamExtendedExtractFromRegisterMinors);

                SqlParameter ParamExtendedExtractFromRegisterTemporarilyВrawn = new SqlParameter();
                ParamExtendedExtractFromRegisterTemporarilyВrawn.ParameterName = "@ParamExtendedExtractFromRegisterTemporarilyВrawn";
                ParamExtendedExtractFromRegisterTemporarilyВrawn.Value = CheckBoxExtendedExtractFromRegisterTemporarilyВrawn.Checked.ToString();
                CommandSaveExtendedExtractFromRegister.Parameters.Add(ParamExtendedExtractFromRegisterTemporarilyВrawn);

                Conn.Open();
                CommandSaveExtendedExtractFromRegister.ExecuteNonQuery();
                Conn.Close();
            }
        }

        protected void LinkButtonDownLoadTechnicalPassport_Click(object sender, EventArgs e)
        {
            string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();
            string NameDirectory = Server.MapPath("DealDocuments/"
                 + Request.Cookies["Deal"]["IdDeal"].ToString());
            string[] FileNameArray = Directory.GetFiles(NameDirectory, "TechnicalPassport*");
            string FileNameWithoutPath = System.IO.Path.GetFileName(FileNameArray[0]);

            string Ftp = "ftp://waws-prod-db3-051.ftp.azurewebsites.windows.net/site/wwwroot/DealDocuments/" +
                "/" + IdDeal + "/";

            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(Ftp + FileNameWithoutPath);
            request.Method = WebRequestMethods.Ftp.DownloadFile;

            request.Credentials = new NetworkCredential(@"ProWizard\ProWizard", "100Million");
            request.UsePassive = true;
            request.UseBinary = true;
            request.EnableSsl = false;

            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            using (MemoryStream stream = new MemoryStream())
            {
                response.GetResponseStream().CopyTo(stream);
                Response.AddHeader("content-disposition", "attachment;filename=" + FileNameWithoutPath);
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
        }
        protected void LinkButtonTechnicalPassport_Click(object sender, EventArgs e)
        {
            LinkButtonPassport.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonSertificateOfOwnerShip.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonDocumentFoundation.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonExtractFromRegister.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonExtendedExtractFromRegister.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonTechnicalPassport.ForeColor = System.Drawing.ColorTranslator.FromHtml("#AA4825");
            LinkButtonConsentOfSpouse.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonResolutionOfGuardianship.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonDebtForUtility.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonCertificateFromTaxInspection.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandStatus = Conn.CreateCommand();
            CommandStatus.CommandText = "SELECT StatusDeal, StatusTechnicalPassport"
                + " FROM dbo.TableDeal WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandStatus.Parameters.Add(ParamIdDeal);

            SqlDataReader dReader;

            Conn.Open();
            dReader = CommandStatus.ExecuteReader();

            if (dReader.Read())

            {
                string StatusDeal = dReader["StatusDeal"].ToString();
                string StatusTechnicalPassport = dReader["StatusTechnicalPassport"].ToString();

                if (StatusDeal == "Проверка документов")
                {
                    if (StatusTechnicalPassport == "Подготовка")
                    {
                        PanelWaiting.Visible = true;

                        PanelPassport.Visible = false;
                        PanelCertificateFromTaxInspection.Visible = false;
                        PanelConsentOfSpouse.Visible = false;
                        PanelDebtForUtility.Visible = false;
                        PanelDocumentFoundation.Visible = false;
                        PanelExtendedExtractFromRegister.Visible = false;
                        PanelExtractFromRegister.Visible = false;
                        PanelResolutionOfGuardianship.Visible = false;
                        PanelSertificateOfOwnerShip.Visible = false;
                        PanelTechnicalPassport.Visible = false;
                    }

                    if (StatusTechnicalPassport == "Отклонен")
                    {
                        PanelDenide.Visible = true;

                        PanelPassport.Visible = false;
                        PanelCertificateFromTaxInspection.Visible = false;
                        PanelConsentOfSpouse.Visible = false;
                        PanelDebtForUtility.Visible = false;
                        PanelDocumentFoundation.Visible = false;
                        PanelExtendedExtractFromRegister.Visible = false;
                        PanelExtractFromRegister.Visible = false;
                        PanelResolutionOfGuardianship.Visible = false;
                        PanelSertificateOfOwnerShip.Visible = false;
                        PanelTechnicalPassport.Visible = false;
                    }

                    if (StatusTechnicalPassport == "На согласовании")
                    {
                        PanelTechnicalPassport.Visible = true;

                        PanelPassport.Visible = false;
                        PanelCertificateFromTaxInspection.Visible = false;
                        PanelConsentOfSpouse.Visible = false;
                        PanelDebtForUtility.Visible = false;
                        PanelDocumentFoundation.Visible = false;
                        PanelExtendedExtractFromRegister.Visible = false;
                        PanelExtractFromRegister.Visible = false;
                        PanelResolutionOfGuardianship.Visible = false;
                        PanelSertificateOfOwnerShip.Visible = false;
                    }

                    if (StatusTechnicalPassport == "Проверен")
                    {
                        PanelTechnicalPassport.Visible = true;

                        PanelPassport.Visible = false;
                        PanelCertificateFromTaxInspection.Visible = false;
                        PanelConsentOfSpouse.Visible = false;
                        PanelDebtForUtility.Visible = false;
                        PanelDocumentFoundation.Visible = false;
                        PanelExtendedExtractFromRegister.Visible = false;
                        PanelExtractFromRegister.Visible = false;
                        PanelResolutionOfGuardianship.Visible = false;
                        PanelSertificateOfOwnerShip.Visible = false;
                    }
                }
            }
            Conn.Close();
        }
        protected void LinkButtonDenideTechnicalPassport_Click(object sender, EventArgs e)
        {
            string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();
            string NameDirectory = Server.MapPath("DealDocuments/"
                 + Request.Cookies["Deal"]["IdDeal"].ToString());
            string[] FileName = Directory.GetFiles(NameDirectory, "TechnicalPassport*");
            string FileNameWithoutPath = System.IO.Path.GetFileName(FileName[0]);

            File.Delete(Server.MapPath("DealDocuments/" + IdDeal + "/") + FileNameWithoutPath);

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

            SqlCommand CommandSetStatusMainDocuments = new SqlCommand("UPDATE dbo.TableDeal SET"
                + " StatusTechnicalPassport = @ParamStatusTechnicalPassport" 
                + " WHERE IdDeal = @ParamIdDeal", Conn);

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandSetStatusMainDocuments.Parameters.Add(ParamIdDeal);

            SqlParameter ParamStatusTechnicalPassport = new SqlParameter();
            ParamStatusTechnicalPassport.ParameterName = "@ParamStatusTechnicalPassport";
            ParamStatusTechnicalPassport.Value = "Отклонен";
            CommandSetStatusMainDocuments.Parameters.Add(ParamStatusTechnicalPassport);

            Conn.Open();
            CommandSetStatusMainDocuments.ExecuteNonQuery();
            Conn.Close();

            PanelTechnicalPassport.Visible = false;
            PanelDenide.Visible = true;
        }
        protected void LinkButtonSaveTechnicalPassport_Click(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

            if (CheckBoxTechnicalPassportCharacteristic.Checked)

            {
                SqlCommand CommandSaveTechnicalPassport = new SqlCommand("UPDATE dbo.TableDeal"
                    + " SET StatusTechnicalPassport = @ParamStatusTechnicalPassport,"
                    + " TechnicalPassportCharacteristic = @ParamTechnicalPassportCharacteristic"
                    + " WHERE IdDeal = @ParamIdDeal", Conn);

                SqlParameter ParamIdDeal = new SqlParameter();
                ParamIdDeal.ParameterName = "@ParamIdDeal";
                ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandSaveTechnicalPassport.Parameters.Add(ParamIdDeal);

                SqlParameter ParamStatusTechnicalPassport = new SqlParameter();
                ParamStatusTechnicalPassport.ParameterName = "@ParamStatusTechnicalPassport";
                ParamStatusTechnicalPassport.Value = "Проверен";
                CommandSaveTechnicalPassport.Parameters.Add(ParamStatusTechnicalPassport);

                SqlParameter ParamTechnicalPassportCharacteristic = new SqlParameter();
                ParamTechnicalPassportCharacteristic.ParameterName = "@ParamTechnicalPassportCharacteristic";
                ParamTechnicalPassportCharacteristic.Value = CheckBoxTechnicalPassportCharacteristic.Checked.ToString();
                CommandSaveTechnicalPassport.Parameters.Add(ParamTechnicalPassportCharacteristic);

                Conn.Open();
                CommandSaveTechnicalPassport.ExecuteNonQuery();
                Conn.Close();
            }

            else

            {
                SqlCommand CommandSaveTechnicalPassport = new SqlCommand("UPDATE dbo.TableDeal"
                    + " SET StatusTechnicalPassport = @ParamStatusTechnicalPassport,"
                    + " TechnicalPassportCharacteristic = @ParamTechnicalPassportCharacteristic"
                    + " WHERE IdDeal = @ParamIdDeal", Conn);

                SqlParameter ParamIdDeal = new SqlParameter();
                ParamIdDeal.ParameterName = "@ParamIdDeal";
                ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandSaveTechnicalPassport.Parameters.Add(ParamIdDeal);

                SqlParameter ParamStatusTechnicalPassport = new SqlParameter();
                ParamStatusTechnicalPassport.ParameterName = "@ParamStatusTechnicalPassport";
                ParamStatusTechnicalPassport.Value = "На согласовании";
                CommandSaveTechnicalPassport.Parameters.Add(ParamStatusTechnicalPassport);

                SqlParameter ParamTechnicalPassportCharacteristic = new SqlParameter();
                ParamTechnicalPassportCharacteristic.ParameterName = "@ParamTechnicalPassportCharacteristic";
                ParamTechnicalPassportCharacteristic.Value = CheckBoxTechnicalPassportCharacteristic.Checked.ToString();
                CommandSaveTechnicalPassport.Parameters.Add(ParamTechnicalPassportCharacteristic);

                Conn.Open();
                CommandSaveTechnicalPassport.ExecuteNonQuery();
                Conn.Close();
            }
        }

        protected void LinkButtonDownLoadConsentOfSpouse_Click(object sender, EventArgs e)
        {
            string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();
            string NameDirectory = Server.MapPath("DealDocuments/"
                 + Request.Cookies["Deal"]["IdDeal"].ToString());
            string[] FileNameArray = Directory.GetFiles(NameDirectory, "ConsentOfSpouse*");
            string FileNameWithoutPath = System.IO.Path.GetFileName(FileNameArray[0]);

            string Ftp = "ftp://waws-prod-db3-051.ftp.azurewebsites.windows.net/site/wwwroot/DealDocuments/" +
                "/" + IdDeal + "/";

            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(Ftp + FileNameWithoutPath);
            request.Method = WebRequestMethods.Ftp.DownloadFile;

            request.Credentials = new NetworkCredential(@"ProWizard\ProWizard", "100Million");
            request.UsePassive = true;
            request.UseBinary = true;
            request.EnableSsl = false;

            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            using (MemoryStream stream = new MemoryStream())
            {
                response.GetResponseStream().CopyTo(stream);
                Response.AddHeader("content-disposition", "attachment;filename=" + FileNameWithoutPath);
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
        }
        protected void LinkButtonConsentOfSpouse_Click(object sender, EventArgs e)
        {
            LinkButtonPassport.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonSertificateOfOwnerShip.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonDocumentFoundation.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonExtractFromRegister.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonExtendedExtractFromRegister.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonTechnicalPassport.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonConsentOfSpouse.ForeColor = System.Drawing.ColorTranslator.FromHtml("#AA4825");
            LinkButtonResolutionOfGuardianship.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonDebtForUtility.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonCertificateFromTaxInspection.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandStatus = Conn.CreateCommand();
            CommandStatus.CommandText = "SELECT StatusDeal, StatusConsentOfSpouse"
                + " FROM dbo.TableDeal WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandStatus.Parameters.Add(ParamIdDeal);

            SqlDataReader dReader;

            Conn.Open();
            dReader = CommandStatus.ExecuteReader();

            if (dReader.Read())

            {
                string StatusDeal = dReader["StatusDeal"].ToString();
                string StatusConsentOfSpouse = dReader["StatusConsentOfSpouse"].ToString();

                if (StatusDeal == "Проверка документов")
                {
                    if (StatusConsentOfSpouse == "Подготовка")
                    {
                        PanelWaiting.Visible = true;

                        PanelPassport.Visible = false;
                        PanelCertificateFromTaxInspection.Visible = false;
                        PanelConsentOfSpouse.Visible = false;
                        PanelDebtForUtility.Visible = false;
                        PanelDocumentFoundation.Visible = false;
                        PanelExtendedExtractFromRegister.Visible = false;
                        PanelExtractFromRegister.Visible = false;
                        PanelResolutionOfGuardianship.Visible = false;
                        PanelSertificateOfOwnerShip.Visible = false;
                        PanelTechnicalPassport.Visible = false;
                    }

                    if (StatusConsentOfSpouse == "Отклонен")
                    {
                        PanelDenide.Visible = true;

                        PanelPassport.Visible = false;
                        PanelCertificateFromTaxInspection.Visible = false;
                        PanelConsentOfSpouse.Visible = false;
                        PanelDebtForUtility.Visible = false;
                        PanelDocumentFoundation.Visible = false;
                        PanelExtendedExtractFromRegister.Visible = false;
                        PanelExtractFromRegister.Visible = false;
                        PanelResolutionOfGuardianship.Visible = false;
                        PanelSertificateOfOwnerShip.Visible = false;
                        PanelTechnicalPassport.Visible = false;
                    }

                    if (StatusConsentOfSpouse == "На согласовании")
                    {
                        PanelConsentOfSpouse.Visible = true;

                        PanelPassport.Visible = false;
                        PanelCertificateFromTaxInspection.Visible = false;
                        PanelDebtForUtility.Visible = false;
                        PanelDocumentFoundation.Visible = false;
                        PanelExtendedExtractFromRegister.Visible = false;
                        PanelExtractFromRegister.Visible = false;
                        PanelResolutionOfGuardianship.Visible = false;
                        PanelSertificateOfOwnerShip.Visible = false;
                        PanelTechnicalPassport.Visible = false;
                    }

                    if (StatusConsentOfSpouse == "Проверен")
                    {
                        PanelConsentOfSpouse.Visible = true;

                        PanelPassport.Visible = false;
                        PanelCertificateFromTaxInspection.Visible = false;
                        PanelDebtForUtility.Visible = false;
                        PanelDocumentFoundation.Visible = false;
                        PanelExtendedExtractFromRegister.Visible = false;
                        PanelExtractFromRegister.Visible = false;
                        PanelResolutionOfGuardianship.Visible = false;
                        PanelSertificateOfOwnerShip.Visible = false;
                        PanelTechnicalPassport.Visible = false;
                    }
                }
            }
            Conn.Close();
        }
        protected void LinkButtonDenideConsentOfSpouse_Click(object sender, EventArgs e)
        {
            string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();
            string NameDirectory = Server.MapPath("DealDocuments/"
                 + Request.Cookies["Deal"]["IdDeal"].ToString());
            string[] FileName = Directory.GetFiles(NameDirectory, "ConsentOfSpouse*");
            string FileNameWithoutPath = System.IO.Path.GetFileName(FileName[0]);

            File.Delete(Server.MapPath("DealDocuments/" + IdDeal + "/") + FileNameWithoutPath);

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

            SqlCommand CommandSetStatusMainDocuments = new SqlCommand("UPDATE dbo.TableDeal SET"
                + " StatusConsentOfSpouse = @ParamStatusConsentOfSpouse"
                + " WHERE IdDeal = @ParamIdDeal", Conn);

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandSetStatusMainDocuments.Parameters.Add(ParamIdDeal);

            SqlParameter ParamStatusConsentOfSpouse = new SqlParameter();
            ParamStatusConsentOfSpouse.ParameterName = "@ParamStatusConsentOfSpouse";
            ParamStatusConsentOfSpouse.Value = "Отклонен";
            CommandSetStatusMainDocuments.Parameters.Add(ParamStatusConsentOfSpouse);

            Conn.Open();
            CommandSetStatusMainDocuments.ExecuteNonQuery();
            Conn.Close();

            PanelConsentOfSpouse.Visible = false;
            PanelDenide.Visible = true;
        }
        protected void LinkButtonSaveConsentOfSpouse_Click(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

            if (CheckBoxConsentOfSpouseCharacteristic.Checked)

            {
                SqlCommand CommandSaveConsentOfSpouse = new SqlCommand("UPDATE dbo.TableDeal"
                                + " SET StatusConsentOfSpouse = @ParamStatusConsentOfSpouse,"
                                + " ConsentOfSpouseCharacteristic = @ParamConsentOfSpouseCharacteristic"
                                + " WHERE IdDeal = @ParamIdDeal", Conn);

                SqlParameter ParamIdDeal = new SqlParameter();
                ParamIdDeal.ParameterName = "@ParamIdDeal";
                ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandSaveConsentOfSpouse.Parameters.Add(ParamIdDeal);

                SqlParameter ParamStatusConsentOfSpouse = new SqlParameter();
                ParamStatusConsentOfSpouse.ParameterName = "@ParamStatusConsentOfSpouse";
                ParamStatusConsentOfSpouse.Value = "Проверен";
                CommandSaveConsentOfSpouse.Parameters.Add(ParamStatusConsentOfSpouse);

                SqlParameter ConsentOfSpouseCharacteristic = new SqlParameter();
                ConsentOfSpouseCharacteristic.ParameterName = "@ParamConsentOfSpouseCharacteristic";
                ConsentOfSpouseCharacteristic.Value = CheckBoxConsentOfSpouseCharacteristic.Checked.ToString();
                CommandSaveConsentOfSpouse.Parameters.Add(ConsentOfSpouseCharacteristic);

                Conn.Open();
                CommandSaveConsentOfSpouse.ExecuteNonQuery();
                Conn.Close();
            }

            else

            {
                SqlCommand CommandSaveConsentOfSpouse = new SqlCommand("UPDATE dbo.TableDeal"
                               + " SET StatusConsentOfSpouse = @ParamStatusConsentOfSpouse,"
                               + " ConsentOfSpouseCharacteristic = @ParamConsentOfSpouseCharacteristic"
                               + " WHERE IdDeal = @ParamIdDeal", Conn);

                SqlParameter ParamIdDeal = new SqlParameter();
                ParamIdDeal.ParameterName = "@ParamIdDeal";
                ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandSaveConsentOfSpouse.Parameters.Add(ParamIdDeal);

                SqlParameter ParamStatusConsentOfSpouse = new SqlParameter();
                ParamStatusConsentOfSpouse.ParameterName = "@ParamStatusConsentOfSpouse";
                ParamStatusConsentOfSpouse.Value = "На согласовании";
                CommandSaveConsentOfSpouse.Parameters.Add(ParamStatusConsentOfSpouse);

                SqlParameter ConsentOfSpouseCharacteristic = new SqlParameter();
                ConsentOfSpouseCharacteristic.ParameterName = "@ParamConsentOfSpouseCharacteristic";
                ConsentOfSpouseCharacteristic.Value = CheckBoxConsentOfSpouseCharacteristic.Checked.ToString();
                CommandSaveConsentOfSpouse.Parameters.Add(ConsentOfSpouseCharacteristic);

                Conn.Open();
                CommandSaveConsentOfSpouse.ExecuteNonQuery();
                Conn.Close();
            }
        }

        protected void LinkButtonDownLoadResolutionOfGuardianship_Click(object sender, EventArgs e)
        {
            string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();
            string NameDirectory = Server.MapPath("DealDocuments/"
                 + Request.Cookies["Deal"]["IdDeal"].ToString());
            string[] FileNameArray = Directory.GetFiles(NameDirectory, "ResolutionOfGuardianship*");
            string FileNameWithoutPath = System.IO.Path.GetFileName(FileNameArray[0]);

            string Ftp = "ftp://waws-prod-db3-051.ftp.azurewebsites.windows.net/site/wwwroot/DealDocuments/" +
                "/" + IdDeal + "/";

            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(Ftp + FileNameWithoutPath);
            request.Method = WebRequestMethods.Ftp.DownloadFile;

            request.Credentials = new NetworkCredential(@"ProWizard\ProWizard", "100Million");
            request.UsePassive = true;
            request.UseBinary = true;
            request.EnableSsl = false;

            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            using (MemoryStream stream = new MemoryStream())
            {
                response.GetResponseStream().CopyTo(stream);
                Response.AddHeader("content-disposition", "attachment;filename=" + FileNameWithoutPath);
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
        }
        protected void LinkButtonResolutionOfGuardianship_Click(object sender, EventArgs e)
        {
            LinkButtonPassport.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonSertificateOfOwnerShip.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonDocumentFoundation.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonExtractFromRegister.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonExtendedExtractFromRegister.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonTechnicalPassport.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonConsentOfSpouse.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonResolutionOfGuardianship.ForeColor = System.Drawing.ColorTranslator.FromHtml("#AA4825");
            LinkButtonDebtForUtility.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonCertificateFromTaxInspection.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandStatus = Conn.CreateCommand();
            CommandStatus.CommandText = "SELECT StatusDeal, StatusResolutionOfGuardianship"
                + " FROM dbo.TableDeal WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandStatus.Parameters.Add(ParamIdDeal);

            SqlDataReader dReader;

            Conn.Open();
            dReader = CommandStatus.ExecuteReader();

            if (dReader.Read())

            {
                string StatusDeal = dReader["StatusDeal"].ToString();
                string StatusResolutionOfGuardianship = dReader["StatusResolutionOfGuardianship"].ToString();

                if (StatusDeal == "Проверка документов")
                {
                    if (StatusResolutionOfGuardianship == "Подготовка")
                    {
                        PanelWaiting.Visible = true;

                        PanelPassport.Visible = false;
                        PanelCertificateFromTaxInspection.Visible = false;
                        PanelConsentOfSpouse.Visible = false;
                        PanelDebtForUtility.Visible = false;
                        PanelDocumentFoundation.Visible = false;
                        PanelExtendedExtractFromRegister.Visible = false;
                        PanelExtractFromRegister.Visible = false;
                        PanelResolutionOfGuardianship.Visible = false;
                        PanelSertificateOfOwnerShip.Visible = false;
                        PanelTechnicalPassport.Visible = false;
                    }

                    if (StatusResolutionOfGuardianship == "Отклонен")
                    {
                        PanelDenide.Visible = true;

                        PanelPassport.Visible = false;
                        PanelCertificateFromTaxInspection.Visible = false;
                        PanelConsentOfSpouse.Visible = false;
                        PanelDebtForUtility.Visible = false;
                        PanelDocumentFoundation.Visible = false;
                        PanelExtendedExtractFromRegister.Visible = false;
                        PanelExtractFromRegister.Visible = false;
                        PanelResolutionOfGuardianship.Visible = false;
                        PanelSertificateOfOwnerShip.Visible = false;
                        PanelTechnicalPassport.Visible = false;
                    }

                    if (StatusResolutionOfGuardianship == "На согласовании")
                    {
                        PanelResolutionOfGuardianship.Visible = true;

                        PanelPassport.Visible = false;
                        PanelCertificateFromTaxInspection.Visible = false;
                        PanelConsentOfSpouse.Visible = false;
                        PanelDebtForUtility.Visible = false;
                        PanelDocumentFoundation.Visible = false;
                        PanelExtendedExtractFromRegister.Visible = false;
                        PanelExtractFromRegister.Visible = false;
                        PanelSertificateOfOwnerShip.Visible = false;
                        PanelTechnicalPassport.Visible = false;
                    }

                    if (StatusResolutionOfGuardianship == "Проверен")
                    {
                        PanelResolutionOfGuardianship.Visible = true;

                        PanelPassport.Visible = false;
                        PanelCertificateFromTaxInspection.Visible = false;
                        PanelConsentOfSpouse.Visible = false;
                        PanelDebtForUtility.Visible = false;
                        PanelDocumentFoundation.Visible = false;
                        PanelExtendedExtractFromRegister.Visible = false;
                        PanelExtractFromRegister.Visible = false;
                        PanelSertificateOfOwnerShip.Visible = false;
                        PanelTechnicalPassport.Visible = false;
                    }
                }
            }
            Conn.Close();
        }
        protected void LinkButtonDenideResolutionOfGuardianship_Click(object sender, EventArgs e)
        {
            string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();
            string NameDirectory = Server.MapPath("DealDocuments/"
                 + Request.Cookies["Deal"]["IdDeal"].ToString());
            string[] FileName = Directory.GetFiles(NameDirectory, "ResolutionOfGuardianship*");
            string FileNameWithoutPath = System.IO.Path.GetFileName(FileName[0]);

            File.Delete(Server.MapPath("DealDocuments/" + IdDeal + "/") + FileNameWithoutPath);

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

            SqlCommand CommandSetStatusMainDocuments = new SqlCommand("UPDATE dbo.TableDeal SET"
                + " StatusResolutionOfGuardianship = @ParamStatusResolutionOfGuardianship"
                + " WHERE IdDeal = @ParamIdDeal", Conn);

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandSetStatusMainDocuments.Parameters.Add(ParamIdDeal);

            SqlParameter ParamStatusResolutionOfGuardianship = new SqlParameter();
            ParamStatusResolutionOfGuardianship.ParameterName = "@ParamStatusResolutionOfGuardianship";
            ParamStatusResolutionOfGuardianship.Value = "Отклонен";
            CommandSetStatusMainDocuments.Parameters.Add(ParamStatusResolutionOfGuardianship);

            Conn.Open();
            CommandSetStatusMainDocuments.ExecuteNonQuery();
            Conn.Close();

            PanelResolutionOfGuardianship.Visible = false;
            PanelDenide.Visible = true;
        }
        protected void LinkButtonSaveResolutionOfGuardianship_Click(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

            if (CheckBoxResolutionOfGuardianshipCharacteristic.Checked)

            {
                SqlCommand CommandSaveParamResolutionOfGuardianshipCharacteristic = new SqlCommand("UPDATE dbo.TableDeal"
                + " SET StatusResolutionOfGuardianship = @ParamStatusResolutionOfGuardianship,"
                + " ResolutionOfGuardianshipCharacteristic = @ParamResolutionOfGuardianshipCharacteristic"
                + " WHERE IdDeal = @ParamIdDeal", Conn);

                SqlParameter ParamIdDeal = new SqlParameter();
                ParamIdDeal.ParameterName = "@ParamIdDeal";
                ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandSaveParamResolutionOfGuardianshipCharacteristic.Parameters.Add(ParamIdDeal);


                SqlParameter ParamStatusResolutionOfGuardianship = new SqlParameter();
                ParamStatusResolutionOfGuardianship.ParameterName = "@ParamStatusResolutionOfGuardianship";
                ParamStatusResolutionOfGuardianship.Value = "Проверен";
                CommandSaveParamResolutionOfGuardianshipCharacteristic.Parameters.Add(ParamStatusResolutionOfGuardianship);

                SqlParameter ParamResolutionOfGuardianshipCharacteristic = new SqlParameter();
                ParamResolutionOfGuardianshipCharacteristic.ParameterName = "@ParamResolutionOfGuardianshipCharacteristic";
                ParamResolutionOfGuardianshipCharacteristic.Value = CheckBoxResolutionOfGuardianshipCharacteristic.Checked.ToString();
                CommandSaveParamResolutionOfGuardianshipCharacteristic.Parameters.Add(ParamResolutionOfGuardianshipCharacteristic);

                Conn.Open();
                CommandSaveParamResolutionOfGuardianshipCharacteristic.ExecuteNonQuery();
                Conn.Close();

            }

            else

            {
                SqlCommand CommandSaveParamResolutionOfGuardianshipCharacteristic = new SqlCommand("UPDATE dbo.TableDeal"
             + " SET StatusResolutionOfGuardianship = @ParamStatusResolutionOfGuardianship,"
             + " ResolutionOfGuardianshipCharacteristic = @ParamResolutionOfGuardianshipCharacteristic"
             + " WHERE IdDeal = @ParamIdDeal", Conn);

                SqlParameter ParamIdDeal = new SqlParameter();
                ParamIdDeal.ParameterName = "@ParamIdDeal";
                ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandSaveParamResolutionOfGuardianshipCharacteristic.Parameters.Add(ParamIdDeal);

                SqlParameter ParamStatusResolutionOfGuardianship = new SqlParameter();
                ParamStatusResolutionOfGuardianship.ParameterName = "@ParamStatusResolutionOfGuardianship";
                ParamStatusResolutionOfGuardianship.Value = "На согласовании";
                CommandSaveParamResolutionOfGuardianshipCharacteristic.Parameters.Add(ParamStatusResolutionOfGuardianship);

                SqlParameter ParamResolutionOfGuardianshipCharacteristic = new SqlParameter();
                ParamResolutionOfGuardianshipCharacteristic.ParameterName = "@ParamResolutionOfGuardianshipCharacteristic";
                ParamResolutionOfGuardianshipCharacteristic.Value = CheckBoxResolutionOfGuardianshipCharacteristic.Checked.ToString();
                CommandSaveParamResolutionOfGuardianshipCharacteristic.Parameters.Add(ParamResolutionOfGuardianshipCharacteristic);

                Conn.Open();
                CommandSaveParamResolutionOfGuardianshipCharacteristic.ExecuteNonQuery();
                Conn.Close();
            }
        }

        protected void LinkButtonDownLoadDebtForUtility_Click(object sender, EventArgs e)
        {
            string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();
            string NameDirectory = Server.MapPath("DealDocuments/"
                 + Request.Cookies["Deal"]["IdDeal"].ToString());
            string[] FileNameArray = Directory.GetFiles(NameDirectory, "DebtForUtility*");
            string FileNameWithoutPath = System.IO.Path.GetFileName(FileNameArray[0]);

            string Ftp = "ftp://waws-prod-db3-051.ftp.azurewebsites.windows.net/site/wwwroot/DealDocuments/" +
                "/" + IdDeal + "/";

            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(Ftp + FileNameWithoutPath);
            request.Method = WebRequestMethods.Ftp.DownloadFile;

            request.Credentials = new NetworkCredential(@"ProWizard\ProWizard", "100Million");
            request.UsePassive = true;
            request.UseBinary = true;
            request.EnableSsl = false;

            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            using (MemoryStream stream = new MemoryStream())
            {
                response.GetResponseStream().CopyTo(stream);
                Response.AddHeader("content-disposition", "attachment;filename=" + FileNameWithoutPath);
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
        }
        protected void LinkButtonDebtForUtility_Click(object sender, EventArgs e)
        {
            LinkButtonPassport.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonSertificateOfOwnerShip.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonDocumentFoundation.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonExtractFromRegister.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonExtendedExtractFromRegister.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonTechnicalPassport.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonConsentOfSpouse.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonResolutionOfGuardianship.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonDebtForUtility.ForeColor = System.Drawing.ColorTranslator.FromHtml("#AA4825");
            LinkButtonCertificateFromTaxInspection.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandStatus = Conn.CreateCommand();
            CommandStatus.CommandText = "SELECT StatusDeal, StatusDebtForUtility"
                + " FROM dbo.TableDeal WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandStatus.Parameters.Add(ParamIdDeal);

            SqlDataReader dReader;

            Conn.Open();
            dReader = CommandStatus.ExecuteReader();

            if (dReader.Read())

            {
                string StatusDeal = dReader["StatusDeal"].ToString();
                string StatusDebtForUtility = dReader["StatusDebtForUtility"].ToString();

                if (StatusDeal == "Проверка документов")
                {
                    if (StatusDebtForUtility == "Подготовка")
                    {
                        PanelWaiting.Visible = true;

                        PanelPassport.Visible = false;
                        PanelCertificateFromTaxInspection.Visible = false;
                        PanelConsentOfSpouse.Visible = false;
                        PanelDebtForUtility.Visible = false;
                        PanelDocumentFoundation.Visible = false;
                        PanelExtendedExtractFromRegister.Visible = false;
                        PanelExtractFromRegister.Visible = false;
                        PanelResolutionOfGuardianship.Visible = false;
                        PanelSertificateOfOwnerShip.Visible = false;
                        PanelTechnicalPassport.Visible = false;
                    }

                    if (StatusDebtForUtility == "Отклонен")
                    {
                        PanelDenide.Visible = true;

                        PanelPassport.Visible = false;
                        PanelCertificateFromTaxInspection.Visible = false;
                        PanelConsentOfSpouse.Visible = false;
                        PanelDebtForUtility.Visible = false;
                        PanelDocumentFoundation.Visible = false;
                        PanelExtendedExtractFromRegister.Visible = false;
                        PanelExtractFromRegister.Visible = false;
                        PanelResolutionOfGuardianship.Visible = false;
                        PanelSertificateOfOwnerShip.Visible = false;
                        PanelTechnicalPassport.Visible = false;
                    }

                    if (StatusDebtForUtility == "На согласовании")
                    {
                        PanelDebtForUtility.Visible = true;

                        PanelPassport.Visible = false;
                        PanelCertificateFromTaxInspection.Visible = false;
                        PanelConsentOfSpouse.Visible = false;
                        PanelDocumentFoundation.Visible = false;
                        PanelExtendedExtractFromRegister.Visible = false;
                        PanelExtractFromRegister.Visible = false;
                        PanelResolutionOfGuardianship.Visible = false;
                        PanelSertificateOfOwnerShip.Visible = false;
                        PanelTechnicalPassport.Visible = false;
                    }

                    if (StatusDebtForUtility == "Проверен")
                    {
                        PanelDebtForUtility.Visible = true;

                        PanelPassport.Visible = false;
                        PanelCertificateFromTaxInspection.Visible = false;
                        PanelConsentOfSpouse.Visible = false;
                        PanelDocumentFoundation.Visible = false;
                        PanelExtendedExtractFromRegister.Visible = false;
                        PanelExtractFromRegister.Visible = false;
                        PanelResolutionOfGuardianship.Visible = false;
                        PanelSertificateOfOwnerShip.Visible = false;
                        PanelTechnicalPassport.Visible = false;
                    }
                }
            }
            Conn.Close();
        }
        protected void LinkButtonDenideDebtForUtility_Click(object sender, EventArgs e)
        {
            string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();
            string NameDirectory = Server.MapPath("DealDocuments/"
                 + Request.Cookies["Deal"]["IdDeal"].ToString());
            string[] FileName = Directory.GetFiles(NameDirectory, "DebtForUtility*");
            string FileNameWithoutPath = System.IO.Path.GetFileName(FileName[0]);

            File.Delete(Server.MapPath("DealDocuments/" + IdDeal + "/") + FileNameWithoutPath);

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

            SqlCommand CommandSetStatusMainDocuments = new SqlCommand("UPDATE dbo.TableDeal SET"
                + " StatusDebtForUtility = @ParamStatusDebtForUtility" 
                + " WHERE IdDeal = @ParamIdDeal", Conn);

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandSetStatusMainDocuments.Parameters.Add(ParamIdDeal);

            SqlParameter ParamStatusDebtForUtility = new SqlParameter();
            ParamStatusDebtForUtility.ParameterName = "@ParamStatusDebtForUtility";
            ParamStatusDebtForUtility.Value = "Отклонен";
            CommandSetStatusMainDocuments.Parameters.Add(ParamStatusDebtForUtility);

            Conn.Open();
            CommandSetStatusMainDocuments.ExecuteNonQuery();
            Conn.Close();

            PanelDebtForUtility.Visible = false;
            PanelDenide.Visible = true;
        }
        protected void LinkButtonSaveDebtForUtility_Click(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

            if (CheckBoxDebtForUtilityCharacteristic.Checked)

            {
                SqlCommand CommandSaveDebtForUtility = new SqlCommand("UPDATE dbo.TableDeal"
                               + " SET StatusDebtForUtility = @ParamStatusDebtForUtility,"
                               + " DebtForUtilityCharacteristic = @ParamDebtForUtilityCharacteristic"
                               + " WHERE IdDeal = @ParamIdDeal", Conn);

                SqlParameter ParamIdDeal = new SqlParameter();
                ParamIdDeal.ParameterName = "@ParamIdDeal";
                ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandSaveDebtForUtility.Parameters.Add(ParamIdDeal);

                SqlParameter ParamStatusDebtForUtility = new SqlParameter();
                ParamStatusDebtForUtility.ParameterName = "@ParamStatusDebtForUtility";
                ParamStatusDebtForUtility.Value = "Проверен";
                CommandSaveDebtForUtility.Parameters.Add(ParamStatusDebtForUtility);

                SqlParameter ParamDebtForUtilityCharacteristic = new SqlParameter();
                ParamDebtForUtilityCharacteristic.ParameterName = "@ParamDebtForUtilityCharacteristic";
                ParamDebtForUtilityCharacteristic.Value = CheckBoxDebtForUtilityCharacteristic.Checked.ToString();
                CommandSaveDebtForUtility.Parameters.Add(ParamDebtForUtilityCharacteristic);

                Conn.Open();
                CommandSaveDebtForUtility.ExecuteNonQuery();
                Conn.Close();
            }

            else

            {
                SqlCommand CommandSaveDebtForUtility = new SqlCommand("UPDATE dbo.TableDeal"
                                               + " SET StatusDebtForUtility = @ParamStatusDebtForUtility,"
                                               + " DebtForUtilityCharacteristic = @ParamDebtForUtilityCharacteristic"
                                               + " WHERE IdDeal = @ParamIdDeal", Conn);

                SqlParameter ParamIdDeal = new SqlParameter();
                ParamIdDeal.ParameterName = "@ParamIdDeal";
                ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandSaveDebtForUtility.Parameters.Add(ParamIdDeal);

                SqlParameter ParamStatusDebtForUtility = new SqlParameter();
                ParamStatusDebtForUtility.ParameterName = "@ParamStatusDebtForUtility";
                ParamStatusDebtForUtility.Value = "На согласовании";
                CommandSaveDebtForUtility.Parameters.Add(ParamStatusDebtForUtility);

                SqlParameter ParamDebtForUtilityCharacteristic = new SqlParameter();
                ParamDebtForUtilityCharacteristic.ParameterName = "@ParamDebtForUtilityCharacteristic";
                ParamDebtForUtilityCharacteristic.Value = CheckBoxDebtForUtilityCharacteristic.Checked.ToString();
                CommandSaveDebtForUtility.Parameters.Add(ParamDebtForUtilityCharacteristic);

                Conn.Open();
                CommandSaveDebtForUtility.ExecuteNonQuery();
                Conn.Close();
            }
        }

        protected void LinkButtonDownLoadCertificateFromTaxInspection_Click(object sender, EventArgs e)
        {
            string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();
            string NameDirectory = Server.MapPath("DealDocuments/"
                 + Request.Cookies["Deal"]["IdDeal"].ToString());
            string[] FileNameArray = Directory.GetFiles(NameDirectory, "CertificateFromTaxInspection*");
            string FileNameWithoutPath = System.IO.Path.GetFileName(FileNameArray[0]);

            string Ftp = "ftp://waws-prod-db3-051.ftp.azurewebsites.windows.net/site/wwwroot/DealDocuments/" +
                "/" + IdDeal + "/";

            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(Ftp + FileNameWithoutPath);
            request.Method = WebRequestMethods.Ftp.DownloadFile;

            request.Credentials = new NetworkCredential(@"ProWizard\ProWizard", "100Million");
            request.UsePassive = true;
            request.UseBinary = true;
            request.EnableSsl = false;

            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            using (MemoryStream stream = new MemoryStream())
            {
                response.GetResponseStream().CopyTo(stream);
                Response.AddHeader("content-disposition", "attachment;filename=" + FileNameWithoutPath);
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
        }
        protected void LinkButtonCertificateFromTaxInspection_Click(object sender, EventArgs e)
        {
            LinkButtonPassport.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonSertificateOfOwnerShip.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonDocumentFoundation.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonExtractFromRegister.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonExtendedExtractFromRegister.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonTechnicalPassport.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonConsentOfSpouse.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonResolutionOfGuardianship.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonDebtForUtility.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonCertificateFromTaxInspection.ForeColor = System.Drawing.ColorTranslator.FromHtml("#AA4825");

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandStatus = Conn.CreateCommand();
            CommandStatus.CommandText = "SELECT StatusDeal, StatusCertificateFromTaxInspection"
                + " FROM dbo.TableDeal WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandStatus.Parameters.Add(ParamIdDeal);

            SqlDataReader dReader;

            Conn.Open();
            dReader = CommandStatus.ExecuteReader();

            if (dReader.Read())

            {
                string StatusDeal = dReader["StatusDeal"].ToString();
                string StatusCertificateFromTaxInspection = dReader["StatusCertificateFromTaxInspection"].ToString();

                if (StatusDeal == "Проверка документов")
                {
                    if (StatusCertificateFromTaxInspection == "Подготовка")
                    {
                        PanelWaiting.Visible = true;

                        PanelPassport.Visible = false;
                        PanelCertificateFromTaxInspection.Visible = false;
                        PanelConsentOfSpouse.Visible = false;
                        PanelDebtForUtility.Visible = false;
                        PanelDocumentFoundation.Visible = false;
                        PanelExtendedExtractFromRegister.Visible = false;
                        PanelExtractFromRegister.Visible = false;
                        PanelResolutionOfGuardianship.Visible = false;
                        PanelSertificateOfOwnerShip.Visible = false;
                        PanelTechnicalPassport.Visible = false;
                    }

                    if (StatusCertificateFromTaxInspection == "Отклонен")
                    {
                        PanelDenide.Visible = true;

                        PanelPassport.Visible = false;
                        PanelCertificateFromTaxInspection.Visible = false;
                        PanelConsentOfSpouse.Visible = false;
                        PanelDebtForUtility.Visible = false;
                        PanelDocumentFoundation.Visible = false;
                        PanelExtendedExtractFromRegister.Visible = false;
                        PanelExtractFromRegister.Visible = false;
                        PanelResolutionOfGuardianship.Visible = false;
                        PanelSertificateOfOwnerShip.Visible = false;
                        PanelTechnicalPassport.Visible = false;
                    }

                    if (StatusCertificateFromTaxInspection == "На согласовании")
                    {
                        PanelCertificateFromTaxInspection.Visible = true;

                        PanelPassport.Visible = false;
                        PanelConsentOfSpouse.Visible = false;
                        PanelDebtForUtility.Visible = false;
                        PanelDocumentFoundation.Visible = false;
                        PanelExtendedExtractFromRegister.Visible = false;
                        PanelExtractFromRegister.Visible = false;
                        PanelResolutionOfGuardianship.Visible = false;
                        PanelSertificateOfOwnerShip.Visible = false;
                        PanelTechnicalPassport.Visible = false;
                    }

                    if (StatusCertificateFromTaxInspection == "Проверен")
                    {
                        PanelCertificateFromTaxInspection.Visible = true;

                        PanelPassport.Visible = false;
                        PanelConsentOfSpouse.Visible = false;
                        PanelDebtForUtility.Visible = false;
                        PanelDocumentFoundation.Visible = false;
                        PanelExtendedExtractFromRegister.Visible = false;
                        PanelExtractFromRegister.Visible = false;
                        PanelResolutionOfGuardianship.Visible = false;
                        PanelSertificateOfOwnerShip.Visible = false;
                        PanelTechnicalPassport.Visible = false;
                    }
                }
            }
            Conn.Close();
        }
        protected void LinkButtonDenideCertificateFromTaxInspection_Click(object sender, EventArgs e)
        {
            string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();
            string NameDirectory = Server.MapPath("DealDocuments/"
                 + Request.Cookies["Deal"]["IdDeal"].ToString());
            string[] FileName = Directory.GetFiles(NameDirectory, "CertificateFromTaxInspection*");
            string FileNameWithoutPath = System.IO.Path.GetFileName(FileName[0]);

            File.Delete(Server.MapPath("DealDocuments/" + IdDeal + "/") + FileNameWithoutPath);

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

            SqlCommand CommandSetStatusMainDocuments = new SqlCommand("UPDATE dbo.TableDeal SET"
                + " StatusCertificateFromTaxInspection = @ParamStatusCertificateFromTaxInspection"
                + " WHERE IdDeal = @ParamIdDeal", Conn);

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandSetStatusMainDocuments.Parameters.Add(ParamIdDeal);

            SqlParameter ParamStatusCertificateFromTaxInspection = new SqlParameter();
            ParamStatusCertificateFromTaxInspection.ParameterName = "@ParamStatusCertificateFromTaxInspection";
            ParamStatusCertificateFromTaxInspection.Value = "Отклонен";
            CommandSetStatusMainDocuments.Parameters.Add(ParamStatusCertificateFromTaxInspection);

            Conn.Open();
            CommandSetStatusMainDocuments.ExecuteNonQuery();
            Conn.Close();

            PanelCertificateFromTaxInspection.Visible = false;
            PanelDenide.Visible = true;
        }
        protected void LinkButtonSaveCertificateFromTaxInspection_Click(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

            if (CheckBoxCertificateFromTaxInspectionCharacteristic.Checked)

            {
                SqlCommand CommandSaveCertificateFromTaxInspection = new SqlCommand("UPDATE dbo.TableDeal"
                               + " SET StatusCertificateFromTaxInspection = @ParamStatusCertificateFromTaxInspection,"
                               + " CertificateFromTaxInspectionCharacteristic = @ParamCertificateFromTaxInspectionCharacteristic"
                               + " WHERE IdDeal = @ParamIdDeal", Conn);

                SqlParameter ParamIdDeal = new SqlParameter();
                ParamIdDeal.ParameterName = "@ParamIdDeal";
                ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandSaveCertificateFromTaxInspection.Parameters.Add(ParamIdDeal);

                SqlParameter ParamStatusCertificateFromTaxInspection = new SqlParameter();
                ParamStatusCertificateFromTaxInspection.ParameterName = "@ParamStatusCertificateFromTaxInspection";
                ParamStatusCertificateFromTaxInspection.Value = "Проверен";
                CommandSaveCertificateFromTaxInspection.Parameters.Add(ParamStatusCertificateFromTaxInspection);

                SqlParameter CertificateFromTaxInspectionCharacteristic = new SqlParameter();
                CertificateFromTaxInspectionCharacteristic.ParameterName = "@ParamCertificateFromTaxInspectionCharacteristic";
                CertificateFromTaxInspectionCharacteristic.Value = CheckBoxCertificateFromTaxInspectionCharacteristic.Checked.ToString();
                CommandSaveCertificateFromTaxInspection.Parameters.Add(CertificateFromTaxInspectionCharacteristic);

                Conn.Open();
                CommandSaveCertificateFromTaxInspection.ExecuteNonQuery();
                Conn.Close();
            }

            else

            {
                SqlCommand CommandSaveCertificateFromTaxInspection = new SqlCommand("UPDATE dbo.TableDeal"
                         + " SET StatusCertificateFromTaxInspection = @ParamStatusCertificateFromTaxInspection,"
                         + " CertificateFromTaxInspectionCharacteristic = @ParamCertificateFromTaxInspectionCharacteristic"
                         + " WHERE IdDeal = @ParamIdDeal", Conn);

                SqlParameter ParamIdDeal = new SqlParameter();
                ParamIdDeal.ParameterName = "@ParamIdDeal";
                ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandSaveCertificateFromTaxInspection.Parameters.Add(ParamIdDeal);

                SqlParameter ParamStatusCertificateFromTaxInspection = new SqlParameter();
                ParamStatusCertificateFromTaxInspection.ParameterName = "@ParamStatusCertificateFromTaxInspection";
                ParamStatusCertificateFromTaxInspection.Value = "На согласовании";
                CommandSaveCertificateFromTaxInspection.Parameters.Add(ParamStatusCertificateFromTaxInspection);

                SqlParameter CertificateFromTaxInspectionCharacteristic = new SqlParameter();
                CertificateFromTaxInspectionCharacteristic.ParameterName = "@ParamCertificateFromTaxInspectionCharacteristic";
                CertificateFromTaxInspectionCharacteristic.Value = CheckBoxCertificateFromTaxInspectionCharacteristic.Checked.ToString();
                CommandSaveCertificateFromTaxInspection.Parameters.Add(CertificateFromTaxInspectionCharacteristic);

                Conn.Open();
                CommandSaveCertificateFromTaxInspection.ExecuteNonQuery();
                Conn.Close();
            }
        }

        protected void ButtonForward_Click(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

            SqlCommand CommandDefineStatusMainDocuments = Conn.CreateCommand();
            CommandDefineStatusMainDocuments.CommandText = "SELECT StatusPassport,"
                + " StatusSertificateOfOwnerShip,"
                + " StatusDocumentFoundation, "
                + " StatusExtractFromRegister, "
                + " StatusExtendedExtractFromRegister,"
                + " StatusTechnicalPassport, "
                + " StatusConsentOfSpouse, "
                + " StatusResolutionOfGuardianship,"
                + " StatusDebtForUtility, "
                + " StatusCertificateFromTaxInspection"
                + " FROM dbo.TableDeal" 
                + " WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandDefineStatusMainDocuments.Parameters.Add(ParamIdDeal);

            SqlDataReader dReaderStatusMainDocuments;

            Conn.Open();
            dReaderStatusMainDocuments = CommandDefineStatusMainDocuments.ExecuteReader();

            if (dReaderStatusMainDocuments.Read())

            {
                string StatusPassport = dReaderStatusMainDocuments["StatusPassport"].ToString();
                string StatusSertificateOfOwnerShip = dReaderStatusMainDocuments["StatusSertificateOfOwnerShip"].ToString();
                string StatusDocumentFoundation = dReaderStatusMainDocuments["StatusDocumentFoundation"].ToString();
                string StatusExtractFromRegister = dReaderStatusMainDocuments["StatusExtractFromRegister"].ToString();
                string StatusExtendedExtractFromRegister = dReaderStatusMainDocuments["StatusExtendedExtractFromRegister"].ToString();
                string StatusTechnicalPassport = dReaderStatusMainDocuments["StatusTechnicalPassport"].ToString();
                string StatusConsentOfSpouse = dReaderStatusMainDocuments["StatusConsentOfSpouse"].ToString();
                string StatusResolutionOfGuardianship = dReaderStatusMainDocuments["StatusResolutionOfGuardianship"].ToString();
                string StatusDebtForUtility = dReaderStatusMainDocuments["StatusDebtForUtility"].ToString();
                string StatusCertificateFromTaxInspection = dReaderStatusMainDocuments["StatusCertificateFromTaxInspection"].ToString();

                if (StatusPassport == "Проверен" & StatusSertificateOfOwnerShip == "Проверен" & StatusDocumentFoundation == "Проверен"
                    & StatusExtractFromRegister == "Проверен" & StatusExtendedExtractFromRegister == "Проверен"
                    & StatusTechnicalPassport == "Проверен" & StatusConsentOfSpouse == "Проверен" & StatusResolutionOfGuardianship == "Проверен"
                    & StatusDebtForUtility == "Проверен" & StatusCertificateFromTaxInspection == "Проверен")

                {
                    SqlCommand CommandSetStatusPrepareDocuments = new SqlCommand("UPDATE dbo.TableDeal"
                                       + " SET StatusDeal = @ParamStatusDeal"
                                       + " WHERE IdDeal = @ParamIdDealTWO", Conn);

                    SqlParameter ParamStatusDeal = new SqlParameter();
                    ParamStatusDeal.ParameterName = "@ParamStatusDeal";
                    ParamStatusDeal.Value = "Подготовка к сделке";
                    CommandSetStatusPrepareDocuments.Parameters.Add(ParamStatusDeal);

                    SqlParameter ParamIdDealTWO = new SqlParameter();
                    ParamIdDealTWO.ParameterName = "@ParamIdDealTWO";
                    ParamIdDealTWO.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                    CommandSetStatusPrepareDocuments.Parameters.Add(ParamIdDealTWO);

                    dReaderStatusMainDocuments.Close();

                    CommandSetStatusPrepareDocuments.ExecuteNonQuery();

                    Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormContractBuyer"));
                }

                else

                {
                    PanelForward.Visible = false;
                    PanelReminder.Visible = true;
                }
            }
            Conn.Close();
        }
        protected void ButtonForce_Click(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandSetStatusPrepareDocuments = new SqlCommand("UPDATE dbo.TableDeal"
                                      + " SET StatusDeal = @ParamStatusDeal"
                                      + " WHERE IdDeal = @ParamIdDealTWO", Conn);

            SqlParameter ParamStatusDeal = new SqlParameter();
            ParamStatusDeal.ParameterName = "@ParamStatusDeal";
            ParamStatusDeal.Value = "Подготовка к сделке";
            CommandSetStatusPrepareDocuments.Parameters.Add(ParamStatusDeal);

            SqlParameter ParamIdDealTWO = new SqlParameter();
            ParamIdDealTWO.ParameterName = "@ParamIdDealTWO";
            ParamIdDealTWO.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandSetStatusPrepareDocuments.Parameters.Add(ParamIdDealTWO);

            Conn.Open();
            CommandSetStatusPrepareDocuments.ExecuteNonQuery();
            Conn.Close();

            Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormContractBuyer"));
        }
        protected void ButtonToBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);
        }
    }
}