﻿using System;
using System.Collections.Generic;
using EasyDox;
using System.Data.SqlClient;
using System.Configuration;
using System.Net;
using System.IO;
using System.Web;

namespace WebApplicationProWizard
{
    public partial class Word : System.Web.UI.Page
    {
        protected void Page_PreRender(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

            SqlCommand CommandCountContract = Conn.CreateCommand();
            CommandCountContract.CommandText = "SELECT COUNT (*) FROM dbo.TableContract"
              + " WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandCountContract.Parameters.Add(ParamIdDeal);

            Conn.Open();
            string CountContract = Convert.ToString(CommandCountContract.ExecuteScalar());
            Conn.Close();

            if (CountContract == "0")
            {
                SqlCommand CommandCountPreContract = Conn.CreateCommand();
                CommandCountPreContract.CommandText = "SELECT COUNT (*)" 
                  + " FROM dbo.TablePreContract"
                  + " WHERE IdDeal = @ParamIdDealTWO";

                SqlParameter ParamIdDealTWO = new SqlParameter();
                ParamIdDealTWO.ParameterName = "@ParamIdDealTWO";
                ParamIdDealTWO.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandCountPreContract.Parameters.Add(ParamIdDealTWO);

                Conn.Open();
                string CountPreContract = Convert.ToString(CommandCountPreContract.ExecuteScalar());
                Conn.Close();

                if (CountPreContract == "1")
                {
                    SqlCommand CommandPreContract = Conn.CreateCommand();
                    CommandPreContract.CommandText = "SELECT" +
                        " ContractCity, " +
                        " ContractCost," +
                        " ContractDate," +
                        " ContractDeposit," +

                        " Buyer," +
                        " BuyerBirthDate," +

                        " Seller," +
                        " SellerBirthDate," +

                        " Address," +
                        " Floor," +
                        " Room," +
                        " Area" +

                        " FROM dbo.TablePreContract WHERE IdDeal = @ParamIdDealTHREE";

                    SqlParameter ParamIdDealTHREE = new SqlParameter();
                    ParamIdDealTHREE.ParameterName = "@ParamIdDealTHREE";
                    ParamIdDealTHREE.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                    CommandPreContract.Parameters.Add(ParamIdDealTHREE);

                    SqlDataReader dReader;

                    Conn.Open();
                    dReader = CommandPreContract.ExecuteReader();

                    if (dReader.Read())

                    {
                        string ContractCity = dReader["ContractCity"].ToString();
                        string ContractCost = dReader["ContractCost"].ToString();
                        string ContractDeposit = dReader["ContractDeposit"].ToString();
                        string ContractDate = dReader["ContractDate"].ToString();

                        string Buyer = dReader["Buyer"].ToString();
                        string BuyerBirthDate = dReader["BuyerBirthDate"].ToString();

                        string Seller = dReader["Seller"].ToString();
                        string SellerBirthDate = dReader["SellerBirthDate"].ToString();

                        string ContractAddress = dReader["Address"].ToString();
                        string ContractFloor = dReader["Floor"].ToString();
                        string ContractRoom = dReader["Room"].ToString();
                        string ContractArea = dReader["Area"].ToString();

                        dReader.Close();

                        SqlCommand CommandCreateContract =
                            new SqlCommand("INSERT INTO dbo.TableContract" +
                 " (IdDeal," +

                 " ContractCity," +
                 " ContractDate," +
                 " ContractCost," +
                 " ContractDeposit," +

                 " Buyer," +
                 " BuyerBirthDate," +

                 " Seller," +
                 " SellerBirthDate," +

                 " Address," +
                 " Floor," +
                 " Room," +
                 " Area)" +

                 " Values" +

                 " (@ParamIdDeal4," +

                 " @ParamContractCity," +
                 " @ParamContractDate," +
                 " @ParamContractCost," +
                 " @ParamContractDeposit," +

                 " @ParamBuyer," +
                 " @ParamBuyerBirthDate," +

                 " @ParamSeller," +
                 " @ParamSellerBirthDate," +

                 " @ParamAddress," +
                 " @ParamFloor," +
                 " @ParamRoom," +
                 " @ParamArea)",

                 Conn);

                        SqlParameter ParamIdDeal4 = new SqlParameter();
                        ParamIdDeal4.ParameterName = "@ParamIdDeal4";
                        ParamIdDeal4.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                        CommandCreateContract.Parameters.Add(ParamIdDeal4);

                        SqlParameter ParamContractCity = new SqlParameter();
                        ParamContractCity.ParameterName = "@ParamContractCity";
                        ParamContractCity.Value = ContractCity;
                        CommandCreateContract.Parameters.Add(ParamContractCity);

                        SqlParameter ParamContractDate = new SqlParameter();
                        ParamContractDate.ParameterName = "@ParamContractDate";
                        ParamContractDate.Value = ContractDate;
                        CommandCreateContract.Parameters.Add(ParamContractDate);

                        SqlParameter ParamContractCost = new SqlParameter();
                        ParamContractCost.ParameterName = "@ParamContractCost";
                        ParamContractCost.Value = ContractCost;
                        CommandCreateContract.Parameters.Add(ParamContractCost);

                        SqlParameter ParamContractDeposit = new SqlParameter();
                        ParamContractDeposit.ParameterName = "@ParamContractDeposit";
                        ParamContractDeposit.Value = ContractDeposit;
                        CommandCreateContract.Parameters.Add(ParamContractDeposit);

                        SqlParameter ParamBuyer = new SqlParameter();
                        ParamBuyer.ParameterName = "@ParamBuyer";
                        ParamBuyer.Value = Buyer;
                        CommandCreateContract.Parameters.Add(ParamBuyer);

                        SqlParameter ParamBuyerBirthDate = new SqlParameter();
                        ParamBuyerBirthDate.ParameterName = "@ParamBuyerBirthDate";
                        ParamBuyerBirthDate.Value = BuyerBirthDate;
                        CommandCreateContract.Parameters.Add(ParamBuyerBirthDate);

                        SqlParameter ParamSeller = new SqlParameter();
                        ParamSeller.ParameterName = "@ParamSeller";
                        ParamSeller.Value = Seller;
                        CommandCreateContract.Parameters.Add(ParamSeller);

                        SqlParameter ParamSellerBirthDate = new SqlParameter();
                        ParamSellerBirthDate.ParameterName = "@ParamSellerBirthDate";
                        ParamSellerBirthDate.Value = SellerBirthDate;
                        CommandCreateContract.Parameters.Add(ParamSellerBirthDate);

                        SqlParameter ParamAddress = new SqlParameter();
                        ParamAddress.ParameterName = "@ParamAddress";
                        ParamAddress.Value = ContractAddress;
                        CommandCreateContract.Parameters.Add(ParamAddress);

                        SqlParameter ParamFloor = new SqlParameter();
                        ParamFloor.ParameterName = "@ParamFloor";
                        ParamFloor.Value = ContractFloor;
                        CommandCreateContract.Parameters.Add(ParamFloor);

                        SqlParameter ParamRoom = new SqlParameter();
                        ParamRoom.ParameterName = "@ParamRoom";
                        ParamRoom.Value = ContractRoom;
                        CommandCreateContract.Parameters.Add(ParamRoom);

                        SqlParameter ParamArea = new SqlParameter();
                        ParamArea.ParameterName = "@ParamArea";
                        ParamArea.Value = ContractArea;
                        CommandCreateContract.Parameters.Add(ParamArea);

                        CommandCreateContract.ExecuteNonQuery();
                        Conn.Close();

                        TextBoxContractAddress.Text = ContractAddress;
                        TextBoxContractArea.Text = ContractArea;
                        TextBoxContractFloor.Text = ContractFloor;
                        TextBoxContractRoom.Text = ContractRoom;

                        TextBoxContractBuyer.Text = Buyer;
                        TextBoxContractBuyerBirthDate.Text = BuyerBirthDate;

                        TextBoxContractSeller.Text = Seller;
                        TextBoxContractSellerBirthDate.Text = SellerBirthDate;

                        TextBoxContractCity.Text = ContractCity;
                        TextBoxContractCost.Text = ContractCost;
                        TextBoxContractDate.Text = ContractDate;
                        TextBoxContractDeposit.Text = ContractDeposit;

                    }
                }
            }

            if (CountContract == "1")
            {
                SqlCommand CommandContract = Conn.CreateCommand();
                CommandContract.CommandText = "SELECT" +

                    " ContractCity, " +
                    " ContractDate," +
                    " ContractCost," +
                    " ContractDeposit," +

                    " Buyer," +
                    " BuyerBirthDate," +
                    " BuyerDocumentSerialNumber," +
                    " BuyerDocumentIssue," +
                    " BuyerDocumentDate," +
                    " BuyerAddressRegistration," +

                    " Seller," +
                    " SellerBirthDate," +
                    " SellerDocumentSerialNumber," +
                    " SellerDocumentIssue," +
                    " SellerDocumentDate," +
                    " SellerAddressRegistration," +

                    " CadastralNumber," +
                    " DocumentFoundationType," +
                    " DocumentFoundationDate," +
                    " SertificateOfOwnerSerialNumber," +
                    " SertificateOfOwnerShipDate," +
                    " SertificateOfOwnerRegistrationNumber," +

                    " Address," +
                    " Floor," +
                    " Room," +
                    " Area" +

                    " FROM dbo.TableContract WHERE IdDeal = @ParamIdDeal5";

                SqlParameter ParamIdDeal5 = new SqlParameter();
                ParamIdDeal5.ParameterName = "@ParamIdDeal5";
                ParamIdDeal5.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandContract.Parameters.Add(ParamIdDeal5);

                SqlDataReader dReader;

                Conn.Open();
                dReader = CommandContract.ExecuteReader();

                if (dReader.Read())

                {
                    TextBoxContractCity.Text = dReader["ContractCity"].ToString();
                    TextBoxContractDate.Text = dReader["ContractDate"].ToString();
                    TextBoxContractCost.Text = dReader["ContractCost"].ToString();
                    TextBoxContractDeposit.Text = dReader["ContractDeposit"].ToString();

                    TextBoxContractBuyer.Text = dReader["Buyer"].ToString();
                    TextBoxContractBuyerBirthDate.Text = dReader["BuyerBirthDate"].ToString();
                    TextBoxContractBuyerDocumentSerialNumber.Text = dReader["BuyerDocumentSerialNumber"].ToString();
                    TextBoxContractBuyerDocumentIssue.Text = dReader["BuyerDocumentIssue"].ToString();
                    TextBoxContractBuyerDocumentDate.Text = dReader["BuyerDocumentDate"].ToString();
                    TextBoxContracBuyerAddressRegistration.Text = dReader["BuyerAddressRegistration"].ToString();

                    TextBoxContractSeller.Text = dReader["Seller"].ToString();
                    TextBoxContractSellerBirthDate.Text = dReader["SellerBirthDate"].ToString();
                    TextBoxContractSellerDocumentSerialNumber.Text = dReader["SellerDocumentSerialNumber"].ToString();
                    TextBoxContractSellerDocumentIssue.Text = dReader["SellerDocumentIssue"].ToString();
                    TextBoxContractSellerDocumentDate.Text = dReader["SellerDocumentDate"].ToString();
                    TextBoxContracSellerAddressRegistration.Text = dReader["SellerAddressRegistration"].ToString();

                    TextBoxContractCadastralNumber.Text = dReader["CadastralNumber"].ToString();
                    DropDownListContractDocumentFoundationType.Text = dReader["DocumentFoundationType"].ToString();
                    TextBoxContractDocumentFoundationDate.Text = dReader["DocumentFoundationDate"].ToString();
                    TextBoxContractSertificateOfOwnerSerialNumber.Text = dReader["SertificateOfOwnerSerialNumber"].ToString();
                    TextBoxContractSertificateOfOwnerShipDate.Text = dReader["SertificateOfOwnerShipDate"].ToString();
                    TextBoxContractSertificateOfOwnerRegistrationNumber.Text = dReader["SertificateOfOwnerRegistrationNumber"].ToString();

                    TextBoxContractAddress.Text = dReader["Address"].ToString();
                    TextBoxContractFloor.Text = dReader["Floor"].ToString();
                    TextBoxContractRoom.Text = dReader["Room"].ToString();
                    TextBoxContractArea.Text = dReader["Area"].ToString();

                    dReader.Close();
                }

                Conn.Close();
            }
        }

        protected void ButtonContract_Click(object sender, EventArgs e)
        {
            String IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

            SqlCommand CommandCountContract = Conn.CreateCommand();
            CommandCountContract.CommandText = "SELECT COUNT (*) FROM dbo.TableContract"
              + " WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = IdDeal;
            CommandCountContract.Parameters.Add(ParamIdDeal);

            Conn.Open();
            string CountContract = Convert.ToString(CommandCountContract.ExecuteScalar());
            Conn.Close();

            if (CountContract == "0")
            {
                var FieldValues = new Dictionary<string, string>
            {
                { "Город заключения договора", TextBoxContractCity.Text },
                { "Дата заключения договора", TextBoxContractDate.Text },
                { "Стоимость квартиры", TextBoxContractCost.Text},
                { "Депозит", TextBoxContractDeposit.Text},

                { "ФИО продавца", TextBoxContractSeller.Text },
                { "Дата рождения продавца", TextBoxContractSellerBirthDate.Text },
                { "Серия номер паспорта продавца", TextBoxContractSellerDocumentSerialNumber.Text },
                { "Кем и где выдан паспорт продавца", TextBoxContractSellerDocumentIssue.Text },
                { "Дата выдачи паспорта продавца", TextBoxContractSellerDocumentDate.Text },
                { "Адрес регистрации продавца", TextBoxContracSellerAddressRegistration.Text },

                { "ФИО покупателя", TextBoxContractBuyer.Text },
                { "Дата рождения покупателя", TextBoxContractBuyerBirthDate.Text },
                { "Серия номер паспорта покупателя", TextBoxContractBuyerDocumentSerialNumber.Text },
                { "Кем и где выдан паспорт покупателя", TextBoxContractBuyerDocumentIssue.Text },
                { "Дата выдачи паспорта покупателя", TextBoxContractBuyerDocumentDate.Text },
                { "Адрес регистрации покупателя", TextBoxContracBuyerAddressRegistration.Text },

                { "Количество комнат прописью", TextBoxContractRoom.Text },
                { "Общая площадь", TextBoxContractArea.Text },
                { "Этаж", TextBoxContractFloor.Text },
                { "Адрес квартиры", TextBoxContractAddress.Text },
                { "Кадастровый номер", TextBoxContractCadastralNumber.Text },

                { "Документ основание", DropDownListContractDocumentFoundationType.SelectedValue },
                { "Дата документа основания", TextBoxContractDocumentFoundationDate.Text },

                { "Свидетельство серия номер", TextBoxContractSertificateOfOwnerSerialNumber.Text },
                { "Свидетельство дата", TextBoxContractSertificateOfOwnerShipDate.Text },
                { "Номер записи", TextBoxContractSertificateOfOwnerRegistrationNumber.Text }
            };

                var Engine = new Engine();

                Engine.Merge(Server.MapPath("Content/Contract.docx"),
                FieldValues,
                Server.MapPath("DealDocuments/"
              + Request.Cookies["Deal"]["IdDeal"].ToString() +
                "/Contract.docx"));

                SqlCommand CommandCreateContract = new SqlCommand("INSERT INTO dbo.TableContract" +
                    " (IdDeal," +

                    " ContractCity," +
                    " ContractDate," +
                    " ContractCost," +
                    " ContractDeposit," +

                    " Seller," +
                    " SellerBirthDate," +
                    " SellerDocumentSerialNumber," +
                    " SellerDocumentIssue," +
                    " SellerDocumentDate," +
                    " SellerAddressRegistration," +

                    " Buyer," +
                    " BuyerBirthDate," +
                    " BuyerDocumentSerialNumber," +
                    " BuyerDocumentIssue," +
                    " BuyerDocumentDate," +
                    " BuyerAddressRegistration," +

                    " Room," +
                    " Area," +
                    " Floor," +
                    " Address," +

                    " CadastralNumber," +
                    " DocumentFoundationType," +
                    " DocumentFoundationDate," +

                    " SertificateOfOwnerSerialNumber," +
                    " SertificateOfOwnerRegistrationNumber," +
                    " SertificateOfOwnerShipDate)" +

                    " VALUES" +

                    " (@ParamIdDealTWO," +

                    " @ParamContractCity," +
                    " @ParamContractDate," +
                    " @ParamContractCost," +
                    " @ParamContractDeposit," +

                    " @ParamSeller," +
                    " @ParamSellerBirthDate," +
                    " @ParamSellerDocumentSerialNumber," +
                    " @ParamSellerDocumentIssue," +
                    " @ParamSellerDocumentDate," +
                    " @ParamSellerAddressRegistration," +

                    " @ParamBuyer," +
                    " @ParamBuyerBirthDate," +
                    " @ParamBuyerDocumentSerialNumber," +
                    " @ParamBuyerDocumentIssue," +
                    " @ParamBuyerDocumentDate," +
                    " @ParamBuyerAddressRegistration," +

                    " @ParamRoom," +
                    " @ParamArea," +
                    " @ParamFloor," +
                    " @ParamAddress," +

                    " @ParamCadastralNumber," +
                    " @ParamDocumentFoundationType," +
                    " @ParamDocumentFoundationDate," +
                    " @ParamSertificateOfOwnerSerialNumber," +
                    " @ParamSertificateOfOwnerRegistrationNumber," +
                    " @ParamSertificateOfOwnerShipDate)",

                    Conn);

                SqlParameter ParamDocumentFoundationType = new SqlParameter();
                ParamDocumentFoundationType.ParameterName = "@ParamDocumentFoundationType";
                ParamDocumentFoundationType.Value = DropDownListContractDocumentFoundationType.SelectedValue;
                CommandCreateContract.Parameters.Add(ParamDocumentFoundationType);

                SqlParameter ParamDocumentFoundationDate = new SqlParameter();
                ParamDocumentFoundationDate.ParameterName = "@ParamDocumentFoundationDate";
                ParamDocumentFoundationDate.Value = TextBoxContractDocumentFoundationDate.Text;
                CommandCreateContract.Parameters.Add(ParamDocumentFoundationDate);

                SqlParameter ParamContractDeposit = new SqlParameter();
                ParamContractDeposit.ParameterName = "@ParamContractDeposit";
                ParamContractDeposit.Value = TextBoxContractDeposit.Text;
                CommandCreateContract.Parameters.Add(ParamContractDeposit);

                SqlParameter ParamSertificateOfOwnerSerialNumber = new SqlParameter();
                ParamSertificateOfOwnerSerialNumber.ParameterName = "@ParamSertificateOfOwnerSerialNumber";
                ParamSertificateOfOwnerSerialNumber.Value = TextBoxContractSertificateOfOwnerSerialNumber.Text;
                CommandCreateContract.Parameters.Add(ParamSertificateOfOwnerSerialNumber);

                SqlParameter ParamSertificateOfOwnerShipDate = new SqlParameter();
                ParamSertificateOfOwnerShipDate.ParameterName = "@ParamSertificateOfOwnerShipDate";
                ParamSertificateOfOwnerShipDate.Value = TextBoxContractSertificateOfOwnerShipDate.Text;
                CommandCreateContract.Parameters.Add(ParamSertificateOfOwnerShipDate);

                SqlParameter ParamRoom = new SqlParameter();
                ParamRoom.ParameterName = "@ParamRoom";
                ParamRoom.Value = TextBoxContractRoom.Text;
                CommandCreateContract.Parameters.Add(ParamRoom);

                SqlParameter ParamArea = new SqlParameter();
                ParamArea.ParameterName = "@ParamArea";
                ParamArea.Value = TextBoxContractArea.Text;
                CommandCreateContract.Parameters.Add(ParamArea);

                SqlParameter ParamFloor = new SqlParameter();
                ParamFloor.ParameterName = "@ParamFloor";
                ParamFloor.Value = TextBoxContractFloor.Text;
                CommandCreateContract.Parameters.Add(ParamFloor);

                SqlParameter ParamAddress = new SqlParameter();
                ParamAddress.ParameterName = "@ParamAddress";
                ParamAddress.Value = TextBoxContractAddress.Text;
                CommandCreateContract.Parameters.Add(ParamAddress);

                SqlParameter ParamCadastralNumber = new SqlParameter();
                ParamCadastralNumber.ParameterName = "@ParamCadastralNumber";
                ParamCadastralNumber.Value = TextBoxContractCadastralNumber.Text;
                CommandCreateContract.Parameters.Add(ParamCadastralNumber);

                SqlParameter ParamIdDealTWOTWO = new SqlParameter();
                ParamIdDealTWOTWO.ParameterName = "@ParamIdDealTWO";
                ParamIdDealTWOTWO.Value = IdDeal;
                CommandCreateContract.Parameters.Add(ParamIdDealTWOTWO);

                SqlParameter ParamContractCity = new SqlParameter();
                ParamContractCity.ParameterName = "@ParamContractCity";
                ParamContractCity.Value = TextBoxContractCity.Text;
                CommandCreateContract.Parameters.Add(ParamContractCity);

                SqlParameter ParamContractDate = new SqlParameter();
                ParamContractDate.ParameterName = "@ParamContractDate";
                ParamContractDate.Value = TextBoxContractDate.Text;
                CommandCreateContract.Parameters.Add(ParamContractDate);

                SqlParameter ParamContractCost = new SqlParameter();
                ParamContractCost.ParameterName = "@ParamContractCost";
                ParamContractCost.Value = TextBoxContractCost.Text;
                CommandCreateContract.Parameters.Add(ParamContractCost);

                SqlParameter ParamSeller = new SqlParameter();
                ParamSeller.ParameterName = "@ParamSeller";
                ParamSeller.Value = TextBoxContractSeller.Text;
                CommandCreateContract.Parameters.Add(ParamSeller);

                SqlParameter ParamSellerBirthDate = new SqlParameter();
                ParamSellerBirthDate.ParameterName = "@ParamSellerBirthDate";
                ParamSellerBirthDate.Value = TextBoxContractSellerBirthDate.Text;
                CommandCreateContract.Parameters.Add(ParamSellerBirthDate);

                SqlParameter ParamSellerDocumentSerialNumber = new SqlParameter();
                ParamSellerDocumentSerialNumber.ParameterName = "@ParamSellerDocumentSerialNumber";
                ParamSellerDocumentSerialNumber.Value = TextBoxContractSellerDocumentSerialNumber.Text;
                CommandCreateContract.Parameters.Add(ParamSellerDocumentSerialNumber);

                SqlParameter ParamSellerDocumentIssue = new SqlParameter();
                ParamSellerDocumentIssue.ParameterName = "@ParamSellerDocumentIssue";
                ParamSellerDocumentIssue.Value = TextBoxContractSellerDocumentIssue.Text;
                CommandCreateContract.Parameters.Add(ParamSellerDocumentIssue);

                SqlParameter ParamSellerDocumentDate = new SqlParameter();
                ParamSellerDocumentDate.ParameterName = "@ParamSellerDocumentDate";
                ParamSellerDocumentDate.Value = TextBoxContractSellerDocumentDate.Text;
                CommandCreateContract.Parameters.Add(ParamSellerDocumentDate);

                SqlParameter ParamSellerAddressRegistration = new SqlParameter();
                ParamSellerAddressRegistration.ParameterName = "@ParamSellerAddressRegistration";
                ParamSellerAddressRegistration.Value = TextBoxContracSellerAddressRegistration.Text;
                CommandCreateContract.Parameters.Add(ParamSellerAddressRegistration);

                SqlParameter ParamBuyer = new SqlParameter();
                ParamBuyer.ParameterName = "@ParamBuyer";
                ParamBuyer.Value = TextBoxContractBuyer.Text;
                CommandCreateContract.Parameters.Add(ParamBuyer);

                SqlParameter ParamBuyerBirthDate = new SqlParameter();
                ParamBuyerBirthDate.ParameterName = "@ParamBuyerBirthDate";
                ParamBuyerBirthDate.Value = TextBoxContractBuyerBirthDate.Text;
                CommandCreateContract.Parameters.Add(ParamBuyerBirthDate);

                SqlParameter ParamBuyerDocumentSerialNumber = new SqlParameter();
                ParamBuyerDocumentSerialNumber.ParameterName = "@ParamBuyerDocumentSerialNumber";
                ParamBuyerDocumentSerialNumber.Value = TextBoxContractBuyerDocumentSerialNumber.Text;
                CommandCreateContract.Parameters.Add(ParamBuyerDocumentSerialNumber);

                SqlParameter ParamBuyerDocumentIssue = new SqlParameter();
                ParamBuyerDocumentIssue.ParameterName = "@ParamBuyerDocumentIssue";
                ParamBuyerDocumentIssue.Value = TextBoxContractBuyerDocumentIssue.Text;
                CommandCreateContract.Parameters.Add(ParamBuyerDocumentIssue);

                SqlParameter ParamBuyerDocumentDate = new SqlParameter();
                ParamBuyerDocumentDate.ParameterName = "@ParamBuyerDocumentDate";
                ParamBuyerDocumentDate.Value = TextBoxContractBuyerDocumentDate.Text;
                CommandCreateContract.Parameters.Add(ParamBuyerDocumentDate);

                SqlParameter ParamBuyerAddressRegistration = new SqlParameter();
                ParamBuyerAddressRegistration.ParameterName = "@ParamBuyerAddressRegistration";
                ParamBuyerAddressRegistration.Value = TextBoxContracBuyerAddressRegistration.Text;
                CommandCreateContract.Parameters.Add(ParamBuyerAddressRegistration);

                SqlParameter ParamSertificateOfOwnerRegistrationNumber = new SqlParameter();
                ParamSertificateOfOwnerRegistrationNumber.ParameterName = "@ParamSertificateOfOwnerRegistrationNumber";
                ParamSertificateOfOwnerRegistrationNumber.Value = TextBoxContractSertificateOfOwnerRegistrationNumber.Text;
                CommandCreateContract.Parameters.Add(ParamSertificateOfOwnerRegistrationNumber);

                Conn.Open();
                CommandCreateContract.ExecuteNonQuery();
                Conn.Close();
            }

            else
            {
                var FieldValues = new Dictionary<string, string>
            {
                { "Город заключения договора", TextBoxContractCity.Text },
                { "Дата заключения договора", TextBoxContractDate.Text },
                { "Стоимость квартиры", TextBoxContractCost.Text},
                { "Депозит", TextBoxContractDeposit.Text},

                { "ФИО продавца", TextBoxContractSeller.Text },
                { "Дата рождения продавца", TextBoxContractSellerBirthDate.Text },
                { "Серия номер паспорта продавца", TextBoxContractSellerDocumentSerialNumber.Text },
                { "Кем и где выдан паспорт продавца", TextBoxContractSellerDocumentIssue.Text },
                { "Дата выдачи паспорта продавца", TextBoxContractSellerDocumentDate.Text },
                { "Адрес регистрации продавца", TextBoxContracSellerAddressRegistration.Text },

                { "ФИО покупателя", TextBoxContractBuyer.Text },
                { "Дата рождения покупателя", TextBoxContractBuyerBirthDate.Text },
                { "Серия номер паспорта покупателя", TextBoxContractBuyerDocumentSerialNumber.Text },
                { "Кем и где выдан паспорт покупателя", TextBoxContractBuyerDocumentIssue.Text },
                { "Дата выдачи паспорта покупателя", TextBoxContractBuyerDocumentDate.Text },
                { "Адрес регистрации покупателя", TextBoxContracBuyerAddressRegistration.Text },

                { "Количество комнат прописью", TextBoxContractRoom.Text },
                { "Общая площадь", TextBoxContractArea.Text },
                { "Этаж", TextBoxContractFloor.Text },
                { "Адрес квартиры", TextBoxContractAddress.Text },
                { "Кадастровый номер", TextBoxContractCadastralNumber.Text },

                { "Документ основание", DropDownListContractDocumentFoundationType.SelectedValue },
                { "Дата документа основания", TextBoxContractDocumentFoundationDate.Text },
                { "Свидетельство серия номер", TextBoxContractSertificateOfOwnerSerialNumber.Text },
                { "Номер записи", TextBoxContractSertificateOfOwnerRegistrationNumber.Text },
                { "Свидетельство дата", TextBoxContractSertificateOfOwnerShipDate.Text },
            };

                var Engine = new Engine();

                Engine.Merge(Server.MapPath("Content/Contract.docx"),
                FieldValues,
                Server.MapPath("DealDocuments/"
              + Request.Cookies["Deal"]["IdDeal"].ToString() +
               "/Contract.docx"));

                SqlCommand CommandUpdateContract = new SqlCommand("UPDATE dbo.TableContract" +
                    " SET ContractCity = @ParamContractCity," +
                    " ContractDate = @ParamContractDate," +
                    " ContractCost = @ParamContractCost," +
                    " ContractDeposit = @ParamContractDeposit," +

                    " Seller = @ParamSeller," +
                    " SellerBirthDate = @ParamSellerBirthDate," +
                    " SellerDocumentSerialNumber = @ParamSellerDocumentSerialNumber," +
                    " SellerDocumentIssue = @ParamSellerDocumentIssue," +
                    " SellerDocumentDate = @ParamSellerDocumentDate," +
                    " SellerAddressRegistration = @ParamSellerAddressRegistration," +

                    " Buyer = @ParamBuyer," +
                    " BuyerBirthDate = @ParamBuyerBirthDate," +
                    " BuyerDocumentSerialNumber = @ParamBuyerDocumentSerialNumber," +
                    " BuyerDocumentIssue = @ParamBuyerDocumentIssue," +
                    " BuyerDocumentDate = @ParamBuyerDocumentDate," +
                    " BuyerAddressRegistration = @ParamBuyerAddressRegistration," +

                    " Room = @ParamRoom," +
                    " Area = @ParamArea," +
                    " Floor = @ParamFloor," +
                    " Address = @ParamAddress," +

                    " CadastralNumber = @ParamCadastralNumber," +
                    " DocumentFoundationType = @ParamDocumentFoundationType," +
                    " DocumentFoundationDate = @ParamDocumentFoundationDate," +

                    " SertificateOfOwnerSerialNumber = @ParamSertificateOfOwnerSerialNumber," +
                    " SertificateOfOwnerRegistrationNumber = @ParamSertificateOfOwnerRegistrationNumber," +
                    " SertificateOfOwnerShipDate = @ParamSertificateOfOwnerShipDate" +

                    " WHERE IdDeal = @ParamIdDealTWO", Conn);

                SqlParameter ParamDocumentFoundationType = new SqlParameter();
                ParamDocumentFoundationType.ParameterName = "@ParamDocumentFoundationType";
                ParamDocumentFoundationType.Value = DropDownListContractDocumentFoundationType.SelectedValue;
                CommandUpdateContract.Parameters.Add(ParamDocumentFoundationType);

                SqlParameter ParamContractDeposit = new SqlParameter();
                ParamContractDeposit.ParameterName = "@ParamContractDeposit";
                ParamContractDeposit.Value = TextBoxContractDeposit.Text;
                CommandUpdateContract.Parameters.Add(ParamContractDeposit);

                SqlParameter ParamDocumentFoundationDate = new SqlParameter();
                ParamDocumentFoundationDate.ParameterName = "@ParamDocumentFoundationDate";
                ParamDocumentFoundationDate.Value = TextBoxContractDocumentFoundationDate.Text;
                CommandUpdateContract.Parameters.Add(ParamDocumentFoundationDate);

                SqlParameter ParamSertificateOfOwnerSerialNumber = new SqlParameter();
                ParamSertificateOfOwnerSerialNumber.ParameterName = "@ParamSertificateOfOwnerSerialNumber";
                ParamSertificateOfOwnerSerialNumber.Value = TextBoxContractSertificateOfOwnerSerialNumber.Text;
                CommandUpdateContract.Parameters.Add(ParamSertificateOfOwnerSerialNumber);

                SqlParameter ParamSertificateOfOwnerShipDate = new SqlParameter();
                ParamSertificateOfOwnerShipDate.ParameterName = "@ParamSertificateOfOwnerShipDate";
                ParamSertificateOfOwnerShipDate.Value = TextBoxContractSertificateOfOwnerShipDate.Text;
                CommandUpdateContract.Parameters.Add(ParamSertificateOfOwnerShipDate);

                SqlParameter ParamRoom = new SqlParameter();
                ParamRoom.ParameterName = "@ParamRoom";
                ParamRoom.Value = TextBoxContractRoom.Text;
                CommandUpdateContract.Parameters.Add(ParamRoom);

                SqlParameter ParamArea = new SqlParameter();
                ParamArea.ParameterName = "@ParamArea";
                ParamArea.Value = TextBoxContractArea.Text;
                CommandUpdateContract.Parameters.Add(ParamArea);

                SqlParameter ParamFloor = new SqlParameter();
                ParamFloor.ParameterName = "@ParamFloor";
                ParamFloor.Value = TextBoxContractFloor.Text;
                CommandUpdateContract.Parameters.Add(ParamFloor);

                SqlParameter ParamAddress = new SqlParameter();
                ParamAddress.ParameterName = "@ParamAddress";
                ParamAddress.Value = TextBoxContractAddress.Text;
                CommandUpdateContract.Parameters.Add(ParamAddress);

                SqlParameter ParamCadastralNumber = new SqlParameter();
                ParamCadastralNumber.ParameterName = "@ParamCadastralNumber";
                ParamCadastralNumber.Value = TextBoxContractCadastralNumber.Text;
                CommandUpdateContract.Parameters.Add(ParamCadastralNumber);

                SqlParameter ParamIdDealTWOTWOTWO = new SqlParameter();
                ParamIdDealTWOTWOTWO.ParameterName = "@ParamIdDealTWO";
                ParamIdDealTWOTWOTWO.Value = IdDeal;
                CommandUpdateContract.Parameters.Add(ParamIdDealTWOTWOTWO);

                SqlParameter ParamContractCity = new SqlParameter();
                ParamContractCity.ParameterName = "@ParamContractCity";
                ParamContractCity.Value = TextBoxContractCity.Text;
                CommandUpdateContract.Parameters.Add(ParamContractCity);

                SqlParameter ParamContractDate = new SqlParameter();
                ParamContractDate.ParameterName = "@ParamContractDate";
                ParamContractDate.Value = TextBoxContractDate.Text;
                CommandUpdateContract.Parameters.Add(ParamContractDate);

                SqlParameter ParamContractCost = new SqlParameter();
                ParamContractCost.ParameterName = "@ParamContractCost";
                ParamContractCost.Value = TextBoxContractCost.Text;
                CommandUpdateContract.Parameters.Add(ParamContractCost);

                SqlParameter ParamSeller = new SqlParameter();
                ParamSeller.ParameterName = "@ParamSeller";
                ParamSeller.Value = TextBoxContractSeller.Text;
                CommandUpdateContract.Parameters.Add(ParamSeller);

                SqlParameter ParamSellerBirthDate = new SqlParameter();
                ParamSellerBirthDate.ParameterName = "@ParamSellerBirthDate";
                ParamSellerBirthDate.Value = TextBoxContractSellerBirthDate.Text;
                CommandUpdateContract.Parameters.Add(ParamSellerBirthDate);

                SqlParameter ParamSellerDocumentSerialNumber = new SqlParameter();
                ParamSellerDocumentSerialNumber.ParameterName = "@ParamSellerDocumentSerialNumber";
                ParamSellerDocumentSerialNumber.Value = TextBoxContractSellerDocumentSerialNumber.Text;
                CommandUpdateContract.Parameters.Add(ParamSellerDocumentSerialNumber);

                SqlParameter ParamSellerDocumentIssue = new SqlParameter();
                ParamSellerDocumentIssue.ParameterName = "@ParamSellerDocumentIssue";
                ParamSellerDocumentIssue.Value = TextBoxContractSellerDocumentIssue.Text;
                CommandUpdateContract.Parameters.Add(ParamSellerDocumentIssue);

                SqlParameter ParamSellerDocumentDate = new SqlParameter();
                ParamSellerDocumentDate.ParameterName = "@ParamSellerDocumentDate";
                ParamSellerDocumentDate.Value = TextBoxContractSellerDocumentDate.Text;
                CommandUpdateContract.Parameters.Add(ParamSellerDocumentDate);

                SqlParameter ParamSellerAddressRegistration = new SqlParameter();
                ParamSellerAddressRegistration.ParameterName = "@ParamSellerAddressRegistration";
                ParamSellerAddressRegistration.Value = TextBoxContracSellerAddressRegistration.Text;
                CommandUpdateContract.Parameters.Add(ParamSellerAddressRegistration);

                SqlParameter ParamBuyer = new SqlParameter();
                ParamBuyer.ParameterName = "@ParamBuyer";
                ParamBuyer.Value = TextBoxContractBuyer.Text;
                CommandUpdateContract.Parameters.Add(ParamBuyer);

                SqlParameter ParamBuyerBirthDate = new SqlParameter();
                ParamBuyerBirthDate.ParameterName = "@ParamBuyerBirthDate";
                ParamBuyerBirthDate.Value = TextBoxContractBuyerBirthDate.Text;
                CommandUpdateContract.Parameters.Add(ParamBuyerBirthDate);

                SqlParameter ParamBuyerDocumentSerialNumber = new SqlParameter();
                ParamBuyerDocumentSerialNumber.ParameterName = "@ParamBuyerDocumentSerialNumber";
                ParamBuyerDocumentSerialNumber.Value = TextBoxContractBuyerDocumentSerialNumber.Text;
                CommandUpdateContract.Parameters.Add(ParamBuyerDocumentSerialNumber);

                SqlParameter ParamBuyerDocumentIssue = new SqlParameter();
                ParamBuyerDocumentIssue.ParameterName = "@ParamBuyerDocumentIssue";
                ParamBuyerDocumentIssue.Value = TextBoxContractBuyerDocumentIssue.Text;
                CommandUpdateContract.Parameters.Add(ParamBuyerDocumentIssue);

                SqlParameter ParamBuyerDocumentDate = new SqlParameter();
                ParamBuyerDocumentDate.ParameterName = "@ParamBuyerDocumentDate";
                ParamBuyerDocumentDate.Value = TextBoxContractBuyerDocumentDate.Text;
                CommandUpdateContract.Parameters.Add(ParamBuyerDocumentDate);

                SqlParameter ParamBuyerAddressRegistration = new SqlParameter();
                ParamBuyerAddressRegistration.ParameterName = "@ParamBuyerAddressRegistration";
                ParamBuyerAddressRegistration.Value = TextBoxContracBuyerAddressRegistration.Text;
                CommandUpdateContract.Parameters.Add(ParamBuyerAddressRegistration);

                SqlParameter ParamSertificateOfOwnerRegistrationNumber = new SqlParameter();
                ParamSertificateOfOwnerRegistrationNumber.ParameterName = "@ParamSertificateOfOwnerRegistrationNumber";
                ParamSertificateOfOwnerRegistrationNumber.Value = TextBoxContractSertificateOfOwnerRegistrationNumber.Text;
                CommandUpdateContract.Parameters.Add(ParamSertificateOfOwnerRegistrationNumber);

                Conn.Open();
                CommandUpdateContract.ExecuteNonQuery();
                Conn.Close();
            }

            string FileName = "Contract.docx";
            string Ftp = "ftp://waws-prod-db3-051.ftp.azurewebsites.windows.net/site/wwwroot/DealDocuments/" + IdDeal + "/";

            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(Ftp + FileName);
            request.Method = WebRequestMethods.Ftp.DownloadFile;

            request.Credentials = new NetworkCredential(@"ProWizard\ProWizard", "100Million");
            request.UsePassive = true;
            request.UseBinary = true;
            request.EnableSsl = false;

            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            using (MemoryStream stream = new MemoryStream())
            {
                response.GetResponseStream().CopyTo(stream);
                Response.AddHeader("content-disposition", "attachment;filename=" + FileName);
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
        }

        protected void ButtonSave_Click(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

            SqlCommand CommandCountContract = Conn.CreateCommand();
            CommandCountContract.CommandText = "SELECT Count (*) FROM dbo.TableContract"
              + " WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandCountContract.Parameters.Add(ParamIdDeal);

            Conn.Open();
            string CountContract = Convert.ToString(CommandCountContract.ExecuteScalar());
            Conn.Close();

            if (CountContract == "0")
            {

                SqlCommand CommandCreateContract = new SqlCommand("Insert into dbo.TableContract" +
                    " (IdDeal," +

                    " ContractCity," +
                    " ContractDate," +
                    " ContractCost," +
                    " ContractDeposit," +

                    " Seller," +
                    " SellerBirthDate," +
                    " SellerDocumentSerialNumber," +
                    " SellerDocumentIssue," +
                    " SellerDocumentDate," +
                    " SellerAddressRegistration," +

                    " Buyer," +
                    " BuyerBirthDate," +
                    " BuyerDocumentSerialNumber," +
                    " BuyerDocumentIssue," +
                    " BuyerDocumentDate," +
                    " BuyerAddressRegistration," +

                    " Room," +
                    " Area," +
                    " Floor," +
                    " Address," +

                    " CadastralNumber," +
                    " DocumentFoundationType," +
                    " DocumentFoundationDate," +

                    " SertificateOfOwnerSerialNumber," +
                    " SertificateOfOwnerRegistrationNumber," +
                    " SertificateOfOwnerShipDate)" +

                    " Values" +

                    " (@ParamIdDealTWO," +

                    " @ParamContractCity," +
                    " @ParamContractDate," +
                    " @ParamContractCost," +
                    " @ParamContractDeposit," +

                    " @ParamSeller," +
                    " @ParamSellerBirthDate," +
                    " @ParamSellerDocumentSerialNumber," +
                    " @ParamSellerDocumentIssue," +
                    " @ParamSellerDocumentDate," +
                    " @ParamSellerAddressRegistration," +

                    " @ParamBuyer," +
                    " @ParamBuyerBirthDate," +
                    " @ParamBuyerDocumentSerialNumber," +
                    " @ParamBuyerDocumentIssue," +
                    " @ParamBuyerDocumentDate," +
                    " @ParamBuyerAddressRegistration," +

                    " @ParamRoom," +
                    " @ParamArea," +
                    " @ParamFloor," +
                    " @ParamAddress," +

                    " @ParamCadastralNumber," +
                    " @ParamDocumentFoundationType," +
                    " @ParamDocumentFoundationDate," +
                    " @ParamSertificateOfOwnerSerialNumber," +
                    " @ParamSertificateOfOwnerRegistrationNumber," +
                    " @ParamSertificateOfOwnerShipDate)",

                    Conn);

                SqlParameter ParamDocumentFoundationType = new SqlParameter();
                ParamDocumentFoundationType.ParameterName = "@ParamDocumentFoundationType";
                ParamDocumentFoundationType.Value = DropDownListContractDocumentFoundationType.SelectedValue;
                CommandCreateContract.Parameters.Add(ParamDocumentFoundationType);

                SqlParameter ParamDocumentFoundationDate = new SqlParameter();
                ParamDocumentFoundationDate.ParameterName = "@ParamDocumentFoundationDate";
                ParamDocumentFoundationDate.Value = TextBoxContractDocumentFoundationDate.Text;
                CommandCreateContract.Parameters.Add(ParamDocumentFoundationDate);

                SqlParameter ParamContractDeposit = new SqlParameter();
                ParamContractDeposit.ParameterName = "@ParamContractDeposit";
                ParamContractDeposit.Value = TextBoxContractDeposit.Text;
                CommandCreateContract.Parameters.Add(ParamContractDeposit);

                SqlParameter ParamSertificateOfOwnerSerialNumber = new SqlParameter();
                ParamSertificateOfOwnerSerialNumber.ParameterName = "@ParamSertificateOfOwnerSerialNumber";
                ParamSertificateOfOwnerSerialNumber.Value = TextBoxContractSertificateOfOwnerSerialNumber.Text;
                CommandCreateContract.Parameters.Add(ParamSertificateOfOwnerSerialNumber);

                SqlParameter ParamSertificateOfOwnerShipDate = new SqlParameter();
                ParamSertificateOfOwnerShipDate.ParameterName = "@ParamSertificateOfOwnerShipDate";
                ParamSertificateOfOwnerShipDate.Value = TextBoxContractSertificateOfOwnerShipDate.Text;
                CommandCreateContract.Parameters.Add(ParamSertificateOfOwnerShipDate);

                SqlParameter ParamRoom = new SqlParameter();
                ParamRoom.ParameterName = "@ParamRoom";
                ParamRoom.Value = TextBoxContractRoom.Text;
                CommandCreateContract.Parameters.Add(ParamRoom);

                SqlParameter ParamArea = new SqlParameter();
                ParamArea.ParameterName = "@ParamArea";
                ParamArea.Value = TextBoxContractArea.Text;
                CommandCreateContract.Parameters.Add(ParamArea);

                SqlParameter ParamFloor = new SqlParameter();
                ParamFloor.ParameterName = "@ParamFloor";
                ParamFloor.Value = TextBoxContractFloor.Text;
                CommandCreateContract.Parameters.Add(ParamFloor);

                SqlParameter ParamAddress = new SqlParameter();
                ParamAddress.ParameterName = "@ParamAddress";
                ParamAddress.Value = TextBoxContractAddress.Text;
                CommandCreateContract.Parameters.Add(ParamAddress);

                SqlParameter ParamCadastralNumber = new SqlParameter();
                ParamCadastralNumber.ParameterName = "@ParamCadastralNumber";
                ParamCadastralNumber.Value = TextBoxContractCadastralNumber.Text;
                CommandCreateContract.Parameters.Add(ParamCadastralNumber);

                SqlParameter ParamIdDealTWO = new SqlParameter();
                ParamIdDealTWO.ParameterName = "@ParamIdDealTWO";
                ParamIdDealTWO.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandCreateContract.Parameters.Add(ParamIdDealTWO);

                SqlParameter ParamContractCity = new SqlParameter();
                ParamContractCity.ParameterName = "@ParamContractCity";
                ParamContractCity.Value = TextBoxContractCity.Text;
                CommandCreateContract.Parameters.Add(ParamContractCity);

                SqlParameter ParamContractDate = new SqlParameter();
                ParamContractDate.ParameterName = "@ParamContractDate";
                ParamContractDate.Value = TextBoxContractDate.Text;
                CommandCreateContract.Parameters.Add(ParamContractDate);

                SqlParameter ParamContractCost = new SqlParameter();
                ParamContractCost.ParameterName = "@ParamContractCost";
                ParamContractCost.Value = TextBoxContractCost.Text;
                CommandCreateContract.Parameters.Add(ParamContractCost);

                SqlParameter ParamSeller = new SqlParameter();
                ParamSeller.ParameterName = "@ParamSeller";
                ParamSeller.Value = TextBoxContractSeller.Text;
                CommandCreateContract.Parameters.Add(ParamSeller);

                SqlParameter ParamSellerBirthDate = new SqlParameter();
                ParamSellerBirthDate.ParameterName = "@ParamSellerBirthDate";
                ParamSellerBirthDate.Value = TextBoxContractSellerBirthDate.Text;
                CommandCreateContract.Parameters.Add(ParamSellerBirthDate);

                SqlParameter ParamSellerDocumentSerialNumber = new SqlParameter();
                ParamSellerDocumentSerialNumber.ParameterName = "@ParamSellerDocumentSerialNumber";
                ParamSellerDocumentSerialNumber.Value = TextBoxContractSellerDocumentSerialNumber.Text;
                CommandCreateContract.Parameters.Add(ParamSellerDocumentSerialNumber);

                SqlParameter ParamSellerDocumentIssue = new SqlParameter();
                ParamSellerDocumentIssue.ParameterName = "@ParamSellerDocumentIssue";
                ParamSellerDocumentIssue.Value = TextBoxContractSellerDocumentIssue.Text;
                CommandCreateContract.Parameters.Add(ParamSellerDocumentIssue);

                SqlParameter ParamSellerDocumentDate = new SqlParameter();
                ParamSellerDocumentDate.ParameterName = "@ParamSellerDocumentDate";
                ParamSellerDocumentDate.Value = TextBoxContractSellerDocumentDate.Text;
                CommandCreateContract.Parameters.Add(ParamSellerDocumentDate);

                SqlParameter ParamSellerAddressRegistration = new SqlParameter();
                ParamSellerAddressRegistration.ParameterName = "@ParamSellerAddressRegistration";
                ParamSellerAddressRegistration.Value = TextBoxContracSellerAddressRegistration.Text;
                CommandCreateContract.Parameters.Add(ParamSellerAddressRegistration);

                SqlParameter ParamBuyer = new SqlParameter();
                ParamBuyer.ParameterName = "@ParamBuyer";
                ParamBuyer.Value = TextBoxContractBuyer.Text;
                CommandCreateContract.Parameters.Add(ParamBuyer);

                SqlParameter ParamBuyerBirthDate = new SqlParameter();
                ParamBuyerBirthDate.ParameterName = "@ParamBuyerBirthDate";
                ParamBuyerBirthDate.Value = TextBoxContractBuyerBirthDate.Text;
                CommandCreateContract.Parameters.Add(ParamBuyerBirthDate);

                SqlParameter ParamBuyerDocumentSerialNumber = new SqlParameter();
                ParamBuyerDocumentSerialNumber.ParameterName = "@ParamBuyerDocumentSerialNumber";
                ParamBuyerDocumentSerialNumber.Value = TextBoxContractBuyerDocumentSerialNumber.Text;
                CommandCreateContract.Parameters.Add(ParamBuyerDocumentSerialNumber);

                SqlParameter ParamBuyerDocumentIssue = new SqlParameter();
                ParamBuyerDocumentIssue.ParameterName = "@ParamBuyerDocumentIssue";
                ParamBuyerDocumentIssue.Value = TextBoxContractBuyerDocumentIssue.Text;
                CommandCreateContract.Parameters.Add(ParamBuyerDocumentIssue);

                SqlParameter ParamBuyerDocumentDate = new SqlParameter();
                ParamBuyerDocumentDate.ParameterName = "@ParamBuyerDocumentDate";
                ParamBuyerDocumentDate.Value = TextBoxContractBuyerDocumentDate.Text;
                CommandCreateContract.Parameters.Add(ParamBuyerDocumentDate);

                SqlParameter ParamBuyerAddressRegistration = new SqlParameter();
                ParamBuyerAddressRegistration.ParameterName = "@ParamBuyerAddressRegistration";
                ParamBuyerAddressRegistration.Value = TextBoxContracBuyerAddressRegistration.Text;
                CommandCreateContract.Parameters.Add(ParamBuyerAddressRegistration);

                SqlParameter ParamSertificateOfOwnerRegistrationNumber = new SqlParameter();
                ParamSertificateOfOwnerRegistrationNumber.ParameterName = "@ParamSertificateOfOwnerRegistrationNumber";
                ParamSertificateOfOwnerRegistrationNumber.Value = TextBoxContractSertificateOfOwnerRegistrationNumber.Text;
                CommandCreateContract.Parameters.Add(ParamSertificateOfOwnerRegistrationNumber);

                Conn.Open();
                CommandCreateContract.ExecuteNonQuery();
                Conn.Close();
            }

            else
            {
                SqlCommand CommandUpdateContract = new SqlCommand("UPDATE dbo.TableContract" +
                    " Set ContractCity = @ParamContractCity," +
                    " ContractDate = @ParamContractDate," +
                    " ContractCost = @ParamContractCost," +
                    " ContractDeposit = @ParamContractDeposit," +

                    " Seller = @ParamSeller," +
                    " SellerBirthDate = @ParamSellerBirthDate," +
                    " SellerDocumentSerialNumber = @ParamSellerDocumentSerialNumber," +
                    " SellerDocumentIssue = @ParamSellerDocumentIssue," +
                    " SellerDocumentDate = @ParamSellerDocumentDate," +
                    " SellerAddressRegistration = @ParamSellerAddressRegistration," +

                    " Buyer = @ParamBuyer," +
                    " BuyerBirthDate = @ParamBuyerBirthDate," +
                    " BuyerDocumentSerialNumber = @ParamBuyerDocumentSerialNumber," +
                    " BuyerDocumentIssue = @ParamBuyerDocumentIssue," +
                    " BuyerDocumentDate = @ParamBuyerDocumentDate," +
                    " BuyerAddressRegistration = @ParamBuyerAddressRegistration," +

                    " Room = @ParamRoom," +
                    " Area = @ParamArea," +
                    " Floor = @ParamFloor," +
                    " Address = @ParamAddress," +

                    " CadastralNumber = @ParamCadastralNumber," +
                    " DocumentFoundationType = @ParamDocumentFoundationType," +
                    " DocumentFoundationDate = @ParamDocumentFoundationDate," +

                    " SertificateOfOwnerSerialNumber = @ParamSertificateOfOwnerSerialNumber," +
                    " SertificateOfOwnerRegistrationNumber = @ParamSertificateOfOwnerRegistrationNumber," +
                    " SertificateOfOwnerShipDate = @ParamSertificateOfOwnerShipDate" +

                    " WHERE IdDeal = @ParamIdDealTWO", Conn);

                SqlParameter ParamDocumentFoundationType = new SqlParameter();
                ParamDocumentFoundationType.ParameterName = "@ParamDocumentFoundationType";
                ParamDocumentFoundationType.Value = DropDownListContractDocumentFoundationType.SelectedValue;
                CommandUpdateContract.Parameters.Add(ParamDocumentFoundationType);

                SqlParameter ParamContractDeposit = new SqlParameter();
                ParamContractDeposit.ParameterName = "@ParamContractDeposit";
                ParamContractDeposit.Value = TextBoxContractDeposit.Text;
                CommandUpdateContract.Parameters.Add(ParamContractDeposit);

                SqlParameter ParamDocumentFoundationDate = new SqlParameter();
                ParamDocumentFoundationDate.ParameterName = "@ParamDocumentFoundationDate";
                ParamDocumentFoundationDate.Value = TextBoxContractDocumentFoundationDate.Text;
                CommandUpdateContract.Parameters.Add(ParamDocumentFoundationDate);

                SqlParameter ParamSertificateOfOwnerSerialNumber = new SqlParameter();
                ParamSertificateOfOwnerSerialNumber.ParameterName = "@ParamSertificateOfOwnerSerialNumber";
                ParamSertificateOfOwnerSerialNumber.Value = TextBoxContractSertificateOfOwnerSerialNumber.Text;
                CommandUpdateContract.Parameters.Add(ParamSertificateOfOwnerSerialNumber);

                SqlParameter ParamSertificateOfOwnerShipDate = new SqlParameter();
                ParamSertificateOfOwnerShipDate.ParameterName = "@ParamSertificateOfOwnerShipDate";
                ParamSertificateOfOwnerShipDate.Value = TextBoxContractSertificateOfOwnerShipDate.Text;
                CommandUpdateContract.Parameters.Add(ParamSertificateOfOwnerShipDate);

                SqlParameter ParamRoom = new SqlParameter();
                ParamRoom.ParameterName = "@ParamRoom";
                ParamRoom.Value = TextBoxContractRoom.Text;
                CommandUpdateContract.Parameters.Add(ParamRoom);

                SqlParameter ParamArea = new SqlParameter();
                ParamArea.ParameterName = "@ParamArea";
                ParamArea.Value = TextBoxContractArea.Text;
                CommandUpdateContract.Parameters.Add(ParamArea);

                SqlParameter ParamFloor = new SqlParameter();
                ParamFloor.ParameterName = "@ParamFloor";
                ParamFloor.Value = TextBoxContractFloor.Text;
                CommandUpdateContract.Parameters.Add(ParamFloor);

                SqlParameter ParamAddress = new SqlParameter();
                ParamAddress.ParameterName = "@ParamAddress";
                ParamAddress.Value = TextBoxContractAddress.Text;
                CommandUpdateContract.Parameters.Add(ParamAddress);

                SqlParameter ParamCadastralNumber = new SqlParameter();
                ParamCadastralNumber.ParameterName = "@ParamCadastralNumber";
                ParamCadastralNumber.Value = TextBoxContractCadastralNumber.Text;
                CommandUpdateContract.Parameters.Add(ParamCadastralNumber);

                SqlParameter ParamIdDealTWO = new SqlParameter();
                ParamIdDealTWO.ParameterName = "@ParamIdDealTWO";
                ParamIdDealTWO.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandUpdateContract.Parameters.Add(ParamIdDealTWO);

                SqlParameter ParamContractCity = new SqlParameter();
                ParamContractCity.ParameterName = "@ParamContractCity";
                ParamContractCity.Value = TextBoxContractCity.Text;
                CommandUpdateContract.Parameters.Add(ParamContractCity);

                SqlParameter ParamContractDate = new SqlParameter();
                ParamContractDate.ParameterName = "@ParamContractDate";
                ParamContractDate.Value = TextBoxContractDate.Text;
                CommandUpdateContract.Parameters.Add(ParamContractDate);

                SqlParameter ParamContractCost = new SqlParameter();
                ParamContractCost.ParameterName = "@ParamContractCost";
                ParamContractCost.Value = TextBoxContractCost.Text;
                CommandUpdateContract.Parameters.Add(ParamContractCost);

                SqlParameter ParamSeller = new SqlParameter();
                ParamSeller.ParameterName = "@ParamSeller";
                ParamSeller.Value = TextBoxContractSeller.Text;
                CommandUpdateContract.Parameters.Add(ParamSeller);

                SqlParameter ParamSellerBirthDate = new SqlParameter();
                ParamSellerBirthDate.ParameterName = "@ParamSellerBirthDate";
                ParamSellerBirthDate.Value = TextBoxContractSellerBirthDate.Text;
                CommandUpdateContract.Parameters.Add(ParamSellerBirthDate);

                SqlParameter ParamSellerDocumentSerialNumber = new SqlParameter();
                ParamSellerDocumentSerialNumber.ParameterName = "@ParamSellerDocumentSerialNumber";
                ParamSellerDocumentSerialNumber.Value = TextBoxContractSellerDocumentSerialNumber.Text;
                CommandUpdateContract.Parameters.Add(ParamSellerDocumentSerialNumber);

                SqlParameter ParamSellerDocumentIssue = new SqlParameter();
                ParamSellerDocumentIssue.ParameterName = "@ParamSellerDocumentIssue";
                ParamSellerDocumentIssue.Value = TextBoxContractSellerDocumentIssue.Text;
                CommandUpdateContract.Parameters.Add(ParamSellerDocumentIssue);

                SqlParameter ParamSellerDocumentDate = new SqlParameter();
                ParamSellerDocumentDate.ParameterName = "@ParamSellerDocumentDate";
                ParamSellerDocumentDate.Value = TextBoxContractSellerDocumentDate.Text;
                CommandUpdateContract.Parameters.Add(ParamSellerDocumentDate);

                SqlParameter ParamSellerAddressRegistration = new SqlParameter();
                ParamSellerAddressRegistration.ParameterName = "@ParamSellerAddressRegistration";
                ParamSellerAddressRegistration.Value = TextBoxContracSellerAddressRegistration.Text;
                CommandUpdateContract.Parameters.Add(ParamSellerAddressRegistration);

                SqlParameter ParamBuyer = new SqlParameter();
                ParamBuyer.ParameterName = "@ParamBuyer";
                ParamBuyer.Value = TextBoxContractBuyer.Text;
                CommandUpdateContract.Parameters.Add(ParamBuyer);

                SqlParameter ParamBuyerBirthDate = new SqlParameter();
                ParamBuyerBirthDate.ParameterName = "@ParamBuyerBirthDate";
                ParamBuyerBirthDate.Value = TextBoxContractBuyerBirthDate.Text;
                CommandUpdateContract.Parameters.Add(ParamBuyerBirthDate);

                SqlParameter ParamBuyerDocumentSerialNumber = new SqlParameter();
                ParamBuyerDocumentSerialNumber.ParameterName = "@ParamBuyerDocumentSerialNumber";
                ParamBuyerDocumentSerialNumber.Value = TextBoxContractBuyerDocumentSerialNumber.Text;
                CommandUpdateContract.Parameters.Add(ParamBuyerDocumentSerialNumber);

                SqlParameter ParamBuyerDocumentIssue = new SqlParameter();
                ParamBuyerDocumentIssue.ParameterName = "@ParamBuyerDocumentIssue";
                ParamBuyerDocumentIssue.Value = TextBoxContractBuyerDocumentIssue.Text;
                CommandUpdateContract.Parameters.Add(ParamBuyerDocumentIssue);

                SqlParameter ParamBuyerDocumentDate = new SqlParameter();
                ParamBuyerDocumentDate.ParameterName = "@ParamBuyerDocumentDate";
                ParamBuyerDocumentDate.Value = TextBoxContractBuyerDocumentDate.Text;
                CommandUpdateContract.Parameters.Add(ParamBuyerDocumentDate);

                SqlParameter ParamBuyerAddressRegistration = new SqlParameter();
                ParamBuyerAddressRegistration.ParameterName = "@ParamBuyerAddressRegistration";
                ParamBuyerAddressRegistration.Value = TextBoxContracBuyerAddressRegistration.Text;
                CommandUpdateContract.Parameters.Add(ParamBuyerAddressRegistration);

                SqlParameter ParamSertificateOfOwnerRegistrationNumber = new SqlParameter();
                ParamSertificateOfOwnerRegistrationNumber.ParameterName = "@ParamSertificateOfOwnerRegistrationNumber";
                ParamSertificateOfOwnerRegistrationNumber.Value = TextBoxContractSertificateOfOwnerRegistrationNumber.Text;
                CommandUpdateContract.Parameters.Add(ParamSertificateOfOwnerRegistrationNumber);

                Conn.Open();
                CommandUpdateContract.ExecuteNonQuery();
                Conn.Close();
            }
        }

        protected void LinkButtonToHome_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

            SqlCommand CommandFindEmailUser = Conn.CreateCommand();
            CommandFindEmailUser.CommandText = "SELECT IdUser FROM dbo.TableUser WHERE PassKey = @ParamPassKeyEmail";

            SqlParameter ParamPassKeyEmail = new SqlParameter();
            ParamPassKeyEmail.ParameterName = "@ParamPassKeyEmail";
            ParamPassKeyEmail.Value = Request.Cookies["CookieProWizard"]["PassKey"].ToString();
            CommandFindEmailUser.Parameters.Add(ParamPassKeyEmail);

            Conn.Open();
            string IdUser = CommandFindEmailUser.ExecuteScalar().ToString();

            SqlCommand CommandDeal = Conn.CreateCommand();
            CommandDeal.CommandText = "SELECT IdUserBuyer,"
            + " IdUserSeller From dbo.TableDeal WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandDeal.Parameters.Add(ParamIdDeal);

            SqlDataReader dReaderTWO;
            dReaderTWO = CommandDeal.ExecuteReader();

            if (dReaderTWO.Read())
            {
                string IdUserBuyer = dReaderTWO["IdUserBuyer"].ToString();
                string IdUserSeller = dReaderTWO["IdUserSeller"].ToString();

                if (IdUserSeller == IdUser)
                {
                    Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormContractSeller"));

                }

                if (IdUserBuyer == IdUser)
                {
                    Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormContractBuyer"));

                }
            }
            Conn.Close();
        }
    }
}
