﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Web;

namespace WebApplicationProWizard.WebForms.Getting
{
    public partial class WebFormGettingBuyer : System.Web.UI.Page
    {
        protected void Page_PreRender(object sender, EventArgs e)
        {
            string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();

            if (File.Exists(Server.MapPath("/DealDocuments/" + IdDeal
                    + "/Acceptance.docx")))
            {
                LinkButtonDownloadAcceptance.Visible = true;
            }
        }

        protected void LinkButtonDownloadAcceptance_Click(object sender, EventArgs e)
        {
            string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();
            string FileName = "Acceptance.docx";
            string Ftp = "ftp://waws-prod-db3-051.ftp.azurewebsites.windows.net/site/wwwroot/DealDocuments/" + IdDeal + "/";

            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(Ftp + FileName);
            request.Method = WebRequestMethods.Ftp.DownloadFile;

            request.Credentials = new NetworkCredential(@"ProWizard\ProWizard", "100Million");
            request.UsePassive = true;
            request.UseBinary = true;
            request.EnableSsl = false;

            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            using (MemoryStream stream = new MemoryStream())
            {
                response.GetResponseStream().CopyTo(stream);
                Response.AddHeader("content-disposition", "attachment;filename=" + FileName);
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
        }

        protected void LinkButtonForwardToAcceptance_Click(object sender, EventArgs e)
        {
            Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormGettingDocument"));
        }
    }
}