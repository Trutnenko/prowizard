﻿using System;
using System.Configuration;
using System.Data.SqlClient;

namespace WebApplicationProWizard
{
    public class ClassFindIdUserInCookie
    {

        public static void FindIdUser(string PassKey, out string IdUserValue)
        {
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            Conn.Open();

            SqlCommand CommandFindIdUser = Conn.CreateCommand();
            CommandFindIdUser.CommandText = "SELECT IdUser From dbo.TableUser Where PassKey = @PassKey";

            SqlParameter ParamFindUser = new SqlParameter();
            ParamFindUser.ParameterName = "@PassKey";
            ParamFindUser.Value = PassKey;
            CommandFindIdUser.Parameters.Add(ParamFindUser);
            IdUserValue = Convert.ToString(CommandFindIdUser.ExecuteScalar());

            Conn.Close();
        }
    }
}