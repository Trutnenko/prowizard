﻿using System;
using PayOnline.Sdk.Payments;
using System.Web;
using System.Net;
using System.IO;

namespace WebApplicationProWizard
{
    public partial class Test : System.Web.UI.Page
    {
        protected void Button1_Click(object sender, EventArgs e)
        {
            //Задаем параметры конфигурации
            var settings = new PaymentGatewaySettings()
            {
                //Указываем локализацию, по умолчанию Россия
                Language = "RU",
                //Указываем идентификатор мерчанта
                MerchantId = 76056,
                //Указываем приватный ключ, выданный мерчанту
                ApiKey = "e0f147a1-35d1-41c3-844a-475c093da222",
                //Указываем адрес сервера процессинга PayOnline
                GatewayUrl = "https://secure.payonlinesystem.com"
            };
            //Создаем экземпляр библиотеки процессинга
            PaymentGateway client = new PaymentGateway(settings);
            //Создаем запрос для получения ссылки на платежную форму
            PaymentUrlRequest pureq = new PaymentUrlRequest()
            {
                //Номер заказа в Вашем магазине
                OrderId = /*Request.Form["orderId"]*/TextBox2.Text.ToString(),
                //Сумма
                Amount = /*decimal.Parse(Request.Form["amount"]*/decimal.Parse(TextBox1.Text.ToString()),
                //Валюта
                Currency = "RUB",
                //Дополнительные опции платежа:
                //Описание
                //OrderDdescription = Request.Form["orderdescription"],
                //Дата валидности ссылки
                //ValidUntil = DateTime.Parse(Request.Form["validuntil"]),
                //URL страницы возврата, по умолчанию та же страница
                ReturnUrl = "http://prowizard.azurewebsites.net",
                //URL страницы ошибки
                FailUrl = "http://prowizard.azurewebsites.net/404.aspx",
                //Email плательщика
                //Email = Request.Form["email"],
                //Поле IndustryData - по согласованию с PayOnline
                //IndustryData = null,
                //Дополнительные параметры по согласованию с PayOnline
                //CustomParameters = new NameValueCollection() { { "some", "data" } }
            };
            //Уходим на страницу оплаты
            Response.Redirect(client.GetPaymentUrl(pureq).Url);
        }

        protected void ButtonFindFiles_Click(object sender, EventArgs e)
        {
            string Folder = TextBoxFiles.Text.ToString();
            string NameFile;

            ClassFiles.FirstNameFile(Folder, out NameFile);

            TextBoxNameFile.Text = NameFile;
        }

        protected void LinkButtonUploadTest_Click(object sender, EventArgs e)
        {
            foreach (string f in Request.Files.AllKeys)
            {
                HttpPostedFile file = Request.Files[f];
                file.SaveAs(Server.MapPath("Document/") + file.FileName);
            }
        }

        protected void LinkButtonDeleteTest_Click(object sender, EventArgs e)
        {
            File.Delete(Server.MapPath("Document/")+ "1.jpg");
        }

        protected void LinkButtonDownload_Click(object sender, EventArgs e)
        {
            //string FileName = "1.jpg";
            //string Ftp = "ftp://waws-prod-db3-051.ftp.azurewebsites.windows.net/site/wwwroot/Document/";

            //FtpWebRequest request = (FtpWebRequest)WebRequest.Create(Ftp + FileName);
            //request.Method = WebRequestMethods.Ftp.DownloadFile;

            //request.Credentials = new NetworkCredential(@"ProWizard\ProWizard", "100Million");
            //request.UsePassive = true;
            //request.UseBinary = true;
            //request.EnableSsl = false;

            //FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            //using (MemoryStream stream = new MemoryStream())
            //{
            //    response.GetResponseStream().CopyTo(stream);
            //    Response.AddHeader("content-disposition", "attachment;filename=" + FileName);
            //    Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //    Response.BinaryWrite(stream.ToArray());
            //    Response.End();
            //}

            //string remoteUri = "http://homga.ru/DealDocuments/";
            //string fileName = "ms-banner.gif", myStringWebResource = null;
            //WebClient myWebClient = new WebClient();
            //myStringWebResource = remoteUri + fileName;
            //myWebClient.DownloadFile(myStringWebResource, fileName);

        }

        protected void LinkButtonCreateFolder_Click(object sender, EventArgs e)
        {
            DirectoryInfo di = Directory.CreateDirectory(Server.MapPath("Document/TestFolder"));
        }
    }
}