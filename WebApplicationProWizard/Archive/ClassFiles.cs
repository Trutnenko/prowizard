﻿using System.Collections.Generic;
using System.IO;
using System.Net;
 
namespace WebApplicationProWizard
{
    public class ClassFiles
    {
        public static void CreateFolder(string Folder)
        {
            string FtpUrl = "ftp://trutnenko.myjino.ru/";
            string FtpUserName = "trutnenko";
            string FtpPassword = "100Million";

            WebRequest request = WebRequest.Create(FtpUrl + Folder);
            request.Method = WebRequestMethods.Ftp.MakeDirectory;
            request.Credentials = new NetworkCredential(FtpUserName, FtpPassword);
            using (var resp = (FtpWebResponse)request.GetResponse())
            {
            }
        }

        public static void Upload(string Folder, string FileName, byte[] Bytes)
        {
            string FtpUrl = "ftp://trutnenko.myjino.ru/";
            string FtpUserName = "trutnenko";
            string FtpPassword = "100Million";

            FtpWebRequest ftpReq = (FtpWebRequest)WebRequest.Create(FtpUrl + Folder + FileName);

            ftpReq.UseBinary = true;
            ftpReq.Method = WebRequestMethods.Ftp.UploadFile;
            ftpReq.Credentials = new NetworkCredential(FtpUserName, FtpPassword);

            ftpReq.ContentLength = Bytes.Length;
            using (Stream s = ftpReq.GetRequestStream())
            {
                s.Write(Bytes, 0, Bytes.Length);
            }
        }

        public static void FirstNameFile(string Folder, out string NameFile)
        {
            string FtpUrl = "ftp://trutnenko.myjino.ru/";
            string FtpUserName = "trutnenko";
            string FtpPassword = "100Million";

            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(FtpUrl + Folder);
            request.Method = WebRequestMethods.Ftp.ListDirectory;
            request.Credentials = new NetworkCredential(FtpUserName, FtpPassword);
            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream);

            List<string> result = new List<string>();

            while (!reader.EndOfStream)
            {
                result.Add(reader.ReadLine());
            }

            NameFile = result[0].ToString();
        }

        public static void Delete(string Folder)
        {
            string FtpUrl = "ftp://trutnenko.myjino.ru/";
            string FtpUserName = "trutnenko";
            string FtpPassword = "100Million";

            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(FtpUrl + Folder);
            request.Method = WebRequestMethods.Ftp.ListDirectory;
            request.Credentials = new NetworkCredential(FtpUserName, FtpPassword);
            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream);

            List<string> result = new List<string>();

            while (!reader.EndOfStream)
            {
                result.Add(reader.ReadLine());
            }

            reader.Close();
            response.Close();

            string Name = result[0].ToString();

            FtpWebRequest requestFileDelete = (FtpWebRequest)WebRequest.Create(FtpUrl + Folder + Name);
            requestFileDelete.Credentials = new NetworkCredential(FtpUserName, FtpPassword);
            requestFileDelete.Method = WebRequestMethods.Ftp.DeleteFile;

            FtpWebResponse responseFileDelete = (FtpWebResponse)requestFileDelete.GetResponse();
        }
    }
}