﻿<%@ Page
    Language="C#"
    AutoEventWireup="true"
    CodeBehind="ContractAddBuyers.aspx.cs"
    Inherits="WebApplicationProWizard.ContractAddBuyer" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link rel="stylesheet" href="style.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div style="position: absolute; height: 1000px; width: 100%; top: 0px; background-color: #FBFBFB; z-index: 0;">
        </div>
        <div style="position: relative; margin: 0 auto; width: 1000px; height: 950px; top: 50px; background-color: #FBFBFB; z-index: 1;">
            <div style="position: relative; height: 20px; top: 10px; width: 1000px; z-index: 2; margin-left: auto; margin-right: auto; margin-top: 0;">
                <div style="float: right; width: 40px;">
                    <asp:ImageButton
                        ID="ImageButtonClose"
                        runat="server" ImageUrl="~/Content/Close.png"
                        OnClick="LinkButtonToHome_Click"
                        Height="30px" />
                </div>
            </div>
            <div style="position: absolute; top: 50px; width: 900px; height: 200px;">
                <asp:Panel
                    runat="server"
                    ID="Panel1"
                    Visible="False">
                    <asp:Label
                        runat="server"
                        Style="color: #000000"
                        CssClass="Header1">
                    ПОКУПАТЕЛЬ 2
                    </asp:Label>
                    <div style="position: absolute; top: 50px; width: 400px; left: 0px; height: 125px;">
                        <asp:TextBox
                            ID="TextBoxContractBuyer1"
                            placeholder="  Фамилия, имя, отчество"
                            runat="server"
                            Visible="True"
                            ViewStateMode="Enabled"
                            Width="350px"
                            CssClass="InputMain"></asp:TextBox>
                        <br />
                        <br />
                        <asp:TextBox
                            ID="TextBoxContracBuyerAddressRegistration1"
                            placeholder="  Адрес регистрации"
                            runat="server"
                            Visible="True"
                            ViewStateMode="Enabled"
                            Width="350px"
                            CssClass="InputMain"></asp:TextBox>
                        <br />
                        <br />
                        <asp:TextBox
                            ID="TextBoxContractBuyerBirthDate1"
                            placeholder="  Дата рождения"
                            runat="server"
                            Visible="True"
                            ViewStateMode="Enabled"
                            Width="150px"
                            CssClass="InputMain"></asp:TextBox>
                    </div>
                    <div style="position: absolute; top: 50px; width: 400px; left: 500px; height: 125px;">
                        <asp:TextBox
                            ID="TextBoxContractBuyerDocumentIssue1"
                            placeholder="  Паспорт: Кем выдан"
                            runat="server"
                            Visible="True"
                            ViewStateMode="Enabled"
                            Width="350px"
                            CssClass="InputMain"></asp:TextBox>
                        <br />
                        <br />
                        <asp:TextBox
                            ID="TextBoxContractBuyerDocumentDate1"
                            placeholder="  Дата выдачи"
                            runat="server"
                            Visible="True"
                            ViewStateMode="Enabled"
                            Width="150px"
                            CssClass="InputMain"></asp:TextBox>
                        <br />
                        <br />
                        <asp:TextBox
                            ID="TextBoxContractBuyerDocumentSerialNumber1"
                            placeholder="  Серия и номер"
                            runat="server"
                            Visible="True"
                            ViewStateMode="Enabled"
                            Width="150px"
                            CssClass="InputMain"></asp:TextBox>
                    </div>
                </asp:Panel>
            </div>
            <div style="position: absolute; top: 300px; width: 900px; height: 200px;">
                <asp:Panel
                    runat="server"
                    ID="Panel2"
                    Visible="False">
                    <asp:Label
                        runat="server"
                        Style="color: #000000"
                        CssClass="Header1">
                    ПОКУПАТЕЛЬ 3
                    </asp:Label>
                    <div style="position: absolute; top: 50px; width: 400px; left: 0px; height: 125px;">
                        <asp:TextBox
                            ID="TextBoxContractBuyer2"
                            placeholder="  Фамилия, имя, отчество"
                            runat="server"
                            Visible="True"
                            ViewStateMode="Enabled"
                            Width="350px"
                            CssClass="InputMain"></asp:TextBox>
                        <br />
                        <br />
                        <asp:TextBox
                            ID="TextBoxContracBuyerAddressRegistration2"
                            placeholder="  Адрес регистрации"
                            runat="server"
                            Visible="True"
                            ViewStateMode="Enabled"
                            Width="350px"
                            CssClass="InputMain"></asp:TextBox>
                        <br />
                        <br />
                        <asp:TextBox
                            ID="TextBoxContractBuyerBirthDate2"
                            placeholder="  Дата рождения"
                            runat="server"
                            Visible="True"
                            ViewStateMode="Enabled"
                            Width="150px"
                            CssClass="InputMain"></asp:TextBox>
                    </div>
                    <div style="position: absolute; top: 50px; width: 400px; left: 500px; height: 125px;">
                        <asp:TextBox
                            ID="TextBoxContractBuyerDocumentIssue2"
                            placeholder="  Паспорт: Кем выдан"
                            runat="server"
                            Visible="True"
                            ViewStateMode="Enabled"
                            Width="350px"
                            CssClass="InputMain"></asp:TextBox>
                        <br />
                        <br />
                        <asp:TextBox
                            ID="TextBoxContractBuyerDocumentDate2"
                            placeholder="  Дата выдачи"
                            runat="server"
                            Visible="True"
                            ViewStateMode="Enabled"
                            Width="150px"
                            CssClass="InputMain"></asp:TextBox>
                        <br />
                        <br />
                        <asp:TextBox
                            ID="TextBoxContractBuyerDocumentSerialNumber2"
                            placeholder="  Серия и номер"
                            runat="server"
                            Visible="True"
                            ViewStateMode="Enabled"
                            Width="150px"
                            CssClass="InputMain"></asp:TextBox>
                    </div>
                </asp:Panel>
            </div>
            <div style="position: absolute; top: 550px; width: 900px; height: 200px;">
                <asp:Panel
                    runat="server"
                    ID="Panel3"
                    Visible="False">
                    <asp:Label
                        runat="server"
                        Style="color: #000000"
                        CssClass="Header1">
                    ПОКУПАТЕЛЬ 4
                    </asp:Label>
                    <div style="position: absolute; top: 50px; width: 400px; left: 0px; height: 125px;">
                        <asp:TextBox ID="TextBoxContractBuyer3"
                            placeholder="  Фамилия, имя, отчество"
                            runat="server"
                            Visible="True"
                            ViewStateMode="Enabled"
                            Width="350px"
                            CssClass="InputMain"></asp:TextBox>
                        <br />
                        <br />
                        <asp:TextBox ID="TextBoxContracBuyerAddressRegistration3"
                            placeholder="  Адрес регистрации"
                            runat="server"
                            Visible="True"
                            ViewStateMode="Enabled"
                            Width="350px"
                            CssClass="InputMain"></asp:TextBox>
                        <br />
                        <br />
                        <asp:TextBox ID="TextBoxContractBuyerBirthDate3"
                            placeholder="  Дата рождения"
                            runat="server"
                            Visible="True"
                            ViewStateMode="Enabled"
                            Width="150px"
                            CssClass="InputMain"></asp:TextBox>
                    </div>
                    <div style="position: absolute; top: 50px; width: 400px; left: 500px; height: 125px;">
                        <asp:TextBox ID="TextBoxContractBuyerDocumentIssue3"
                            placeholder="  Паспорт: Кем выдан"
                            runat="server"
                            Visible="True"
                            ViewStateMode="Enabled"
                            Width="350px"
                            CssClass="InputMain"></asp:TextBox>
                        <br />
                        <br />
                        <asp:TextBox ID="TextBoxContractBuyerDocumentDate3"
                            placeholder="  Дата выдачи"
                            runat="server"
                            Visible="True"
                            ViewStateMode="Enabled"
                            Width="150px"
                            CssClass="InputMain"></asp:TextBox>
                        <br />
                        <br />
                        <asp:TextBox ID="TextBoxContractBuyerDocumentSerialNumber3"
                            placeholder="  Серия и номер"
                            runat="server"
                            Visible="True"
                            ViewStateMode="Enabled"
                            Width="150px"
                            CssClass="InputMain"></asp:TextBox>
                    </div>
                </asp:Panel>
            </div>
            <div style="position: absolute; top: 30px; width: 900px; height: 20px;">
                <asp:Panel
                    runat="server"
                    ID="PanelButtons1">
                    <asp:LinkButton
                        ID="LinkButtonAdd1"
                        runat="server"
                        OnClick="LinkButtonAdd1_Click"
                        EnableViewState="False"
                        ViewStateMode="Disabled">
                    Добавить
                    </asp:LinkButton>
                </asp:Panel>
            </div>
            <div style="position: absolute; top: 270px; width: 900px; height: 20px;">
                <asp:Panel
                    runat="server"
                    ID="PanelButtons2" Visible="False">
                    <asp:LinkButton
                        ID="LinkButtonAdd2"
                        runat="server"
                        OnClick="LinkButtonAdd2_Click"
                        EnableViewState="False"
                        ViewStateMode="Disabled">
                    Добавить
                    </asp:LinkButton>
                    &nbsp;&nbsp;
                <asp:LinkButton
                    ID="LinkButtonRemove2"
                    runat="server"
                    EnableViewState="False"
                    OnClick="LinkButtonRemove2_Click"
                    ViewStateMode="Disabled">
                    Удалить
                </asp:LinkButton>
                    &nbsp;&nbsp;
                <asp:LinkButton
                    ID="LinkButtonSave2"
                    runat="server"
                    EnableViewState="False"
                    OnClick="LinkButtonSave2_Click"
                    ViewStateMode="Disabled">
                    Сохранить
                </asp:LinkButton>
                </asp:Panel>
            </div>
            <div style="position: absolute; top: 520px; width: 900px; height: 20px;">
                <asp:Panel
                    runat="server"
                    ID="PanelButtons3" Visible="False">
                    <asp:LinkButton
                        ID="LinkButtonAdd3"
                        runat="server"
                        OnClick="LinkButtonAdd3_Click"
                        EnableViewState="False"
                        ViewStateMode="Disabled">
                    Добавить
                    </asp:LinkButton>
                    &nbsp;&nbsp;
                <asp:LinkButton
                    ID="LinkButtonRemove3"
                    runat="server"
                    EnableViewState="False"
                    OnClick="LinkButtonRemove3_Click"
                    ViewStateMode="Disabled">
                    Удалить
                </asp:LinkButton>
                    &nbsp;&nbsp;
                <asp:LinkButton
                    ID="LinkButtonSave3"
                    runat="server"
                    EnableViewState="False"
                    OnClick="LinkButtonSave3_Click"
                    ViewStateMode="Disabled">
                    Сохранить
                </asp:LinkButton>
                </asp:Panel>
            </div>
            <div style="position: absolute; top: 770px; width: 900px; height: 20px;">
                <asp:Panel
                    runat="server"
                    ID="PanelButtons4" Visible="False">
                    <asp:LinkButton
                        ID="LinkButtonRemove4"
                        runat="server"
                        EnableViewState="False"
                        OnClick="LinkButtonRemove4_Click"
                        ViewStateMode="Disabled">
                    Удалить
                    </asp:LinkButton>
                    &nbsp;&nbsp;
                <asp:LinkButton
                    ID="LinkButtonSave4"
                    runat="server"
                    EnableViewState="False"
                    OnClick="LinkButtonSave4_Click"
                    ViewStateMode="Disabled">
                    Сохранить
                </asp:LinkButton>
                </asp:Panel>
            </div>
        </div>
    </form>
</body>
</html>
