﻿<%@ Page 
    Language="C#"
    AutoEventWireup="true"
    CodeBehind="Test.aspx.cs"
    Inherits="WebApplicationProWizard.Test" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title> 
</head>
<body>
    <form id="form1" runat="server" style="background-color:#FBFBFB;">
        <div>
            Стоимость<br />
            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            <br />
            <br />
            Код сделки<br />
            <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
            <br />
            <br />
            <br />
            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Button" />
            <br />
            <br />
            имя папки<br />
            <asp:TextBox ID="TextBoxFiles" runat="server"></asp:TextBox>
            <br />
            <br />
            имя первого файла<br />
            <asp:TextBox ID="TextBoxNameFile" runat="server"></asp:TextBox>
            <br />
            <br />
            <br />
            <asp:Button ID="ButtonFindFiles" runat="server" OnClick="ButtonFindFiles_Click" Text="Button" />
        </div>
        
        <br />
        <br />
        Загрузка на FS Azure
        <br />
        <br />
        <asp:FileUpload ID="FileUploadTest" runat="server" />
        <br />
        <br />
        <asp:LinkButton ID="LinkButtonUploadTest" runat ="server" OnClick="LinkButtonUploadTest_Click">
            Загрузить
        </asp:LinkButton>
        <br />
        <br />
        <asp:LinkButton ID="LinkButtonUploadTest0" runat ="server" OnClick="LinkButtonCreateFolder_Click">Создать папку </asp:LinkButton>
        <br />
        <br />
        <asp:LinkButton ID="LinkButtonDownload" runat ="server" OnClick="LinkButtonDownload_Click">
            Скачать
        </asp:LinkButton>
        <br />
        <br />
        <asp:LinkButton ID="LinkButtonDeleteTest" runat ="server" OnClick="LinkButtonDeleteTest_Click">
            Удалить
        </asp:LinkButton>

    </form>
</body>
</html>
