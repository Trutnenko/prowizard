﻿using System.Configuration;
using System.Data.SqlClient;

namespace WebApplicationProWizard
{
    public class ClassDefineBuyerOrSeller
    {
        public static void Define(
            out string TypeUser, 
            out string IdCurrentUser,
            out string IdUserSeller,
            out string IdUserBuyer,
            string IdDeal, 
            string PassKey)
        {
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

            SqlCommand CommandFindIdUser = Conn.CreateCommand();
            CommandFindIdUser.CommandText = "SELECT" +
                " IdUser FROM dbo.TableUser" +
                " WHERE PassKey = @ParamPassKey";

            SqlParameter ParamPassKey = new SqlParameter();
            ParamPassKey.ParameterName = "@ParamPassKey";
            ParamPassKey.Value = PassKey;
            CommandFindIdUser.Parameters.Add(ParamPassKey);

            Conn.Open();
            IdCurrentUser = CommandFindIdUser.ExecuteScalar().ToString();

            SqlCommand CommandDefineSeller = Conn.CreateCommand();
            CommandDefineSeller.CommandText = "SELECT"
            + " IdUserSeller FROM dbo.TableDeal" 
            + " WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = IdDeal;
            CommandDefineSeller.Parameters.Add(ParamIdDeal);

            IdUserSeller = CommandDefineSeller.ExecuteScalar().ToString();

            SqlCommand CommandDefineBuyer = Conn.CreateCommand();
            CommandDefineBuyer.CommandText = "SELECT"
            + " IdUserBuyer FROM dbo.TableDeal"
            + " WHERE IdDeal = @ParamIdDealTWO";

            SqlParameter ParamIdDealTWO = new SqlParameter();
            ParamIdDealTWO.ParameterName = "@ParamIdDealTWO";
            ParamIdDealTWO.Value = IdDeal;
            CommandDefineBuyer.Parameters.Add(ParamIdDealTWO);

            IdUserBuyer = CommandDefineBuyer.ExecuteScalar().ToString();

            Conn.Close();

            if (IdCurrentUser == IdUserBuyer)
            {
                TypeUser = "Buyer";
            }

            if (IdCurrentUser == IdUserSeller)
            {
                TypeUser = "Seller";
            }

            else
            {
                TypeUser = "None";
            }
        }
    }
}


