﻿using System;
using System.Configuration;
using System.Data.SqlClient;

namespace WebApplicationProWizard
{
    public partial class ContractAddBuyer : System.Web.UI.Page
    {
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

                SqlCommand CommandExistBuyers = Conn.CreateCommand();
                CommandExistBuyers.CommandText = "SELECT COUNT (*) FROM dbo.TableContractBuyers"
                  + " WHERE IdDeal = @ParamIdDeal";

                SqlParameter ParamIdDeal = new SqlParameter();
                ParamIdDeal.ParameterName = "@ParamIdDeal";
                ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandExistBuyers.Parameters.Add(ParamIdDeal);

                Conn.Open();
                string ExistBuyers = Convert.ToString(CommandExistBuyers.ExecuteScalar());
                Conn.Close();

                if (ExistBuyers != "0")
                {
                    SqlCommand CommandPreContract = Conn.CreateCommand();
                    CommandPreContract.CommandText = "SELECT" +
                        " CountBuyers," +

                        " Buyer1," +
                        " BuyerBirthDate1," +
                        " BuyerDocumentSerialNumber1," +
                        " BuyerDocumentIssue1," +
                        " BuyerDocumentDate1," +
                        " BuyerAddressRegistration1," +

                        " Buyer2," +
                        " BuyerBirthDate2," +
                        " BuyerDocumentSerialNumber2," +
                        " BuyerDocumentIssue2," +
                        " BuyerDocumentDate2," +
                        " BuyerAddressRegistration2," +

                        " Buyer3," +
                        " BuyerBirthDate3," +
                        " BuyerDocumentSerialNumber3," +
                        " BuyerDocumentIssue3," +
                        " BuyerDocumentDate3," +
                        " BuyerAddressRegistration3" +

                    " FROM dbo.TableContractBuyers WHERE IdDeal = @ParamIdDealTHREE";

                    SqlParameter ParamIdDealTHREE = new SqlParameter();
                    ParamIdDealTHREE.ParameterName = "@ParamIdDealTHREE";
                    ParamIdDealTHREE.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                    CommandPreContract.Parameters.Add(ParamIdDealTHREE);

                    SqlDataReader dReader;

                    Conn.Open();
                    dReader = CommandPreContract.ExecuteReader();

                    if (dReader.Read())

                    {
                        string CountBuyers = dReader["CountBuyers"].ToString();

                        string Buyer1 = dReader["Buyer1"].ToString();
                        string BuyerBirthDate1 = dReader["BuyerBirthDate1"].ToString();
                        string BuyerDocumentSerialNumber1 = dReader["BuyerDocumentSerialNumber1"].ToString();
                        string BuyerDocumentIssue1 = dReader["BuyerDocumentIssue1"].ToString();
                        string BuyerDocumentDate1 = dReader["BuyerDocumentDate1"].ToString();
                        string BuyerAddressRegistration1 = dReader["BuyerAddressRegistration1"].ToString();

                        string Buyer2 = dReader["Buyer2"].ToString();
                        string BuyerBirthDate2 = dReader["BuyerBirthDate2"].ToString();
                        string BuyerDocumentSerialNumber2 = dReader["BuyerDocumentSerialNumber2"].ToString();
                        string BuyerDocumentIssue2 = dReader["BuyerDocumentIssue2"].ToString();
                        string BuyerDocumentDate2 = dReader["BuyerDocumentDate2"].ToString();
                        string BuyerAddressRegistration2 = dReader["BuyerAddressRegistration2"].ToString();

                        string Buyer3 = dReader["Buyer3"].ToString();
                        string BuyerBirthDate3 = dReader["BuyerBirthDate3"].ToString();
                        string BuyerDocumentSerialNumber3 = dReader["BuyerDocumentSerialNumber3"].ToString();
                        string BuyerDocumentIssue3 = dReader["BuyerDocumentIssue3"].ToString();
                        string BuyerDocumentDate3 = dReader["BuyerDocumentDate3"].ToString();
                        string BuyerAddressRegistration3 = dReader["BuyerAddressRegistration3"].ToString();

                        dReader.Close();

                        if (CountBuyers == "1")
                        {
                            Panel1.Visible = true;
                            PanelButtons1.Visible = false;
                            PanelButtons2.Visible = true;

                            TextBoxContractBuyer1.Text = Buyer1;
                            TextBoxContractBuyerBirthDate1.Text = BuyerBirthDate1;
                            TextBoxContractBuyerDocumentSerialNumber1.Text = BuyerDocumentSerialNumber1;
                            TextBoxContractBuyerDocumentIssue1.Text = BuyerDocumentIssue1;
                            TextBoxContractBuyerDocumentDate1.Text = BuyerDocumentDate1;
                            TextBoxContracBuyerAddressRegistration1.Text = BuyerAddressRegistration1;
                        }

                        if (CountBuyers == "2")
                        {
                            Panel1.Visible = true;
                            Panel2.Visible = true;
                            PanelButtons1.Visible = false;
                            PanelButtons2.Visible = false;
                            PanelButtons3.Visible = true;
                            PanelButtons4.Visible = false;

                            TextBoxContractBuyer1.Text = Buyer1;
                            TextBoxContractBuyerBirthDate1.Text = BuyerBirthDate1;
                            TextBoxContractBuyerDocumentSerialNumber1.Text = BuyerDocumentSerialNumber1;
                            TextBoxContractBuyerDocumentIssue1.Text = BuyerDocumentIssue1;
                            TextBoxContractBuyerDocumentDate1.Text = BuyerDocumentDate1;
                            TextBoxContracBuyerAddressRegistration1.Text = BuyerAddressRegistration1;

                            TextBoxContractBuyer2.Text = Buyer2;
                            TextBoxContractBuyerBirthDate2.Text = BuyerBirthDate2;
                            TextBoxContractBuyerDocumentSerialNumber2.Text = BuyerDocumentSerialNumber2;
                            TextBoxContractBuyerDocumentIssue2.Text = BuyerDocumentIssue2;
                            TextBoxContractBuyerDocumentDate2.Text = BuyerDocumentDate2;
                            TextBoxContracBuyerAddressRegistration2.Text = BuyerAddressRegistration2;
                        }

                        if (CountBuyers == "3")
                        {
                            Panel1.Visible = true;
                            Panel2.Visible = true;
                            Panel3.Visible = true;
                            PanelButtons1.Visible = false;
                            PanelButtons2.Visible = false;
                            PanelButtons3.Visible = false;
                            PanelButtons4.Visible = true;

                            TextBoxContractBuyer1.Text = Buyer1;
                            TextBoxContractBuyerBirthDate1.Text = BuyerBirthDate1;
                            TextBoxContractBuyerDocumentSerialNumber1.Text = BuyerDocumentSerialNumber1;
                            TextBoxContractBuyerDocumentIssue1.Text = BuyerDocumentIssue1;
                            TextBoxContractBuyerDocumentDate1.Text = BuyerDocumentDate1;
                            TextBoxContracBuyerAddressRegistration1.Text = BuyerAddressRegistration1;

                            TextBoxContractBuyer2.Text = Buyer2;
                            TextBoxContractBuyerBirthDate2.Text = BuyerBirthDate2;
                            TextBoxContractBuyerDocumentSerialNumber2.Text = BuyerDocumentSerialNumber2;
                            TextBoxContractBuyerDocumentIssue2.Text = BuyerDocumentIssue2;
                            TextBoxContractBuyerDocumentDate2.Text = BuyerDocumentDate2;
                            TextBoxContracBuyerAddressRegistration2.Text = BuyerAddressRegistration2;

                            TextBoxContractBuyer3.Text = Buyer3;
                            TextBoxContractBuyerBirthDate3.Text = BuyerBirthDate3;
                            TextBoxContractBuyerDocumentSerialNumber3.Text = BuyerDocumentSerialNumber3;
                            TextBoxContractBuyerDocumentIssue3.Text = BuyerDocumentIssue3;
                            TextBoxContractBuyerDocumentDate3.Text = BuyerDocumentDate3;
                            TextBoxContracBuyerAddressRegistration3.Text = BuyerAddressRegistration3;
                        }
                    }
                }
            }
        }

        protected void LinkButtonToHome_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormContractDocument"));
        }

        protected void LinkButtonAdd1_Click(object sender, EventArgs e)
        {
            Panel1.Visible = true;
            PanelButtons1.Visible = false;
            PanelButtons2.Visible = true;
        }

        protected void LinkButtonAdd2_Click(object sender, EventArgs e)
        {
            Panel2.Visible = true;
            PanelButtons2.Visible = false;
            PanelButtons3.Visible = true;
        }
        protected void LinkButtonRemove2_Click(object sender, EventArgs e)
        {
            Panel1.Visible = false;
            PanelButtons2.Visible = false;
            PanelButtons1.Visible = true;

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandCountBuyers = Conn.CreateCommand();
            CommandCountBuyers.CommandText = "SELECT COUNT (*) FROM dbo.TableContractBuyers"
              + " WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandCountBuyers.Parameters.Add(ParamIdDeal);

            Conn.Open();
            string CountBuyers = Convert.ToString(CommandCountBuyers.ExecuteScalar());
            Conn.Close();

            if (CountBuyers != "0")
            {
                SqlCommand CommandDELETEBuyer2 = new SqlCommand("DELETE FROM dbo.TableContractBuyers" +
                    " WHERE IdDeal = @ParamIdDealTWO", Conn);

                SqlParameter ParamIdDealTWO = new SqlParameter();
                ParamIdDealTWO.ParameterName = "@ParamIdDealTWO";
                ParamIdDealTWO.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandDELETEBuyer2.Parameters.Add(ParamIdDealTWO);

                Conn.Open();
                CommandDELETEBuyer2.ExecuteNonQuery();
                Conn.Close();
            }
        }

        protected void LinkButtonSave2_Click(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

            SqlCommand CommandCountBuyers = Conn.CreateCommand();
            CommandCountBuyers.CommandText = "SELECT COUNT (*) FROM dbo.TableContractBuyers"
              + " WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandCountBuyers.Parameters.Add(ParamIdDeal);

            Conn.Open();
            string CountBuyers = Convert.ToString(CommandCountBuyers.ExecuteScalar());
            Conn.Close();

            if (CountBuyers == "0")
            {
                SqlCommand CommandCreateBuyer2 = new SqlCommand("INSERT INTO dbo.TableContractBuyers" +
                    " (IdDeal," +

                    " CountBuyers," +

                    " Buyer1," +
                    " BuyerBirthDate1," +
                    " BuyerDocumentSerialNumber1," +
                    " BuyerDocumentIssue1," +
                    " BuyerDocumentDate1," +
                    " BuyerAddressRegistration1)" +

                    " VALUES" +

                    " (@ParamIdDealTWO," +

                    " @ParamCountBuyers," +

                    " @ParamBuyer," +
                    " @ParamBuyerBirthDate," +
                    " @ParamBuyerDocumentSerialNumber," +
                    " @ParamBuyerDocumentIssue," +
                    " @ParamBuyerDocumentDate," +
                    " @ParamBuyerAddressRegistration)",

                    Conn);

                SqlParameter ParamIdDealTWO = new SqlParameter();
                ParamIdDealTWO.ParameterName = "@ParamIdDealTWO";
                ParamIdDealTWO.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandCreateBuyer2.Parameters.Add(ParamIdDealTWO);

                SqlParameter ParamCountBuyers = new SqlParameter();
                ParamCountBuyers.ParameterName = "@ParamCountBuyers";
                ParamCountBuyers.Value = "1";
                CommandCreateBuyer2.Parameters.Add(ParamCountBuyers);

                //--------

                SqlParameter ParamBuyer = new SqlParameter();
                ParamBuyer.ParameterName = "@ParamBuyer";
                ParamBuyer.Value = TextBoxContractBuyer1.Text;
                CommandCreateBuyer2.Parameters.Add(ParamBuyer);

                SqlParameter ParamBuyerBirthDate = new SqlParameter();
                ParamBuyerBirthDate.ParameterName = "@ParamBuyerBirthDate";
                ParamBuyerBirthDate.Value = TextBoxContractBuyerBirthDate1.Text;
                CommandCreateBuyer2.Parameters.Add(ParamBuyerBirthDate);

                SqlParameter ParamBuyerDocumentSerialNumber = new SqlParameter();
                ParamBuyerDocumentSerialNumber.ParameterName = "@ParamBuyerDocumentSerialNumber";
                ParamBuyerDocumentSerialNumber.Value = TextBoxContractBuyerDocumentSerialNumber1.Text;
                CommandCreateBuyer2.Parameters.Add(ParamBuyerDocumentSerialNumber);

                SqlParameter ParamBuyerDocumentIssue = new SqlParameter();
                ParamBuyerDocumentIssue.ParameterName = "@ParamBuyerDocumentIssue";
                ParamBuyerDocumentIssue.Value = TextBoxContractBuyerDocumentIssue1.Text;
                CommandCreateBuyer2.Parameters.Add(ParamBuyerDocumentIssue);

                SqlParameter ParamBuyerDocumentDate = new SqlParameter();
                ParamBuyerDocumentDate.ParameterName = "@ParamBuyerDocumentDate";
                ParamBuyerDocumentDate.Value = TextBoxContractBuyerDocumentDate1.Text;
                CommandCreateBuyer2.Parameters.Add(ParamBuyerDocumentDate);

                SqlParameter ParamBuyerAddressRegistration = new SqlParameter();
                ParamBuyerAddressRegistration.ParameterName = "@ParamBuyerAddressRegistration";
                ParamBuyerAddressRegistration.Value = TextBoxContracBuyerAddressRegistration1.Text;
                CommandCreateBuyer2.Parameters.Add(ParamBuyerAddressRegistration);

                Conn.Open();
                CommandCreateBuyer2.ExecuteNonQuery();
                Conn.Close();
            }

            else
            {
                SqlCommand CommandUpdateBuyer2 = new SqlCommand("UPDATE dbo.TableContractBuyers" +
                    " SET Buyer1 = @ParamBuyer," +

                    " CountBuyers = @ParamCountBuyers," +

                    " BuyerBirthDate1 = @ParamBuyerBirthDate," +
                    " BuyerDocumentSerialNumber1 = @ParamBuyerDocumentSerialNumber," +
                    " BuyerDocumentIssue1 = @ParamBuyerDocumentIssue," +
                    " BuyerDocumentDate1 = @ParamBuyerDocumentDate," +
                    " BuyerAddressRegistration1 = @ParamBuyerAddressRegistration" +

                    " WHERE IdDeal = @ParamIdDealTWO", Conn);

                SqlParameter ParamIdDealTWO = new SqlParameter();
                ParamIdDealTWO.ParameterName = "@ParamIdDealTWO";
                ParamIdDealTWO.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandUpdateBuyer2.Parameters.Add(ParamIdDealTWO);

                SqlParameter ParamCountBuyers = new SqlParameter();
                ParamCountBuyers.ParameterName = "@ParamCountBuyers";
                ParamCountBuyers.Value = "1";
                CommandUpdateBuyer2.Parameters.Add(ParamCountBuyers);

                //--------

                SqlParameter ParamBuyer = new SqlParameter();
                ParamBuyer.ParameterName = "@ParamBuyer";
                ParamBuyer.Value = TextBoxContractBuyer1.Text;
                CommandUpdateBuyer2.Parameters.Add(ParamBuyer);

                SqlParameter ParamBuyerBirthDate = new SqlParameter();
                ParamBuyerBirthDate.ParameterName = "@ParamBuyerBirthDate";
                ParamBuyerBirthDate.Value = TextBoxContractBuyerBirthDate1.Text;
                CommandUpdateBuyer2.Parameters.Add(ParamBuyerBirthDate);

                SqlParameter ParamBuyerDocumentSerialNumber = new SqlParameter();
                ParamBuyerDocumentSerialNumber.ParameterName = "@ParamBuyerDocumentSerialNumber";
                ParamBuyerDocumentSerialNumber.Value = TextBoxContractBuyerDocumentSerialNumber1.Text;
                CommandUpdateBuyer2.Parameters.Add(ParamBuyerDocumentSerialNumber);

                SqlParameter ParamBuyerDocumentIssue = new SqlParameter();
                ParamBuyerDocumentIssue.ParameterName = "@ParamBuyerDocumentIssue";
                ParamBuyerDocumentIssue.Value = TextBoxContractBuyerDocumentIssue1.Text;
                CommandUpdateBuyer2.Parameters.Add(ParamBuyerDocumentIssue);

                SqlParameter ParamBuyerDocumentDate = new SqlParameter();
                ParamBuyerDocumentDate.ParameterName = "@ParamBuyerDocumentDate";
                ParamBuyerDocumentDate.Value = TextBoxContractBuyerDocumentDate1.Text;
                CommandUpdateBuyer2.Parameters.Add(ParamBuyerDocumentDate);

                SqlParameter ParamBuyerAddressRegistration = new SqlParameter();
                ParamBuyerAddressRegistration.ParameterName = "@ParamBuyerAddressRegistration";
                ParamBuyerAddressRegistration.Value = TextBoxContracBuyerAddressRegistration1.Text;
                CommandUpdateBuyer2.Parameters.Add(ParamBuyerAddressRegistration);

                Conn.Open();
                CommandUpdateBuyer2.ExecuteNonQuery();
                Conn.Close();
            }
        }

        protected void LinkButtonAdd3_Click(object sender, EventArgs e)
        {
            Panel3.Visible = true;
            PanelButtons3.Visible = false;
            PanelButtons4.Visible = true;
        }
        protected void LinkButtonRemove3_Click(object sender, EventArgs e)
        {
            Panel2.Visible = false;
            PanelButtons3.Visible = false;
            PanelButtons2.Visible = true;

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandCountBuyers = Conn.CreateCommand();
            CommandCountBuyers.CommandText = "SELECT COUNT (*) FROM dbo.TableContractBuyers"
              + " WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandCountBuyers.Parameters.Add(ParamIdDeal);

            Conn.Open();
            string CountBuyers = Convert.ToString(CommandCountBuyers.ExecuteScalar());
            Conn.Close();

            if (CountBuyers != "0")
            {
                SqlCommand CommandDELETEBuyer2 = new SqlCommand("UPDATE dbo.TableContractBuyers" +
                    " SET Buyer2 = @ParamBuyer2," +

                    " CountBuyers = @ParamCountBuyers," +

                    " BuyerBirthDate2 = @ParamBuyerBirthDate2," +
                    " BuyerDocumentSerialNumber2 = @ParamBuyerDocumentSerialNumber2," +
                    " BuyerDocumentIssue2 = @ParamBuyerDocumentIssue2," +
                    " BuyerDocumentDate2 = @ParamBuyerDocumentDate2," +
                    " BuyerAddressRegistration2 = @ParamBuyerAddressRegistration2" +

                    " WHERE IdDeal = @ParamIdDealTWO", Conn);

                SqlParameter ParamIdDealTWO = new SqlParameter();
                ParamIdDealTWO.ParameterName = "@ParamIdDealTWO";
                ParamIdDealTWO.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandDELETEBuyer2.Parameters.Add(ParamIdDealTWO);

                SqlParameter ParamCountBuyers = new SqlParameter();
                ParamCountBuyers.ParameterName = "@ParamCountBuyers";
                ParamCountBuyers.Value = "1";
                CommandDELETEBuyer2.Parameters.Add(ParamCountBuyers);

                SqlParameter ParamBuyer2 = new SqlParameter();
                ParamBuyer2.ParameterName = "@ParamBuyer2";
                ParamBuyer2.Value = "";
                CommandDELETEBuyer2.Parameters.Add(ParamBuyer2);

                SqlParameter ParamBuyerBirthDate2 = new SqlParameter();
                ParamBuyerBirthDate2.ParameterName = "@ParamBuyerBirthDate2";
                ParamBuyerBirthDate2.Value = "";
                CommandDELETEBuyer2.Parameters.Add(ParamBuyerBirthDate2);

                SqlParameter ParamBuyerDocumentSerialNumber2 = new SqlParameter();
                ParamBuyerDocumentSerialNumber2.ParameterName = "@ParamBuyerDocumentSerialNumber2";
                ParamBuyerDocumentSerialNumber2.Value = "";
                CommandDELETEBuyer2.Parameters.Add(ParamBuyerDocumentSerialNumber2);

                SqlParameter ParamBuyerDocumentIssue2 = new SqlParameter();
                ParamBuyerDocumentIssue2.ParameterName = "@ParamBuyerDocumentIssue2";
                ParamBuyerDocumentIssue2.Value = "";
                CommandDELETEBuyer2.Parameters.Add(ParamBuyerDocumentIssue2);

                SqlParameter ParamBuyerDocumentDate2 = new SqlParameter();
                ParamBuyerDocumentDate2.ParameterName = "@ParamBuyerDocumentDate2";
                ParamBuyerDocumentDate2.Value = "";
                CommandDELETEBuyer2.Parameters.Add(ParamBuyerDocumentDate2);

                SqlParameter ParamBuyerAddressRegistration2 = new SqlParameter();
                ParamBuyerAddressRegistration2.ParameterName = "@ParamBuyerAddressRegistration2";
                ParamBuyerAddressRegistration2.Value = "";
                CommandDELETEBuyer2.Parameters.Add(ParamBuyerAddressRegistration2);

                Conn.Open();
                CommandDELETEBuyer2.ExecuteNonQuery();
                Conn.Close();
            }
        }
        protected void LinkButtonSave3_Click(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandCountBuyers = Conn.CreateCommand();
            CommandCountBuyers.CommandText = "SELECT COUNT (*) FROM dbo.TableContractBuyers"
              + " WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandCountBuyers.Parameters.Add(ParamIdDeal);

            Conn.Open();
            string CountBuyers = Convert.ToString(CommandCountBuyers.ExecuteScalar());
            Conn.Close();

            if (CountBuyers == "0")
            {
                SqlCommand CommandCreateBuyers = new SqlCommand("INSERT INTO dbo.TableContractBuyers" +
                    " (IdDeal," +

                    " CountBuyers," +

                    " Buyer1," +
                    " BuyerBirthDate1," +
                    " BuyerDocumentSerialNumber1," +
                    " BuyerDocumentIssue1," +
                    " BuyerDocumentDate1," +
                    " BuyerAddressRegistration1," +

                    " Buyer2," +
                    " BuyerBirthDate2," +
                    " BuyerDocumentSerialNumber2," +
                    " BuyerDocumentIssue2," +
                    " BuyerDocumentDate2," +
                    " BuyerAddressRegistration2)" +

                    " VALUES" +

                    " (@ParamIdDealTWO," +

                    " @ParamCountBuyers," +

                    " @ParamBuyer1," +
                    " @ParamBuyerBirthDate1," +
                    " @ParamBuyerDocumentSerialNumber1," +
                    " @ParamBuyerDocumentIssue1," +
                    " @ParamBuyerDocumentDate1," +
                    " @ParamBuyerAddressRegistration1," +

                    " @ParamBuyer2," +
                    " @ParamBuyerBirthDate2," +
                    " @ParamBuyerDocumentSerialNumber2," +
                    " @ParamBuyerDocumentIssue2," +
                    " @ParamBuyerDocumentDate2," +
                    " @ParamBuyerAddressRegistration2)",

                    Conn);

                SqlParameter ParamIdDealTWO = new SqlParameter();
                ParamIdDealTWO.ParameterName = "@ParamIdDealTWO";
                ParamIdDealTWO.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandCreateBuyers.Parameters.Add(ParamIdDealTWO);

                SqlParameter ParamCountBuyers = new SqlParameter();
                ParamCountBuyers.ParameterName = "@ParamCountBuyers";
                ParamCountBuyers.Value = "2";
                CommandCreateBuyers.Parameters.Add(ParamCountBuyers);

                //--------

                SqlParameter ParamBuyer1 = new SqlParameter();
                ParamBuyer1.ParameterName = "@ParamBuyer1";
                ParamBuyer1.Value = TextBoxContractBuyer1.Text;
                CommandCreateBuyers.Parameters.Add(ParamBuyer1);

                SqlParameter ParamBuyerBirthDate1 = new SqlParameter();
                ParamBuyerBirthDate1.ParameterName = "@ParamBuyerBirthDate1";
                ParamBuyerBirthDate1.Value = TextBoxContractBuyerBirthDate1.Text;
                CommandCreateBuyers.Parameters.Add(ParamBuyerBirthDate1);

                SqlParameter ParamBuyerDocumentSerialNumber1 = new SqlParameter();
                ParamBuyerDocumentSerialNumber1.ParameterName = "@ParamBuyerDocumentSerialNumber1";
                ParamBuyerDocumentSerialNumber1.Value = TextBoxContractBuyerDocumentSerialNumber1.Text;
                CommandCreateBuyers.Parameters.Add(ParamBuyerDocumentSerialNumber1);

                SqlParameter ParamBuyerDocumentIssue1 = new SqlParameter();
                ParamBuyerDocumentIssue1.ParameterName = "@ParamBuyerDocumentIssue1";
                ParamBuyerDocumentIssue1.Value = TextBoxContractBuyerDocumentIssue1.Text;
                CommandCreateBuyers.Parameters.Add(ParamBuyerDocumentIssue1);

                SqlParameter ParamBuyerDocumentDate1 = new SqlParameter();
                ParamBuyerDocumentDate1.ParameterName = "@ParamBuyerDocumentDate1";
                ParamBuyerDocumentDate1.Value = TextBoxContractBuyerDocumentDate1.Text;
                CommandCreateBuyers.Parameters.Add(ParamBuyerDocumentDate1);

                SqlParameter ParamBuyerAddressRegistration1 = new SqlParameter();
                ParamBuyerAddressRegistration1.ParameterName = "@ParamBuyerAddressRegistration1";
                ParamBuyerAddressRegistration1.Value = TextBoxContracBuyerAddressRegistration1.Text;
                CommandCreateBuyers.Parameters.Add(ParamBuyerAddressRegistration1);

                //--------

                SqlParameter ParamBuyer2 = new SqlParameter();
                ParamBuyer2.ParameterName = "@ParamBuyer2";
                ParamBuyer2.Value = TextBoxContractBuyer2.Text;
                CommandCreateBuyers.Parameters.Add(ParamBuyer2);

                SqlParameter ParamBuyerBirthDate2 = new SqlParameter();
                ParamBuyerBirthDate2.ParameterName = "@ParamBuyerBirthDate2";
                ParamBuyerBirthDate2.Value = TextBoxContractBuyerBirthDate2.Text;
                CommandCreateBuyers.Parameters.Add(ParamBuyerBirthDate2);

                SqlParameter ParamBuyerDocumentSerialNumber2 = new SqlParameter();
                ParamBuyerDocumentSerialNumber2.ParameterName = "@ParamBuyerDocumentSerialNumber2";
                ParamBuyerDocumentSerialNumber2.Value = TextBoxContractBuyerDocumentSerialNumber2.Text;
                CommandCreateBuyers.Parameters.Add(ParamBuyerDocumentSerialNumber2);

                SqlParameter ParamBuyerDocumentIssue2 = new SqlParameter();
                ParamBuyerDocumentIssue2.ParameterName = "@ParamBuyerDocumentIssue2";
                ParamBuyerDocumentIssue2.Value = TextBoxContractBuyerDocumentIssue2.Text;
                CommandCreateBuyers.Parameters.Add(ParamBuyerDocumentIssue2);

                SqlParameter ParamBuyerDocumentDate2 = new SqlParameter();
                ParamBuyerDocumentDate2.ParameterName = "@ParamBuyerDocumentDate2";
                ParamBuyerDocumentDate2.Value = TextBoxContractBuyerDocumentDate2.Text;
                CommandCreateBuyers.Parameters.Add(ParamBuyerDocumentDate2);

                SqlParameter ParamBuyerAddressRegistration2 = new SqlParameter();
                ParamBuyerAddressRegistration2.ParameterName = "@ParamBuyerAddressRegistration2";
                ParamBuyerAddressRegistration2.Value = TextBoxContracBuyerAddressRegistration2.Text;
                CommandCreateBuyers.Parameters.Add(ParamBuyerAddressRegistration2);

                Conn.Open();
                CommandCreateBuyers.ExecuteNonQuery();
                Conn.Close();
            }

            else
            {
                SqlCommand CommandUpdateBuyers = new SqlCommand("UPDATE dbo.TableContractBuyers" +
                    " SET Buyer1 = @ParamBuyer1," +

                    " CountBuyers = @ParamCountBuyers," +

                    " BuyerBirthDate1 = @ParamBuyerBirthDate1," +
                    " BuyerDocumentSerialNumber1 = @ParamBuyerDocumentSerialNumber1," +
                    " BuyerDocumentIssue1 = @ParamBuyerDocumentIssue1," +
                    " BuyerDocumentDate1 = @ParamBuyerDocumentDate1," +
                    " BuyerAddressRegistration1 = @ParamBuyerAddressRegistration1," +

                    " Buyer2 = @ParamBuyer2," +
                    " BuyerBirthDate2 = @ParamBuyerBirthDate2," +
                    " BuyerDocumentSerialNumber2 = @ParamBuyerDocumentSerialNumber2," +
                    " BuyerDocumentIssue2 = @ParamBuyerDocumentIssue2," +
                    " BuyerDocumentDate2 = @ParamBuyerDocumentDate2," +
                    " BuyerAddressRegistration2 = @ParamBuyerAddressRegistration2" +

                    " WHERE IdDeal = @ParamIdDealTWO", Conn);

                SqlParameter ParamIdDealTWO = new SqlParameter();
                ParamIdDealTWO.ParameterName = "@ParamIdDealTWO";
                ParamIdDealTWO.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandUpdateBuyers.Parameters.Add(ParamIdDealTWO);

                SqlParameter ParamCountBuyers = new SqlParameter();
                ParamCountBuyers.ParameterName = "@ParamCountBuyers";
                ParamCountBuyers.Value = "2";
                CommandUpdateBuyers.Parameters.Add(ParamCountBuyers);

                //--------

                SqlParameter ParamBuyer1 = new SqlParameter();
                ParamBuyer1.ParameterName = "@ParamBuyer1";
                ParamBuyer1.Value = TextBoxContractBuyer1.Text;
                CommandUpdateBuyers.Parameters.Add(ParamBuyer1);

                SqlParameter ParamBuyerBirthDate1 = new SqlParameter();
                ParamBuyerBirthDate1.ParameterName = "@ParamBuyerBirthDate1";
                ParamBuyerBirthDate1.Value = TextBoxContractBuyerBirthDate1.Text;
                CommandUpdateBuyers.Parameters.Add(ParamBuyerBirthDate1);

                SqlParameter ParamBuyerDocumentSerialNumber1 = new SqlParameter();
                ParamBuyerDocumentSerialNumber1.ParameterName = "@ParamBuyerDocumentSerialNumber1";
                ParamBuyerDocumentSerialNumber1.Value = TextBoxContractBuyerDocumentSerialNumber1.Text;
                CommandUpdateBuyers.Parameters.Add(ParamBuyerDocumentSerialNumber1);

                SqlParameter ParamBuyerDocumentIssue1 = new SqlParameter();
                ParamBuyerDocumentIssue1.ParameterName = "@ParamBuyerDocumentIssue1";
                ParamBuyerDocumentIssue1.Value = TextBoxContractBuyerDocumentIssue1.Text;
                CommandUpdateBuyers.Parameters.Add(ParamBuyerDocumentIssue1);

                SqlParameter ParamBuyerDocumentDate1 = new SqlParameter();
                ParamBuyerDocumentDate1.ParameterName = "@ParamBuyerDocumentDate1";
                ParamBuyerDocumentDate1.Value = TextBoxContractBuyerDocumentDate1.Text;
                CommandUpdateBuyers.Parameters.Add(ParamBuyerDocumentDate1);

                SqlParameter ParamBuyerAddressRegistration1 = new SqlParameter();
                ParamBuyerAddressRegistration1.ParameterName = "@ParamBuyerAddressRegistration1";
                ParamBuyerAddressRegistration1.Value = TextBoxContracBuyerAddressRegistration1.Text;
                CommandUpdateBuyers.Parameters.Add(ParamBuyerAddressRegistration1);

                //--------

                SqlParameter ParamBuyer2 = new SqlParameter();
                ParamBuyer2.ParameterName = "@ParamBuyer2";
                ParamBuyer2.Value = TextBoxContractBuyer2.Text;
                CommandUpdateBuyers.Parameters.Add(ParamBuyer2);

                SqlParameter ParamBuyerBirthDate2 = new SqlParameter();
                ParamBuyerBirthDate2.ParameterName = "@ParamBuyerBirthDate2";
                ParamBuyerBirthDate2.Value = TextBoxContractBuyerBirthDate2.Text;
                CommandUpdateBuyers.Parameters.Add(ParamBuyerBirthDate2);

                SqlParameter ParamBuyerDocumentSerialNumber2 = new SqlParameter();
                ParamBuyerDocumentSerialNumber2.ParameterName = "@ParamBuyerDocumentSerialNumber2";
                ParamBuyerDocumentSerialNumber2.Value = TextBoxContractBuyerDocumentSerialNumber2.Text;
                CommandUpdateBuyers.Parameters.Add(ParamBuyerDocumentSerialNumber2);

                SqlParameter ParamBuyerDocumentIssue2 = new SqlParameter();
                ParamBuyerDocumentIssue2.ParameterName = "@ParamBuyerDocumentIssue2";
                ParamBuyerDocumentIssue2.Value = TextBoxContractBuyerDocumentIssue2.Text;
                CommandUpdateBuyers.Parameters.Add(ParamBuyerDocumentIssue2);

                SqlParameter ParamBuyerDocumentDate2 = new SqlParameter();
                ParamBuyerDocumentDate2.ParameterName = "@ParamBuyerDocumentDate2";
                ParamBuyerDocumentDate2.Value = TextBoxContractBuyerDocumentDate2.Text;
                CommandUpdateBuyers.Parameters.Add(ParamBuyerDocumentDate2);

                SqlParameter ParamBuyerAddressRegistration2 = new SqlParameter();
                ParamBuyerAddressRegistration2.ParameterName = "@ParamBuyerAddressRegistration2";
                ParamBuyerAddressRegistration2.Value = TextBoxContracBuyerAddressRegistration2.Text;
                CommandUpdateBuyers.Parameters.Add(ParamBuyerAddressRegistration2);

                Conn.Open();
                CommandUpdateBuyers.ExecuteNonQuery();
                Conn.Close();
            }
        }

        protected void LinkButtonRemove4_Click(object sender, EventArgs e)
        {
            Panel3.Visible = false;
            PanelButtons4.Visible = false;
            PanelButtons3.Visible = true;

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandCountBuyers = Conn.CreateCommand();
            CommandCountBuyers.CommandText = "SELECT COUNT (*) FROM dbo.TableContractBuyers"
              + " WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandCountBuyers.Parameters.Add(ParamIdDeal);

            Conn.Open();
            string CountBuyers = Convert.ToString(CommandCountBuyers.ExecuteScalar());
            Conn.Close();

            if (CountBuyers != "0")
            {
                SqlCommand CommandDELETEBuyer3 = new SqlCommand("UPDATE dbo.TableContractBuyers" +
                    " SET Buyer3 = @ParamBuyer3," +

                    " CountBuyers = @ParamCountBuyers," +

                    " BuyerBirthDate3 = @ParamBuyerBirthDate3," +
                    " BuyerDocumentSerialNumber3 = @ParamBuyerDocumentSerialNumber3," +
                    " BuyerDocumentIssue3 = @ParamBuyerDocumentIssue3," +
                    " BuyerDocumentDate3 = @ParamBuyerDocumentDate3," +
                    " BuyerAddressRegistration3 = @ParamBuyerAddressRegistration3" +

                    " WHERE IdDeal = @ParamIdDealTWO", Conn);

                SqlParameter ParamIdDealTWO = new SqlParameter();
                ParamIdDealTWO.ParameterName = "@ParamIdDealTWO";
                ParamIdDealTWO.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandDELETEBuyer3.Parameters.Add(ParamIdDealTWO);

                SqlParameter ParamCountBuyers = new SqlParameter();
                ParamCountBuyers.ParameterName = "@ParamCountBuyers";
                ParamCountBuyers.Value = "2";
                CommandDELETEBuyer3.Parameters.Add(ParamCountBuyers);

                SqlParameter ParamBuyer3 = new SqlParameter();
                ParamBuyer3.ParameterName = "@ParamBuyer3";
                ParamBuyer3.Value = "";
                CommandDELETEBuyer3.Parameters.Add(ParamBuyer3);

                SqlParameter ParamBuyerBirthDate3 = new SqlParameter();
                ParamBuyerBirthDate3.ParameterName = "@ParamBuyerBirthDate3";
                ParamBuyerBirthDate3.Value = "";
                CommandDELETEBuyer3.Parameters.Add(ParamBuyerBirthDate3);

                SqlParameter ParamBuyerDocumentSerialNumber3 = new SqlParameter();
                ParamBuyerDocumentSerialNumber3.ParameterName = "@ParamBuyerDocumentSerialNumber3";
                ParamBuyerDocumentSerialNumber3.Value = "";
                CommandDELETEBuyer3.Parameters.Add(ParamBuyerDocumentSerialNumber3);

                SqlParameter ParamBuyerDocumentIssue3 = new SqlParameter();
                ParamBuyerDocumentIssue3.ParameterName = "@ParamBuyerDocumentIssue3";
                ParamBuyerDocumentIssue3.Value = "";
                CommandDELETEBuyer3.Parameters.Add(ParamBuyerDocumentIssue3);

                SqlParameter ParamBuyerDocumentDate3 = new SqlParameter();
                ParamBuyerDocumentDate3.ParameterName = "@ParamBuyerDocumentDate3";
                ParamBuyerDocumentDate3.Value = "";
                CommandDELETEBuyer3.Parameters.Add(ParamBuyerDocumentDate3);

                SqlParameter ParamBuyerAddressRegistration3 = new SqlParameter();
                ParamBuyerAddressRegistration3.ParameterName = "@ParamBuyerAddressRegistration3";
                ParamBuyerAddressRegistration3.Value = "";
                CommandDELETEBuyer3.Parameters.Add(ParamBuyerAddressRegistration3);

                Conn.Open();
                CommandDELETEBuyer3.ExecuteNonQuery();
                Conn.Close();
            }
        }
        protected void LinkButtonSave4_Click(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandCountBuyers = Conn.CreateCommand();
            CommandCountBuyers.CommandText = "SELECT COUNT (*) FROM dbo.TableContractBuyers"
              + " WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandCountBuyers.Parameters.Add(ParamIdDeal);

            Conn.Open();
            string CountBuyers = Convert.ToString(CommandCountBuyers.ExecuteScalar());
            Conn.Close();

            if (CountBuyers == "0")
            {
                SqlCommand CommandCreateBuyers = new SqlCommand("INSERT INTO dbo.TableContractBuyers" +
                    " (IdDeal," +

                    " CountBuyers," +

                    " Buyer1," +
                    " BuyerBirthDate1," +
                    " BuyerDocumentSerialNumber1," +
                    " BuyerDocumentIssue1," +
                    " BuyerDocumentDate1," +
                    " BuyerAddressRegistration1," +

                    " Buyer2," +
                    " BuyerBirthDate2," +
                    " BuyerDocumentSerialNumber2," +
                    " BuyerDocumentIssue2," +
                    " BuyerDocumentDate2," +
                    " BuyerAddressRegistration2," +

                    " Buyer3," +
                    " BuyerBirthDate3," +
                    " BuyerDocumentSerialNumber3," +
                    " BuyerDocumentIssue3," +
                    " BuyerDocumentDate3," +
                    " BuyerAddressRegistration3)" +

                    " VALUES" +

                    " (@ParamIdDealTWO," +

                    " @ParamCountBuyers," +

                    " @ParamBuyer1," +
                    " @ParamBuyerBirthDate1," +
                    " @ParamBuyerDocumentSerialNumber1," +
                    " @ParamBuyerDocumentIssue1," +
                    " @ParamBuyerDocumentDate1," +
                    " @ParamBuyerAddressRegistration1," +

                    " @ParamBuyer2," +
                    " @ParamBuyerBirthDate2," +
                    " @ParamBuyerDocumentSerialNumber2," +
                    " @ParamBuyerDocumentIssue2," +
                    " @ParamBuyerDocumentDate2," +
                    " @ParamBuyerAddressRegistration2," +

                    " @ParamBuyer3," +
                    " @ParamBuyerBirthDate3," +
                    " @ParamBuyerDocumentSerialNumber3," +
                    " @ParamBuyerDocumentIssue3," +
                    " @ParamBuyerDocumentDate3," +
                    " @ParamBuyerAddressRegistration3)",

                    Conn);

                SqlParameter ParamIdDealTWO = new SqlParameter();
                ParamIdDealTWO.ParameterName = "@ParamIdDealTWO";
                ParamIdDealTWO.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandCreateBuyers.Parameters.Add(ParamIdDealTWO);

                SqlParameter ParamCountBuyers = new SqlParameter();
                ParamCountBuyers.ParameterName = "@ParamCountBuyers";
                ParamCountBuyers.Value = "3";
                CommandCreateBuyers.Parameters.Add(ParamCountBuyers);

                //--------

                SqlParameter ParamBuyer1 = new SqlParameter();
                ParamBuyer1.ParameterName = "@ParamBuyer1";
                ParamBuyer1.Value = TextBoxContractBuyer1.Text;
                CommandCreateBuyers.Parameters.Add(ParamBuyer1);

                SqlParameter ParamBuyerBirthDate1 = new SqlParameter();
                ParamBuyerBirthDate1.ParameterName = "@ParamBuyerBirthDate1";
                ParamBuyerBirthDate1.Value = TextBoxContractBuyerBirthDate1.Text;
                CommandCreateBuyers.Parameters.Add(ParamBuyerBirthDate1);

                SqlParameter ParamBuyerDocumentSerialNumber1 = new SqlParameter();
                ParamBuyerDocumentSerialNumber1.ParameterName = "@ParamBuyerDocumentSerialNumber1";
                ParamBuyerDocumentSerialNumber1.Value = TextBoxContractBuyerDocumentSerialNumber1.Text;
                CommandCreateBuyers.Parameters.Add(ParamBuyerDocumentSerialNumber1);

                SqlParameter ParamBuyerDocumentIssue1 = new SqlParameter();
                ParamBuyerDocumentIssue1.ParameterName = "@ParamBuyerDocumentIssue1";
                ParamBuyerDocumentIssue1.Value = TextBoxContractBuyerDocumentIssue1.Text;
                CommandCreateBuyers.Parameters.Add(ParamBuyerDocumentIssue1);

                SqlParameter ParamBuyerDocumentDate1 = new SqlParameter();
                ParamBuyerDocumentDate1.ParameterName = "@ParamBuyerDocumentDate1";
                ParamBuyerDocumentDate1.Value = TextBoxContractBuyerDocumentDate1.Text;
                CommandCreateBuyers.Parameters.Add(ParamBuyerDocumentDate1);

                SqlParameter ParamBuyerAddressRegistration1 = new SqlParameter();
                ParamBuyerAddressRegistration1.ParameterName = "@ParamBuyerAddressRegistration1";
                ParamBuyerAddressRegistration1.Value = TextBoxContracBuyerAddressRegistration1.Text;
                CommandCreateBuyers.Parameters.Add(ParamBuyerAddressRegistration1);

                //--------

                SqlParameter ParamBuyer2 = new SqlParameter();
                ParamBuyer2.ParameterName = "@ParamBuyer2";
                ParamBuyer2.Value = TextBoxContractBuyer2.Text;
                CommandCreateBuyers.Parameters.Add(ParamBuyer2);

                SqlParameter ParamBuyerBirthDate2 = new SqlParameter();
                ParamBuyerBirthDate2.ParameterName = "@ParamBuyerBirthDate2";
                ParamBuyerBirthDate2.Value = TextBoxContractBuyerBirthDate2.Text;
                CommandCreateBuyers.Parameters.Add(ParamBuyerBirthDate2);

                SqlParameter ParamBuyerDocumentSerialNumber2 = new SqlParameter();
                ParamBuyerDocumentSerialNumber2.ParameterName = "@ParamBuyerDocumentSerialNumber2";
                ParamBuyerDocumentSerialNumber2.Value = TextBoxContractBuyerDocumentSerialNumber2.Text;
                CommandCreateBuyers.Parameters.Add(ParamBuyerDocumentSerialNumber2);

                SqlParameter ParamBuyerDocumentIssue2 = new SqlParameter();
                ParamBuyerDocumentIssue2.ParameterName = "@ParamBuyerDocumentIssue2";
                ParamBuyerDocumentIssue2.Value = TextBoxContractBuyerDocumentIssue2.Text;
                CommandCreateBuyers.Parameters.Add(ParamBuyerDocumentIssue2);

                SqlParameter ParamBuyerDocumentDate2 = new SqlParameter();
                ParamBuyerDocumentDate2.ParameterName = "@ParamBuyerDocumentDate2";
                ParamBuyerDocumentDate2.Value = TextBoxContractBuyerDocumentDate2.Text;
                CommandCreateBuyers.Parameters.Add(ParamBuyerDocumentDate2);

                SqlParameter ParamBuyerAddressRegistration2 = new SqlParameter();
                ParamBuyerAddressRegistration2.ParameterName = "@ParamBuyerAddressRegistration2";
                ParamBuyerAddressRegistration2.Value = TextBoxContracBuyerAddressRegistration2.Text;
                CommandCreateBuyers.Parameters.Add(ParamBuyerAddressRegistration2);

                //--------

                SqlParameter ParamBuyer3 = new SqlParameter();
                ParamBuyer3.ParameterName = "@ParamBuyer3";
                ParamBuyer3.Value = TextBoxContractBuyer3.Text;
                CommandCreateBuyers.Parameters.Add(ParamBuyer3);

                SqlParameter ParamBuyerBirthDate3 = new SqlParameter();
                ParamBuyerBirthDate3.ParameterName = "@ParamBuyerBirthDate3";
                ParamBuyerBirthDate3.Value = TextBoxContractBuyerBirthDate3.Text;
                CommandCreateBuyers.Parameters.Add(ParamBuyerBirthDate3);

                SqlParameter ParamBuyerDocumentSerialNumber3 = new SqlParameter();
                ParamBuyerDocumentSerialNumber3.ParameterName = "@ParamBuyerDocumentSerialNumber3";
                ParamBuyerDocumentSerialNumber3.Value = TextBoxContractBuyerDocumentSerialNumber3.Text;
                CommandCreateBuyers.Parameters.Add(ParamBuyerDocumentSerialNumber3);

                SqlParameter ParamBuyerDocumentIssue3 = new SqlParameter();
                ParamBuyerDocumentIssue3.ParameterName = "@ParamBuyerDocumentIssue3";
                ParamBuyerDocumentIssue3.Value = TextBoxContractBuyerDocumentIssue3.Text;
                CommandCreateBuyers.Parameters.Add(ParamBuyerDocumentIssue3);

                SqlParameter ParamBuyerDocumentDate3 = new SqlParameter();
                ParamBuyerDocumentDate3.ParameterName = "@ParamBuyerDocumentDate3";
                ParamBuyerDocumentDate3.Value = TextBoxContractBuyerDocumentDate3.Text;
                CommandCreateBuyers.Parameters.Add(ParamBuyerDocumentDate3);

                SqlParameter ParamBuyerAddressRegistration3 = new SqlParameter();
                ParamBuyerAddressRegistration3.ParameterName = "@ParamBuyerAddressRegistration3";
                ParamBuyerAddressRegistration3.Value = TextBoxContracBuyerAddressRegistration3.Text;
                CommandCreateBuyers.Parameters.Add(ParamBuyerAddressRegistration3);

                Conn.Open();
                CommandCreateBuyers.ExecuteNonQuery();
                Conn.Close();
            }

            else
            {
                SqlCommand CommandUpdateBuyers = new SqlCommand("UPDATE dbo.TableContractBuyers" +
                    " SET Buyer1 = @ParamBuyer1," +

                    " CountBuyers = @ParamCountBuyers," +

                    " BuyerBirthDate1 = @ParamBuyerBirthDate1," +
                    " BuyerDocumentSerialNumber1 = @ParamBuyerDocumentSerialNumber1," +
                    " BuyerDocumentIssue1 = @ParamBuyerDocumentIssue1," +
                    " BuyerDocumentDate1 = @ParamBuyerDocumentDate1," +
                    " BuyerAddressRegistration1 = @ParamBuyerAddressRegistration1," +

                    " Buyer2 = @ParamBuyer2," +
                    " BuyerBirthDate2 = @ParamBuyerBirthDate2," +
                    " BuyerDocumentSerialNumber2 = @ParamBuyerDocumentSerialNumber2," +
                    " BuyerDocumentIssue2 = @ParamBuyerDocumentIssue2," +
                    " BuyerDocumentDate2 = @ParamBuyerDocumentDate2," +
                    " BuyerAddressRegistration2 = @ParamBuyerAddressRegistration2," +

                    " Buyer3 = @ParamBuyer3," +
                    " BuyerBirthDate3 = @ParamBuyerBirthDate3," +
                    " BuyerDocumentSerialNumber3 = @ParamBuyerDocumentSerialNumber3," +
                    " BuyerDocumentIssue3 = @ParamBuyerDocumentIssue3," +
                    " BuyerDocumentDate3 = @ParamBuyerDocumentDate3," +
                    " BuyerAddressRegistration3 = @ParamBuyerAddressRegistration3" +

                    " WHERE IdDeal = @ParamIdDealTWO", Conn);

                SqlParameter ParamIdDealTWO = new SqlParameter();
                ParamIdDealTWO.ParameterName = "@ParamIdDealTWO";
                ParamIdDealTWO.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandUpdateBuyers.Parameters.Add(ParamIdDealTWO);

                SqlParameter ParamCountBuyers = new SqlParameter();
                ParamCountBuyers.ParameterName = "@ParamCountBuyers";
                ParamCountBuyers.Value = "3";
                CommandUpdateBuyers.Parameters.Add(ParamCountBuyers);

                //--------

                SqlParameter ParamBuyer1 = new SqlParameter();
                ParamBuyer1.ParameterName = "@ParamBuyer1";
                ParamBuyer1.Value = TextBoxContractBuyer1.Text;
                CommandUpdateBuyers.Parameters.Add(ParamBuyer1);

                SqlParameter ParamBuyerBirthDate1 = new SqlParameter();
                ParamBuyerBirthDate1.ParameterName = "@ParamBuyerBirthDate1";
                ParamBuyerBirthDate1.Value = TextBoxContractBuyerBirthDate1.Text;
                CommandUpdateBuyers.Parameters.Add(ParamBuyerBirthDate1);

                SqlParameter ParamBuyerDocumentSerialNumber1 = new SqlParameter();
                ParamBuyerDocumentSerialNumber1.ParameterName = "@ParamBuyerDocumentSerialNumber1";
                ParamBuyerDocumentSerialNumber1.Value = TextBoxContractBuyerDocumentSerialNumber1.Text;
                CommandUpdateBuyers.Parameters.Add(ParamBuyerDocumentSerialNumber1);

                SqlParameter ParamBuyerDocumentIssue1 = new SqlParameter();
                ParamBuyerDocumentIssue1.ParameterName = "@ParamBuyerDocumentIssue1";
                ParamBuyerDocumentIssue1.Value = TextBoxContractBuyerDocumentIssue1.Text;
                CommandUpdateBuyers.Parameters.Add(ParamBuyerDocumentIssue1);

                SqlParameter ParamBuyerDocumentDate1 = new SqlParameter();
                ParamBuyerDocumentDate1.ParameterName = "@ParamBuyerDocumentDate1";
                ParamBuyerDocumentDate1.Value = TextBoxContractBuyerDocumentDate1.Text;
                CommandUpdateBuyers.Parameters.Add(ParamBuyerDocumentDate1);

                SqlParameter ParamBuyerAddressRegistration1 = new SqlParameter();
                ParamBuyerAddressRegistration1.ParameterName = "@ParamBuyerAddressRegistration1";
                ParamBuyerAddressRegistration1.Value = TextBoxContracBuyerAddressRegistration1.Text;
                CommandUpdateBuyers.Parameters.Add(ParamBuyerAddressRegistration1);

                //--------

                SqlParameter ParamBuyer2 = new SqlParameter();
                ParamBuyer2.ParameterName = "@ParamBuyer2";
                ParamBuyer2.Value = TextBoxContractBuyer2.Text;
                CommandUpdateBuyers.Parameters.Add(ParamBuyer2);

                SqlParameter ParamBuyerBirthDate2 = new SqlParameter();
                ParamBuyerBirthDate2.ParameterName = "@ParamBuyerBirthDate2";
                ParamBuyerBirthDate2.Value = TextBoxContractBuyerBirthDate2.Text;
                CommandUpdateBuyers.Parameters.Add(ParamBuyerBirthDate2);

                SqlParameter ParamBuyerDocumentSerialNumber2 = new SqlParameter();
                ParamBuyerDocumentSerialNumber2.ParameterName = "@ParamBuyerDocumentSerialNumber2";
                ParamBuyerDocumentSerialNumber2.Value = TextBoxContractBuyerDocumentSerialNumber2.Text;
                CommandUpdateBuyers.Parameters.Add(ParamBuyerDocumentSerialNumber2);

                SqlParameter ParamBuyerDocumentIssue2 = new SqlParameter();
                ParamBuyerDocumentIssue2.ParameterName = "@ParamBuyerDocumentIssue2";
                ParamBuyerDocumentIssue2.Value = TextBoxContractBuyerDocumentIssue2.Text;
                CommandUpdateBuyers.Parameters.Add(ParamBuyerDocumentIssue2);

                SqlParameter ParamBuyerDocumentDate2 = new SqlParameter();
                ParamBuyerDocumentDate2.ParameterName = "@ParamBuyerDocumentDate2";
                ParamBuyerDocumentDate2.Value = TextBoxContractBuyerDocumentDate2.Text;
                CommandUpdateBuyers.Parameters.Add(ParamBuyerDocumentDate2);

                SqlParameter ParamBuyerAddressRegistration2 = new SqlParameter();
                ParamBuyerAddressRegistration2.ParameterName = "@ParamBuyerAddressRegistration2";
                ParamBuyerAddressRegistration2.Value = TextBoxContracBuyerAddressRegistration2.Text;
                CommandUpdateBuyers.Parameters.Add(ParamBuyerAddressRegistration2);

                //--------

                SqlParameter ParamBuyer3 = new SqlParameter();
                ParamBuyer3.ParameterName = "@ParamBuyer3";
                ParamBuyer3.Value = TextBoxContractBuyer3.Text;
                CommandUpdateBuyers.Parameters.Add(ParamBuyer3);

                SqlParameter ParamBuyerBirthDate3 = new SqlParameter();
                ParamBuyerBirthDate3.ParameterName = "@ParamBuyerBirthDate3";
                ParamBuyerBirthDate3.Value = TextBoxContractBuyerBirthDate3.Text;
                CommandUpdateBuyers.Parameters.Add(ParamBuyerBirthDate3);

                SqlParameter ParamBuyerDocumentSerialNumber3 = new SqlParameter();
                ParamBuyerDocumentSerialNumber3.ParameterName = "@ParamBuyerDocumentSerialNumber3";
                ParamBuyerDocumentSerialNumber3.Value = TextBoxContractBuyerDocumentSerialNumber3.Text;
                CommandUpdateBuyers.Parameters.Add(ParamBuyerDocumentSerialNumber3);

                SqlParameter ParamBuyerDocumentIssue3 = new SqlParameter();
                ParamBuyerDocumentIssue3.ParameterName = "@ParamBuyerDocumentIssue3";
                ParamBuyerDocumentIssue3.Value = TextBoxContractBuyerDocumentIssue3.Text;
                CommandUpdateBuyers.Parameters.Add(ParamBuyerDocumentIssue3);

                SqlParameter ParamBuyerDocumentDate3 = new SqlParameter();
                ParamBuyerDocumentDate3.ParameterName = "@ParamBuyerDocumentDate3";
                ParamBuyerDocumentDate3.Value = TextBoxContractBuyerDocumentDate3.Text;
                CommandUpdateBuyers.Parameters.Add(ParamBuyerDocumentDate3);

                SqlParameter ParamBuyerAddressRegistration3 = new SqlParameter();
                ParamBuyerAddressRegistration3.ParameterName = "@ParamBuyerAddressRegistration3";
                ParamBuyerAddressRegistration3.Value = TextBoxContracBuyerAddressRegistration3.Text;
                CommandUpdateBuyers.Parameters.Add(ParamBuyerAddressRegistration3);

                Conn.Open();
                CommandUpdateBuyers.ExecuteNonQuery();
                Conn.Close();
            }
        }
    }
}


// этот код был на contract.aspx - определяет количество доп покупателей
// требуется чтобы отобразить правильное название кнопки на contract.aspx

//SqlCommand CommandExistBuyers = Conn.CreateCommand();
//CommandExistBuyers.CommandText = "SELECT COUNT (*) FROM dbo.TableContractBuyers"
//              + " WHERE IdDeal = @ParamIdDealNEW";

//            SqlParameter ParamIdDealNEW = new SqlParameter();
//ParamIdDealNEW.ParameterName = "@ParamIdDealNEW";
//            ParamIdDealNEW.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
//CommandExistBuyers.Parameters.Add(ParamIdDealNEW);

//            Conn.Open();
//            string ExistBuyers = Convert.ToString(CommandExistBuyers.ExecuteScalar());
//Conn.Close();

//            if (ExistBuyers != "0")
//            {
//                SqlCommand CommandPreContract = Conn.CreateCommand();
//CommandPreContract.CommandText = "SELECT CountBuyers" +
//                " FROM dbo.TableContractBuyers WHERE IdDeal = @ParamIdDealTHREE";

//                SqlParameter ParamIdDealTHREE = new SqlParameter();
//ParamIdDealTHREE.ParameterName = "@ParamIdDealTHREE";
//                ParamIdDealTHREE.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
//CommandPreContract.Parameters.Add(ParamIdDealTHREE);

//                SqlDataReader dReader;

//Conn.Open();
//                dReader = CommandPreContract.ExecuteReader();

//                if (dReader.Read())

//                {
//                    string CountBuyers = dReader["CountBuyers"].ToString();

//dReader.Close();

//                    if (CountBuyers == "1")
//                    {
//                        LinkButtonAddBuyers.Text = "Еще 1";
//                    }

//                    if (CountBuyers == "2")
//                    {
//                        LinkButtonAddBuyers.Text = "Еще 2";
//                    }

//                    if (CountBuyers == "3")
//                    {
//                        LinkButtonAddBuyers.Text = "Еще 3";
//                    }
//                }
//            }



//Скрипт для создания нужной таблицы 

//CREATE TABLE[dbo].[TableContractBuyers]
//(

//[IdDeal] INT            NULL,

//[CountBuyers]                NVARCHAR(10)  NULL,
//    [Buyer1]                     NVARCHAR(100) NULL,
//    [BuyerBirthDate1]            NVARCHAR(50)  NULL,
//    [BuyerDocumentSerialNumber1] NVARCHAR(50)  NULL,
//    [BuyerDocumentIssue1]        NVARCHAR(200) NULL,
//    [BuyerDocumentDate1]         NVARCHAR(50)  NULL,
//    [BuyerAddressRegistration1]  NVARCHAR(100) NULL,
//    [Buyer2]                     NVARCHAR(100) NULL,
//    [BuyerBirthDate2]            NVARCHAR(50)  NULL,
//    [BuyerDocumentSerialNumber2] NVARCHAR(50)  NULL,
//    [BuyerDocumentIssue2]        NVARCHAR(200) NULL,
//    [BuyerDocumentDate2]         NVARCHAR(50)  NULL,
//    [BuyerAddressRegistration2]  NVARCHAR(100) NULL,
//    [Buyer3]                     NVARCHAR(100) NULL,
//    [BuyerBirthDate3]            NVARCHAR(50)  NULL,
//    [BuyerDocumentSerialNumber3] NVARCHAR(50)  NULL,
//    [BuyerDocumentIssue3]        NVARCHAR(200) NULL,
//    [BuyerDocumentDate3]         NVARCHAR(50)  NULL,
//    [BuyerAddressRegistration3]  NVARCHAR(100) NULL
//);

