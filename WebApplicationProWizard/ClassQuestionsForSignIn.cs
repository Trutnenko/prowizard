﻿using System;
using System.Configuration;
using System.Data.SqlClient;

namespace WebApplicationProWizard.Сlasses
{
    public class ClassQuestionsForSignIn
    {
        public static void QuestionMethod(out string Question, out string Answer)
        {
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            Conn.Open();

            Random RandomId = new Random();
            var RandomIdValue = RandomId.Next(1, 3);

            SqlCommand CommandQuestion = Conn.CreateCommand();
            CommandQuestion.CommandText = "SELECT Question From dbo.TableQuestions Where IdQuestion = @ParmaIdQuestion";
            
            SqlParameter ParamFindAnswer = new SqlParameter();
            ParamFindAnswer.ParameterName = "@ParmaIdQuestion";
            ParamFindAnswer.Value = RandomIdValue.ToString();
            CommandQuestion.Parameters.Add(ParamFindAnswer);
            Question = Convert.ToString(CommandQuestion.ExecuteScalar());

            SqlCommand CommandAnswer = Conn.CreateCommand();
            CommandAnswer.CommandText = "SELECT Answer From dbo.TableQuestions Where IdQuestion = @ParmaIdQuestion";

            ParamFindAnswer = new SqlParameter();
            ParamFindAnswer.ParameterName = "@ParmaIdQuestion";
            ParamFindAnswer.Value = RandomIdValue.ToString();
            CommandAnswer.Parameters.Add(ParamFindAnswer);
            Answer = Convert.ToString(CommandAnswer.ExecuteScalar());

            Conn.Close();
        }      
    }
}