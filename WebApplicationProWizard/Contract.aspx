﻿<%@ Page
    Language="C#"
    AutoEventWireup="true"
    MaintainScrollPositionOnPostback="true"
    CodeBehind="Contract.aspx.cs"
    Inherits="WebApplicationProWizard.Word" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link rel="stylesheet" href="style.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div style="position: absolute; left: 0px; height: 1650px; width: 100%; top: 0px; background-color: #FFFFFF; z-index: 0;">
        </div>
        <div style="position: relative; height: 20px; top: 50px; margin: 0 auto; width: 1000px; z-index: 10;">
            <div style="position: fixed; margin: 0 auto; width: 50px;">
                <asp:ImageButton 
                    ID="ImageButtonClose"
                    runat="server" 
                    ImageUrl="~/Content/Close.png"
                    OnClick="LinkButtonToHome_Click"
                    Style="left:950px; position:absolute"
                    Height="30px" />
            </div>
        </div>
        <div style="position: fixed; left: 0px; width: 100%; height: 35px; top: 0px; background-color: #FFFFFF; z-index: 99999;">
            <div style="position: relative; left: 0px; margin: 0 auto; width: 1000px; top: 0px;">
                <asp:Label ID="LabelPreContract"
                    runat="server"
                    CssClass="Header1"
                    Style="color: #47525D;"
                    Text="Договор купли-продажи"> 
                </asp:Label>
            </div>
            <div style="position: absolute; width: 100%; top: 35px; height: 2px; background-color: #E8ECF0; left: 0px; z-index: 99998;">
            </div>
        </div>
        <div style="position: relative; left: 0px; margin: 0 auto; width: 1000px; height: 820px; top: 0px; background-color: #FFFFFF; z-index: 1;">
            <div style="position: absolute; top: 30px; width: 900px; height: 200px;">
                <asp:Label runat="server" CssClass="Header2">
                     Форма доступна для редактирования продавцу и покупателю
                          <br />
                          <br />
                     Покупателю - заполните серые поля
                          <br />
                     Продавцу - заполните желтые поля
                          <br />
                          <br />
                     После того как все поля будут заполнены сформируйте договор
                     <br />
                    <br />
                     Если в сделке несколько продавцов или покупателей, 
                     то напишите нам на Homga@Homga.ru - мы составим договор за вас
                </asp:Label>
            </div>
            <div style="position: absolute; top: 260px; width: 550px; height: 350px;">
                <asp:Label ID="Label1" runat="server" CssClass="Header0">
                    ДОГОВОР
                </asp:Label>
                <br />
                <br />
                <asp:Label runat="server" CssClass="Header0">
                     |
                </asp:Label>
                <asp:TextBox
                    ID="TextBoxContractDate"
                    placeholder="  Дата заключения договора"
                    runat="server"
                    Visible="True"
                    ViewStateMode="Enabled"
                    Width="150px"
                    CssClass="InputMain" 
                    ></asp:TextBox>
                <br />
                <br />
                <asp:Label runat="server" CssClass="Header0">
                     |
                </asp:Label>
                <asp:TextBox
                    ID="TextBoxContractCity"
                    placeholder="  Город заключения договора"
                    runat="server"
                    Visible="True"
                    ViewStateMode="Enabled"
                    Width="300px"
                    CssClass="InputMain" 
                    ></asp:TextBox>
                <br />
                <br />
                <asp:Label runat="server" CssClass="Header0" Style="color:#D2A800;">
                     |
                </asp:Label>
                <asp:TextBox
                    ID="TextBoxContractAddress"
                    placeholder="  Адрес квартиры"
                    runat="server"
                    Visible="True"
                    ViewStateMode="Enabled"
                    Width="300px"
                    CssClass="InputMain" 
                    ></asp:TextBox>
                <br />
                <br />
                <asp:Label runat="server" CssClass="Header0" Style="color:#D2A800;">
                     |
                </asp:Label>
                <asp:TextBox
                    ID="TextBoxContractCadastralNumber"
                    placeholder="  Кадастровый номер"
                    runat="server"
                    Visible="True"
                    ViewStateMode="Enabled"
                    Width="300px"
                    CssClass="InputMain" 
                    ></asp:TextBox>
            </div>
            <div style="position: absolute; top: 260px; left: 550px; width: 450px; height: 350px;">
                <br />
                <br />
                <br />
                <asp:Label runat="server" CssClass="Header0">
                     |
                </asp:Label>
                <asp:TextBox
                    ID="TextBoxContractCost"
                    placeholder="  Стоимость"
                    runat="server"
                    Visible="True"
                    ViewStateMode="Enabled"
                    Width="150px"
                    CssClass="InputMain" 
                    ></asp:TextBox>
                <br />
                <br />
                <asp:Label runat="server" CssClass="Header0">
                     |
                </asp:Label>
                <asp:TextBox
                    ID="TextBoxContractDeposit"
                    placeholder="  Размер аванса"
                    runat="server"
                    Visible="True"
                    ViewStateMode="Enabled"
                    Width="150px"
                    CssClass="InputMain" 
                    ></asp:TextBox>
                <br />
                <br />
                <asp:Label runat="server" CssClass="Header0" Style="color:#D2A800;">
                     |
                </asp:Label>
                <asp:TextBox ID="TextBoxContractFloor"
                    runat="server"
                    Width="75px"
                    Visible="True"
                    ViewStateMode="Enabled"
                    CssClass="InputMain" 
                    ></asp:TextBox>
                &nbsp;Этаж&nbsp;&nbsp;&nbsp;&nbsp;
                <br />
                <br />
                <asp:Label runat="server" CssClass="Header0" Style="color:#D2A800;">
                     |
                </asp:Label>
                <asp:TextBox ID="TextBoxContractRoom"
                    runat="server"
                    Width="75px"
                    Visible="True"
                    ViewStateMode="Enabled"
                    CssClass="InputMain" 
                    ></asp:TextBox>
                &nbsp;Количество комнат&nbsp;&nbsp;&nbsp;&nbsp;
                <br />
                <br />
                <asp:Label runat="server" CssClass="Header0" Style="color:#D2A800;">
                     |
                </asp:Label>
                <asp:TextBox ID="TextBoxContractArea"
                    runat="server"
                    Width="75px"
                    Visible="True"
                    ViewStateMode="Enabled"
                    CssClass="InputMain" 
                    ></asp:TextBox>
                &nbsp;Общая площадь
            </div>
            <div style="position: absolute; top: 650px; width: 550px; height: 250px;">
                <asp:Label
                    runat="server"
                    CssClass="Header0">
                    ПОКУПАТЕЛЬ
                </asp:Label>
                <div style="position: absolute; top: 50px; width: 400px; left: 0px; height: 125px;">
                <asp:Label runat="server" CssClass="Header0">
                     |
                </asp:Label>
                    <asp:TextBox
                        ID="TextBoxContractBuyer"
                        placeholder="  Фамилия, имя, отчество"
                        runat="server"
                        Visible="True"
                        ViewStateMode="Enabled"
                        Width="350px"
                        CssClass="InputMain" 
                        ></asp:TextBox>
                    <br />
                    <br />
                <asp:Label runat="server" CssClass="Header0">
                     |
                </asp:Label>
                    <asp:TextBox
                        ID="TextBoxContracBuyerAddressRegistration"
                        placeholder="  Адрес регистрации"
                        runat="server"
                        Visible="True"
                        ViewStateMode="Enabled"
                        Width="350px"
                        CssClass="InputMain" 
                        ></asp:TextBox>
                    <br />
                    <br />
                <asp:Label runat="server" CssClass="Header0">
                     |
                </asp:Label>
                    <asp:TextBox
                        ID="TextBoxContractBuyerBirthDate"
                        placeholder="  Дата рождения"
                        runat="server"
                        Visible="True"
                        ViewStateMode="Enabled"
                        Width="150px"
                        CssClass="InputMain" 
                        ></asp:TextBox>
                </div>
                <div style="position: absolute; top: 50px; left: 550px; width: 450px; height: 125px;">
                <asp:Label runat="server" CssClass="Header0">
                     |
                </asp:Label>
                    <asp:TextBox
                        ID="TextBoxContractBuyerDocumentIssue"
                        placeholder="  Паспорт: Кем выдан"
                        runat="server"
                        Visible="True"
                        ViewStateMode="Enabled"
                        Width="350px"
                        CssClass="InputMain" 
                        ></asp:TextBox>
                    <br />
                    <br />
                <asp:Label runat="server" CssClass="Header0">
                     |
                </asp:Label>
                    <asp:TextBox
                        ID="TextBoxContractBuyerDocumentDate"
                        placeholder="  Дата выдачи"
                        runat="server"
                        Visible="True"
                        ViewStateMode="Enabled"
                        Width="150px"
                        CssClass="InputMain" 
                        ></asp:TextBox>
                    <br />
                    <br />
                <asp:Label runat="server" CssClass="Header0">
                     |
                </asp:Label>
                    <asp:TextBox
                        ID="TextBoxContractBuyerDocumentSerialNumber"
                        placeholder="  Серия и номер"
                        runat="server"
                        Visible="True"
                        ViewStateMode="Enabled"
                        Width="150px"
                        CssClass="InputMain" 
                        ></asp:TextBox>
                </div>
            </div>
            <div style="position: absolute; top: 940px; width: 550px; height: 250px;">
                <asp:Label
                    runat="server"
                    CssClass="Header0">
                    ПРОДАВЕЦ
                </asp:Label>
                <div style="position: absolute; top: 50px; width: 400px; left: 0px; height: 125px;">
                <asp:Label runat="server" CssClass="Header0" Style="color:#D2A800;">
                     |
                </asp:Label>
                    <asp:TextBox
                        ID="TextBoxContractSeller"
                        placeholder="  Фамилия, имя, отчество"
                        runat="server"
                        Visible="True"
                        ViewStateMode="Enabled"
                        Width="350px"
                        CssClass="InputMain" 
                        ></asp:TextBox>
                    <br />
                    <br />
                <asp:Label runat="server" CssClass="Header0" Style="color:#D2A800;">
                     |
                </asp:Label>
                    <asp:TextBox
                        ID="TextBoxContracSellerAddressRegistration"
                        placeholder="  Адрес регистрации"
                        runat="server"
                        Visible="True"
                        ViewStateMode="Enabled"
                        Width="350px"
                        CssClass="InputMain" 
                        ></asp:TextBox>
                    <br />
                    <br />
                <asp:Label runat="server" CssClass="Header0" Style="color:#D2A800;">
                     |
                </asp:Label>
                    <asp:TextBox
                        ID="TextBoxContractSellerBirthDate"
                        placeholder="  Дата рождения"
                        runat="server"
                        Visible="True"
                        ViewStateMode="Enabled"
                        Width="150px"
                        CssClass="InputMain" 
                        ></asp:TextBox>
                </div>
                <div style="position: absolute; top: 50px; left: 550px; width: 450px; height: 125px;">
                <asp:Label runat="server" CssClass="Header0" Style="color:#D2A800;">
                     |
                </asp:Label>
                    <asp:TextBox
                        ID="TextBoxContractSellerDocumentIssue"
                        placeholder="  Паспорт: Кем выдан"
                        runat="server"
                        Visible="True"
                        ViewStateMode="Enabled"
                        Width="350px"
                        CssClass="InputMain" 
                        ></asp:TextBox>
                    <br />
                    <br />
                <asp:Label runat="server" CssClass="Header0" Style="color:#D2A800;">
                     |
                </asp:Label>
                    <asp:TextBox
                        ID="TextBoxContractSellerDocumentDate"
                        placeholder="  Дата выдачи"
                        runat="server"
                        Visible="True"
                        ViewStateMode="Enabled"
                        Width="150px"
                        CssClass="InputMain" 
                        ></asp:TextBox>
                    <br />
                    <br />
                <asp:Label runat="server" CssClass="Header0" Style="color:#D2A800;">
                     |
                </asp:Label>
                    <asp:TextBox
                        ID="TextBoxContractSellerDocumentSerialNumber"
                        placeholder="  Серия и номер"
                        runat="server"
                        Visible="True"
                        ViewStateMode="Enabled"
                        Width="150px"
                        CssClass="InputMain" 
                        ></asp:TextBox>
                </div>
            </div>
            <div style="position: absolute; top: 1230px; width: 550px; height: 250px;">
                <asp:Label
                    runat="server"
                    CssClass="Header1">
                    СВИДЕТЕЛЬСТВО О ПРАВЕ СОБСТВЕННОСТИ
                </asp:Label>
                <br />
                <asp:Label runat="server" CssClass="Header0" Style="color:#D2A800;">
                     |
                </asp:Label>
                <asp:TextBox
                    ID="TextBoxContractSertificateOfOwnerRegistrationNumber"
                    placeholder="  Запись о регистрации №"
                    runat="server"
                    Visible="True"
                    ViewStateMode="Enabled"
                    Width="300px"
                    CssClass="InputMain" 
                    ></asp:TextBox>
                <br />
                <br />
                <asp:Label runat="server" CssClass="Header0" Style="color:#D2A800;">
                     |
                </asp:Label>
                <asp:TextBox
                    ID="TextBoxContractSertificateOfOwnerShipDate"
                    placeholder="  Дата"
                    runat="server"
                    Visible="True"
                    ViewStateMode="Enabled"
                    Width="150px"
                    CssClass="InputMain" 
                    ></asp:TextBox>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <br />
                <br />
                <asp:Label runat="server" CssClass="Header0" Style="color:#D2A800;">
                     |
                </asp:Label>
                <asp:TextBox
                    ID="TextBoxContractSertificateOfOwnerSerialNumber"
                    placeholder="  Серия и номер"
                    runat="server"
                    Visible="True"
                    ViewStateMode="Enabled"
                    Width="150px"
                    CssClass="InputMain" 
                    ></asp:TextBox>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <br />
                <br />
            </div>
            <div style="position: absolute; top: 1230px; left: 550px; width: 450px; height: 350px;">
                <asp:Label
                    runat="server"
                    CssClass="Header1">
                    ДОКУМЕНТ ОСНОВАНИЕ
                </asp:Label>
                <br />
                <asp:Label runat="server" CssClass="Header0" Style="color:#D2A800;">
                     |
                </asp:Label>
                <asp:DropDownList
                    ID="DropDownListContractDocumentFoundationType"
                    runat="server"
                    Width="200px"
                    BorderWidth="2px" 
                    >
                    <asp:ListItem Value="Договор купли-продажи">Договор купли-продажи</asp:ListItem>
                    <asp:ListItem Value="Договор дарения">Договор дарения</asp:ListItem>
                    <asp:ListItem>Приватизация</asp:ListItem>
                    <asp:ListItem>Наследство</asp:ListItem>
                </asp:DropDownList>
                <br />
                <br />
                <asp:Label runat="server" CssClass="Header0" Style="color:#D2A800;">
                     |
                </asp:Label>
                <asp:TextBox
                    ID="TextBoxContractDocumentFoundationDate"
                    placeholder="  Дата"
                    runat="server"
                    Visible="True"
                    ViewStateMode="Enabled"
                    Width="150px"
                    CssClass="InputMain" 
                    ></asp:TextBox>
            </div>
            <div style="position: absolute; top: 1550px; height: 60px; width: 350px; left: 300px;">
                <asp:Button ID="ButtonSave"
                    runat="server"
                    OnClick="ButtonSave_Click"
                    CssClass="ButtonElse"
                    Text="Сохранить"
                    Width="140px" />
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="ButtonContract"
                    runat="server"
                    OnClick="ButtonContract_Click"
                    Text="Получить договор"
                    CssClass="ButtonMain"
                    Width="180px" />
            </div>
            <br />
        </div>
        <link href="https://cdn.jsdelivr.net/jquery.suggestions/16.10/css/suggestions.css" type="text/css" rel="stylesheet" />
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <!--[if lt IE 10]>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ajaxtransport-xdomainrequest/1.0.1/jquery.xdomainrequest.min.js"></script>
<![endif]-->
        <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.suggestions/16.10/js/jquery.suggestions.min.js"></script>
        <script type="text/javascript">
            $("#TextBoxContracBuyerAddressRegistration, #TextBoxContracSellerAddressRegistration, #TextBoxContractAddress").suggestions({
                serviceUrl: "https://suggestions.dadata.ru/suggestions/api/4_1/rs",
                token: "cd4443f5075044960d566d861e9096ec70aef82e",
                type: "ADDRESS",
                count: 5,
                onSelect: function (suggestion) {
                    console.log(suggestion);
                }
            });
        </script>
        <script type="text/javascript">
            $("#TextBoxContractCity").suggestions({
                serviceUrl: "https://suggestions.dadata.ru/suggestions/api/4_1/rs",
                token: "cd4443f5075044960d566d861e9096ec70aef82e",
                type: "ADDRESS",
                hint: false,
                bounds: "city",
                onSelect: function (suggestion) {
                    console.log(suggestion);
                }
            });
        </script>
        <script type="text/javascript">
            $("#TextBoxContractBuyer, #TextBoxContractSeller").suggestions({
                serviceUrl: "https://suggestions.dadata.ru/suggestions/api/4_1/rs",
                token: "cd4443f5075044960d566d861e9096ec70aef82e",
                type: "NAME",
                onSelect: function (suggestion) {
                    console.log(suggestion);
                }
            });
        </script>
    </form>
</body>
</html>
