﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SignIn.aspx.cs" Inherits="WebApplicationProWizard.SignIn" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link rel="stylesheet" href="style.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server" style="left: 0px; right: 0px; height: 100%; position: absolute; top: 0px; background-color: #FFFFFF;">
        <div style="position: absolute; width: 100%; height: 78px; top: 0px; background-color: #FFFFFF; z-index: 99999;">
            <div style="position: relative; width: 1000px; margin: 0 auto; height: 90px;">
                <div style="position: absolute; top: 5px; width: 100%; text-align: center;">
                    <asp:Label ID="LabelHomga"
                        runat="server"
                        CssClass="Header0"
                        Style="color: #0070C0; font-weight: 700;"
                        Text="Homga"
                        Font-Size="48px"></asp:Label>
                </div>
            </div>
            <div style="position: absolute; width: 100%; top: 78px; height: 2px; background-color: #E8ECF0; left: 0px; z-index: 99998;">
            </div>
            <div style="position: relative; width: 1000px; margin: 0 auto; height: 600px;">
                                <asp:ImageButton ID="ImageButtonClose"
                    runat="server" ImageUrl="~/Content/Close.png"
                    Style="position:absolute; right:0px;"
                    OnClick="LinkButtonToHome_Click"
                    Height="40px" />
                <div style="position: absolute; width: 400px; top: 100px; height: 400px; left: 300px; background-color: #282a3b; border-radius: 10px;">
                    <div style="position: absolute; left: 50px; width:350px;">
                        <asp:Panel ID="PanelSignIn"
                            runat="server"
                            Visible="True"
                            Style="position: absolute">
                            <br />
                            <br />
                            <br />
                            <br />
                            <asp:TextBox ID="TextBoxEmail"
                                runat="server"
                                placeholder="  E-mail"
                                TextMode="Email"
                                CssClass="InputLoginRegistration"
                                ViewStateMode="Enabled">
                            </asp:TextBox>
                            <br />
                            <br />
                            <asp:TextBox ID="TextBoxPasswort"
                                runat="server"
                                placeholder="  Пароль"
                                TextMode="Password"
                                CssClass="InputLoginRegistration"
                                ViewStateMode="Enabled">
                            </asp:TextBox>
                            <br />
                            <br />
                            <br />
                            <asp:LinkButton ID="LinkButtonSignIn"
                                runat="server"
                                OnClick="LinkButtonSign_Click"
                                CssClass="ButtonLoginRegistration"
                                Style="position: absolute; left: 0px; top: 200px;"
                                ViewStateMode="Enabled">Войти
                            </asp:LinkButton>
                            <br />
                            <br />
                            <br />
                            <br />
                            <asp:LinkButton ID="LinkButtonForNewPasswort"
                                runat="server"
                                OnClick="LinkButtonForNewPasswort_Click"
                                CssClass="ButtonLoginRegistrationElse"
                                Style="color: #FFFFFF; text-decoration: none;"
                                ViewStateMode="Disabled">Восстановить пароль
                            </asp:LinkButton>
                            <br />
                            <br />
                            <asp:Label ID="LabelInfoSignIn"
                                runat="server"
                                Text="LabelInfoSignIn"
                                ViewStateMode="Disabled"
                                CssClass="Header1"
                                Style="color: #FFFFFF"
                                Visible="False">
                            </asp:Label>
                        </asp:Panel>
                        <br />
                        <br />
                        <br />
                        <br />
                        <asp:Panel ID="PanelForRestore" runat="server"
                            Visible="False"
                            Width="175px"
                            Style="position: absolute">
                            <asp:TextBox ID="TextBoxEmailForRestore"
                                runat="server"
                                placeholder="  E-mail"
                                TextMode="Email"
                                CssClass="InputLoginRegistration"
                                ViewStateMode="Enabled">
                            </asp:TextBox>
                            <br />
                            <br />
                            <br />
                            <asp:LinkButton ID="LinkButtonSentNewPassport"
                                runat="server"
                                OnClick="LinkButtonSentNewPassport_Click"
                                CssClass="ButtonLoginRegistration"
                                ViewStateMode="Disabled">Отправить пароль
                            </asp:LinkButton>
                            <br />
                            <br />
                            <asp:LinkButton ID="LinkButtonBackToMain"
                                runat="server"
                                OnClick="LinkButtonBackToMain_Click"
                                CssClass="ButtonLoginRegistrationElse"
                                Style="text-decoration: none; color: #FFFFFF;"
                                ViewStateMode="Disabled">Назад
                            </asp:LinkButton>
                            <br />
                            <br />
                            <asp:Label ID="LabelInfoForRestore"
                                runat="server"
                                Text="LabelInfoSignIn"
                                Style="color: #FFFFFF"
                                CssClass="Header3"
                                ViewStateMode="Disabled"
                                Visible="False">
                            </asp:Label>
                        </asp:Panel>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
