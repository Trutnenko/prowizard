﻿<%@ Page
    Language="C#"
    AutoEventWireup="true"
    MaintainScrollPositionOnPostback="true"
    CodeBehind="Index.aspx.cs"
    Inherits="WebApplicationProWizard.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="yandex-verification" content="1e1106599aa5f12a" />
    <title>Купите недвижимость без лишних расходов
    </title>
    <meta name="description"
        content="Используйте Хомга чтобы правильно выбрать 
        недвижимость, проверить и оформить без переплат посредникам" />
    <link rel="stylesheet" href="style.css" type="text/css" />
    <link rel="icon" href="​/favicon.png" type="image/png" />
</head>

<body>
    <form id="form1" runat="server" style="left: 0px; right: 0px; height: 100%; position: absolute; top: 0px; background-color: #FBFBFB;">
        <div style="position: fixed; width: 100%; height: 35px; top: 0px; background-color: #FFFFFF; z-index: 99999;">
            <div style="position: relative; width: 1000px; margin: 0 auto; height: 90px;">
                <div style="position: absolute; width: 100%; text-align: center;">
                    <asp:Label ID="Label1"
                        runat="server"
                        CssClass="Header0"
                        Style="color: #0070C0; font-weight: 700; top: 20px;"
                        Text="Homga"
                        Font-Size="22px"></asp:Label>
                    <asp:LinkButton ID="LinkButtonPanelSignS"
                        runat="server"
                        OnClick="LinkButtonPanelSign_Click"
                        CssClass="ButtonSectionLoginRegistration"
                        ViewStateMode="Enabled"
                        Font-Size="16px"
                        Style="position: absolute; right: 50px; top: 8px;"
                        ForeColor="#0070C0">Войти</asp:LinkButton>

                </div>
            </div>
            <div style="position: absolute; width: 100%; top: 35px; height: 2px; background-color: #E8ECF0; left: 0px; z-index: 99998;">
            </div>
        </div>
        <div style="position: absolute; width: 100%; height: 78px; top: 0px; background-color: #FFFFFF; z-index: 99999;">
            <div style="position: relative; width: 1000px; margin: 0 auto; height: 90px;">
                <div style="position: absolute; top: 5px; width: 100%; text-align: center;">
                    <asp:Label ID="LabelHomga"
                        runat="server"
                        CssClass="Header0"
                        Style="color: #0070C0; font-weight: 700;"
                        Text="Homga"
                        Font-Size="48px"></asp:Label>
                    <asp:LinkButton ID="LinkButtonPanelSign"
                        runat="server"
                        OnClick="LinkButtonPanelSign_Click"
                        CssClass="ButtonSectionLoginRegistration"
                        ViewStateMode="Enabled"
                        Font-Size="16px"
                        Style="position: absolute; right: 50px; top: 25px; height: 18px;"
                        ForeColor="#0070C0">Войти</asp:LinkButton>
                </div>
            </div>
            <div style="position: absolute; width: 100%; top: 78px; height: 2px; background-color: #E8ECF0; left: 0px; z-index: 99998;">
            </div>
        </div>
        <div style="position: absolute; top: 80px; height: 800px; width: 100%;">
            <img src="content/2S.jpg" style="z-index: 0; width: 100%; height: 100%; object-fit: cover;" />
        </div>
        <div style="position: absolute; top: 80px; height: 810px; width: 100%;">
            <div style="position: relative; margin: 0 auto; top: 0px; width: 1000px; z-index: 100;">
                <div style="position: absolute; top: 35px; left: 0px; width: 500px; height: 200px;">
                    <asp:Label ID="LabelDetail"
                        runat="server"
                        CssClass="Header0"
                        Text="Купите недвижимость <br /> на вторичном рынке<br /> без лишних расходов">
                    </asp:Label>
                    <br />
                    <br />
                    <br />
                    <asp:Label ID="LabelBenefit"
                        runat="server"
                        CssClass="Header2"
                        Text="Используйте Хомга чтобы подобрать и проверить,<br/>получите документы и оформите сделку за 12 тыс. рублей">
                    </asp:Label>
                </div>
                <div style="position: absolute; top: 40px; height: 420px; left: 600px; width: 350px; background-color: #FFFFFF; border-radius: 10px;">
                    <br />
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                   <asp:LinkButton ID="LinkButtonPanelRegistration"
                       runat="server"
                       OnClick="LinkButtonPanelRegistration_Click"
                       CssClass="ButtonSectionLoginRegistration"
                       ViewStateMode="Enabled"
                       ForeColor="#285473"
                       Font-Size="16px">Пройти регистрацию и начать сделку</asp:LinkButton>
                    <br />
                    <br />
                    <asp:Panel ID="PanelRegistration"
                        runat="server"
                        Visible="True">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:TextBox ID="TextBoxEmailRegistration"
                            runat="server"
                            placeholder="  E-mail"
                            TextMode="Email"
                            CssClass="InputLoginRegistration"
                            ViewStateMode="Enabled">
                        </asp:TextBox>
                        <br />
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:TextBox ID="TextBoxPasswortRegistration"
                            runat="server"
                            placeholder="  Пароль"
                            TextMode="Password"
                            CssClass="InputLoginRegistration"
                            ViewStateMode="Enabled">
                        </asp:TextBox>
                        <br />
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="LabelWhat"
                            runat="server"
                            Style="color: #285473"
                            CssClass="Header3"
                            Text="Вопрос:">
                        </asp:Label>
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="LabelQuestion"
                            runat="server"
                            Style="color: #285473"
                            CssClass="Header3"
                            Text="Label">
                        </asp:Label>
                        <br />
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:TextBox ID="TextBoxAnswer"
                            runat="server"
                            placeholder="  Ответ"
                            CssClass="InputLoginRegistration"
                            ViewStateMode="Enabled">
                        </asp:TextBox>
                        <br />
                        <br />
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:LinkButton ID="LinkButtonRegistration"
                            runat="server"
                            OnClick="LinkButtonRegistration_Click"
                            CssClass="ButtonLoginRegistration"
                            Style="position: absolute; top: 320px;"
                            ViewStateMode="Enabled"> Ок
                        </asp:LinkButton>
                        <br />
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="LabelInfoRegistration"
                            runat="server"
                            CssClass="Header3"
                            Style="color: #285473; position: absolute; top: 380px;"
                            Text="LabelInfoRegistration"
                            Visible="False">
                        </asp:Label>
                    </asp:Panel>
                </div>
            </div>
        </div>
        <div style="position: absolute; top: 880px; width: 100%; background-color: #FFFFFF; height: 700px;">
            <div style="position: relative; margin: 0 auto; top: 0px; width: 1000px; text-align: left;">
                <br />
                <asp:LinkButton ID="LinkButtonOpenStepBuyer"
                    runat="server"
                    OnClick="LinkButtonOpenStepBuyer_Click"
                    CssClass="Header0"
                    ViewStateMode="Enabled"
                    ForeColor="#000000"
                    Style="position: absolute; left: 160px">Покупателю</asp:LinkButton>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:LinkButton ID="LinkButtonOpenStepSeller"
                    runat="server"
                    OnClick="LinkButtonOpenStepSeller_Click"
                    CssClass="Header0"
                    ViewStateMode="Enabled"
                    ForeColor="#000000"
                    Style="position: absolute; left: 400px">Продавцу</asp:LinkButton>
                <!--noindex-->
                <asp:Panel runat="server" ID="PanelBuyerAboutStep" Visible="True" Style="position: absolute; top: 100px;">
                    <img src="content/B1.png" style="position: absolute; left: 0px; top: 20px;" />
                    <img src="content/B2.png" style="position: absolute; left: 0px; top: 200px;" />
                    <img src="content/B3.png" style="position: absolute; left: 0px; top: 380px;" />
                    <div style="position: absolute; left: 160px; width: 700px; height: 200px; text-align: left; top: 0px;">
                        <p class="Header0" style="color: #000000; font-size: 25px;">
                            Выберите и забронируйте
                        </p>
                        <p class="Header2" style="color: #000000;">
                            Чтобы избежать мошенников используйте наши 
                                рекоммендации при поиске и выборе жилья
                                <br />
                            Получите от Хомга предварительный договор и подпишите с продавцом
                        </p>
                        <br />
                        <br />
                        <p class="Header0" style="color: #000000; font-size: 25px;">
                            Проверьте и подготовьтесь к сделке
                        </p>
                        <p class="Header2" style="color: #000000;">
                            Хомга вместе с Вами проверит документы продавца 
                                <br />
                            Получите от Хомга договор купли-продажи и условия передачи денег
                        </p>
                        <br />
                        <br />
                        <p class="Header0" style="color: #000000; font-size: 25px;">
                            Оформите сделку и получите ключи
                        </p>
                        <p class="Header2" style="color: #000000;">
                            Подпишите и отдайте документы на регистрацию
                                <br />
                            Хомга получит и проверит выписку ЕГРН
                                <br />
                            Получите от Хомга акт приемки-передачи и подпишите с продавцом    
                        </p>
                        <br />
                        <br />
                        <br />
                        <p style="color: #000000;" class="Header2">
                            Используйте по всей России при покупке на вторичном рынке квартир и долей/комнат.
                            <br />
                            Вам помогает специалист. Решайте вопросы через Telegram, WhatsApp или Viber
                            <br />
                            <br />
                            Ограничение: При покупке доли потребуется нотариальное оформление сделки.
                        </p>
                    </div>
                </asp:Panel>
                <asp:Panel runat="server" ID="PanelSellerAboutStep" Visible="False" Style="position: absolute; top: 100px;">
                    <img src="content/B4.png" style="position: absolute; left: 0px; top: 15px;" />
                    <img src="content/B5.png" style="position: absolute; left: 0px; top: 190px;" />
                    <img src="content/B6.png" style="position: absolute; left: 0px; top: 375px;" />
                    <div style="position: absolute; left: 160px; width: 700px; height: 200px; text-align: left; top: 0px;">
                        <p class="Header0" style="color: #000000; font-size: 25px;">
                            Получите аванс
                        </p>
                        <p class="Header2" style="color: #000000;">
                            Подпишите предварительный договор и заберите аванс
                        </p>
                        <br />
                        <br />
                        <p class="Header0" style="color: #000000; font-size: 25px;">
                            Пройдите проверки и подготовьтесь к сделке
                        </p>
                        <p class="Header2" style="color: #000000;">
                            Отправьте покупателю документы на проверку
                                <br />
                            Получите от Хомга договор купли-продажи и условия передачи денег
                        </p>
                        <br />
                        <br />
                        <p class="Header0" style="color: #000000; font-size: 25px;">
                            Оформите сделку и получите деньги
                        </p>
                        <p class="Header2" style="color: #000000;">
                            Подпишите с покупателем договор купли-продажи
                                <br />
                            Отдайте документы на регистрацию
                                <br />
                            Подпишите акт приемки-передачи и передайте ключи                         
                        </p>
                        <br />
                        <br />
                        <br />
                        <p style="color: #000000;" class="Header2">
                            Используйте по всей России при покупке на вторичном рынке квартир и долей/комнат.
                            <br />
                            Вам помогает специалист. Решайте вопросы через Telegram, WhatsApp или Viber
                            <br />
                            <br />
                            Ограничение: При покупке доли потребуется нотариальное оформление сделки.
                        </p>
                    </div>
                </asp:Panel>
                <!--/noindex-->
            </div>
        </div>
        <div style="position: absolute; top: 1780px; width: 100%; height: 50px; background-color: #0070C0;">
            <div style="position: relative; margin: 0 auto; top: 0px; width: 1000px; left: 0px;">
                <div style="position: absolute; top: 10px; left: 70px; width: 800px; text-align: left;">
                    <asp:LinkButton ID="LinkButtonCompanyAbout"
                        runat="server"
                        CssClass="Header2"
                        ViewStateMode="Enabled"
                        Style="font-size: 12px;"
                        ForeColor="White" OnClick="LinkButtonCompanyAbout_Click">О компании</asp:LinkButton>
                    &nbsp;
                    <asp:LinkButton ID="LinkButtonVacancy"
                        runat="server"
                        CssClass="Header2"
                        ViewStateMode="Enabled"
                        Style="font-size: 12px;"
                        ForeColor="White" OnClick="LinkButtonVacancy_Click">Вакансии</asp:LinkButton>
                    &nbsp;
                    <asp:LinkButton ID="LinkButtonPubOffer"
                        runat="server"
                        CssClass="Header2"
                        ViewStateMode="Enabled"
                        Style="font-size: 12px;"
                        ForeColor="White">Договор оферта</asp:LinkButton>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
