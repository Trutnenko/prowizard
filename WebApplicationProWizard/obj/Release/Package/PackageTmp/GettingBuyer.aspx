﻿<%@ Page
    Title=""
    Language="C#"
    MasterPageFile="~/Main.Master"
    AutoEventWireup="true"
    CodeBehind="GettingBuyer.aspx.cs"
    Inherits="WebApplicationProWizard.WebForms.Getting.WebFormGettingBuyer"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="GettingBuyer" runat="server">
    <link rel="stylesheet" href="style.css" type="text/css" />
    <div style="position: absolute; height: 100%; width: 100%; top: 72px; background-color: #FFFFFF; z-index: 0;">
    </div>
    <div style="position: relative; margin: 0 auto; width: 1000px; background-color: #FFFFFF;">
        <div style="position: absolute; top: 80px; height: 100px;">
            <asp:Label 
                runat="server" 
                ID="Label1" 
                CssClass="Header1">
                Выписка ЕГРН
            </asp:Label>
            <br />
            <br />
            <asp:Label 
                runat="server" 
                ID="Label2" 
                CssClass="Header2">
                Мы закажем из РосРеестра выписку ЕГРН и проверим что Вы стали собственником
            </asp:Label>
            <br />
            <br />
            <br />
            <asp:Label 
                runat="server" 
                ID="LabelMain" 
                CssClass="Header1">
                   Акт приемки-передачи
            </asp:Label>
            <br />
            <br />
            <asp:Label 
                runat="server" 
                ID="LabelAbout" 
                CssClass="Header2">
                    После проверки ЕГРН сформируйте акт приемки-передачи и подпишите с продавцом
            </asp:Label>
            <asp:LinkButton 
                ID="LinkButtonDownloadAcceptance"
                runat="server"
                OnClick="LinkButtonDownloadAcceptance_Click"
                Visible="False"
                CssClass="ButtonContract">Скачать</asp:LinkButton>
            <br />
            <br />
            <asp:LinkButton 
                ID="LinkButtonForwardToAcceptance"
                runat="server"
                OnClick="LinkButtonForwardToAcceptance_Click"
                Visible="True"
                CssClass="ButtonContract">Получить акт</asp:LinkButton>
        </div>
    </div>
</asp:Content>
