﻿<%@ Page
    Language="C#"
    AutoEventWireup="true"
    MaintainScrollPositionOnPostback="true"
    CodeBehind="Acceptance.aspx.cs"
    Inherits="WebApplicationProWizard.WebFormGettingDocument" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="style.css" type="text/css" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div style="position: absolute; left: 0px; height: 1050px; width: 100%; top: 0px; background-color: #FFFFFF; z-index: 0;">
        </div>
        <div style="position: relative; height: 20px; top: 50px; margin: 0 auto; width: 1000px; z-index: 10;">
            <div style="position: fixed; margin: 0 auto; width: 50px;">
                <asp:ImageButton
                    ID="ImageButtonClose"
                    runat="server"
                    ImageUrl="~/Content/Close.png"
                    OnClick="LinkButtonToHome_Click"
                    Style="left: 950px; position: absolute"
                    Height="30px" />
            </div>
        </div>
        <div style="position: fixed; left: 0px; width: 100%; height: 35px; top: 0px; background-color: #FFFFFF; z-index: 99999;">
            <div style="position: relative; left: 0px; margin: 0 auto; width: 1000px; top: 0px;">
                <asp:Label ID="LabelPreContract"
                    runat="server"
                    CssClass="Header1"
                    Style="color: #47525D;"
                    Text="Акт приемки-передачи"> 
                </asp:Label>
            </div>
            <div style="position: absolute; width: 100%; top: 35px; height: 2px; background-color: #E8ECF0; left: 0px; z-index: 99998;">
            </div>
        </div>
        <div style="position: absolute; left: 0px; height: 800px; width: 100%; top: 0px; background-color: #FFFFFF; z-index: 0;">
        </div>
        <div style="position: relative; left: 0px; margin: 0 auto; width: 1000px; top: 0px; z-index: 1;">
            <div style="position: absolute; top: 60px; width: 550px; height: 300px;">
                <asp:Label runat="server" CssClass="Header0">
                Квартира
                </asp:Label>
                <br />
                <br />
                <asp:Label runat="server" CssClass="Header0">
                     |
                </asp:Label>
                <asp:TextBox ID="TextBoxContractAddress"
                    placeholder="  Полный адрес квартиры с городом"
                    runat="server"
                    Width="350px"
                    Visible="True"
                    ViewStateMode="Enabled"
                    CssClass="InputMain"></asp:TextBox>
                <br />
                <br />
                <asp:Label runat="server" CssClass="Header0">
                     |
                </asp:Label>
                <asp:TextBox ID="TextBoxContractFloor"
                    runat="server"
                    Width="75px"
                    Visible="True"
                    ViewStateMode="Enabled"
                    CssClass="InputMain"></asp:TextBox>
                &nbsp;Этаж&nbsp;&nbsp;&nbsp;&nbsp;
                <br />
                <br />
                <asp:Label runat="server" CssClass="Header0">
                     |
                </asp:Label>
                <asp:TextBox ID="TextBoxContractRoom"
                    runat="server"
                    Width="75px"
                    Visible="True"
                    ViewStateMode="Enabled"
                    CssClass="InputMain"></asp:TextBox>
                &nbsp;Количество комнат&nbsp;&nbsp;&nbsp;&nbsp;
                <br />
                <br />
                <asp:Label runat="server" CssClass="Header0">
                     |
                </asp:Label>
                <asp:TextBox ID="TextBoxContractArea"
                    runat="server"
                    Width="75px"
                    Visible="True"
                    ViewStateMode="Enabled"
                    CssClass="InputMain"></asp:TextBox>
                &nbsp;Общая площадь
            </div>
            <div style="position: absolute; top: 420px; width: 550px; height: 250px;">
                <asp:Label runat="server" CssClass="Header0">
                 Покупатель
                </asp:Label>
                <br />
                <br />
                <asp:Label runat="server" CssClass="Header0">
                     |
                </asp:Label>
                <asp:TextBox ID="TextBoxContractBuyer"
                    placeholder="  Фамилия, имя, отчество"
                    runat="server"
                    Width="350px"
                    Visible="True"
                    ViewStateMode="Enabled"
                    CssClass="InputMain"></asp:TextBox>
                <br />
                <br />
                <asp:Label runat="server" CssClass="Header0">
                     |
                </asp:Label>
                <asp:TextBox ID="TextBoxContractBuyerBirthDate"
                    placeholder="  Дата рождения"
                    runat="server"
                    Width="150px"
                    Visible="True"
                    ViewStateMode="Enabled"
                    CssClass="InputMain"></asp:TextBox>
                <br />
                <br />
                <br />
            </div>
            <div style="position: absolute; left: 550px; right: 0px; top: 420px; width: 400px; height: 250px;">
                <asp:Label runat="server" CssClass="Header0">
                        Продавец
                </asp:Label>
                <br />
                <br />
                <asp:Label runat="server" CssClass="Header0" Style="color: #D2A800;">
                     |
                </asp:Label>
                <asp:TextBox ID="TextBoxContractSeller"
                    placeholder="  Фамилия, имя, отчество"
                    runat="server"
                    Width="350px"
                    Visible="True"
                    ViewStateMode="Enabled"
                    CssClass="InputMain"></asp:TextBox>
                <br />
                <br />
                <asp:Label runat="server" CssClass="Header0" Style="color: #D2A800;">
                     |
                </asp:Label>
                <asp:TextBox ID="TextBoxContractSellerBirthDate"
                    placeholder="  Дата рождения"
                    runat="server"
                    Width="150px"
                    Visible="True"
                    ViewStateMode="Enabled"
                    CssClass="InputMain"></asp:TextBox>
                <br />
                <br />
            </div>
            <div style="position: absolute; top: 700px; height: 60px; width: 100%; font-size: small;">
                <div style="position: absolute; height: 60px; width: 350px; left: 350px;">
                    <asp:Button ID="ButtonSave"
                        runat="server"
                        OnClick="ButtonSave_Click"
                        CssClass="ButtonElse"
                        Text="Сохранить"
                        Width="140px" />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="ButtonPreContract"
                        runat="server"
                        OnClick="ButtonAcceptance_Click"
                        Text="Получить акт"
                        CssClass="ButtonMain"
                        Width="180px" />
                </div>
            </div>
        </div>
        <link href="https://cdn.jsdelivr.net/jquery.suggestions/16.10/css/suggestions.css" type="text/css" rel="stylesheet" />
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <!--[if lt IE 10]>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ajaxtransport-xdomainrequest/1.0.1/jquery.xdomainrequest.min.js"></script>
<![endif]-->
        <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.suggestions/16.10/js/jquery.suggestions.min.js"></script>
        <script type="text/javascript">
            $("#TextBoxContractAddress").suggestions({
                serviceUrl: "https://suggestions.dadata.ru/suggestions/api/4_1/rs",
                token: "cd4443f5075044960d566d861e9096ec70aef82e",
                type: "ADDRESS",
                count: 5,
                onSelect: function (suggestion) {
                    console.log(suggestion);
                }
            });
        </script>
        <script type="text/javascript">
            $("#TextBoxContractCity").suggestions({
                serviceUrl: "https://suggestions.dadata.ru/suggestions/api/4_1/rs",
                token: "cd4443f5075044960d566d861e9096ec70aef82e",
                type: "ADDRESS",
                hint: false,
                bounds: "city",
                onSelect: function (suggestion) {
                    console.log(suggestion);
                }
            });
        </script>
        <script type="text/javascript">
            $("#TextBoxContractBuyer, #TextBoxContractSeller").suggestions({
                serviceUrl: "https://suggestions.dadata.ru/suggestions/api/4_1/rs",
                token: "cd4443f5075044960d566d861e9096ec70aef82e",
                type: "NAME",
                onSelect: function (suggestion) {
                    console.log(suggestion);
                }
            });
        </script>
    </form>
</body>
</html>
