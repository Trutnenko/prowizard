﻿<%@ Page
    Title=""
    Language="C#"
    MasterPageFile="~/Main.Master"
    AutoEventWireup="true"
    CodeBehind="Call.aspx.cs"
    Inherits="WebApplicationProWizard.WebForms.WebForm"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CallSeller" runat="server">
    <link rel="stylesheet" href="style.css" type="text/css" />
    <div style="position: absolute; height: 100%; width: 100%; top: 72px; background-color: #FFFFFF; z-index: 0;">
    </div>
    <div style="position: relative; margin: 0 auto; width: 1000px; background-color: #FFFFFF;">
        <div style="position: absolute; top: 80px;">
            <asp:Panel ID="PanelPrepare"
                runat="server"
                Visible="True">
                <asp:Label
                    ID="LabelMain"
                    CssClass="Header1"
                    runat="server">
                 Для продолжения потребуется 
                 подключить продавца                
                </asp:Label>
            </asp:Panel>
        </div>
        <div style="position: absolute; top: 150px; height: 80px;">
            <asp:Panel CssClass="Header2" ID="PanelConnect" runat="server" Visible="True">
                <asp:Label runat="server" ID="LabelAbout">
                    1)	Попросите продавца пройти регистрацию в системе <br /> 
                    2)	Уточните логин (email) продавца <br /> 
                    3)	Укажите логин в этой форме и продолжите сделку <br /> 
                </asp:Label>
            </asp:Panel>
        </div>
        <div style="position: absolute; top: 280px; height: 80px;">
            <asp:TextBox ID="TextBoxEmailSeller"
                placeholder="E-mail продавца"
                runat="server"
                TextMode="Email"
                Visible="False"
                Width="250px"
                CssClass="InputMain"></asp:TextBox>
            &nbsp;&nbsp;
                <asp:LinkButton ID="LinkButtonFindSeller"
                    runat="server"
                    OnClick="LinkButtonFindSeller_Click"
                    Visible="False"
                    CssClass="ButtonElse">
                    Найти
                </asp:LinkButton>
            <asp:Label ID="LabelInformation"
                runat="server"
                Text="LabelInformation"
                Visible="False"
                CssClass="HeaderReminder"></asp:Label>
        </div>
        <div style="position: absolute; top: 410px; height: 60px; width: 350px; left: 350px;">
            <asp:Button ID="ButtonSendInvitation"
                runat="server"
                Text="Отправить приглашение"
                OnClick="ButtonSendInvitation_Click"
                Width="180px"
                CssClass="ButtonElse" Visible="False" />
            <asp:Button ID="ButtonForward"
                runat="server"
                Text="Продолжить"
                OnClick="ButtonForward_Click"
                Width="140px"
                CssClass="ButtonMain"
                Visible="False" />
        </div>
    </div>
</asp:Content>
