﻿<%@ Page
    Title="Проведение сделки"
    Language="C#"
    MasterPageFile="~/Main.Master"
    AutoEventWireup="true"
    CodeBehind="DealBuyer.aspx.cs"
    Inherits="WebApplicationProWizard.WebForms.Deal.WebFormDealBuyer"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="DealBuyer" runat="server">
    <link rel="stylesheet" href="style.css" type="text/css" />
    <div style="position: absolute; height: 800px; width: 100%; top: 72px; background-color: #FFFFFF; z-index: 0;">
    </div>
    <div style="position: relative; margin: 0 auto; width: 1000px; background-color: #FFFFFF;">
        <div style="position: absolute; top: 80px; width: 100%; height: 400px;">
                <asp:Label ID="LabelStageClose"
                    runat="server"
                    Text="Этап завершен"
                    ForeColor="#525252"
                    CssClass="Header1"
                    Visible="False">
                </asp:Label>
                <asp:LinkButton ID="LinkButtonPanel1"
                        runat="server"
                        OnClick="LinkButtonPanel1_Click"
                        CssClass="Header1"
                        ViewStateMode="Enabled"
                        >Покупка квартиры целиком у одного продавца</asp:LinkButton>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:LinkButton ID="LinkButtonPanel2"
                        runat="server"
                        OnClick="LinkButtonPanel2_Click"
                        CssClass="Header1"
                        ViewStateMode="Enabled"
                        >Покупка доли/у нескольких продавцов</asp:LinkButton>
                <br />
                <br />
                <div style="position: absolute; width: 900px; text-align: left; top:80px;">
                    <asp:Panel 
                        ID="Panel1" 
                        runat="server" 
                        Visible="True">
                        <asp:Label ID="LabelAbout1" runat="server" CssClass="Header1">
                        Если вы покупаете квартиру целиком у одного покупателя
                        </asp:Label>
                        <br />
                        <asp:Label ID="LabelAboutDetail1" runat="server" CssClass="Header2">
                        1. Заложите деньги в ячейку. Используйте сервисы на выбор: <br />
                        2. Только после выполнения п.1 подпишите с продавцом договор купли-продажи <br />
                        3. Отдайте через МФЦ документы на регистрацию. МФЦ в городах: Москва, Санкт-Петербург,
                            Новосибирск, Екатеринбург, Нижний Новгород, Казань
                        </asp:Label>                      
                    </asp:Panel>
                    <asp:Panel 
                        ID="Panel2" 
                        runat="server" 
                        Visible="False">
                        <asp:Label ID="Label5" runat="server" CssClass="Header1">
                        Если вы покупаете квартиру комнату или долю в квартире
                        </asp:Label>
                        <br />
                        <asp:Label ID="Label6" runat="server" CssClass="Header2">
                        1. Подробное описание <br />
                        2. Подробное описание <br />
                        </asp:Label>
                        <br />
                        <br />
                    </asp:Panel>
                </div>
            </div>
        <div style="position: absolute; top: 700px; height: 60px; width: 350px; left: 350px;">
            <asp:Panel ID="PanelReminder"
                runat="server"
                EnableViewState="False"
                ViewStateMode="Disabled"
                Visible="False"
                Width="400px"
                CssClass="HeaderReminder">
                Этап будет заблокирован! Продолжить?&nbsp;&nbsp;
                <br />
                <br />
                <asp:Button ID="ButtonToBack"
                    runat="server"
                    Text="&lt; Назад"
                    OnClick="ButtonToBack_Click"
                    CssClass="ButtonElse"
                    Width="140px" />
                &nbsp;&nbsp;
         <asp:Button ID="ButtonForce"
             runat="server"
             OnClick="ButtonForce_Click"
             CssClass="ButtonMain"
             Text="Продолжить >"
             Width="140px" />
            </asp:Panel>
        </div>
        <div style="position: absolute; top: 700px; height: 60px; width: 350px; left: 350px;">
            <asp:Panel ID="PanelForward"
                runat="server"
                EnableViewState="False"
                ViewStateMode="Disabled"
                Visible="True"
                CssClass="HeaderReminder"
                Width="600px">
                &nbsp;
                                <asp:Button ID="ButtonForward"
                                    runat="server"
                                    OnClick="ButtonForward_Click"
                                    Style="background-color: white;"
                                    Text="Продолжить &gt;"
                                    CssClass="ButtonMain"
                                    Width="140px" />
            </asp:Panel>
        </div>
    </div>
</asp:Content>
