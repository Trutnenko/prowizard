﻿<%@ Page
    Title=""
    Language="C#"
    MasterPageFile="~/Main.Master"
    AutoEventWireup="true"
    CodeBehind="DocumentsBuyer.aspx.cs"
    Inherits="WebApplicationProWizard.WebForms.MainDocuments.WebFormMainDocumentsBuyer"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainDocumentBuyer" runat="server">
    <link rel="stylesheet" href="style.css" type="text/css" />
    <div style="position: absolute; height: 850px; width: 100%; top: 72px; background-color: #FFFFFF; z-index: 0;">
    </div>
    <div style="position: relative; margin: 0 auto; width: 1000px; background-color: #FFFFFF;">
        <div style="position: absolute; top: 80px; width: 100%; height: 130px;">
            <asp:Panel ID="PanelCommonInformation"
                runat="server"
                EnableViewState="False"
                ViewStateMode="Disabled">
                <asp:Label
                    runat="server"
                    ID="LabelAbout"
                    CssClass="Header2">
                        Попросите продавца прислать документы и проверьте их при помощи наших подсказок. <br />                
                        Если продавцов несколько, то вам должны прислать документы по всем продавцам.
                </asp:Label>
            </asp:Panel>
            <asp:Panel ID="PanelCloseForm"
                runat="server"
                Visible="False">
                <asp:Label ID="Label2"
                    runat="server"
                    Text="Этап завершен"
                    ForeColor="#525252"
                    CssClass="Header1"> </asp:Label>
            </asp:Panel>
        </div>
        <div style="position: absolute; top: 200px; width: 500px; height: 600px;">
            <asp:LinkButton ID="LinkButtonPassport"
                runat="server"
                OnClick="LinkPassport_Click"
                CssClass="Document">
                    Паспорт</asp:LinkButton>
            &nbsp;&nbsp;
                <asp:Label ID="LabelPassport"
                    runat="server"
                    CssClass="Status"></asp:Label>
            <br />
            <br />
            <asp:LinkButton ID="LinkButtonSertificateOfOwnerShip"
                runat="server"
                OnClick="LinkButtonSertificateOfOwnerShip_Click"
                CssClass="Document">Свидетельство</asp:LinkButton>
            &nbsp;&nbsp;
                <asp:Label ID="LabelSertificateOfOwnerShip"
                    runat="server"
                    CssClass="Status"></asp:Label>
            <br />
            <br />
            <asp:LinkButton ID="LinkButtonDocumentFoundation"
                runat="server"
                OnClick="LinkButtonDocumentFoundation_Click"
                CssClass="Document">Документ основание</asp:LinkButton>
            &nbsp;&nbsp;
                <asp:Label ID="LabelDocumentFoundation"
                    runat="server"
                    CssClass="Status"></asp:Label>
            <br />
            <br />
            <asp:LinkButton ID="LinkButtonExtractFromRegister"
                runat="server"
                OnClick="LinkButtonExtractFromRegister_Click"
                CssClass="Document">
                        Выписка ЕГРП</asp:LinkButton>
            &nbsp;&nbsp;
                <asp:Label ID="LabelExtractFromRegister"
                    runat="server"
                    CssClass="Status"></asp:Label>
            <br />
            <br />
            <asp:LinkButton ID="LinkButtonExtendedExtractFromRegister"
                runat="server"
                OnClick="LinkButtonExtendedExtractFromRegister_Click"
                CssClass="Document">Расширенная выписка</asp:LinkButton>
            &nbsp;&nbsp;
                <asp:Label ID="LabelExtendedExtractFromRegister"
                    runat="server"
                    Text="LabelExtendedExtractFromRegister"
                    CssClass="Status"></asp:Label>
            <br />
            <br />
            <asp:LinkButton ID="LinkButtonTechnicalPassport"
                runat="server"
                OnClick="LinkButtonTechnicalPassport_Click"
                CssClass="Document">Технический паспорт</asp:LinkButton>
            &nbsp;&nbsp;
                <asp:Label ID="LabelTechnicalPassport"
                    runat="server"
                    Text="LabelTechnicalPassport"
                    CssClass="Status"></asp:Label>
            <br />
            <br />
            <asp:LinkButton ID="LinkButtonConsentOfSpouse"
                runat="server"
                OnClick="LinkButtonConsentOfSpouse_Click"
                CssClass="Document">Согласие супруга</asp:LinkButton>
            &nbsp;&nbsp;
                <asp:Label ID="LabelConsentOfSpouse"
                    runat="server"
                    Text="LabelConsentOfSpouse"
                    CssClass="Status"></asp:Label>
            <br />
            <br />
            <asp:LinkButton ID="LinkButtonResolutionOfGuardianship"
                runat="server"
                OnClick="LinkButtonResolutionOfGuardianship_Click"
                CssClass="Document">Разрешение опеки</asp:LinkButton>
            &nbsp;&nbsp;
                <asp:Label ID="LabelResolutionOfGuardianship"
                    runat="server"
                    Text="LabelResolutionOfGuardianship"
                    CssClass="Status"></asp:Label>
            <br />
            <br />
            <asp:LinkButton ID="LinkButtonDebtForUtility"
                runat="server"
                OnClick="LinkButtonDebtForUtility_Click"
                CssClass="Document">Справка по КУ</asp:LinkButton>
            &nbsp;&nbsp;
                <asp:Label ID="LabelDebtForUtility"
                    runat="server"
                    CssClass="Status"
                    Text="LabelDebtForUtility"></asp:Label>
            <br />
            <br />
            <asp:LinkButton ID="LinkButtonCertificateFromTaxInspection"
                runat="server"
                OnClick="LinkButtonCertificateFromTaxInspection_Click"
                CssClass="Document">Справка по налогам</asp:LinkButton>
            &nbsp;&nbsp;
                <asp:Label ID="LabelCertificateFromTaxInspection"
                    runat="server"
                    CssClass="Status"
                    Text="LabelCertificateFromTaxInspection"></asp:Label>
        </div>
        <div style="position: absolute; top: 200px; left: 500px; width: 450px; height: 600px;">
            <asp:Panel ID="PanelDenide"
                runat="server"
                ViewStateMode="Disabled"
                Visible="False"
                EnableViewState="False"
                CssClass="Header2">
                Вы отклонили документ, для решения вопроса 
                    свяжитесь с продавцом и попросите 
                    прислать корректный документ
            </asp:Panel>
            <asp:Panel ID="PanelWaiting"
                runat="server"
                ViewStateMode="Disabled"
                Visible="False"
                EnableViewState="False"
                CssClass="Header2">
                Документ ожидается от продавца
            </asp:Panel>
            <asp:Panel ID="PanelDenideClose"
                runat="server"
                ViewStateMode="Disabled"
                Visible="False"
                EnableViewState="False"
                CssClass="Header2">
                Вы отклонили документ
            </asp:Panel>
            <asp:Panel ID="PanelWaitingClose"
                runat="server"
                ViewStateMode="Disabled"
                Visible="False"
                EnableViewState="False"
                CssClass="Header2">
                Документ не был прислан продавцом
            </asp:Panel>
            <asp:Panel ID="PanelPassport"
                runat="server"
                ViewStateMode="Enabled"
                Visible="False">
                <asp:LinkButton ID="LinkButtonDownLoadPassport"
                    runat="server"
                    OnClick="LinkButtonDownLoadPassport_Click"
                    EnableViewState="False"
                    ViewStateMode="Disabled"
                    CssClass="ButtonInDocument">Скачать</asp:LinkButton>
                &nbsp;&nbsp;
                    <asp:LinkButton ID="LinkButtonDenidePassport"
                        runat="server"
                        OnClick="LinkButtonDenidePassport_Click"
                        EnableViewState="False"
                        ViewStateMode="Disabled"
                        CssClass="ButtonInDocument">Отклонить</asp:LinkButton>
                &nbsp;&nbsp;
                    <asp:LinkButton ID="LinkButtonSavePassport"
                        runat="server"
                        OnClick="LinkButtonSavePassport_Click"
                        EnableViewState="False"
                        CssClass="ButtonInDocument"
                        ViewStateMode="Disabled">Сохранить</asp:LinkButton>
                <br />
                <br />
                <asp:CheckBox ID="CheckBoxPassportOwner"
                    runat="server"
                    Text=" Паспорт предъявлен собственником"
                    ViewStateMode="Disabled"
                    EnableViewState="False"
                    CssClass="Header2" />
                <br />
                <br />
                <asp:CheckBox ID="CheckBoxPassportActual"
                    runat="server"
                    Text=" Паспорт действителен"
                    ViewStateMode="Disabled"
                    EnableViewState="False"
                    CssClass="Header2" />
                <br />
                <br />
                <asp:CheckBox ID="CheckBoxPassportSpouse"
                    runat="server"
                    Text=" Требуется согласие супруга"
                    ViewStateMode="Disabled"
                    EnableViewState="False"
                    CssClass="Header2" />
            </asp:Panel>
            <asp:Panel ID="PanelSertificateOfOwnerShip"
                runat="server"
                ViewStateMode="Enabled"
                Visible="False">
                <asp:LinkButton ID="LinkButtonDownLoadSertificateOfOwnerShip"
                    runat="server"
                    OnClick="LinkButtonDownLoadSertificateOfOwnerShip_Click"
                    EnableViewState="False"
                    CssClass="ButtonInDocument"
                    ViewStateMode="Disabled">Скачать</asp:LinkButton>
                &nbsp;&nbsp;
                    <asp:LinkButton ID="LinkButtonDenideSertificateOfOwnerShip"
                        runat="server"
                        OnClick="LinkButtonDenideSertificateOfOwnerShip_Click"
                        EnableViewState="False"
                        ViewStateMode="Disabled"
                        CssClass="ButtonInDocument">Отклонить</asp:LinkButton>
                &nbsp;&nbsp;
                    <asp:LinkButton ID="LinkButtonSaveSertificateOfOwnerShip"
                        runat="server"
                        OnClick="LinkButtonSaveSertificateOfOwnerShip_Click"
                        EnableViewState="False"
                        CssClass="ButtonInDocument"
                        ViewStateMode="Disabled">Сохранить</asp:LinkButton>
                <br />
                <br />
                <asp:CheckBox ID="CheckBoxSertificateOfOwnerShipPassport"
                    runat="server"
                    Text=" Паспортные данные совпадают"
                    ViewStateMode="Disabled"
                    EnableViewState="False"
                    CssClass="Header2" />
                <br />
                <br />
                <asp:CheckBox ID="CheckBoxSertificateOfOwnerShipAddress"
                    runat="server"
                    Text=" Совпадает адрес"
                    ViewStateMode="Disabled"
                    EnableViewState="False"
                    CssClass="Header2" />
                <br />
                <br />
                <asp:CheckBox ID="CheckBoxSertificateOfOwnerShipBurden"
                    runat="server"
                    Text=" Обремения отсутствуют"
                    ViewStateMode="Disabled"
                    EnableViewState="False"
                    CssClass="Header2" />
            </asp:Panel>
            <asp:Panel ID="PanelDocumentFoundation"
                runat="server"
                ViewStateMode="Enabled"
                Visible="False">
                <asp:LinkButton ID="LinkButtonDownLoadDocumentFoundation"
                    runat="server"
                    OnClick="LinkButtonDownLoadDocumentFoundation_Click"
                    EnableViewState="False"
                    CssClass="ButtonInDocument"
                    ViewStateMode="Disabled">Скачать</asp:LinkButton>
                &nbsp;&nbsp;
                    <asp:LinkButton ID="LinkButtonDenideDocumentFoundation"
                        runat="server"
                        OnClick="LinkButtonDenideDocumentFoundation_Click"
                        EnableViewState="False"
                        CssClass="ButtonInDocument"
                        ViewStateMode="Disabled">Отклонить</asp:LinkButton>
                &nbsp;&nbsp;
                    <asp:LinkButton ID="LinkButtonSaveDocumentFoundation"
                        runat="server"
                        OnClick="LinkButtonSaveDocumentFoundation_Click"
                        EnableViewState="False"
                        CssClass="ButtonInDocument"
                        ViewStateMode="Disabled">Сохранить</asp:LinkButton>
                <br />
                <br />
                <asp:CheckBox ID="CheckBoxDocumentFoundationPassport"
                    runat="server"
                    Text=" Паспортные данные совпадают"
                    ViewStateMode="Disabled"
                    EnableViewState="False"
                    CssClass="Header2" />
                <br />
                <br />
                <asp:CheckBox ID="CheckBoxDocumentFoundationAddress"
                    runat="server"
                    Text=" Адрес совпадает"
                    ViewStateMode="Disabled"
                    EnableViewState="False"
                    CssClass="Header2" />
                <br />
                <br />
                <asp:CheckBox ID="CheckBoxDocumentFoundationBurden"
                    runat="server"
                    Text=" Обременения отсутствуют"
                    ViewStateMode="Enabled"
                    CssClass="Header2" />
            </asp:Panel>
            <asp:Panel ID="PanelExtractFromRegister"
                runat="server"
                ViewStateMode="Enabled"
                Visible="False">
                <asp:LinkButton ID="LinkButtonDownLoadExtractFromRegister"
                    runat="server"
                    OnClick="LinkButtonDownLoadExtractFromRegister_Click"
                    EnableViewState="False"
                    CssClass="ButtonInDocument"
                    ViewStateMode="Disabled">Скачать</asp:LinkButton>
                &nbsp;&nbsp;
                    <asp:LinkButton ID="LinkButtonDenideExtractFromRegister"
                        runat="server"
                        OnClick="LinkButtonDenideExtractFromRegister_Click"
                        EnableViewState="False"
                        CssClass="ButtonInDocument"
                        ViewStateMode="Disabled">Отклонить</asp:LinkButton>
                &nbsp;&nbsp;
                    <asp:LinkButton ID="LinkButtonSaveExtractFromRegister"
                        runat="server"
                        OnClick="LinkButtonSaveExtractFromRegister_Click"
                        EnableViewState="False"
                        CssClass="ButtonInDocument"
                        ViewStateMode="Disabled">Сохранить</asp:LinkButton>
                <br />
                <br />
                <asp:CheckBox ID="CheckBoxExtractFromRegisterPassport"
                    runat="server"
                    Text=" Паспортные данные совпадают"
                    ViewStateMode="Disabled"
                    EnableViewState="False"
                    CssClass="Header2" />
                <br />
                <br />
                <asp:CheckBox ID="CheckBoxExtractFromRegisterCharacteristic"
                    runat="server"
                    Text=" Параметры квартиры корректные"
                    ViewStateMode="Disabled"
                    EnableViewState="False"
                    CssClass="Header2" />
                <br />
                <br />
                <asp:CheckBox ID="CheckBoxExtractFromRegisterBurden"
                    runat="server"
                    Text=" Обременения отсутствуют"
                    ViewStateMode="Disabled"
                    EnableViewState="False"
                    CssClass="Header2" />
            </asp:Panel>
            <asp:Panel ID="PanelExtendedExtractFromRegister"
                runat="server"
                ViewStateMode="Enabled"
                Visible="False">
                <asp:LinkButton ID="LinkButtonDownLoadExtendedExtractFromRegister"
                    runat="server"
                    OnClick="LinkButtonDownLoadExtendedExtractFromRegister_Click"
                    EnableViewState="False"
                    CssClass="ButtonInDocument"
                    ViewStateMode="Disabled">Скачать</asp:LinkButton>
                &nbsp;&nbsp;
                    <asp:LinkButton ID="LinkButtonDenideExtendedExtractFromRegister"
                        runat="server"
                        OnClick="LinkButtonDenideExtendedExtractFromRegister_Click"
                        EnableViewState="False"
                        CssClass="ButtonInDocument"
                        ViewStateMode="Disabled">Отклонить</asp:LinkButton>
                &nbsp;&nbsp;
                    <asp:LinkButton ID="LinkButtonSaveExtendedExtractFromRegister"
                        runat="server"
                        OnClick="LinkButtonSaveExtendedExtractFromRegister_Click"
                        EnableViewState="False"
                        CssClass="ButtonInDocument"
                        ViewStateMode="Disabled">Сохранить</asp:LinkButton>
                <br />
                <br />
                <asp:CheckBox ID="CheckBoxExtendedExtractFromRegisterPeople"
                    runat="server"
                    Text=" Никто не прописан"
                    ViewStateMode="Disabled"
                    EnableViewState="False"
                    CssClass="Header2" />
                <br />
                <br />
                <asp:CheckBox ID="CheckBoxExtendedExtractFromRegisterMinors"
                    runat="server"
                    Text=" Права несовершеннолетних не нарушены"
                    ViewStateMode="Disabled"
                    EnableViewState="False"
                    CssClass="Header2" />
                <br />
                <br />
                <asp:CheckBox ID="CheckBoxExtendedExtractFromRegisterTemporarilyВrawn"
                    runat="server"
                    Text=" Нет временно выписанных граждан"
                    ViewStateMode="Disabled"
                    EnableViewState="False"
                    CssClass="Header2" />
            </asp:Panel>
            <asp:Panel ID="PanelTechnicalPassport"
                runat="server"
                ViewStateMode="Enabled"
                Visible="False">
                <asp:LinkButton ID="LinkButtonDownLoadTechnicalPassport"
                    runat="server"
                    OnClick="LinkButtonDownLoadTechnicalPassport_Click"
                    EnableViewState="False"
                    CssClass="ButtonInDocument"
                    ViewStateMode="Disabled">Скачать</asp:LinkButton>
                &nbsp;&nbsp;
                    <asp:LinkButton ID="LinkButtonDenideTechnicalPassport"
                        runat="server"
                        OnClick="LinkButtonDenideTechnicalPassport_Click"
                        EnableViewState="False"
                        CssClass="ButtonInDocument"
                        ViewStateMode="Disabled">Отклонить</asp:LinkButton>
                &nbsp;&nbsp;
                    <asp:LinkButton ID="LinkButtonSaveTechnicalPassport"
                        runat="server"
                        OnClick="LinkButtonSaveTechnicalPassport_Click"
                        EnableViewState="False"
                        CssClass="ButtonInDocument"
                        ViewStateMode="Disabled">Сохранить</asp:LinkButton>
                <br />
                <br />
                <asp:CheckBox ID="CheckBoxTechnicalPassportCharacteristic"
                    runat="server"
                    Text=" Параметры квартиры корректные"
                    ViewStateMode="Disabled"
                    EnableViewState="False"
                    CssClass="Header2" />
            </asp:Panel>
            <asp:Panel ID="PanelConsentOfSpouse"
                runat="server"
                ViewStateMode="Enabled"
                Visible="False">
                <asp:LinkButton ID="LinkButtonDownLoadConsentOfSpouse"
                    runat="server"
                    OnClick="LinkButtonDownLoadConsentOfSpouse_Click"
                    EnableViewState="False"
                    CssClass="ButtonInDocument"
                    ViewStateMode="Disabled">Скачать</asp:LinkButton>
                &nbsp;&nbsp;
                    <asp:LinkButton ID="LinkButtonDenideConsentOfSpouse"
                        runat="server"
                        OnClick="LinkButtonDenideConsentOfSpouse_Click"
                        EnableViewState="False"
                        CssClass="ButtonInDocument"
                        ViewStateMode="Disabled">Отклонить</asp:LinkButton>
                &nbsp;&nbsp;
                    <asp:LinkButton ID="LinkButtonSaveConsentOfSpouse"
                        runat="server"
                        OnClick="LinkButtonSaveConsentOfSpouse_Click"
                        EnableViewState="False"
                        CssClass="ButtonInDocument"
                        ViewStateMode="Disabled">Сохранить</asp:LinkButton>
                <br />
                <br />
                <asp:CheckBox ID="CheckBoxConsentOfSpouseCharacteristic"
                    runat="server"
                    Text=" Паспортные данные совпадают"
                    ViewStateMode="Disabled"
                    EnableViewState="False"
                    CssClass="Header2" />
            </asp:Panel>
            <asp:Panel ID="PanelResolutionOfGuardianship"
                runat="server"
                ViewStateMode="Enabled"
                Visible="False">
                <asp:LinkButton ID="LinkButtonDownLoadResolutionOfGuardianship"
                    runat="server"
                    OnClick="LinkButtonDownLoadResolutionOfGuardianship_Click"
                    EnableViewState="False"
                    CssClass="ButtonInDocument"
                    ViewStateMode="Disabled">Скачать</asp:LinkButton>
                &nbsp;&nbsp;
                    <asp:LinkButton ID="LinkButtonDenideResolutionOfGuardianship"
                        runat="server"
                        OnClick="LinkButtonDenideResolutionOfGuardianship_Click"
                        EnableViewState="False"
                        CssClass="ButtonInDocument"
                        ViewStateMode="Disabled">Отклонить</asp:LinkButton>
                &nbsp;&nbsp;
                    <asp:LinkButton ID="LinkButtonSaveResolutionOfGuardianship"
                        runat="server"
                        OnClick="LinkButtonSaveResolutionOfGuardianship_Click"
                        EnableViewState="False"
                        CssClass="ButtonInDocument"
                        ViewStateMode="Disabled">Сохранить</asp:LinkButton>
                <br />
                <br />
                <asp:CheckBox ID="CheckBoxResolutionOfGuardianshipCharacteristic"
                    runat="server"
                    Text=" Паспортные данные совпадают"
                    ViewStateMode="Disabled"
                    EnableViewState="False"
                    CssClass="Header2" />
            </asp:Panel>
            <asp:Panel ID="PanelDebtForUtility"
                runat="server"
                ViewStateMode="Enabled"
                Visible="False">
                <asp:LinkButton ID="LinkButtonDownLoadDebtForUtility"
                    runat="server"
                    OnClick="LinkButtonDownLoadDebtForUtility_Click"
                    CssClass="ButtonInDocument"
                    EnableViewState="False"
                    ViewStateMode="Disabled">Скачать</asp:LinkButton>
                &nbsp;&nbsp;
                    <asp:LinkButton ID="LinkButtonDenideDebtForUtility"
                        runat="server"
                        OnClick="LinkButtonDenideDebtForUtility_Click"
                        EnableViewState="False"
                        CssClass="ButtonInDocument"
                        ViewStateMode="Disabled">Отклонить</asp:LinkButton>
                &nbsp;&nbsp;
                    <asp:LinkButton ID="LinkButtonSaveDebtForUtility"
                        runat="server"
                        OnClick="LinkButtonSaveDebtForUtility_Click"
                        EnableViewState="False"
                        CssClass="ButtonInDocument"
                        ViewStateMode="Disabled">Сохранить</asp:LinkButton>
                <br />
                <br />
                <asp:CheckBox ID="CheckBoxDebtForUtilityCharacteristic"
                    runat="server"
                    Text=" Задолженность по коммунальным платежам отсутствует"
                    ViewStateMode="Disabled"
                    EnableViewState="False"
                    CssClass="Header2" />
            </asp:Panel>
            <asp:Panel ID="PanelCertificateFromTaxInspection"
                runat="server"
                ViewStateMode="Enabled"
                Visible="False">
                <asp:LinkButton ID="LinkButtonDownLoadCertificateFromTaxInspection"
                    runat="server"
                    OnClick="LinkButtonDownLoadCertificateFromTaxInspection_Click"
                    EnableViewState="False"
                    CssClass="ButtonInDocument"
                    ViewStateMode="Disabled">Скачать</asp:LinkButton>
                &nbsp;&nbsp;
                    <asp:LinkButton ID="LinkButtonDenideCertificateFromTaxInspection"
                        runat="server"
                        OnClick="LinkButtonDenideCertificateFromTaxInspection_Click"
                        EnableViewState="False"
                        CssClass="ButtonInDocument"
                        ViewStateMode="Disabled">Отклонить</asp:LinkButton>
                &nbsp;&nbsp;
                    <asp:LinkButton ID="LinkButtonSaveCertificateFromTaxInspection"
                        runat="server"
                        OnClick="LinkButtonSaveCertificateFromTaxInspection_Click"
                        EnableViewState="False"
                        CssClass="ButtonInDocument"
                        ViewStateMode="Disabled">Сохранить</asp:LinkButton>
                <br />
                <br />
                <asp:CheckBox ID="CheckBoxCertificateFromTaxInspectionCharacteristic"
                    runat="server"
                    Text=" Задолженность по налогам отсутствует"
                    ViewStateMode="Disabled"
                    EnableViewState="False"
                    CssClass="Header2" />
            </asp:Panel>
        </div>
        <div style="position: absolute; top: 800px; height: 60px; width: 350px; left: 350px;">
            <asp:Panel ID="PanelReminder"
                runat="server"
                EnableViewState="False"
                ViewStateMode="Disabled"
                Visible="False"
                Width="400px"
                CssClass="HeaderReminder">
                Выполнены не все проверки!&nbsp;&nbsp;
                    <br />
                <br />
                <asp:Button ID="ButtonToBack"
                    runat="server"
                    Text="&lt; Назад"
                    OnClick="ButtonToBack_Click"
                    CssClass="ButtonElse"
                    Width="140px" />
                &nbsp;&nbsp;
         <asp:Button ID="ButtonForce"
             runat="server"
             OnClick="ButtonForce_Click"
             CssClass="ButtonMain"
             Text="Продолжить >"
             Width="140px" />
            </asp:Panel>
        </div>
        <div style="position: absolute; top: 800px; height: 60px; width: 350px; left: 350px;">
            <asp:Panel ID="PanelForward"
                runat="server"
                EnableViewState="False"
                ViewStateMode="Disabled"
                Visible="True"
                CssClass="HeaderReminder"
                Width="600px">
                &nbsp;
                                <asp:Button ID="ButtonForward"
                                    runat="server"
                                    OnClick="ButtonForward_Click"
                                    Style="background-color: white;"
                                    Text="Продолжить &gt;"
                                    CssClass="ButtonMain"
                                    Width="140px" />
            </asp:Panel>
        </div>
    </div>
</asp:Content>
