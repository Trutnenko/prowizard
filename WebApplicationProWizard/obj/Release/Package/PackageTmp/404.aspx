﻿<%@ Page 
    Language="C#" 
    AutoEventWireup="true" 
    MaintainScrollPositionOnPostback="true"
    CodeBehind="404.aspx.cs" 
    Inherits="WebApplicationProWizard._404" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <img src="Content/404.jpg" style="max-width: 100%; min-width: 100%; position: fixed; top: 0px; left: 0px;" />
    <form id="form1" runat="server">
        <div style="position: absolute; top: 200px; width: 100%; left: 0px; background-color: #F3F3F3; height: 200px; opacity: 0.7;">
            <div style="position: absolute; left: 100px;">
                <asp:Label runat="server"
                    Font-Size="100"
                    ForeColor="#0078D7"
                    ID="Label404">
                        404
                </asp:Label>
            </div>
            <div style="position: absolute; left: 350px; top:25px;">
                <asp:Label runat="server"
                    Font-Size="30"
                    ID="Label404Info">
                    Что-то пошло не так
                </asp:Label>
                <br />
                <br />
                <asp:LinkButton runat="server"
                    Font-Size="20"
                    ForeColor="#0078D7"
                    ID="LinkButtonInMain" OnClick="LinkButtonInMain_Click">
                    Вернуться на главную
                </asp:LinkButton>
            </div>
        </div>
    </form>
</body>
</html>
