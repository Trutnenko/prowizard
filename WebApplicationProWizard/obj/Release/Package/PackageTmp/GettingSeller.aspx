﻿<%@ Page
    Title=""
    Language="C#"
    MasterPageFile="~/Main.Master"
    AutoEventWireup="true"
    CodeBehind="GettingSeller.aspx.cs"
    Inherits="WebApplicationProWizard.WebForms.Getting.WebFormGettingSeller"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="GettingSeller" runat="server">
    <link rel="stylesheet" href="style.css" type="text/css" />
    <div style="position: absolute; height: 100%; width: 100%; top: 72px; background-color: #FFFFFF; z-index: 0;">
    </div>
    <div style="position: relative; margin: 0 auto; width: 1000px; background-color: #FFFFFF;">
        <div style="position: absolute; top: 80px; width: 100%; height: 100px;">
            <asp:Label runat="server" ID="Label1" CssClass="Header0">
                Выписка ЕГРН
            </asp:Label>
            <br />
            <br />
            <asp:Label runat="server" ID="Label2" CssClass="Header1">
                Дождитесь пока покупатель проверит выписку ЕГРН и убедиться что права
                собственности перешли на его имя
            </asp:Label>
        </div>
        <div style="position: absolute; top: 250px; width: 100%; height: 100px;">
            <asp:Label runat="server" ID="LabelMain" CssClass="Header0">
                   Акт приемки-передачи
            </asp:Label>
            <br />
            <br />
            <asp:Label runat="server" ID="LabelAbout" CssClass="Header1">
                   После проверки выписки ЕГРН покупатель подпишет с вами акт приемки-передачи.
                   <br />
                   Передайте ключи.
            </asp:Label>
        </div>
    </div>
</asp:Content>
