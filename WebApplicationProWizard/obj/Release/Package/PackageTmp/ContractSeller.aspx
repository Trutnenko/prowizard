﻿<%@ Page
    Title=""
    Language="C#"
    MasterPageFile="~/Main.Master"
    AutoEventWireup="true"
    CodeBehind="ContractSeller.aspx.cs"
    Inherits="WebApplicationProWizard.WebForms.PreliminaryContract.WebFormContractSeller"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContractSeller" runat="server">
    <link rel="stylesheet" href="style.css" type="text/css" />
    <div style="position: absolute; height: 100%; width: 100%; top: 72px; background-color: #FFFFFF; z-index: 0;">
    </div>
    <div style="position: relative; margin: 0 auto; width: 1000px; background-color: #FFFFFF;">
        <div style="position: absolute; top: 80px; width: 100%; height: 100px;">
            <div style="position: absolute; width: 100%; height: 100px;">
                <asp:Panel ID="PanelPrepare"
                    runat="server">
                    <asp:Label runat="server" ID="LabelMain" CssClass="Header0">
                    Договор купли-продажи
                    </asp:Label>
                    <asp:LinkButton ID="LinkButtonDowloadFiles"
                        runat="server"
                        OnClick="LinkButtonDowloadFiles_Click"
                        Visible="False"
                        CssClass="ButtonContract">Скачать</asp:LinkButton>
                    <br />
                    <br />
                    <br />
                    <br />
                    <asp:Label runat="server" ID="LabelAbout" CssClass="Header1">
                    Сформируйте договор купли-продажи и согласуйте с продавцом
                    </asp:Label>
                </asp:Panel>
            </div>
        </div>
        <div style="position: absolute; top: 181px; width: 100%; height: 80px;">
            <div style="position: relative; top: 20px;">
                <asp:LinkButton
                    ID="LinkButtonForwardToContract"
                    runat="server"
                    OnClick="LinkButtonForwardToContract_Click"
                    CssClass="ButtonContract" Visible="False">Получить договор</asp:LinkButton>
            </div>
        </div>      
    </div>
</asp:Content>
