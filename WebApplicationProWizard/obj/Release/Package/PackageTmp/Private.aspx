﻿<%@ Page
    Title=""
    Language="C#"
    MasterPageFile="~/Main.Master"
    AutoEventWireup="true"
    CodeBehind="Private.aspx.cs"
    MaintainScrollPositionOnPostback="true"
    Inherits="WebApplicationProWizard.WebFormPrivate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Private" runat="server">
    <link rel="stylesheet" href="style.css" type="text/css" />
    <div style="position: absolute; bottom: 0px; left: 0px; width: 100%; height: 100%; top: 72px; background-color: #FFFFFF; z-index: 0;">
    </div>
    <div style="position: relative; width: 1000px; top: 50px; height: 50px; margin: 0 auto; background-color: #FFFFFF; left: 0px;">
        <asp:LinkButton ID="LinkButtonSectionBuyer"
            runat="server"
            OnClick="LinkButtonSectionBuyer_Click"
            CssClass="SectionBuyerSeller"
            Style="text-decoration: none;"
            ViewStateMode="Enabled" ForeColor="#0078D7">Раздел покупателя
        </asp:LinkButton>
        &nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="LinkButtonSectionSeller"
                runat="server"
                OnClick="LinkButtonSectionSeller_Click"
                CssClass="SectionBuyerSeller"
                Style="text-decoration: none;"
                ViewStateMode="Enabled">Раздел продавца
            </asp:LinkButton>
    </div>
    <div style="position: relative; width: 1000px; top: 50px; height: 500px; margin: 0 auto; background-color: #FFFFFF;">
        <div style="position: relative; top: 75px; width: 200px;">
            <asp:Button ID="ButtonCreateDeal"
                runat="server"
                Text="+ Создать сделку"
                OnClick="ButtonCreateDeal_Click"
                CssClass="ButtonMain"
                Width="180px" />
            <br />
        </div>
        <div style="position: absolute; top: 50px; left: 500px;">
            <asp:Label ID="LabelStatusBuyer"
                runat="server"
                Text="Здесь будут ваши сделки покупки"
                Visible="False"
                Enabled="False"
                CssClass="Header1"></asp:Label>
        </div>
        <div style="position: absolute; top: 0px; left: 500px">
            <asp:GridView ID="GridViewCurrentDealBuy"
                runat="server"
                AllowPaging="True"
                AutoGenerateColumns="False"
                DataSourceID="DealBuy"
                OnSelectedIndexChanged="GridViewCurrentDealBuy_SelectedIndexChanged"
                CssClass="Header1"
                GridLines="None"
                Style="color: #0078D7" CellPadding="4">
                <AlternatingRowStyle />
                <Columns>
                    <asp:CommandField ShowSelectButton="True" SelectText="Перейти   ">
                        <ControlStyle />
                    </asp:CommandField>
                    <asp:BoundField DataField="IdDeal" SortExpression="IdDeal" />
                    <asp:BoundField DataField="StatusDeal" SortExpression="StatusDeal" />
                </Columns>
                <EditRowStyle />
                <FooterStyle Font-Bold="True" />
                <HeaderStyle Font-Bold="True" />
                <PagerStyle HorizontalAlign="Center" />
                <RowStyle />
                <SelectedRowStyle Font-Bold="True" />
                <SortedAscendingCellStyle />
                <SortedAscendingHeaderStyle />
                <SortedDescendingCellStyle />
                <SortedDescendingHeaderStyle />
            </asp:GridView>
            <asp:SqlDataSource ID="DealBuy" runat="server" ConnectionString="<%$ ConnectionStrings:TableDeal %>"
                SelectCommand="SELECT IdDeal, StatusDeal FROM TableDeal WHERE (IdUserBuyer = @IdUserBuyer)">
                <SelectParameters>
                    <asp:ControlParameter ControlID="LabelIdUserBuyer" Name="IdUserBuyer" PropertyName="Text" Type="String" />
                </SelectParameters>
            </asp:SqlDataSource>
            <asp:Label ID="LabelIdUserBuyer" runat="server" Text="LabelIdUserBuyer" Visible="False"></asp:Label>
        </div>
        <div style="position: absolute; top: 50px; left: 500px;">
            <asp:Label ID="LabelStatusSell"
                runat="server"
                CssClass="Header1"
                Text="Здесь будут отображаться сделки, в которых вы участвуете как продавец"
                Visible="False"></asp:Label>
        </div>
        <div style="position: absolute; top: 0px; left: 500px">
            <asp:GridView ID="GridViewCurrentDealSell"
                runat="server"
                AllowPaging="True"
                AutoGenerateColumns="False"
                DataSourceID="DealSell"
                OnSelectedIndexChanged="GridViewCurrentDealSell_SelectedIndexChanged"
                GridLines="None"
                CssClass="Header1"
                Style="color: #0078D7" CellPadding="4">
                <AlternatingRowStyle />
                <Columns>
                    <asp:CommandField ShowSelectButton="True" SelectText="Перейти   ">
                        <ControlStyle />
                    </asp:CommandField>
                    <asp:BoundField DataField="IdDeal" SortExpression="IdDeal" />
                    <asp:BoundField DataField="StatusDeal" SortExpression="StatusDeal" />
                </Columns>
                <EditRowStyle />
                <FooterStyle Font-Bold="True" />
                <HeaderStyle Font-Bold="True" />
                <PagerStyle HorizontalAlign="Center" />
                <RowStyle />
                <SelectedRowStyle Font-Bold="True" />
                <SortedAscendingCellStyle />
                <SortedAscendingHeaderStyle />
                <SortedDescendingCellStyle />
                <SortedDescendingHeaderStyle />
            </asp:GridView>
            <asp:SqlDataSource ID="DealSell"
                runat="server"
                ConnectionString="<%$ ConnectionStrings:TableDeal %>"
                SelectCommand="SELECT IdDeal, StatusDeal, IdUserBuyer, IdUserSeller FROM TableDeal WHERE (IdUserSeller = @IdUserSeller)">
                <SelectParameters>
                    <asp:ControlParameter
                        ControlID="LabelIdUserBuyer"
                        Name="IdUserSeller"
                        PropertyName="Text"
                        Type="String" />
                </SelectParameters>
            </asp:SqlDataSource>
        </div>
        <div style="position: absolute; top: 0px; left: 450px; height: 100%; width: 1px; background-color: #9FDAE2">
        </div>
    </div>
</asp:Content>
