﻿<%@ Page
    Title=""
    Language="C#"
    MasterPageFile="~/Main.Master"
    AutoEventWireup="true"
    CodeBehind="ContractBuyer.aspx.cs"
    Inherits="WebApplicationProWizard.WebForms.WebFormContractBuyer"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContractBuyer" runat="server">
    <link rel="stylesheet" href="style.css" type="text/css" />
    <div style="position: absolute; height: 100%; width: 100%; top: 72px; background-color: #FFFFFF; z-index: 0;">
    </div>
    <div style="position: relative; margin: 0 auto; width: 1000px; background-color: #FFFFFF;">
        <div style="position: absolute; top: 80px; width: 100%; height: 100px;">
            <asp:Panel ID="PanelPrepare"
                runat="server">
                <asp:Label
                    runat="server"
                    ID="LabelMain"
                    CssClass="Header1">
                    Договор купли-продажи
                </asp:Label>
                <br />
                <br />
                <br />
                <asp:Label
                    runat="server"
                    ID="LabelAbout"
                    CssClass="Header2">
                    Сформируйте договор купли-продажи и согласуйте с продавцом
                </asp:Label>
                <asp:LinkButton
                    ID="LinkButtonDowloadFiles"
                    runat="server"
                    OnClick="LinkButtonDowloadFiles_Click"
                    Visible="False"
                    CssClass="ButtonContract">Скачать</asp:LinkButton>
                <br />
                <br />
                <asp:LinkButton
                    ID="LinkButtonForwardToContract"
                    runat="server"
                    OnClick="LinkButtonForwardToContract_Click"
                    CssClass="ButtonContract"
                    Visible="False">Получить договор</asp:LinkButton>
            </asp:Panel>
        </div>
        <div style="position: absolute; top: 350px; height: 60px; width: 350px; left: 350px;">
            <asp:Panel ID="PanelReminder"
                runat="server"
                EnableViewState="False"
                ViewStateMode="Disabled"
                Visible="False"
                Width="400px"
                CssClass="HeaderReminder">
                Этап будет заблокирован! Продолжить?
                &nbsp;&nbsp;
                <br />
                <br />
                <asp:Button ID="ButtonToBack"
                    runat="server"
                    Text="&lt; Назад"
                    OnClick="ButtonToBack_Click"
                    CssClass="ButtonElse"
                    Width="140px" />
                &nbsp;&nbsp;
         <asp:Button ID="ButtonForce"
             runat="server"
             OnClick="ButtonForce_Click"
             CssClass="ButtonMain"
             Text="Продолжить >"
             Width="140px" />
            </asp:Panel>
        </div>
        <div style="position: absolute; top: 350px; height: 60px; width: 350px; left: 350px;">
            <asp:Panel ID="PanelForward"
                runat="server"
                EnableViewState="False"
                ViewStateMode="Disabled"
                Visible="True"
                CssClass="HeaderReminder"
                Width="600px">
                &nbsp;
            <asp:Button ID="ButtonForward"
                runat="server"
                OnClick="ButtonForward_Click"
                Style="background-color: white;"
                Text="Продолжить &gt;"
                CssClass="ButtonMain"
                Width="140px" />
            </asp:Panel>
        </div>
    </div>
</asp:Content>
