﻿<%@ Page Title="" Language="C#"
    MasterPageFile="~/WebFormMain.Master"
    AutoEventWireup="true"
    CodeBehind="WebFormPreliminaryContractBuyer.aspx.cs"
    Inherits="WebApplicationProWizard.WebForms.WebFormPreliminaryContract"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div style="position: relative; margin: 0 auto; width: 1100px;">
        <div style="position: absolute; top: 80px; background-color: #FFFFFF; width: 100%; height: 100px;">
            <div style="position: relative; left: 20px; top: 20px;">
                <div style="position: absolute; width: 900px; left: 0px;">
                    <asp:Panel ID="PanelCompleted"
                        runat="server"
                        Visible="False"
                        ForeColor="#525252">
                        Панель для отображения информации: Этап завершен
                    </asp:Panel>
                    <asp:Panel ID="PanelPrepare"
                        runat="server"
                        Visible="False"
                        ForeColor="#525252">
                        Панель для отображения информации: Подготовка документа
                    </asp:Panel>
                    <asp:Panel ID="PanelAccept"
                        runat="server"
                        Visible="False"
                        ForeColor="#525252">
                        Панель для отображения информации: Документ согласован
                    </asp:Panel>
                    <asp:Panel ID="PanelDenide"
                        runat="server"
                        Visible="False"
                        ForeColor="#525252">
                        Панель для отображения информации: Документ отклонен
                    </asp:Panel>
                    <asp:Panel ID="PanelUponAgreement"
                        runat="server"
                        Visible="False"
                        ForeColor="#525252">
                        Панель для отображения информации: Документ находится на согласовании
                    </asp:Panel>
                </div>
            </div>
        </div>
        <div style="position: absolute; top: 181px; background-color: #FFFFFF; width: 100%; height: 80px;">
            <div style="position: relative; left: 20px; top: 20px;">
                <asp:Label ID="LabelStatusPreliminaryContract"
                    runat="server" Text="LabelStatusPreliminaryContract"
                    Visible="False"
                    ForeColor="#525252"></asp:Label>
                <asp:LinkButton ID="LinkButtonDownLoadModelDocument"
                    runat="server"
                    OnClick="LinkButtonDownLoadModelDocument_Click"
                    Visible="False"
                    ForeColor="#0078D7"
                    Style="text-decoration: none">Скачать шаблон</asp:LinkButton>
                <asp:LinkButton ID="LinkButtonCancelForAccept"
                    runat="server" OnClick="LinkButtonCancelForAccept_Click"
                    Visible="False"
                    ForeColor="#0078D7">Отменить</asp:LinkButton>
                <asp:LinkButton ID="LinkButtonDowloadFiles"
                    runat="server"
                    OnClick="LinkButtonDowloadFiles_Click"
                    Visible="False"
                    ForeColor="#0078D7"
                    Style="text-decoration: none">Предварительный договор (мой)</asp:LinkButton>
                <asp:LinkButton ID="LinkButtonDowloadFilesSeller"
                    runat="server"
                    OnClick="LinkButtonDowloadFilesSeller_Click"
                    ForeColor="#0078D7"
                    Visible="False"
                    Style="text-decoration: none">Предварительный договор (продавец)</asp:LinkButton>
                <asp:LinkButton ID="LinkButtonUpload"
                    runat="server"
                    OnClick="LinkButtonUpload_Click"
                    Visible="False"
                    ForeColor="#0078D7"
                    Style="text-decoration: none">Отправить на согласование</asp:LinkButton>
                <asp:FileUpload ID="FileUpload"
                    runat="server"
                    AllowMultiple="true"
                    BackColor="White"
                    Visible="False" />
                <asp:Label ID="LabelError"
                    runat="server"
                    Text="LabelError"
                    Visible="False"></asp:Label>
            </div>
        </div>
        <div style="position: absolute; top: 262px; background-color: #FFFFFF; height: 60px; width: 100%; font-size: small;">
            <div style="position: absolute; right: 20px; top: 10px; width: 488px;">
                <asp:Panel ID="PanelReminder"
                    runat="server"
                    EnableViewState="False"
                    ForeColor="#62BB47"
                    Height="40px"
                    ViewStateMode="Disabled"
                    Visible="False"
                    Font-Size="Medium"
                    Width="700px">
                    <strong>Договор не согласован!&nbsp;&nbsp;
                            <asp:Button ID="ButtonToBack"
                                runat="server"
                                Text="&lt; Назад"
                                Style="background-color: white;"
                                BorderColor="#0078D7"
                                BorderWidth="1px"
                                Font-Bold="True"
                                Font-Italic="False"
                                Font-Overline="False"
                                Font-Underline="False"
                                ForeColor="#0078D7"
                                Height="40px"
                                OnClick="ButtonToBack_Click" Width="140px" />
                    </strong>
                    &nbsp;&nbsp;
         <asp:Button ID="ButtonForce"
             runat="server"
             BorderColor="#0078D7"
             BorderWidth="1px"
             Font-Bold="True"
             Font-Italic="False"
             Font-Overline="False"
             Font-Underline="False"
             ForeColor="#0078D7"
             Height="40px"
             OnClick="ButtonForce_Click"
             Style="background-color: white;"
             Text="Продолжить >"
             Width="140px" />
                </asp:Panel>
            </div>
            <div style="position: absolute; right: 20px; top: 10px; width: 160px;">
                <asp:Panel ID="PanelForward"
                    runat="server"
                    EnableViewState="False"
                    ForeColor="#7DC243"
                    Height="40px"
                    ViewStateMode="Disabled"
                    Visible="True"
                    Width="600px">
                    <strong>&nbsp;
                                <asp:Button ID="ButtonForward"
                                    runat="server"
                                    BorderColor="#0078D7"
                                    BorderWidth="1px"
                                    Font-Bold="True"
                                    Font-Italic="False"
                                    Font-Overline="False"
                                    Font-Underline="False"
                                    ForeColor="#0078D7"
                                    Height="40px"
                                    OnClick="ButtonForward_Click"
                                    Style="background-color: white;"
                                    Text="Продолжить &gt;"
                                    Width="140px" />
                    </strong>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
