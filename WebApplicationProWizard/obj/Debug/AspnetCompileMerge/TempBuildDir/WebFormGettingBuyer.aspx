﻿<%@ Page Title="" Language="C#"
    MasterPageFile="~/WebFormMain.Master"
    AutoEventWireup="true"
    CodeBehind="WebFormGettingBuyer.aspx.cs"
    Inherits="WebApplicationProWizard.WebForms.Getting.WebFormGettingBuyer"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div style="position: relative; margin: 0 auto; width: 1100px; font-family: 'Times New Roman';">
        <div style="position: absolute; top: 80px; width: 100%; height: 130px; background-color: #FFFFFF; font-size: medium; color: #FFFFFF;">
            <div style="position: relative; left: 20px; top: 20px; color: #000000">
                Описание этапа
            </div>
        </div>
        <div style="position: absolute; top: 211px; width: 400px; height: 400px; background-color: #FFFFFF; font-size: medium; font-size: medium; color: #FFFFFF;">
            <div style="position: relative; left: 20px; top: 20px;">
                <strong>
                    <asp:LinkButton ID="LinkButtonExtractSecond"
                        runat="server"
                        Font-Strikeout="False"
                        Font-Underline="False"
                        ForeColor="#0078D7"
                        OnClick="LinkButtonExtractSecond_Click">Расширенная выписка</asp:LinkButton>
                </strong>
                <asp:Label ID="LabelExtractSecond"
                    runat="server"
                    Text="LabelExtractSecond"
                    ForeColor="Black"
                    Font-Size="Medium"></asp:Label>
                <strong>
                    <br />
                    <br />
                    <asp:LinkButton ID="LinkButtonAcceptance"
                        runat="server"
                        Font-Strikeout="False"
                        Font-Underline="False"
                        ForeColor="#0078D7"
                        OnClick="LinkButtonAcceptance_Click">Акт приемки-передачи</asp:LinkButton>
                </strong>
                <asp:Label ID="LabelAcceptance"
                    runat="server"
                    Text="LabelAcceptance"
                    ForeColor="Black"
                    Font-Size="Medium"></asp:Label>
            </div>
        </div>
        <div style="position: absolute; top: 211px; left: 401px; width: 699px; height: 400px; background-color: #FFFFFF; font-size: medium; font-size: medium;">
            <div style="position: relative; left: 20px; top: 20px; width: 650px">
                <asp:Panel ID="PanelExtract" runat="server" ViewStateMode="Disabled" Visible="False">
                    <asp:LinkButton ID="LinkButtonDownLoadExtractSecond"
                        runat="server"
                        Font-Size="Medium"
                        ForeColor="Black"
                        OnClick="LinkButtonDownLoadExtractSecond_Click"
                        Visible="False">Расширенная выписка</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonDenideExtractSecond"
                        runat="server"
                        BorderColor="Black"
                        ForeColor="Black"
                        OnClick="LinkButtonDenideExtractSecond_Click"
                        Visible="False">Отклонить</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonSaveExtractSecond"
                        runat="server"
                        ForeColor="Black"
                        OnClick="LinkButtonSaveExtractSecond_Click"
                        Visible="False">Сохранить</asp:LinkButton>
                    <br />
                    <br />
                    <asp:CheckBox ID="CheckBoxExtractSecondRegisterPeople"
                        runat="server"
                        Text="Никто не прописан"
                        ViewStateMode="Enabled"
                        Visible="False" />
                    <br />
                    <br />
                    <asp:CheckBox ID="CheckBoxExtractSecondFromRegisterMinors"
                        runat="server"
                        Text="Права несовершеннолетних не нарушены"
                        ViewStateMode="Enabled"
                        Visible="False" />
                    <br />
                    <br />
                    <asp:CheckBox ID="CheckBoxExtractSecondTemporarilyВrawn"
                        runat="server"
                        Text="Нет временно выписанных граждан"
                        ViewStateMode="Enabled"
                        Visible="False" />
                    <br />
                    <br />
                    Описание выписка из домовой книги
                </asp:Panel>
                <asp:Panel ID="PanelAcceptance"
                    runat="server"
                    ViewStateMode="Disabled"
                    Visible="False">
                    <asp:LinkButton ID="LinkButtonDownLoadModelDocumentAcceptance"
                        runat="server"
                        OnClick="LinkButtonDownLoadModelDocumentAcceptance_Click"
                        Visible="False">Скачать шаблон</asp:LinkButton>
                    <asp:Label ID="LabelModelInformationAcceptance"
                        runat="server"
                        Text="LabelModelInformation"
                        Visible="False"></asp:Label>
                    <asp:LinkButton ID="LinkButtonCancelForAcceptAcceptance"
                        runat="server"
                        OnClick="LinkButtonCancelForAcceptAcceptance_Click"
                        Style="position: relative"
                        Visible="False">Отменить</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonDowloadFilesAcceptance"
                        runat="server"
                        OnClick="LinkButtonDowloadFilesAcceptance_Click"
                        Visible="False">Акт приемки-передачи(покупатель)</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonDowloadFilesSellerAcceptance"
                        runat="server"
                        ForeColor="Red"
                        OnClick="LinkButtonDowloadFilesSellerAcceptance_Click"
                        Visible="False">Акт приемки-передачи(покупатель)</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonUploadAcceptance"
                        runat="server"
                        OnClick="LinkButtonUploadAcceptance_Click"
                        Visible="False">Отправить на согласование</asp:LinkButton>
                    <asp:FileUpload ID="FileUploadAcceptance"
                        runat="server"
                        AllowMultiple="False"
                        BackColor="White"
                        Visible="False" />
                    <asp:Label ID="LabelError"
                        runat="server"
                        Text="LabelError"
                        Visible="False"></asp:Label>
                    <br />
                    <br />
                    Панель: Описание акт приемки-передачи
                </asp:Panel>
                <asp:Panel ID="PanelWait"
                    runat="server"
                    ViewStateMode="Disabled"
                    Visible="False">
                    Панель: документ ожидается от продавца
                </asp:Panel>
                <asp:Panel ID="PanelDenide"
                    runat="server"
                    ViewStateMode="Disabled"
                    Visible="False">
                    Панель: документ отклонен
                </asp:Panel>
            </div>
        </div>
        <div style="position: absolute; top: 612px; background-color: #FFFFFF; height: 60px; width: 100%; font-size: small;">
            <div style="position: absolute; right: 20px; top: 10px; width: 280px;">
                Кнопка для завершения сделки или еще что-то такое
            </div>
        </div>
    </div>

</asp:Content>
