﻿<%@ Page Title=""
    Language="C#"
    MasterPageFile="~/WebFormMain.Master"
    AutoEventWireup="true"
    CodeBehind="WebFormPrepareDealBuyer.aspx.cs"
    Inherits="WebApplicationProWizard.WebForms.PrepareDeal.WebFormPrepareDealBuyer"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div style="position: relative; margin: 0 auto; width: 1100px; font-family: 'Times New Roman';">
        <div style="position: absolute; top: 80px; width: 100%; height: 130px; background-color: #FFFFFF; font-size: medium; color: #000000;">
            <div style="position: relative; left: 20px; top: 20px;">
                <div style="position: absolute; width: 900px; left: 0px; color: #000000;">
                    <asp:Panel ID="PanelCommonInformation" runat="server">
                        На данном этапе вам потребуется согласовать документы для проведения сделки.<br />
                        Внесите в шаблоны свои данных и отправьте их на согласование продавцу.
                    </asp:Panel>
                </div>
            </div>
        </div>
        <div style="position: absolute; top: 211px; width: 400px; height: 300px; background-color: #FFFFFF; font-size: medium; font-size: medium; color: #FFFFFF;">
            <div style="position: relative; left: 20px; top: 20px;">
                <strong>
                    <asp:LinkButton ID="LinkButtonSalesContract"
                        runat="server"
                        OnClick="LinkButtonSalesContract_Click"
                        ForeColor="#0078D7"
                        Font-Strikeout="False">Договор купли-продажи</asp:LinkButton>
                </strong>
                <asp:Label ID="LabelSalesContract"
                    runat="server"
                    Text="LabelSalesContract"
                                        ForeColor="Black"
                    Font-Size="Medium"></asp:Label>
                <strong>
                    <br />
                    <br />
                    <asp:LinkButton ID="LinkButtonReceipt"
                        runat="server"
                        OnClick="LinkButtonReceipt_Click"
                        ForeColor="#0078D7"
                        Font-Strikeout="False">Расписка</asp:LinkButton>
                </strong>
                <asp:Label ID="LabelReceipt"
                    runat="server"
                    Text="LabelReceipt"
                                        ForeColor="Black"
                    Font-Size="Medium"></asp:Label>
            </div>
        </div>
        <div style="position: absolute; top: 211px; left: 401px; width: 699px; height: 300px; background-color: #FFFFFF; font-size: medium; font-size: medium;">
            <div style="position: relative; left: 20px; top: 20px; width: 650px">
                <asp:Panel ID="PanelCompleted"
                    runat="server"
                    ViewStateMode="Disabled"
                    Visible="False">
                    Панель: Этап завершен<br />
                    Форма недоступна для редактирования
                </asp:Panel>
                <asp:Panel ID="PanelPrepare"
                    runat="server"
                    Visible="False"
                    ViewStateMode="Disabled">
                    Панель: Подготовка документа<br />
                </asp:Panel>
                <asp:Panel ID="PanelAccept"
                    runat="server"
                    Visible="False"
                    ViewStateMode="Disabled">
                    Панель: Документ согласован
                </asp:Panel>
                <asp:Panel ID="PanelDenide"
                    runat="server"
                    Visible="False"
                    ViewStateMode="Disabled">
                    Панель: Документ отклонен
                </asp:Panel>
                <asp:Panel ID="PanelUponAgreement"
                    runat="server"
                    Visible="False"
                    ViewStateMode="Disabled">
                    Панель: Документ находится на согласовании
                </asp:Panel>
                <asp:Panel ID="PanelSalesContract"
                    runat="server"
                    ViewStateMode="Disabled"
                    Visible="False">
                    <asp:LinkButton ID="LinkButtonDownLoadModelSalesContract"
                        runat="server"
                        OnClick="LinkButtonDownLoadModelSalesContract_Click"
                        ViewStateMode="Enabled"
                        Visible="False">Скачать шаблон</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonDowloadSalesContractBuyer"
                        runat="server"
                        OnClick="LinkButtonDowloadSalesContractBuyer_Click"
                        ViewStateMode="Enabled"
                        Visible="False">Скачать мои файлы</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonDowloadSalesContractSeller"
                        runat="server"
                        ForeColor="Red"
                        OnClick="LinkButtonDowloadSalesContractSeller_Click"
                        ViewStateMode="Enabled"
                        Visible="False">Скачать файлы продавца</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonSendSalesContract"
                        runat="server"
                        OnClick="LinkButtonSendSalesContract_Click"
                        ViewStateMode="Enabled"
                        Visible="False">Отправить на согласование</asp:LinkButton>
                    <asp:FileUpload ID="FileUploadSalesContract"
                        runat="server"
                        ViewStateMode="Enabled"
                        Visible="False"
                        AllowMultiple="False" />
                    <asp:Label ID="LabelError"
                        runat="server"
                        Text="LabelError"
                        Visible="False"></asp:Label>
                    <asp:LinkButton ID="LinkButtonDenideSalesContract"
                        runat="server"
                        OnClick="LinkButtonDenideSalesContract_Click"
                        ViewStateMode="Disabled"
                        Visible="False">Отменить</asp:LinkButton>
                </asp:Panel>
                <asp:Panel ID="PanelReceipt"
                    runat="server"
                    ViewStateMode="Disabled"
                    Visible="False">
                    <asp:LinkButton ID="LinkButtonDownLoadModelReceipt"
                        runat="server"
                        OnClick="LinkButtonDownLoadModelReceipt_Click"
                        ViewStateMode="Enabled"
                        Visible="False">Скачать шаблон</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonDowloadReceiptBuyer"
                        runat="server"
                        OnClick="LinkButtonDowloadReceiptBuyer_Click"
                        ViewStateMode="Enabled"
                        Visible="False">Скачать мои файлы</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonDowloadReceiptSeller"
                        runat="server"
                        ForeColor="Red"
                        OnClick="LinkButtonDowloadReceiptSeller_Click"
                        ViewStateMode="Enabled"
                        Visible="False">Скачать файлы продавца</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonSendReceipt"
                        runat="server"
                        OnClick="LinkButtonSendReceipt_Click"
                        ViewStateMode="Enabled"
                        Visible="False">Отправить на согласование</asp:LinkButton>
                    <asp:FileUpload ID="FileUploadReceipt"
                        runat="server"
                        ViewStateMode="Enabled"
                        Visible="False"
                        AllowMultiple="False" />
                    <asp:Label ID="LabelErrorTHREE"
                        runat="server"
                        Text="LabelError"
                        Visible="False"></asp:Label>
                    <asp:LinkButton ID="LinkButtonDenideReceipt"
                        runat="server"
                        OnClick="LinkButtonDenideReceipt_Click"
                        ViewStateMode="Disabled"
                        Visible="False">Отменить</asp:LinkButton>
                </asp:Panel>
            </div>
        </div>
        <div style="position: absolute; top: 512px; background-color: #FFFFFF; height: 60px; width: 100%; font-size: small;">
            <div style="position: absolute; right: 20px; top: 10px; width: 520px;">
                <asp:Panel ID="PanelReminder"
                    runat="server"
                    EnableViewState="False"
                    ForeColor="#62BB47"
                    Height="40px"
                    ViewStateMode="Disabled"
                    Visible="False"
                    Font-Size="Medium"
                    Width="700px">
                    <strong>Не все документы согласованы!&nbsp;&nbsp;
                            <asp:Button ID="ButtonToBack"
                                runat="server"
                                Text="&lt; Назад"
                                Style="background-color: white;"
                                BorderColor="#0078D7"
                                BorderWidth="1px"
                                Font-Bold="True"
                                Font-Italic="False"
                                Font-Overline="False"
                                Font-Underline="False"
                                ForeColor="#0078D7"
                                Height="40px"
                                OnClick="ButtonToBack_Click" Width="140px" />
                    </strong>
                    &nbsp;&nbsp;
         <asp:Button ID="ButtonForce"
             runat="server"
             BorderColor="#0078D7"
             BorderWidth="1px"
             Font-Bold="True"
             Font-Italic="False"
             Font-Overline="False"
             Font-Underline="False"
             ForeColor="#0078D7"
             Height="40px"
             OnClick="ButtonForce_Click"
             Style="background-color: white;"
             Text="Продолжить >"
             Width="140px" />
                </asp:Panel>
            </div>
            <div style="position: absolute; right: 20px; top: 10px; width: 145px;">
                <asp:Panel ID="PanelForward"
                    runat="server"
                    EnableViewState="False"
                    ForeColor="#7DC243"
                    Height="40px"
                    ViewStateMode="Disabled"
                    Visible="True"
                    Width="600px">
                    <strong>&nbsp;
                                <asp:Button ID="ButtonForward"
                                    runat="server"
                                    BorderColor="#0078D7"
                                    BorderWidth="1px"
                                    Font-Bold="True"
                                    Font-Italic="False"
                                    Font-Overline="False"
                                    Font-Underline="False"
                                    ForeColor="#0078D7"
                                    Height="40px"
                                    OnClick="ButtonForward_Click"
                                    Style="background-color: white;"
                                    Text="Продолжить &gt;"
                                    Width="140px" />
                    </strong>
                </asp:Panel>
            </div>
        </div>
    </div>

</asp:Content>
