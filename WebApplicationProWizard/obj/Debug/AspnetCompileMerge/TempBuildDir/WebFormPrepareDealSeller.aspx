﻿<%@ Page Title=""
    Language="C#"
    MasterPageFile="~/WebFormMain.Master"
    AutoEventWireup="true"
    CodeBehind="WebFormPrepareDealSeller.aspx.cs"
    Inherits="WebApplicationProWizard.WebForms.PrepareDeal.WebFormPrepareDealSeller"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div style="position: relative; margin: 0 auto; width: 1100px; font-family: 'Times New Roman';">
        <div style="position: absolute; top: 80px; width: 100%; height: 130px; background-color: #FFFFFF; font-size: medium; color: #000000;">
            <div style="position: relative; left: 20px; top: 20px;">
                <div style="position: absolute; width: 900px; left: 0px; color: #000000;">
                    <asp:Panel ID="PanelCommonInformation" runat="server">
                        На данном этапе вам потребуется согласовать документы для проведения сделки.<br />
                        Документы подготавливает покупатель, после их подготовки они будут отправлены вам и доступны для согласования - изменится статус документов.
                    </asp:Panel>
                    <asp:Panel ID="PanelCompleted"
                        runat="server"
                        ViewStateMode="Disabled"
                        Visible="False">
                        Панель: Этап завершен<br />
                        Форма недоступна для редактирования
                    </asp:Panel>
                </div>
            </div>
        </div>
        <div style="position: absolute; top: 211px; width: 400px; height: 300px; background-color: #FFFFFF; font-size: medium; font-size: medium; color: #FFFFFF;">
            <div style="position: relative; left: 20px; top: 20px;">
                <strong>
                    <asp:LinkButton ID="LinkButtonSalesContract"
                        runat="server"
                        OnClick="LinkButtonSalesContract_Click"
                        ForeColor="#0078D7"
                        Font-Strikeout="False">Договор купли-продажи</asp:LinkButton>
                </strong>
                <asp:Label ID="LabelSalesContract"
                    runat="server"
                    Text="LabelSalesContract"
                    ForeColor="Black"
                    Font-Size="Medium"></asp:Label>
                <strong>
                    <br />
                    <br />
                    <asp:LinkButton ID="LinkButtonReceipt"
                        runat="server"
                        OnClick="LinkButtonReceipt_Click"
                        ForeColor="#0078D7"
                        Font-Strikeout="False">Расписка</asp:LinkButton>
                </strong>
                <asp:Label ID="LabelReceipt"
                    runat="server"
                    Text="LabelReceipt"
                    ForeColor="Black"
                    Font-Size="Medium"></asp:Label>
            </div>
        </div>
        <div style="position: absolute; top: 211px; left: 401px; width: 699px; height: 300px; background-color: #FFFFFF; font-size: medium; font-size: medium;">
            <div style="position: relative; left: 20px; top: 20px; width: 650px">
                <asp:Panel ID="PanelPrepare"
                    runat="server"
                    Visible="False"
                    ViewStateMode="Disabled">
                    Панель: Подготовка документа
                </asp:Panel>
                <asp:Panel ID="PanelAccept"
                    runat="server"
                    Visible="False"
                    ViewStateMode="Disabled">
                    Панель: Документ согласован
                </asp:Panel>
                <asp:Panel ID="PanelDenide"
                    runat="server"
                    Visible="False"
                    ViewStateMode="Disabled">
                    Панель: Документ отклонен
                </asp:Panel>
                <asp:Panel ID="PanelUponAgreement"
                    runat="server"
                    Visible="False"
                    ViewStateMode="Disabled">
                    Панель: Документ находится на согласовании
                </asp:Panel>
                <asp:Panel ID="PanelSalesContract"
                    runat="server"
                    ViewStateMode="Disabled"
                    Visible="False">
                    <asp:LinkButton ID="LinkButtonDowloadSalesContractBuyer"
                        runat="server"
                        OnClick="LinkButtonDowloadSalesContractBuyer_Click"
                        ViewStateMode="Disabled"
                        Visible="False">Скачать файлы покупателя</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonDowloadSalesContractSeller"
                        runat="server"
                        OnClick="LinkButtonDowloadSalesContractSeller_Click"
                        ViewStateMode="Disabled"
                        Visible="False">Скачать мои файлы</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonApproveSalesContract"
                        runat="server"
                        OnClick="LinkButtonApproveSalesContract_Click"
                        ViewStateMode="Disabled"
                        Visible="False">Подтвердить</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonDeclineSalesContract"
                        runat="server"
                        OnClick="LinkButtonDeclineSalesContract_Click"
                        ViewStateMode="Disabled"
                        Visible="False">Отправить замечания</asp:LinkButton>
                    <asp:FileUpload ID="FileUploadDeclinedSalesContract"
                        runat="server"
                        AllowMultiple="true"
                        ViewStateMode="Disabled"
                        Visible="False" />
                    <asp:Label ID="LabelError"
                        runat="server"
                        Text="LabelError"
                        Visible="False"></asp:Label>
                </asp:Panel>
                <asp:Panel ID="PanelReceipt"
                    runat="server"
                    ViewStateMode="Disabled"
                    Visible="False">
                    <asp:LinkButton ID="LinkButtonDowloadReceiptBuyer"
                        runat="server"
                        OnClick="LinkButtonDowloadReceiptBuyer_Click"
                        Visible="False">Скачать файлы покупателя</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonDowloadReceiptSeller"
                        runat="server"
                        OnClick="LinkButtonDowloadReceiptSeller_Click"
                        Visible="False">Скачать мои файлы</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonApproveReceipt"
                        runat="server"
                        OnClick="LinkButtonApproveReceipt_Click"
                        Visible="False">Подтвердить</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonDeclineReceipt"
                        runat="server"
                        OnClick="LinkButtonDeclineReceipt_Click"
                        Visible="False">Отправить замечания</asp:LinkButton>
                    <asp:FileUpload ID="FileUploadDeclinedReceipt"
                        runat="server"
                        AllowMultiple="False"
                        Visible="False" />
                    <asp:Label ID="LabelErrorTHREE"
                        runat="server"
                        Text="LabelError"
                        Visible="False"></asp:Label>
                </asp:Panel>
            </div>
        </div>
        <div style="position: absolute; top: 512px; background-color: #FFFFFF; height: 60px; width: 100%; font-size: small;">
            <div style="position: absolute; right: 20px; top: 10px; width: 300px;">
                <asp:Panel ID="PanelForward"
                    runat="server"
                    EnableViewState="False"
                    ForeColor="#7DC243"
                    Height="40px"
                    ViewStateMode="Disabled"
                    Visible="True"
                    Width="600px">
                    <strong>&nbsp;
                Для продолжения сделки
                    </strong>
                </asp:Panel>
            </div>
        </div>
    </div>

</asp:Content>
