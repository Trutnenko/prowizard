﻿<%@ Page Language="C#"
    AutoEventWireup="true"
    CodeBehind="WebFormIndex.aspx.cs"
    Inherits="WebApplicationProWizard.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <style type="text/css">
    </style>
</head>
<body>
    <img src="Content/Test.jpg" style="max-width: 100%; min-width: 100%; position: fixed; top: 0px; left: 0px;" />
    <form id="form1" runat="server">
        <div style="width: 85%; height: 100%; font-family: 'Times New Roman'; font-size: xx-large; color: #FFFFFF; position: fixed; right: 0px; top: 0px; text-align: center;">
            <div style="position: relative; top: 20%; font-family: 'Times New Roman';">
                Купите квартиру без расходов<br />
                на проверку и сопровождение сделки<br />
                <br />
                <asp:Button ID="ButtonInfoProduct"
                    runat="server"
                    BackColor="#0078D7"
                    Height="40px"
                    Text="Узнать больше >"
                    Width="160px"
                    Style="background-color: white;" BorderColor="#0078D7" BorderWidth="3px" Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Underline="False" ForeColor="#0078D7"
                    OnClick="ButtonInfoProduct_Click" />
            </div>
        </div>
        <div style="position: absolute; font-family: 'Times New Roman'; background-color: #FFFFFF; left: 0px; width: 15%; height: 100%; color: #525252; top: 0px; resize: both;">
            <strong>
                <asp:Label ID="ProWizard"
                    runat="server"
                    Text="ProWizard"
                    ForeColor="#0078D7"
                    Style="position: absolute; top: 5%; left: 9%; width: 50%; font-size:xx-large;">
                </asp:Label>
            </strong>

            <strong>
                <asp:LinkButton ID="LinkButtonPanelSign"
                    runat="server"
                    Font-Size="Medium"
                    ForeColor="#0078D7"
                    OnClick="LinkButtonPanelSign_Click"
                    Style="text-decoration: none; z-index: 1; position: absolute; top: 15%; left: 10%"
                    ViewStateMode="Enabled">Войти
                </asp:LinkButton>
                <asp:LinkButton ID="LinkButtonPanelRegistration"
                    runat="server"
                    Font-Size="Medium"
                    ForeColor="#525252"
                    OnClick="LinkButtonPanelRegistration_Click"
                    Style="text-decoration: none; z-index: 1; position: absolute; top: 15%; left: 30%"
                    ViewStateMode="Enabled">Регистрация
                </asp:LinkButton>
            </strong>
            <asp:Panel ID="PanelSignIn"
                runat="server"
                Visible="True"
                Style="z-index: 1; width: 80%; height: 80%; min-width: 150px; position: absolute; top: 20%; left: 10%">
                <asp:TextBox ID="TextBoxEmail"
                    runat="server"
                    BorderColor="#0078D7"
                    BorderWidth="2px"
                    ForeColor="#0078D7"
                    placeholder="E-mail"
                    TextMode="Email"
                    ViewStateMode="Enabled"
                    Font-Overline="False"
                    Font-Size="Small"
                    Style="z-index: 1; width: 80%; height: 4%">
                </asp:TextBox>
                <br />
                <br />
                <asp:TextBox ID="TextBoxPasswort"
                    runat="server"
                    BorderColor="#0078D7"
                    BorderWidth="2px"
                    Font-Size="Small"
                    ForeColor="#0078D7"
                    placeholder="Пароль"
                    TextMode="Password"
                    ViewStateMode="Enabled"
                    Style="z-index: 1; width: 80%; height: 4%">
                </asp:TextBox>
                <br />
                <br />
                <asp:LinkButton ID="LinkButtonSignIn"
                    runat="server"
                    Font-Size="Medium"
                    ForeColor="#525252"
                    OnClick="LinkButtonSign_Click"
                    Style="text-decoration: none"
                    ViewStateMode="Enabled">Ок
                </asp:LinkButton>
                <br />
                <br />
                <asp:LinkButton ID="LinkButtonForNewPasswort"
                    runat="server"
                    Font-Size="Small"
                    ForeColor="#525252"
                    OnClick="LinkButtonForNewPasswort_Click"
                    Style="text-decoration: none; position: absolute; bottom: 5%;"
                    ViewStateMode="Disabled">Восстановить пароль
                </asp:LinkButton>
                <br />
                <br />
                <asp:Label ID="LabelInfoSignIn"
                    runat="server"
                    Font-Size="Medium"
                    ForeColor="#62BB47"
                    Text="LabelInfoSignIn"
                    ViewStateMode="Disabled"
                    Visible="False">
                </asp:Label>
                <br />
            </asp:Panel>
            <asp:Panel ID="PanelRegistration"
                runat="server"
                Visible="False"
                Width="175px"
                Style="z-index: 1; width: 80%; height: 35%; min-width: 150px; position: absolute; top: 20%; left: 10%">
                <asp:TextBox ID="TextBoxEmailRegistration"
                    runat="server"
                    BorderColor="#0078D7"
                    BorderWidth="2px"
                    Font-Size="Small"
                    ForeColor="#0078D7"
                    placeholder="E-mail"
                    TextMode="Email"
                    ViewStateMode="Enabled"
                    Style="z-index: 1; width: 80%; height: 8%">
                </asp:TextBox>
                <br />
                <br />
                <asp:TextBox ID="TextBoxPasswortRegistration"
                    runat="server"
                    BorderColor="#0078D7"
                    BorderWidth="2px"
                    Font-Size="Small"
                    ForeColor="#0078D7"
                    placeholder="Пароль"
                    TextMode="Password"
                    ViewStateMode="Enabled"
                    Style="z-index: 1; width: 80%; height: 8%">
                </asp:TextBox>
                <br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <br />
                Контрольный вопрос:<strong><br />
                </strong>
                <asp:Label ID="LabelQuestion"
                    runat="server"
                    Font-Size="Small"
                    ForeColor="#525252"
                    Text="Label">
                </asp:Label>
                <strong>
                    <br />
                    <br />
                    <asp:TextBox ID="TextBoxAnswer"
                        runat="server"
                        BorderColor="#0078D7"
                        BorderWidth="2px"
                        Font-Size="Small"
                        ForeColor="#0078D7"
                        placeholder="Ответ"
                        ViewStateMode="Enabled"
                        Style="z-index: 1; width: 80%; height: 8%">
                    </asp:TextBox>
                    <br />
                    <br />
                </strong>
                <asp:LinkButton ID="LinkButtonRegistration"
                    runat="server"
                    Font-Size="Medium"
                    ForeColor="#525252"
                    OnClick="LinkButtonRegistration_Click"
                    Style="text-decoration: none"
                    ViewStateMode="Enabled">Ок
                </asp:LinkButton>
                <br />
                <br />
                <asp:Label ID="LabelInfoRegistration"
                    runat="server"
                    Font-Size="Medium"
                    ForeColor="#62BB47"
                    Text="LabelInfoRegistration"
                    Visible="False">
                </asp:Label>
            </asp:Panel>
            <asp:Panel ID="PanelForRestore" runat="server"
                Visible="False"
                Width="175px"
                Style="z-index: 1; width: 80%; height: 35%; min-width: 150px; position: absolute; top: 20%; left: 10%">
                <asp:TextBox ID="TextBoxEmailForRestore"
                    runat="server"
                    BorderColor="#0078D7"
                    BorderWidth="2px"
                    ForeColor="#0078D7"
                    placeholder="E-mail"
                    TextMode="Email"
                    ViewStateMode="Enabled"
                    Font-Overline="False"
                    Font-Size="Small"
                    Style="z-index: 1; width: 80%; height: 8%">
                </asp:TextBox>
                <br />
                <br />
                <asp:LinkButton ID="LinkButtonSentNewPassport"
                    runat="server"
                    Font-Size="Medium"
                    ForeColor="#525252"
                    OnClick="LinkButtonSentNewPassport_Click"
                    Style="text-decoration: none"
                    ViewStateMode="Disabled">Отправить пароль
                </asp:LinkButton>
                <br />
                <br />
                <asp:LinkButton ID="LinkButtonBackToMain"
                    runat="server"
                    Font-Size="Medium"
                    ForeColor="#525252"
                    OnClick="LinkButtonBackToMain_Click"
                    Style="text-decoration: none"
                    ViewStateMode="Disabled">Назад
                </asp:LinkButton>
                <br />
                <br />
                <asp:Label ID="LabelInfoForRestore"
                    runat="server"
                    Font-Size="Medium"
                    ForeColor="#62BB47"
                    Text="LabelInfoSignIn"
                    ViewStateMode="Disabled"
                    Visible="False">
                </asp:Label>
            </asp:Panel>
        </div>
    </form>
</body>
</html>
