﻿<%@ Page Title="" Language="C#"
    MasterPageFile="~/WebFormMain.Master"
    AutoEventWireup="true" CodeBehind="WebFormDealBuyer.aspx.cs"
    Inherits="WebApplicationProWizard.WebForms.Deal.WebFormDealBuyer"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
     </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">

    <div style="position: relative; margin: 0 auto; width: 1100px;">
        <div style="position: absolute; top: 80px; background-color: #FFFFFF; width: 100%; height: 400px;">
            <div style="position: relative; left: 20px; top: 20px;">
                <div style="position: absolute; width: 900px; left: 0px;">
                    <asp:Panel ID="Panel1" runat="server">
                        Вы подошли к самому важному шагу - проведению сделки.<br />
                        1) Возьмите с собой документы, мы их подготовили;<br />
                        2) Договоритесь с продавцом где и в каком время вы будет закладывать денежные средства;<br />
                        3) Отдайте договор купли-продажи на оформление в УФРС.
                    </asp:Panel>
                </div>
            </div>
        </div>
        <div style="position: absolute; top: 481px; background-color: #FFFFFF; height: 60px; width: 100%; font-size: small;">
            <div style="position: absolute; right: 20px; top: 10px; width: 160px;">
                <asp:Panel ID="PanelForward"
                    runat="server"
                    EnableViewState="False"
                    ForeColor="#7DC243"
                    Height="40px"
                    ViewStateMode="Disabled"
                    Visible="True"
                    Width="600px">
                    <strong>&nbsp;
                                <asp:Button ID="ButtonForward"
                                    runat="server"
                                    BorderColor="#0078D7"
                                    BorderWidth="1px"
                                    Font-Bold="True"
                                    Font-Italic="False"
                                    Font-Overline="False"
                                    Font-Underline="False"
                                    ForeColor="#0078D7"
                                    Height="40px"
                                    OnClick="ButtonForward_Click"
                                    Style="background-color: white;"
                                    Text="Продолжить &gt;"
                                    Width="140px" />
                    </strong>
                </asp:Panel>
            </div>
        </div>
    </div>

</asp:Content>
