﻿<%@ Page Title="" Language="C#"
    MasterPageFile="~/WebFormMain.Master"
    AutoEventWireup="true"
    CodeBehind="WebFormCallSeller.aspx.cs"
    Inherits="WebApplicationProWizard.WebForms.WebForm"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div style="position: relative; margin: 0 auto; width: 1100px;">
        <div style="position: absolute; top: 80px; background-color: #FFFFFF; width: 100%; height: 80px;">
            <div style="position: relative; left: 20px; top: 10px;">
                <asp:Panel ID="PanelConnect" runat="server" Visible="True">
                    Использую форму ввода что ниже требуется подключить продавца к сделке, вы сможете сделать это по введеному e-mail продавца.
                </asp:Panel>
                <asp:Panel ID="PanelOtherStatus"
                    runat="server"
                    Visible="False">
                    Этап пройден, продавец уже подключен к данной сделке
                </asp:Panel>
            </div>
        </div>
        <div style="position: absolute; top: 161px; background-color: #FFFFFF; width: 100%; height: 80px;">
            <div style="position: relative; left: 20px; top: 10px; width: 250px;">
                <asp:TextBox ID="TextBoxEmailSeller"
                    placeholder="E-mail продавца"
                    runat="server"
                    TextMode="Email"
                    Visible="False"
                    BorderColor="#0078D7"
                    BorderWidth="1px"
                    ForeColor="#0078D7"
                    Height="25px"
                    Width="150px"></asp:TextBox>
                &nbsp;&nbsp;
                <asp:LinkButton ID="LinkButtonFindSeller"
                    runat="server"
                    OnClick="LinkButtonFindSeller_Click"
                    ForeColor="#0078D7"
                    Visible="False"
                    Style="text-decoration: none">Найти</asp:LinkButton>               
            </div>
            <div style="position: absolute; left: 250px; top: 15px; height: 65px; width: 780px;">
                
                <asp:Label ID="LabelInformation"
                    runat="server"
                    Text="LabelInformation"
                    Visible="False"
                    ForeColor="#7DC243"></asp:Label>
            </div>
        </div>
        <div style="position: absolute; top: 242px; background-color: #FFFFFF; height: 60px; width: 100%; font-size: small;">
            <div style="position: relative; left: 20px; top: 10px;">
                <div style="float: right;">
                    <div style="position: relative; right: 30px; top: 0px;">
                        <strong>
                            <asp:Button ID="ButtonSendInvitation"
                                runat="server"
                                Text="Отправить приглашение"
                                Style="background-color: white;"
                                BorderColor="#535B63"
                                BorderWidth="1px"
                                Font-Bold="True"
                                Font-Italic="False"
                                Font-Overline="False"
                                Font-Underline="False"
                                ForeColor="#535B63"
                                Height="40px"
                                OnClick="ButtonSendInvitation_Click"
                                Width="180px"
                                Enabled="False" />
                            &nbsp;&nbsp;
                            <asp:Button ID="ButtonForward"
                                runat="server"
                                Text="Продолжить"
                                Style="background-color: white;"
                                BorderColor="#535B63"
                                BorderWidth="1px"
                                Font-Bold="True"
                                Font-Italic="False"
                                Font-Overline="False"
                                Font-Underline="False"
                                ForeColor="#535B63"
                                Height="40px"
                                OnClick="ButtonForward_Click"
                                Width="140px"
                                Enabled="False" />
                        </strong>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
