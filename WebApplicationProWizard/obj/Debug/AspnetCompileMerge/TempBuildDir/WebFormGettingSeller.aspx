﻿<%@ Page Title="" Language="C#"
    MasterPageFile="~/WebFormMain.Master"
    AutoEventWireup="true"
    CodeBehind="WebFormGettingSeller.aspx.cs"
    Inherits="WebApplicationProWizard.WebForms.Getting.WebFormGettingSeller"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div style="position: relative; margin: 0 auto; width: 1100px; font-family: 'Times New Roman';">
        <div style="position: absolute; top: 80px; width: 100%; height: 130px; background-color: #FFFFFF; font-size: medium; color: #FFFFFF;">
            <div style="position: relative; left: 20px; top: 20px; color: #000000">
                Описание этапа
            </div>
        </div>
        <div style="position: absolute; top: 211px; width: 400px; height: 400px; background-color: #FFFFFF; font-size: medium; font-size: medium; color: #FFFFFF;">
            <div style="position: relative; left: 20px; top: 20px;">
                <strong>
                    <asp:LinkButton ID="LinkButtonExtract"
                        runat="server"
                        Font-Strikeout="False"
                        Font-Underline="False"
                        ForeColor="#0078D7"
                        OnClick="LinkButtonExtract_Click">Расширенная выписка</asp:LinkButton>
                    <br />
                </strong>
                <asp:Label ID="LabelExtract"
                    runat="server"
                    Text="LabelExtract"
                    ForeColor="Black"
                    Font-Size="Medium"></asp:Label>
                <strong>
                    <br />
                    <br />
                    <asp:LinkButton ID="LinkButtonAcceptance"
                        runat="server"
                        Font-Strikeout="False"
                        Font-Underline="False"
                        ForeColor="#0078D7"
                        OnClick="LinkButtonAcceptance_Click">Акт приемки-передачи</asp:LinkButton>
                    <br />
                </strong>
                <asp:Label ID="LabelAcceptance"
                    runat="server"
                    Text="LabelAcceptance"
                    ForeColor="Black"
                    Font-Size="Medium"></asp:Label>
            </div>
        </div>
        <div style="position: absolute; top: 211px; left: 401px; width: 699px; height: 400px; background-color: #FFFFFF; font-size: medium; font-size: medium;">
            <div style="position: relative; left: 20px; top: 20px; width: 650px">
                <asp:Panel ID="PanelExtract"
                    runat="server"
                    ViewStateMode="Disabled"
                    Visible="False">
                    <asp:LinkButton ID="LinkButtonSendExtract"
                        runat="server"
                        OnClick="LinkButtonSendExtract_Click"
                        Visible="False">Отправить на согласование</asp:LinkButton>
                    <asp:FileUpload ID="FileUploadExtract"
                        runat="server"
                        Visible="False" />
                    <asp:Label ID="LabelError"
                        runat="server"
                        Text="LabelError"
                        Visible="False"></asp:Label>
                    <asp:LinkButton ID="LinkButtonDenideExtract"
                        runat="server"
                        OnClick="LinkButtonDenideExtract_Click"
                        Visible="False">Отменить</asp:LinkButton>
                    <br />
                    <br />
                    Описание документа Расширенная выписка
                </asp:Panel>
                <asp:Panel ID="PanelAcceptance"
                    runat="server"
                    ViewStateMode="Disabled"
                    Visible="False">
                    <asp:LinkButton ID="LinkButtonDowloadFilesBuyerAcceptance"
                        runat="server"
                        OnClick="LinkButtonDowloadFilesBuyerAcceptance_Click"
                        Visible="False">Скачать файлы покупателя</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonDowloadFilesSellerAcceptance"
                        runat="server"
                        OnClick="LinkButtonDowloadFilesSellerAcceptance_Click"
                        Visible="False">Скачать мои файлы</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonApproveAcceptance"
                        runat="server"
                        OnClick="LinkButtonApproveAcceptance_Click"
                        Visible="False">Подтвердить</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonDeclineAcceptance"
                        runat="server"
                        OnClick="LinkButtonDeclineAcceptance_Click"
                        Visible="False">Отправить замечания</asp:LinkButton>
                    <asp:FileUpload ID="FileUploadDeclinedFilesAcceptance"
                        runat="server"
                        AllowMultiple="False"
                        Visible="False" />
                    <asp:Label ID="LabelErrorTWO"
                        runat="server"
                        Text="LabelError"
                        Visible="False"></asp:Label>
                    <br />
                    <br />
                    Описание документа Акт приемки-передачи
                </asp:Panel>
                <asp:Panel ID="PanelWait"
                    runat="server"
                    ViewStateMode="Disabled"
                    Visible="False">
                    Панель: документ ожидается от продавца
                </asp:Panel>
                <asp:Panel ID="PanelDenide"
                    runat="server"
                    ViewStateMode="Disabled"
                    Visible="False">
                    Панель: документ отклонен
                </asp:Panel>
            </div>
        </div>
        <div style="position: absolute; top: 612px; background-color: #FFFFFF; height: 60px; width: 100%; font-size: small;">
            <div style="position: absolute; right: 20px; top: 10px; width: 280px;">
                Кнопка для завершения сделки или еще что-то такое
            </div>
        </div>
    </div>

</asp:Content>
