﻿<%@ Page Title="" Language="C#"
    MasterPageFile="~/WebFormMain.Master"
    AutoEventWireup="true"
    CodeBehind="WebFormMainDocumentsSeller.aspx.cs"
    Inherits="WebApplicationProWizard.WebForms.MainDocuments.WebFormMainDocumentsSeller"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div style="position: relative; margin: 0 auto; width: 1100px; font-family: 'Times New Roman';">
        <div style="position: absolute; top: 80px; width: 100%; height: 130px; background-color: #FFFFFF; font-size: medium; color: #000000;">
            <div style="position: relative; left: 20px; top: 20px;">
                <div style="position: absolute; width: 900px; left: 0px;">
                    <asp:Panel ID="PanelCommonInformation"
                        runat="server"
                        EnableViewState="False"
                        ViewStateMode="Disabled"
                        Visible="False">
                        На данном этапе вам потребуется отправить покупателю документы, перечень которых указан ниже.<br />
                        Отправляйте качественные сканы своих документов, чтобы продавец мог выполнить необходимые проверки.<br />
                        После согласования документов вы сможете перейти на следующий этап.
                    </asp:Panel>
                    <asp:Panel ID="PanelCommonInformationClose"
                        runat="server"
                        Visible="False">
                        Этап пройден, форма недоступна для редактирования
                    </asp:Panel>
                </div>
            </div>
        </div>
        <div style="position: absolute; top: 211px; width: 400px; height: 500px; background-color: #FFFFFF; font-size: medium; font-size: medium; color: #FFFFFF;">
            <div style="position: relative; left: 20px; top: 20px;">
                <div style="position: relative; left: 20px; top: 20px; width: 650px">
                    <strong>
                        <asp:LinkButton ID="LinkButtonPassport"
                            runat="server"
                            OnClick="LinkPassport_Click"
                            ForeColor="#0078D7"
                            Font-Strikeout="False">Паспорт</asp:LinkButton>
                    </strong>
                    <asp:Label ID="LabelPassport"
                        runat="server"
                        Text="LabelPassport"
                        ForeColor="Black"
                        Font-Size="Medium"></asp:Label>
                    <strong>
                        <br />
                        <br />
                        <asp:LinkButton ID="LinkButtonSertificateOfOwnerShip"
                            runat="server"
                            OnClick="LinkButtonSertificateOfOwnerShip_Click"
                            ForeColor="#0078D7"
                            Font-Strikeout="False">Свидетельство</asp:LinkButton>
                    </strong>
                    <asp:Label ID="LabelSertificateOfOwnerShip"
                        runat="server"
                        Text="LabelSertificateOfOwnerShip"
                        ForeColor="Black"
                        Font-Size="Medium"></asp:Label>
                    <strong>
                        <br />
                        <br />
                        <asp:LinkButton ID="LinkButtonDocumentFoundation"
                            runat="server"
                            OnClick="LinkButtonDocumentFoundation_Click"
                            ForeColor="#0078D7"
                            Font-Strikeout="False">Документ основание</asp:LinkButton>
                    </strong>
                    <asp:Label ID="LabelDocumentFoundation"
                        runat="server"
                        Text="LabelDocumentFoundation"
                        ForeColor="Black"
                        Font-Size="Medium"></asp:Label>
                    <strong>
                        <br />
                        <br />
                        <asp:LinkButton ID="LinkButtonExtractFromRegister"
                            runat="server"
                            OnClick="LinkButtonExtractFromRegister_Click"
                            ForeColor="#0078D7"
                            Font-Strikeout="False">Выписка ЕГРП</asp:LinkButton>
                    </strong>
                    <asp:Label ID="LabelExtractFromRegister"
                        runat="server"
                        Text="LabelExtractFromRegister"
                        ForeColor="Black"
                        Font-Size="Medium"></asp:Label>
                    <strong>
                        <br />
                        <br />
                        <asp:LinkButton ID="LinkButtonExtendedExtractFromRegister"
                            runat="server"
                            OnClick="LinkButtonExtendedExtractFromRegister_Click"
                            ForeColor="#0078D7"
                            Font-Strikeout="False">Расширенная выписка</asp:LinkButton>
                    </strong>
                    <asp:Label ID="LabelExtendedExtractFromRegister"
                        runat="server"
                        Text="LabelExtendedExtractFromRegister"
                        ForeColor="Black"
                        Font-Size="Medium"></asp:Label>
                    <strong>
                        <br />
                        <br />
                        <asp:LinkButton ID="LinkButtonTechnicalPassport"
                            runat="server"
                            OnClick="LinkButtonTechnicalPassport_Click"
                            ForeColor="#0078D7"
                            Font-Strikeout="False">Технический паспорт</asp:LinkButton>
                    </strong>
                    <asp:Label ID="LabelTechnicalPassport"
                        runat="server"
                        Text="LabelTechnicalPassport"
                        ForeColor="Black"
                        Font-Size="Medium"></asp:Label>
                    <strong>
                        <br />
                        <br />
                        <asp:LinkButton ID="LinkButtonConsentOfSpouse"
                            runat="server"
                            OnClick="LinkButtonConsentOfSpouse_Click"
                            ForeColor="#0078D7"
                            Font-Strikeout="False">Согласие супруга</asp:LinkButton>
                    </strong>
                    <asp:Label ID="LabelConsentOfSpouse"
                        runat="server"
                        Text="LabelConsentOfSpouse"
                        ForeColor="Black"
                        Font-Size="Medium"></asp:Label>
                    <strong>
                        <br />
                        <br />
                        <asp:LinkButton ID="LinkButtonResolutionOfGuardianship"
                            runat="server"
                            OnClick="LinkButtonResolutionOfGuardianship_Click"
                            ForeColor="#0078D7"
                            Font-Strikeout="False">Разрешение опеки</asp:LinkButton>
                    </strong>
                    <asp:Label ID="LabelResolutionOfGuardianship"
                        runat="server"
                        Text="LabelResolutionOfGuardianship"
                        ForeColor="Black"
                        Font-Size="Medium"></asp:Label>   
                    <strong>
                        <br />
                        <br />
                        <asp:LinkButton ID="LinkButtonDebtForUtility"
                            runat="server"
                            OnClick="LinkButtonDebtForUtility_Click"
                            ForeColor="#0078D7"
                            Font-Strikeout="False">Справка по КУ</asp:LinkButton>
                    </strong>
                    <asp:Label ID="LabelDebtForUtility"
                        runat="server"
                        Text="LabelDebtForUtility"
                        ForeColor="Black"
                        Font-Size="Medium"></asp:Label>
                    <strong>
                        <br />
                        <br />
                        <asp:LinkButton ID="LinkButtonCertificateFromTaxInspection"
                            runat="server"
                            OnClick="LinkButtonCertificateFromTaxInspection_Click"
                            ForeColor="#0078D7"
                            Font-Strikeout="False">Справка по налогам</asp:LinkButton>
                    </strong>
                    <asp:Label ID="LabelCertificateFromTaxInspection"
                        runat="server"
                        Text="LabelCertificateFromTaxInspection"
                        ForeColor="Black"
                        Font-Size="Medium"></asp:Label>
                </div>
            </div>
        </div>
        <div style="position: absolute; top: 211px; left: 401px; width: 699px; height: 500px; background-color: #FFFFFF; font-size: medium; font-size: medium;">
            <asp:Panel ID="PanelPassportMain"
                runat="server"
                ViewStateMode="Enabled"
                Visible="False">
                <asp:LinkButton ID="LinkButtonSendPassport"
                    runat="server"
                    OnClick="LinkButtonSendPassport_Click"
                    Visible="False"
                    ViewStateMode="Disabled">Отправить на согласование</asp:LinkButton>
                <asp:FileUpload ID="FileUploadPassport"
                    runat="server"
                    Visible="False"
                    AllowMultiple="False"
                    ViewStateMode="Disabled" />
                <asp:Label ID="LabelPassportInformation"
                    runat="server"
                    Text="LabelPassportInformation"
                    Visible="False"
                    ViewStateMode="Disabled"></asp:Label>
                <asp:LinkButton ID="LinkButtonDenidePassport"
                    runat="server"
                    OnClick="LinkButtonDenidePassport_Click"
                    Visible="False"
                    ValidateRequestMode="Disabled"
                    ViewStateMode="Disabled">Отменить</asp:LinkButton>
            </asp:Panel>
            <asp:Panel ID="PanelPassportPrepare"
                runat="server"
                ViewStateMode="Disabled"
                Visible="False">
                Здесь должна быть информация про подготовку паспорта и отправку его на согласование
            </asp:Panel>
            <asp:Panel ID="PanelSertificateOfOwnerShipMain"
                runat="server"
                ViewStateMode="Enabled"
                Visible="False">
                <asp:LinkButton ID="LinkButtonSendSertificateOfOwnerShip"
                    runat="server"
                    OnClick="LinkButtonSendSertificateOfOwnerShip_Click"
                    Visible="False"
                    ViewStateMode="Disabled">Отправить на согласование</asp:LinkButton>
                <asp:FileUpload ID="FileUploadSertificateOfOwnerShip"
                    runat="server"
                    Visible="False"
                    AllowMultiple="False"
                    ViewStateMode="Disabled" />
                <asp:Label ID="LabelSertificateOfOwnerShipInformation"
                    runat="server"
                    Text="LabelSertificateOfOwnerShipInformation"
                    Visible="False"
                    ViewStateMode="Disabled"></asp:Label>
                <asp:LinkButton ID="LinkButtonDenideSertificateOfOwnerShip" runat="server" OnClick="LinkButtonDenideSertificateOfOwnerShip_Click" Visible="False" ViewStateMode="Disabled">Отменить</asp:LinkButton>
            </asp:Panel>
            <asp:Panel ID="PanelSertificateOfOwnerShipPrepare"
                runat="server"
                ViewStateMode="Disabled"
                Visible="False">
                Здесь должна быть информация про подготовку свидетельства и отправку его на согласование
            </asp:Panel>
            <asp:Panel ID="PanelDocumentFoundationMain"
                runat="server"
                ViewStateMode="Enabled"
                Visible="False">
                <asp:LinkButton ID="LinkButtonSendDocumentFoundation"
                    runat="server"
                    OnClick="LinkButtonSendDocumentFoundation_Click"
                    Visible="False"
                    ViewStateMode="Disabled">Отправить на согласование</asp:LinkButton>
                <asp:FileUpload ID="FileUploadDocumentFoundation"
                    runat="server"
                    Visible="False"
                    AllowMultiple="False"
                    ViewStateMode="Disabled" />
                <asp:Label ID="LabelDocumentFoundationtInformation"
                    runat="server"
                    Text="LabelDocumentFoundationtInformation"
                    Visible="False"
                    ViewStateMode="Disabled"></asp:Label>
                <asp:LinkButton ID="LinkButtonDenideDocumentFoundation"
                    runat="server"
                    OnClick="LinkButtonDenideDocumentFoundationt_Click"
                    Visible="False"
                    ViewStateMode="Disabled">Отменить</asp:LinkButton>
            </asp:Panel>
            <asp:Panel ID="PanelDocumentFoundationPrepare"
                runat="server"
                ViewStateMode="Disabled"
                Visible="False">
                Здесь должна быть информация про подготовку документа основания и отправку его на согласование
            </asp:Panel>
            <asp:Panel ID="PanelExtractFromRegisterMain"
                runat="server"
                ViewStateMode="Enabled"
                Visible="False">
                <asp:LinkButton ID="LinkButtonSendExtractFromRegister"
                    runat="server"
                    OnClick="LinkButtonSendExtractFromRegister_Click"
                    Visible="False"
                    ViewStateMode="Disabled">Отправить на согласование</asp:LinkButton>
                <asp:FileUpload ID="FileUploadExtractFromRegister"
                    runat="server"
                    Visible="False"
                    AllowMultiple="False"
                    ViewStateMode="Disabled" />
                <asp:Label ID="LabelExtractFromRegisterInformation"
                    runat="server"
                    Text="LabelExtractFromRegisterInformation"
                    Visible="False"
                    ViewStateMode="Disabled"></asp:Label>
                <asp:LinkButton ID="LinkButtonDenideExtractFromRegister"
                    runat="server"
                    OnClick="LinkButtonDenideExtractFromRegister_Click"
                    Visible="False"
                    ViewStateMode="Disabled">Отменить</asp:LinkButton>
            </asp:Panel>
            <asp:Panel ID="PanelExtractFromRegisterPrepare"
                runat="server"
                ViewStateMode="Disabled"
                Visible="False">
                Здесь должна быть информация про подготовку выписки из ЕГРП и отправку его на согласование
            </asp:Panel>
            <asp:Panel ID="PanelExtendedExtractFromRegisterMain"
                runat="server"
                ViewStateMode="Enabled"
                Visible="False">
                <asp:LinkButton ID="LinkButtonSendExtendedExtractFromRegister"
                    runat="server"
                    OnClick="LinkButtonSendExtendedExtractFromRegister_Click"
                    Visible="False"
                    ViewStateMode="Disabled">Отправить на согласование</asp:LinkButton>
                <asp:FileUpload ID="FileUploadExtendedExtractFromRegister"
                    runat="server"
                    Visible="False"
                    AllowMultiple="False"
                    ViewStateMode="Disabled" />
                <asp:Label ID="LabelExtendedExtractFromRegisterInformation"
                    runat="server"
                    Text="LabelExtendedExtractFromRegisterInformation"
                    Visible="False"
                    ViewStateMode="Enabled"></asp:Label>
                <asp:LinkButton ID="LinkButtonDenideExtendedExtractFromRegister"
                    runat="server"
                    OnClick="LinkButtonDenideExtendedExtractFromRegister_Click"
                    Visible="False"
                    ViewStateMode="Disabled">Отменить</asp:LinkButton>
            </asp:Panel>
            <asp:Panel ID="PanelExtendedExtractFromRegisterPrepare"
                runat="server"
                ViewStateMode="Disabled"
                Visible="False">
                Здесь должна быть информация про подготовку выписки из домовой книги и отправку его на согласование
            </asp:Panel>
            <asp:Panel ID="PanelTechnicalPassportMain"
                runat="server"
                ViewStateMode="Enabled"
                Visible="False">
                <asp:LinkButton ID="LinkButtonSendTechnicalPassport"
                    runat="server"
                    OnClick="LinkButtonSendTechnicalPassport_Click"
                    Visible="False"
                    ViewStateMode="Disabled">Отправить на согласование</asp:LinkButton>
                <asp:FileUpload ID="FileUploadTechnicalPassport"
                    runat="server"
                    Visible="False"
                    AllowMultiple="False"
                    ViewStateMode="Disabled" />
                <asp:Label ID="LabelTechnicalPassportInformation"
                    runat="server"
                    Text="LabelTechnicalPassportInformation"
                    Visible="False"
                    ViewStateMode="Disabled"></asp:Label>
                <asp:LinkButton ID="LinkButtonDenideTechnicalPassport"
                    runat="server"
                    OnClick="LinkButtonDenideTechnicalPassport_Click"
                    Visible="False"
                    ViewStateMode="Disabled">Отменить</asp:LinkButton>
            </asp:Panel>
            <asp:Panel ID="PanelTechnicalPassportPrepare"
                runat="server"
                ViewStateMode="Disabled"
                Visible="False">
                Здесь должна быть информация про технического паспорта и отправку его на согласование
            </asp:Panel>
            <asp:Panel ID="PanelConsentOfSpouseMain"
                runat="server"
                ViewStateMode="Enabled"
                Visible="False">
                <asp:LinkButton ID="LinkButtonSendConsentOfSpouse"
                    runat="server"
                    OnClick="LinkButtonSendConsentOfSpouse_Click"
                    Visible="False"
                    ViewStateMode="Disabled">Отправить на согласование</asp:LinkButton>
                <asp:FileUpload ID="FileUploadConsentOfSpouse"
                    runat="server"
                    Visible="False"
                    AllowMultiple="False"
                    ViewStateMode="Disabled" />
                <asp:Label ID="LabelConsentOfSpouseInformation"
                    runat="server"
                    Text="LabelConsentOfSpouseInformation"
                    Visible="False"
                    ViewStateMode="Disabled"></asp:Label>
                <asp:LinkButton ID="LinkButtonDenideConsentOfSpouse"
                    runat="server"
                    OnClick="LinkButtonDenideConsentOfSpouse_Click"
                    Visible="False"
                    ViewStateMode="Disabled">Отменить</asp:LinkButton>
            </asp:Panel>
            <asp:Panel ID="PanelConsentOfSpousePrepare"
                runat="server"
                ViewStateMode="Disabled"
                Visible="False">
                Здесь должна быть информация про согласие супруги и отправку его на согласование
            </asp:Panel>
            <asp:Panel ID="PanelResolutionOfGuardianshipMain"
                runat="server"
                ViewStateMode="Enabled"
                Visible="False">
                <asp:LinkButton ID="LinkButtonSendResolutionOfGuardianship"
                    runat="server"
                    OnClick="LinkButtonSendResolutionOfGuardianship_Click"
                    Visible="False"
                    ViewStateMode="Disabled">Отправить на согласование</asp:LinkButton>
                <asp:FileUpload ID="FileUploadResolutionOfGuardianship"
                    runat="server"
                    Visible="False"
                    AllowMultiple="False"
                    ViewStateMode="Disabled" />
                <asp:Label ID="LabelResolutionOfGuardianshipInformation"
                    runat="server"
                    Text="LabelResolutionOfGuardianshipInformation"
                    Visible="False"
                    ViewStateMode="Disabled"></asp:Label>
                <asp:LinkButton ID="LinkButtonDenideResolutionOfGuardianship"
                    runat="server"
                    OnClick="LinkButtonDenideResolutionOfGuardianship_Click"
                    Visible="False"
                    ViewStateMode="Disabled">Отменить</asp:LinkButton>
            </asp:Panel>
            <asp:Panel ID="PanelResolutionOfGuardianshipPrepare"
                runat="server"
                ViewStateMode="Disabled"
                Visible="False">
                Здесь должна быть информация про разрешение органов опеки и отправку его на согласование
            </asp:Panel>
            <asp:Panel ID="PanelDebtForUtilityMain"
                runat="server"
                ViewStateMode="Enabled"
                Visible="False">
                <asp:LinkButton ID="LinkButtonSendDebtForUtility"
                    runat="server"
                    OnClick="LinkButtonSendDebtForUtility_Click"
                    Visible="False"
                    ViewStateMode="Disabled">Отправить на согласование</asp:LinkButton>
                <asp:FileUpload ID="FileUploadDebtForUtility"
                    runat="server"
                    Visible="False"
                    AllowMultiple="False"
                    ViewStateMode="Disabled" />
                <asp:Label ID="LabelDebtForUtilityInformation"
                    runat="server"
                    Text="LabelDebtForUtilityInformation"
                    Visible="False"
                    ViewStateMode="Disabled"></asp:Label>
                <asp:LinkButton ID="LinkButtonDenideDebtForUtility"
                    runat="server"
                    OnClick="LinkButtonDenideDebtForUtility_Click"
                    Visible="False"
                    ViewStateMode="Disabled">Отменить</asp:LinkButton>
            </asp:Panel>
            <asp:Panel ID="PanelDebtForUtilityPrepare"
                runat="server"
                ViewStateMode="Disabled"
                Visible="False">
                Здесь должна быть информация по коммуналке и отправку его на согласование
            </asp:Panel>
            <asp:Panel ID="PanelCertificateFromTaxInspectionMain"
                runat="server"
                ViewStateMode="Enabled"
                Visible="False">
                <asp:LinkButton ID="LinkButtonSendCertificateFromTaxInspection"
                    runat="server"
                    OnClick="LinkButtonSendCertificateFromTaxInspection_Click"
                    Visible="False"
                    ViewStateMode="Disabled">Отправить на согласование</asp:LinkButton>
                <asp:FileUpload ID="FileUploadCertificateFromTaxInspection"
                    runat="server"
                    Visible="False"
                    AllowMultiple="False"
                    ViewStateMode="Disabled" />
                <asp:Label ID="LabelCertificateFromTaxInspectionInformation"
                    runat="server"
                    Text="LabelCertificateFromTaxInspectionInformation"
                    Visible="False"
                    ViewStateMode="Disabled"></asp:Label>
                <asp:LinkButton ID="LinkButtonDenideCertificateFromTaxInspection"
                    runat="server"
                    OnClick="LinkButtonDenideCertificateFromTaxInspection_Click"
                    Visible="False"
                    ViewStateMode="Disabled">Отменить</asp:LinkButton>
            </asp:Panel>
            <asp:Panel ID="PanelCertificateFromTaxInspectionPrepare"
                runat="server"
                ViewStateMode="Disabled"
                Visible="False">
                Здесь должна быть информация по налоговой задолженности и отправку его на согласование
            </asp:Panel>
            <asp:Panel ID="PanelCommonUponAccepting"
                runat="server"
                ViewStateMode="Disabled"
                Visible="False">
                Документ находится на согласовании
            </asp:Panel>
            <asp:Panel ID="PanelCommonDenide"
                runat="server"
                ViewStateMode="Disabled"
                Visible="False">
                Документ отклонен
            </asp:Panel>
            <asp:Panel ID="PanelCommonCheck"
                runat="server"
                ViewStateMode="Disabled"
                Visible="False">
                Документ проверен
            </asp:Panel>
        </div>
        <div style="position: absolute; top: 712px; background-color: #FFFFFF; height: 60px; width: 100%; font-size: small;">
            <div style="position: absolute; right: 20px; top: 10px; width: 300px;">
                <asp:Panel ID="PanelForward"
                    runat="server"
                    EnableViewState="False"
                    ForeColor="#7DC243"
                    Height="40px"
                    ViewStateMode="Disabled"
                    Visible="True"
                    Width="600px">
                    <strong>&nbsp;
                    Описание как перейти на следующий этап
                    </strong>
                </asp:Panel>
            </div>
        </div>
    </div>

</asp:Content>
