﻿<%@ Page Language="C#"
    AutoEventWireup="true"
    CodeBehind="WebFormAbout.aspx.cs"
    Inherits="WebApplicationProWizard.WebFormAbout" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1"
        runat="server"
        style="width: 100%; position: absolute; top: 0px; left: 0px; bottom: 0px; font-family: 'Times New Roman'; background-color: #F3F3F3;">
        <div style="position: relative; width: 100%; height: 50px; background-color: #F3F3F3; top: 0px; color: #FFFFFF; left: 0px; right: 0px;">
            <div style="position: relative; width: 1100px; margin: 0 auto;">
                <div style="position: absolute; top: 10px; font-size: xx-large;">
                    <strong>
                        <asp:Label ID="ProWizard"
                            runat="server"
                            Text="ProWizard"
                            ForeColor="#0078D7"></asp:Label>
                    </strong>
                </div>
                <div style="float: right; top: 2px; width: 180px; height: 48px; background-color: #F3F3F3; font-size: medium; color: #FFFFFF;">
                    <asp:Label ID="LabelEmail"
                        runat="server"
                        Text="EMail"
                        ValidateRequestMode="Disabled"
                        Visible="False"
                        ViewStateMode="Disabled"
                        ForeColor="#000000"
                        Font-Size="Small"></asp:Label>
                </div>
            </div>
        </div>
        <div style="position: relative; width: 100%; height: 2px; background-color: #0078D7;">
        </div>
        <div style="width: 100%; position: relative; top: 0px; background-color: #3A5068; height: 100%; overflow: scroll;">
            <div style="width: 100%; height: 500px; position: relative; background-color: #F1F1F1;">
            </div>
            <div style="position: relative; width: 100%; height: 1px; background-color: #0078D7;">
            </div>
            <div style="width: 100%; height: 500px; position: relative; background-color: #F1F1F1;">
            </div>
            <div style="position: relative; width: 100%; height: 1px; background-color: #0078D7;">
            </div>
            <div style="width: 100%; height: 500px; position: relative; background-color: #F1F1F1;">
            </div>
            <div style="position: relative; width: 100%; height: 1px; background-color: #0078D7;">
            </div>
            <div style="width: 100%; height: 200px; position: relative; background-color: #F1F1F1;">
            </div>
        </div>
    </form>
</body>
</html>
