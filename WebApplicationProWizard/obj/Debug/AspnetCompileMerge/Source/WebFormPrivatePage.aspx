﻿<%@ Page Language="C#"
    AutoEventWireup="true"
    CodeBehind="WebFormPrivatePage.aspx.cs"
    Inherits="WebApplicationProWizard.WebFormPrivatePage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <img src="Content/Test.jpg" style="max-width: 100%; min-width: 100%; position: fixed; top: 0px; left: 0px;" />
        <div style="position: relative; width: 100%; height: 50px; top: 0px; color: #FFFFFF; left: 0px; right: 0px;">
            <div style="position: relative; width: 1100px; margin: 0 auto;">
                <div style="position: absolute; top: 10px; font-size: xx-large;">
                    <strong>
                        <asp:Label ID="ProWizard"
                            runat="server"
                            Text="ProWizard"
                            ForeColor="#0078D7"></asp:Label>
                    </strong>
                </div>
                <div style="float: right; top: 2px; width: 180px; height: 48px; font-size: medium; color: #FFFFFF;">
                    <asp:Label ID="LabelEmail"
                        runat="server"
                        Text="EMail"
                        ValidateRequestMode="Disabled"
                        ViewStateMode="Disabled"
                        ForeColor="Black"
                        Font-Size="Small"></asp:Label>
                    &nbsp;<br />
                    <asp:LinkButton ID="LinkButtonSignOut"
                        runat="server"
                        OnClick="LinkButtonSignOut_Click"
                        ValidateRequestMode="Disabled"
                        ViewStateMode="Disabled"
                        ForeColor="#000000"
                        Style="text-decoration: none"
                        Font-Size="Small">Выйти</asp:LinkButton>
                    &nbsp;&nbsp;
                <asp:LinkButton ID="LinkButtonPrivateSettings"
                    runat="server"
                    OnClick="LinkButtonPrivateSettings_Click"
                    ValidateRequestMode="Disabled"
                    ViewStateMode="Disabled"
                    ForeColor="#00000"
                    Style="text-decoration: none"
                    Font-Size="Small">Настройки</asp:LinkButton>
                </div>
            </div>
        </div>
        <div style="position: relative; width: 100%; height: 2px; background-color: #0078D7; z-index: 1;">
        </div>
        <div style="position: relative; width: 1100px; top: 50px; height: 50px; margin: 0 auto;">
            <asp:LinkButton ID="LinkButtonSectionBuyer"
                runat="server"
                Font-Size="X-Large"
                ForeColor="#0078D7"
                OnClick="LinkButtonSectionBuyer_Click"
                Style="text-decoration: none;"
                ViewStateMode="Enabled">Раздел покупателя
            </asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="LinkButtonSectionSeller"
                runat="server"
                Font-Size="X-Large"
                ForeColor="#525252"
                OnClick="LinkButtonSectionSeller_Click"
                Style="text-decoration: none;"
                ViewStateMode="Enabled">Раздел продавца
            </asp:LinkButton>
        </div>
        <div style="position: relative; width: 1100px; top: 50px; height: 500px; margin: 0 auto; background-color: #FFFFFF;">
            <div style="position: relative; top: 75px; left: 30px; width: 200px;">
                <asp:Button ID="ButtonCreateDeal"
                    runat="server"
                    Text="+ Создать сделку"
                    Style="background-color: white;"
                    BorderColor="#0078D7"
                    BorderWidth="3px"
                    Font-Bold="True"
                    Font-Italic="False"
                    Font-Overline="False"
                    Font-Size="Small"
                    Font-Underline="False"
                    ForeColor="#0078D7"
                    Height="40px"
                    OnClick="ButtonCreateDeal_Click"
                    Width="140px" />
            </div>
            <div style="position: absolute; top: 50px; left: 500px; font-size: large">
                <asp:Label ID="LabelStatusBuyer"
                    runat="server"
                    Text="Здесь будут ваши сделки покупки"
                    Visible="False"
                    Enabled="False"></asp:Label>
            </div>
            <div style="position: absolute; top: 50px; left: 500px">
                <asp:GridView ID="GridViewCurrentDealBuy"
                    runat="server"
                    AllowPaging="True"
                    AutoGenerateColumns="False"
                    DataSourceID="DealBuy"
                    OnSelectedIndexChanged="GridViewCurrentDealBuy_SelectedIndexChanged"
                    GridLines="None"
                    Style="color: #0078D7" CellPadding="4" ForeColor="#FFFFFF">
                    <AlternatingRowStyle BackColor="#FFFFFF" />
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" SelectText="Перейти   ">
                            <ControlStyle ForeColor="#525252" />
                        </asp:CommandField>
                        <asp:BoundField DataField="IdDeal" SortExpression="IdDeal" />
                        <asp:BoundField DataField="StatusDeal" SortExpression="StatusDeal" />
                    </Columns>
                    <EditRowStyle BackColor="#FFFFFF" />
                    <FooterStyle BackColor="#FFFFFF" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#FFFFFF" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#FFFFFF" ForeColor="#525252" HorizontalAlign="Center" />
                    <RowStyle BackColor="#FFFFFF" />
                    <SelectedRowStyle BackColor="#FFFFFF" Font-Bold="True" ForeColor="#FFFFFF" />
                    <SortedAscendingCellStyle BackColor="#FFFFFF" />
                    <SortedAscendingHeaderStyle BackColor="#FFFFFF" />
                    <SortedDescendingCellStyle BackColor="#FFFFFF" />
                    <SortedDescendingHeaderStyle BackColor="#FFFFFF" />
                </asp:GridView>
                <asp:SqlDataSource ID="DealBuy" runat="server" ConnectionString="<%$ ConnectionStrings:TableDeal %>"
                    SelectCommand="SELECT IdDeal, StatusDeal FROM TableDeal WHERE (IdUserBuyer = @IdUserBuyer) AND (NumberObject = 1)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="LabelIdUserBuyer" Name="IdUserBuyer" PropertyName="Text" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:Label ID="LabelIdUserBuyer" runat="server" Text="LabelIdUserBuyer" Visible="False"></asp:Label>
            </div>
            <div style="position: absolute; top: 50px; left: 500px; font-size: large">
                <asp:Label ID="LabelStatusSell"
                    runat="server"
                    Text="Здесь будут отображаться сделки<br/> в которых вы участвуете как продавец.<br/><br/>Сделка появится в списке после того <br/>как покупатель вас подключит"
                    Visible="False"></asp:Label>
            </div>
            <div style="position: absolute; top: 50px; left: 500px">
                <asp:GridView ID="GridViewCurrentDealSell"
                    runat="server"
                    AllowPaging="True"
                    AutoGenerateColumns="False"
                    Visible="False"
                    DataSourceID="DealSell"
                    OnSelectedIndexChanged="GridViewCurrentDealSell_SelectedIndexChanged"
                    GridLines="None"
                    Style="color: #0078D7" CellPadding="4" ForeColor="#FFFFFF">
                    <AlternatingRowStyle BackColor="#FFFFFF" />
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" SelectText="Перейти   ">
                            <ControlStyle ForeColor="#525252" />
                        </asp:CommandField>
                        <asp:BoundField DataField="IdDeal" SortExpression="IdDeal" />
                        <asp:BoundField DataField="StatusDeal" SortExpression="StatusDeal" />
                    </Columns>
                    <EditRowStyle BackColor="#FFFFFF" />
                    <FooterStyle BackColor="#FFFFFF" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#FFFFFF" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#FFFFFF" ForeColor="#525252" HorizontalAlign="Center" />
                    <RowStyle BackColor="#FFFFFF" />
                    <SelectedRowStyle BackColor="#FFFFFF" Font-Bold="True" ForeColor="#FFFFFF" />
                    <SortedAscendingCellStyle BackColor="#FFFFFF" />
                    <SortedAscendingHeaderStyle BackColor="#FFFFFF" />
                    <SortedDescendingCellStyle BackColor="#FFFFFF" />
                    <SortedDescendingHeaderStyle BackColor="#FFFFFF" />
                </asp:GridView>
                <asp:SqlDataSource ID="DealSell"
                    runat="server"
                    ConnectionString="<%$ ConnectionStrings:TableDeal %>"
                    SelectCommand="SELECT IdDeal, StatusDeal, IdUserBuyer, IdUserSeller FROM TableDeal WHERE (IdUserSeller = @IdUserSeller) AND (NumberObject = 1)">
                    <SelectParameters>
                        <asp:ControlParameter
                            ControlID="LabelIdUserBuyer"
                            Name="IdUserSeller"
                            PropertyName="Text"
                            Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </div>
            <div style="position: absolute; top: 0px; left: 450px; height: 100%; width: 1px; background-color: #9FDAE2">
            </div>
        </div>
    </form>
</body>
</html>
