﻿<%@ Page Title="" Language="C#"
    MasterPageFile="~/WebFormMain.Master"
    AutoEventWireup="true"
    CodeBehind="WebFormNewDeal.aspx.cs"
    Inherits="WebApplicationProWizard.WebForms.WebFormNewDeal"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div style="position: relative; margin: 0 auto; width: 1100px; font-family: 'Times New Roman';">
        <div style="position: absolute; top: 80px; width: 100%; height: 130px; background-color: #FFFFFF; font-size: medium; color: #FFFFFF;">
            <div style="position: relative; left: 20px; top: 20px;">
                <asp:Label ID="LabelInformation"
                    runat="server"
                    Text="Из открытых источников подберите варинаты, заполните по каждому опросник и начните сделку"
                    ForeColor="#525252">
                </asp:Label>
                <br />
                <br />
                <asp:TextBox ID="TextBoxLinkObjectONE" placeholder="Вставьте сюда ссылку на объявление"
                    runat="server"
                    TextMode="Url"
                    Width="550px"
                    Columns="1"
                    Visible="False"
                    ViewStateMode="Enabled"
                    BorderColor="#0078D7"
                    BorderWidth="1px"
                    ForeColor="#0078D7"
                    Height="25px"></asp:TextBox>
                <asp:LinkButton ID="LinkButtonRedirectObjectONE"
                    runat="server"
                    OnClick="LinkButtonRedirectObjectOne_Click"
                    Visible="False"
                    ViewStateMode="Enabled"
                    ForeColor="#525252"
                    Style="text-decoration: none">Открыть</asp:LinkButton>
            </div>
        </div>
        <div style="position: absolute; top: 211px; width: 400px; height: 400px; background-color: #FFFFFF; font-size: medium; font-size: medium; color: #FFFFFF;">
            <div style="position: relative; left: 20px; top: 20px;">
                <asp:Panel ID="PanelObjectONE"
                    runat="server"
                    Width="220px"
                    Visible="False"
                    Height="190px">
                    <asp:CheckBox ID="CheckBox3YearOwner"
                        runat="server"
                        EnableTheming="True"
                        Text=" "
                        ViewStateMode="Enabled"
                        Visible="False"
                        Font-Bold="False"
                        Font-Italic="False"
                        Font-Overline="False"
                        Font-Size="Small"
                        Font-Strikeout="False"
                        Font-Underline="False" />
                    <asp:LinkButton ID="LinkButton3YearOwner"
                        runat="server"
                        ForeColor="#0078D7"
                        OnClick="LinkButton3YearOwner_Click"
                        Style="text-decoration: none"
                        ViewStateMode="Enabled"
                        Visible="False">Более 3-х в собственности</asp:LinkButton>
                    <br />
                    <br />
                    <asp:CheckBox ID="CheckBoxPriceContract"
                        runat="server"
                        Text=" "
                        ViewStateMode="Enabled"
                        Visible="False"
                        BorderColor="#66CCFF"
                        ForeColor="#66CCFF" />
                    <asp:LinkButton ID="LinkButtonPriceContract"
                        runat="server"
                        ForeColor="#0078D7"
                        OnClick="LinkButtonPriceContract_Click"
                        Style="text-decoration: none"
                        ViewStateMode="Enabled"
                        Visible="False">Полная стоимость</asp:LinkButton>
                    <br />
                    <br />
                    <asp:CheckBox ID="CheckBoxPriceMarket"
                        runat="server"
                        Text=" "
                        ViewStateMode="Enabled"
                        Visible="False"
                        BorderColor="#66CCFF"
                        ForeColor="#66CCFF" />
                    <asp:LinkButton ID="LinkButtonPriceMarket"
                        runat="server"
                        ForeColor="#0078D7"
                        OnClick="LinkButtonPriceMarket_Click"
                        Style="text-decoration: none"
                        ViewStateMode="Enabled"
                        Visible="False">Рыночная стоимость</asp:LinkButton>
                    <br />
                    <br />
                    <asp:CheckBox ID="CheckBoxContractForOriginal"
                        runat="server"
                        Text=" "
                        ViewStateMode="Enabled"
                        Visible="False" />
                    <asp:LinkButton ID="LinkButtonContractForOriginal"
                        runat="server"
                        ForeColor="#0078D7"
                        OnClick="LinkButtonContractForOriginal_Click"
                        Style="text-decoration: none"
                        ViewStateMode="Enabled"
                        Visible="False">Сделка без доверенностей</asp:LinkButton>
                    <br />
                    <br />
                    <asp:CheckBox ID="CheckBoxExtractOwner"
                        runat="server"
                        Text=" "
                        ViewStateMode="Enabled"
                        Visible="False" />
                    <asp:LinkButton ID="LinkButtonExtractOwner"
                        runat="server"
                        ForeColor="#0078D7"
                        OnClick="LinkButtonExtractOwner_Click"
                        Style="text-decoration: none"
                        ViewStateMode="Enabled"
                        Visible="False">Выписка до сделки</asp:LinkButton>
                    <br />
                    <br />
                    <asp:CheckBox ID="CheckBoxFakeAdress"
                        runat="server"
                        Text=" "
                        ViewStateMode="Enabled"
                        Visible="False" />
                    <asp:LinkButton ID="LinkButtonFakeAdress"
                        runat="server"
                        ForeColor="#0078D7"
                        OnClick="LinkButtonFakeAdress_Click"
                        Style="text-decoration: none"
                        ViewStateMode="Enabled"
                        Visible="False">Не лжеадрес</asp:LinkButton>
                </asp:Panel>
            </div>
        </div>
        <div style="position: absolute; top: 211px; left: 401px; width: 699px; height: 400px; background-color: #FFFFFF; font-size: medium; font-size: medium;">
            <div style="position: relative; left: 20px; top: 20px; width: 650px">
                <asp:Panel ID="Panel3YearOwner"
                    runat="server"
                    Visible="False">
                    Не рекоммендует приобретать квартиры, 
                    которые находятся в собственности у 
                    текущего владельца менее 3-х лет. 
                    Срочная продажа квартиры может быть связана с 
                    обстоятельствами – судебными разбирательствами, 
                    плохими соседями и другими скрытыми причинами. 
                    Про прочих равных стоит выбрать квартиру с более 
                    длительными сроками владения, желательно от 5-ти лет и более.
            <br />
                    Как узнать: Спросить у продавца
            <br />
                    Значимость параметра 3/5
                </asp:Panel>
                <asp:Panel ID="PanelPriceContract" runat="server" Visible="False">
                    Важно зафиксировать в договоре купли-продажи полную стоимость квартиры – 
                    ту сумму, что будет передана Вами взамен передачи 
                    прав собственности на квартиру. ProWizard 
                    категорически не рекомендует приобретить квартиры 
                    с заниженной стоимостью. Полная стоимость 
                    квартиры в договоре купли-продажи это ваш гарант спокойствия 
                    в случае судебных разбирательств и других нештатных ситуаций.
            <br />
                    Как узнать: Спросить у продавца
            <br />
                    Значимость параметра: 5/5
                </asp:Panel>
                <asp:Panel ID="PanelPriceMarket" runat="server" Visible="False">
                    ProWizard не рекоммендует приобретать квартиры, стоимость 
                    которой отличается от среднерыночной. 
                    Низкая стоимость может говорить о том, что 
                    продавец как можно быстрее хочет избавиться от «проблемной» квартиры.
                    <br />
                    Как узнать:
                    <br />
                    Изучить аналогичные предложения
            Значимость параметра: 5/5
                </asp:Panel>
                <asp:Panel ID="PanelContractForOriginal" runat="server" Visible="False">
                    Не выбирайте такие варианты, где сделка оформляется не напрямую с продавцом, 
                    а через доверенных лиц. Доверенность может быть отклонена продавцом с самый неподходящий момент.
            <br />
                    Как узнать: Спросить у продавца
            <br />
                    Значимость параметра: 5/5
                </asp:Panel>
                <asp:Panel ID="PanelPassportFake" runat="server" Visible="False">
                    Убедитесь в том, что продавец является тем, за кого себя выдает.
            <br />
                    Как узнать: Сверить фотографию в паспорте
            <br />
                    Значимость параметра: 5/5
                </asp:Panel>
                <asp:Panel ID="PanelExtractOwner" runat="server" Visible="False">
                    Важно получить согласие от продавца о том что все прописанные в продаваемой квартире 
                    будут выписаны до момента заключения сделки.
            <br />
                    Как узнать: Спросить у продавца
            <br />
                    Значимость параметра: 5/5
                </asp:Panel>
                <asp:Panel ID="PanelFakeAdress" runat="server" Visible="False">
                    Убедитесь в том, что Вы смотрите именно ту квартиру что указана в документах продавца. 
                    Для этого сверьте адрес дома и номер квартиры.
                    <br />
                    Как узнать: Проверить при просмотре квартиры
                    <br />
                    Значимость параметра: 5/5
                </asp:Panel>
                <asp:Panel ID="PanelTypeDocumentBased" runat="server" Visible="False">
                    Документ основания собственности это документ, по которому продавец стал собственником квартиры.
                   <br />
                    Возможны следующие документы основания:<br />
                    1)Договор купли-продажи;<br />
                    2)Приватизация;<br />
                    3)Дарение;<br />
                    4)Решение суда.
                   <br />
                    Про прочих равных предпочтительные остановиться на варианте с договором купли-продажи, 
                    по статистики это самый надежный варинат.<br />
                    Как узнать: Спросить у продавца
                   <br />
                    Значимость параметра: 3/5
                </asp:Panel>
            </div>
        </div>
        <div style="position: absolute; top: 612px; background-color: #FFFFFF; height: 60px; width: 100%; font-size: small;">
                <div style="position: absolute; right: 20px; top: 10px; width: 605px;">
                    <asp:Panel ID="PanelReminderNotCheckAllParametersONE"
                        runat="server"
                        EnableViewState="False"
                        ForeColor="#62BB47"
                        Height="40px"
                        ViewStateMode="Disabled"
                        Visible="False"
                        Font-Size="Medium"
                        Width="700px">
                        <strong>Объект проходит не по всем параметрам!&nbsp;&nbsp;
                            <asp:Button ID="ButtonToBack"
                                runat="server"
                                Text="&lt; Назад"
                                Style="background-color: white;"
                                BorderColor="#0078D7"
                                BorderWidth="1px"
                                Font-Bold="True"
                                Font-Italic="False"
                                Font-Overline="False"
                                Font-Underline="False"
                                ForeColor="#0078D7"
                                Height="40px"
                                OnClick="ButtonToBack_Click" Width="140px" />
                        </strong>
                        &nbsp;&nbsp;
         <asp:Button ID="ButtonForce"
             runat="server"
             BorderColor="#0078D7"
             BorderWidth="1px"
             Font-Bold="True"
             Font-Italic="False"
             Font-Overline="False"
             Font-Underline="False"
             ForeColor="#0078D7"
             Height="40px"
             OnClick="ButtonForce_Click"
             Style="background-color: white;"
             Text="Продолжить >"
             Width="140px" />
                    </asp:Panel>
                </div>
                <div style="position: absolute; right: 20px; top: 10px; width: 280px;">
                    <asp:Panel ID="PanelSaveAndForward"
                        runat="server"
                        EnableViewState="False"
                        ForeColor="#7DC243"
                        Height="40px"
                        ViewStateMode="Disabled"
                        Visible="True"
                        Width="600px">
                        <strong>
                            <asp:Button ID="ButtonSaveDeal"
                                runat="server"
                                BorderColor="#0078D7"
                                BorderWidth="1px"
                                Font-Bold="True"
                                Font-Italic="False"
                                Font-Overline="False"
                                Font-Underline="False"
                                ForeColor="#0078D7"
                                Height="40px"
                                OnClick="ButtonSaveDeal_Click"
                                Style="background-color: white;"
                                Text="Сохранить"
                                Width="140px" />
                            &nbsp;
                                <asp:Button ID="ButtonStartDeal"
                                    runat="server"
                                    BorderColor="#0078D7"
                                    BorderWidth="1px"
                                    Font-Bold="True"
                                    Font-Italic="False"
                                    Font-Overline="False"
                                    Font-Underline="False"
                                    ForeColor="#0078D7"
                                    Height="40px"
                                    OnClick="ButtonStartDeal_Click"
                                    Style="background-color: white;"
                                    Text="Продолжить &gt;"
                                    Width="140px" />
                        </strong>
                    </asp:Panel>
                </div>
        </div>
    </div>
</asp:Content>
