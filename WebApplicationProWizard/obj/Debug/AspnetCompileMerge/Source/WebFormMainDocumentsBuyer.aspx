﻿<%@ Page Title=""
    Language="C#"
    MasterPageFile="~/WebFormMain.Master"
    AutoEventWireup="true"
    CodeBehind="WebFormMainDocumentsBuyer.aspx.cs"
    Inherits="WebApplicationProWizard.WebForms.MainDocuments.WebFormMainDocumentsBuyer"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div style="position: relative; margin: 0 auto; width: 1100px; font-family: 'Times New Roman';">
        <div style="position: absolute; top: 80px; width: 100%; height: 130px; background-color: #FFFFFF; font-size: medium; color: #000000;">
            <div style="position: relative; left: 20px; top: 20px;">
                <div style="position: absolute; width: 900px; left: 0px;">
                    <asp:Panel ID="PanelCommonInformation"
                        runat="server"
                        EnableViewState="False"
                        ViewStateMode="Disabled">
                        На данном этапе вам потребуется проверить поступившие от продавца документы.<br />
                        Мы определели необходимый список, а также перечень необходимых проверок.<br />
                        После того как продавец пришлет документы на согласование изменится их статус и появится возможность проверки документов.<br />
                        Следуя нашим рекоммендациям вы без труда выполните все необходимые проверки<br />
                    </asp:Panel>
                    <asp:Panel ID="PanelCloseForm"
                        runat="server"
                        Visible="False">
                        Панель:Данный этап пройден.
        <br />
                        Форма недоступна для редактирования
                    </asp:Panel>
                </div>
            </div>
        </div>
        <div style="position: absolute; top: 211px; width: 500px; height: 600px; background-color: #FFFFFF; font-size: medium; font-size: medium; color: #FFFFFF;">
            <div style="position: relative; left: 20px; top: 20px;">
                <strong>
                    <asp:LinkButton ID="LinkButtonPassport"
                        runat="server"
                        OnClick="LinkPassport_Click"
                        Font-Strikeout="False"
                        ForeColor="#0078D7"
                        Font-Underline="False">Паспорт</asp:LinkButton>
                </strong>
                <asp:LinkButton ID="LinkButtonDownLoadPassport"
                    runat="server"
                    Font-Size="Medium"
                    ForeColor="Black"
                    OnClick="LinkButtonDownLoadPassport_Click"
                    EnableViewState="False"
                    ViewStateMode="Disabled">Скачать документ</asp:LinkButton>
                <asp:Label ID="LabelPassport"
                    runat="server"
                    Text="LabelPassport"
                    ForeColor="#000000"
                    Font-Size="Medium"></asp:Label>
                <strong>
                    <br />
                    <br />
                    <asp:LinkButton ID="LinkButtonSertificateOfOwnerShip"
                        runat="server"
                        OnClick="LinkButtonSertificateOfOwnerShip_Click"
                        ForeColor="#0078D7"
                        Font-Underline="False">Свидетельство</asp:LinkButton>
                </strong>
                <asp:LinkButton ID="LinkButtonDownLoadSertificateOfOwnerShip"
                    runat="server"
                    Font-Size="Medium"
                    ForeColor="Black"
                    OnClick="LinkButtonDownLoadSertificateOfOwnerShip_Click"
                    EnableViewState="False"
                    ViewStateMode="Disabled">Скачать документ</asp:LinkButton>
                <asp:Label ID="LabelSertificateOfOwnerShip"
                    runat="server"
                    ForeColor="#000000"
                    Text="LabelSertificateOfOwnerShip"></asp:Label>
                <strong>
                    <br />
                    <br />
                    <asp:LinkButton ID="LinkButtonDocumentFoundation"
                        runat="server"
                        OnClick="LinkButtonDocumentFoundation_Click"
                        ForeColor="#0078D7"
                        Font-Underline="False">Документ основание</asp:LinkButton>
                </strong>
                <asp:LinkButton ID="LinkButtonDownLoadDocumentFoundation"
                    runat="server"
                    Font-Size="Medium"
                    ForeColor="Black"
                    OnClick="LinkButtonDownLoadDocumentFoundation_Click"
                    EnableViewState="False"
                    ViewStateMode="Disabled">Скачать документ</asp:LinkButton>
                <asp:Label ID="LabelDocumentFoundation"
                    runat="server"
                    ForeColor="#000000"
                    Text="LabelDocumentFoundation"></asp:Label>
                <strong>
                    <br />
                    <br />
                    <asp:LinkButton ID="LinkButtonExtractFromRegister"
                        runat="server"
                        OnClick="LinkButtonExtractFromRegister_Click"
                        ForeColor="#0078D7"
                        Font-Underline="False">Выписка ЕГРП</asp:LinkButton>
                </strong>
                <asp:LinkButton ID="LinkButtonDownLoadExtractFromRegister"
                    runat="server"
                    Font-Size="Medium"
                    ForeColor="Black"
                    OnClick="LinkButtonDownLoadExtractFromRegister_Click"
                    EnableViewState="False"
                    ViewStateMode="Disabled">Скачать документ</asp:LinkButton>
                <asp:Label ID="LabelExtractFromRegister"
                    runat="server"
                    ForeColor="#000000"
                    Text="LabelExtractFromRegister"></asp:Label>
                <strong>
                    <br />
                    <br />
                    <asp:LinkButton ID="LinkButtonExtendedExtractFromRegister"
                        runat="server"
                        OnClick="LinkButtonExtendedExtractFromRegister_Click"
                        ForeColor="#0078D7"
                        Font-Underline="False">Расширенная выписка</asp:LinkButton>
                </strong>
                <asp:LinkButton ID="LinkButtonDownLoadExtendedExtractFromRegister"
                    runat="server"
                    Font-Size="Medium"
                    ForeColor="Black"
                    OnClick="LinkButtonDownLoadExtendedExtractFromRegister_Click"
                    EnableViewState="False"
                    ViewStateMode="Disabled">Скачать документ</asp:LinkButton>
                <asp:Label ID="LabelExtendedExtractFromRegister"
                    runat="server"
                    ForeColor="#000000"
                    Text="LabelExtendedExtractFromRegister"></asp:Label>
                <strong>
                    <br />
                    <br />
                    <asp:LinkButton ID="LinkButtonTechnicalPassport"
                        runat="server"
                        OnClick="LinkButtonTechnicalPassport_Click"
                        ForeColor="#0078D7"
                        Font-Underline="False">Технический паспорт</asp:LinkButton>
                </strong>
                <asp:LinkButton ID="LinkButtonDownLoadTechnicalPassport"
                    runat="server" EnableTheming="True"
                    Font-Size="Medium" ForeColor="Black"
                    OnClick="LinkButtonDownLoadTechnicalPassport_Click"
                    EnableViewState="False"
                    ViewStateMode="Disabled">Скачать документ</asp:LinkButton>
                <asp:Label ID="LabelTechnicalPassport"
                    runat="server"
                    ForeColor="#000000"
                    Text="LabelTechnicalPassport"></asp:Label>
                <strong>
                    <br />
                    <br />
                    <asp:LinkButton ID="LinkButtonConsentOfSpouse"
                        runat="server"
                        OnClick="LinkButtonConsentOfSpouse_Click"
                        ForeColor="#0078D7"
                        Font-Underline="False">Согласие супруга</asp:LinkButton>
                </strong>
                <asp:LinkButton ID="LinkButtonDownLoadConsentOfSpouse"
                    runat="server"
                    Font-Size="Medium"
                    ForeColor="Black"
                    OnClick="LinkButtonDownLoadConsentOfSpouse_Click"
                    EnableViewState="False"
                    ViewStateMode="Disabled">Скачать документ</asp:LinkButton>
                <asp:Label ID="LabelConsentOfSpouse"
                    runat="server"
                    ForeColor="#000000"
                    Text="LabelConsentOfSpouse"></asp:Label>
                <strong>
                    <br />
                    <br />
                    <asp:LinkButton ID="LinkButtonResolutionOfGuardianship"
                        runat="server"
                        OnClick="LinkButtonResolutionOfGuardianship_Click"
                        ForeColor="#0078D7"
                        Font-Underline="False">Разрешение опеки</asp:LinkButton>
                </strong>
                <asp:LinkButton ID="LinkButtonDownLoadResolutionOfGuardianship"
                    runat="server"
                    Font-Size="Medium"
                    ForeColor="Black"
                    OnClick="LinkButtonDownLoadResolutionOfGuardianship_Click"
                    EnableViewState="False"
                    ViewStateMode="Disabled">Скачать документ</asp:LinkButton>
                <asp:Label ID="LabelResolutionOfGuardianship"
                    runat="server"
                    ForeColor="#000000"
                    Text="LabelResolutionOfGuardianship"></asp:Label>
                <strong>
                    <br />
                    <br />
                    <asp:LinkButton ID="LinkButtonDebtForUtility"
                        runat="server"
                        OnClick="LinkButtonDebtForUtility_Click"
                        ForeColor="#0078D7"
                        Font-Underline="False">Справка по КУ</asp:LinkButton>
                </strong>
                <asp:LinkButton ID="LinkButtonDownLoadDebtForUtility"
                    runat="server"
                    Font-Size="Medium"
                    ForeColor="Black"
                    OnClick="LinkButtonDownLoadDebtForUtility_Click"
                    EnableViewState="False"
                    ViewStateMode="Disabled">Скачать документ</asp:LinkButton>
                <asp:Label ID="LabelDebtForUtility"
                    runat="server"
                    ForeColor="#000000"
                    Text="LabelDebtForUtility"></asp:Label>
                <strong>
                    <br />
                    <br />
                    <asp:LinkButton ID="LinkButtonCertificateFromTaxInspection"
                        runat="server"
                        OnClick="LinkButtonCertificateFromTaxInspection_Click"
                        ForeColor="#0078D7"
                        Font-Underline="False">Справка по налогам</asp:LinkButton>
                </strong>
                <asp:LinkButton ID="LinkButtonDownLoadCertificateFromTaxInspection"
                    runat="server"
                    Font-Size="Medium"
                    ForeColor="Black"
                    OnClick="LinkButtonDownLoadCertificateFromTaxInspection_Click"
                    EnableViewState="False"
                    ViewStateMode="Disabled">Скачать документ</asp:LinkButton>
                <asp:Label ID="LabelCertificateFromTaxInspection"
                    runat="server"
                    ForeColor="#000000"
                    Text="LabelCertificateFromTaxInspection"></asp:Label>
            </div>
        </div>
        <div style="position: absolute; top: 211px; left: 501px; width: 599px; height: 600px; background-color: #FFFFFF; font-size: medium; font-size: medium;">
            <div style="position: relative; left: 20px; top: 20px; width: 650px">
                <asp:Panel ID="PanelPassport"
                    runat="server"
                    ViewStateMode="Enabled"
                    Visible="False">
                    <asp:LinkButton ID="LinkButtonDenidePassport"
                        runat="server"
                        BorderColor="Black"
                        ForeColor="Black"
                        OnClick="LinkButtonDenidePassport_Click"
                        EnableViewState="False"
                        ViewStateMode="Disabled">Отклонить</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonSavePassport"
                        runat="server" ForeColor="Black"
                        OnClick="LinkButtonSavePassport_Click"
                        EnableViewState="False"
                        ViewStateMode="Disabled">Сохранить</asp:LinkButton>
                    <br />
                    <br />
                    <asp:CheckBox ID="CheckBoxPassportOwner"
                        runat="server"
                        Text="Паспорт предъявлен собственником"
                        ViewStateMode="Disabled"
                        EnableViewState="False" />
                    <br />
                    <br />
                    <asp:CheckBox ID="CheckBoxPassportActual"
                        runat="server"
                        Text="Паспорт действителен"
                        ViewStateMode="Disabled"
                        EnableViewState="False" />
                    <br />
                    <br />
                    <asp:CheckBox ID="CheckBoxPassportSpouse"
                        runat="server"
                        Text="Требуется согласие супруга"
                        ViewStateMode="Disabled"
                        EnableViewState="False" />
                </asp:Panel>
                <asp:Panel ID="PanelSertificateOfOwnerShip"
                    runat="server"
                    ViewStateMode="Enabled"
                    Visible="False">
                    <asp:LinkButton ID="LinkButtonDenideSertificateOfOwnerShip"
                        runat="server"
                        BorderColor="Black"
                        ForeColor="Black"
                        OnClick="LinkButtonDenideSertificateOfOwnerShip_Click"
                        EnableViewState="False"
                        ViewStateMode="Disabled">Отклонить</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonSaveSertificateOfOwnerShip"
                        runat="server"
                        ForeColor="Black"
                        OnClick="LinkButtonSaveSertificateOfOwnerShip_Click"
                        EnableViewState="False"
                        ViewStateMode="Disabled">Сохранить</asp:LinkButton>
                    <br />
                    <br />
                    <asp:CheckBox ID="CheckBoxSertificateOfOwnerShipPassport"
                        runat="server"
                        Text="Паспортные данные совпадают"
                        ViewStateMode="Disabled"
                        EnableViewState="False" />
                    <br />
                    <br />
                    <asp:CheckBox ID="CheckBoxSertificateOfOwnerShipAddress"
                        runat="server"
                        Text="Совпадает адрес"
                        ViewStateMode="Disabled"
                        EnableViewState="False" />
                    <br />
                    <br />
                    <asp:CheckBox ID="CheckBoxSertificateOfOwnerShipBurden"
                        runat="server"
                        Text="Обремения отсутствуют"
                        ViewStateMode="Disabled"
                        EnableViewState="False" />
                </asp:Panel>
                <asp:Panel ID="PanelDocumentFoundation"
                    runat="server"
                    ViewStateMode="Enabled"
                    Visible="False">
                    <asp:LinkButton ID="LinkButtonDenideDocumentFoundation"
                        runat="server"
                        BorderColor="Black"
                        ForeColor="Black"
                        OnClick="LinkButtonDenideDocumentFoundation_Click"
                        EnableViewState="False"
                        ViewStateMode="Disabled">Отклонить</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonSaveDocumentFoundation"
                        runat="server"
                        ForeColor="Black"
                        OnClick="LinkButtonSaveDocumentFoundation_Click"
                        EnableViewState="False"
                        ViewStateMode="Disabled">Сохранить</asp:LinkButton>
                    <br />
                    <br />
                    <asp:CheckBox ID="CheckBoxDocumentFoundationPassport"
                        runat="server"
                        Text="Паспортные данные совпадают"
                        ViewStateMode="Disabled"
                        EnableViewState="False" />
                    <br />
                    <br />
                    <asp:CheckBox ID="CheckBoxDocumentFoundationAddress"
                        runat="server"
                        Text="Адрес совпадает"
                        ViewStateMode="Disabled"
                        EnableViewState="False" />
                    <br />
                    <br />
                    <asp:CheckBox ID="CheckBoxDocumentFoundationBurden"
                        runat="server"
                        Text="Обременения отсутствуют"
                        ViewStateMode="Enabled" />
                </asp:Panel>
                <asp:Panel ID="PanelExtractFromRegister"
                    runat="server"
                    ViewStateMode="Enabled"
                    Visible="False">
                    <asp:LinkButton ID="LinkButtonDenideExtractFromRegister"
                        runat="server"
                        BorderColor="Black"
                        ForeColor="Black"
                        OnClick="LinkButtonDenideExtractFromRegister_Click"
                        EnableViewState="False"
                        ViewStateMode="Disabled">Отклонить</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonSaveExtractFromRegister"
                        runat="server"
                        ForeColor="Black"
                        OnClick="LinkButtonSaveExtractFromRegister_Click"
                        EnableViewState="False"
                        ViewStateMode="Disabled">Сохранить</asp:LinkButton>
                    <br />
                    <br />
                    <asp:CheckBox ID="CheckBoxExtractFromRegisterPassport"
                        runat="server"
                        Text="Паспортные данные совпадают"
                        ViewStateMode="Disabled"
                        EnableViewState="False" />
                    <br />
                    <br />
                    <asp:CheckBox ID="CheckBoxExtractFromRegisterCharacteristic"
                        runat="server"
                        Text="Параметры квартиры корректные"
                        ViewStateMode="Disabled"
                        EnableViewState="False" />
                    <br />
                    <br />
                    <asp:CheckBox ID="CheckBoxExtractFromRegisterBurden"
                        runat="server"
                        Text="Обременения отсутствуют"
                        ViewStateMode="Disabled"
                        EnableViewState="False" />
                </asp:Panel>
                <asp:Panel ID="PanelExtendedExtractFromRegister"
                    runat="server"
                    ViewStateMode="Enabled"
                    Visible="False">
                    <asp:LinkButton ID="LinkButtonDenideExtendedExtractFromRegister"
                        runat="server"
                        BorderColor="Black"
                        ForeColor="Black"
                        OnClick="LinkButtonDenideExtendedExtractFromRegister_Click"
                        EnableViewState="False"
                        ViewStateMode="Disabled">Отклонить</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonSaveExtendedExtractFromRegister"
                        runat="server"
                        ForeColor="Black"
                        OnClick="LinkButtonSaveExtendedExtractFromRegister_Click"
                        EnableViewState="False"
                        ViewStateMode="Disabled">Сохранить</asp:LinkButton>
                    <br />
                    <br />
                    <asp:CheckBox ID="CheckBoxExtendedExtractFromRegisterPeople"
                        runat="server"
                        Text="Никто не прописан"
                        ViewStateMode="Disabled"
                        EnableViewState="False" />
                    <br />
                    <br />
                    <asp:CheckBox ID="CheckBoxExtendedExtractFromRegisterMinors"
                        runat="server"
                        Text="Права несовершеннолетних не нарушены"
                        ViewStateMode="Disabled"
                        EnableViewState="False" />
                    <br />
                    <br />
                    <asp:CheckBox ID="CheckBoxExtendedExtractFromRegisterTemporarilyВrawn"
                        runat="server"
                        Text="Нет временно выписанных граждан"
                        ViewStateMode="Disabled"
                        EnableViewState="False" />
                </asp:Panel>
                <asp:Panel ID="PanelTechnicalPassport"
                    runat="server"
                    ViewStateMode="Enabled"
                    Visible="False">
                    <asp:LinkButton ID="LinkButtonDenideTechnicalPassport"
                        runat="server"
                        BorderColor="Black"
                        ForeColor="Black"
                        OnClick="LinkButtonDenideTechnicalPassport_Click"
                        EnableViewState="False"
                        ViewStateMode="Disabled">Отклонить</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonSaveTechnicalPassport"
                        runat="server"
                        ForeColor="Black"
                        OnClick="LinkButtonSaveTechnicalPassport_Click"
                        EnableViewState="False"
                        ViewStateMode="Disabled">Сохранить</asp:LinkButton>
                    <br />
                    <br />
                    <asp:CheckBox ID="CheckBoxTechnicalPassportCharacteristic"
                        runat="server"
                        Text="Параметры квартиры корректные"
                        ViewStateMode="Disabled"
                        EnableViewState="False" />
                </asp:Panel>
                <asp:Panel ID="PanelConsentOfSpouse"
                    runat="server"
                    ViewStateMode="Enabled"
                    Visible="False">
                    <asp:LinkButton ID="LinkButtonDenideConsentOfSpouse"
                        runat="server"
                        BorderColor="Black"
                        ForeColor="Black"
                        OnClick="LinkButtonDenideConsentOfSpouse_Click"
                        EnableViewState="False"
                        ViewStateMode="Disabled">Отклонить</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonSaveConsentOfSpouse"
                        runat="server"
                        ForeColor="Black"
                        OnClick="LinkButtonSaveConsentOfSpouse_Click"
                        EnableViewState="False"
                        ViewStateMode="Disabled">Сохранить</asp:LinkButton>
                    <br />
                    <br />
                    <asp:CheckBox ID="CheckBoxConsentOfSpouseCharacteristic"
                        runat="server"
                        Text="Паспортные данные совпадают"
                        ViewStateMode="Disabled"
                        EnableViewState="False" />
                </asp:Panel>
                <asp:Panel ID="PanelResolutionOfGuardianship"
                    runat="server"
                    ViewStateMode="Enabled"
                    Visible="False">
                    <asp:LinkButton ID="LinkButtonDenideResolutionOfGuardianship"
                        runat="server"
                        BorderColor="Black"
                        ForeColor="Black"
                        OnClick="LinkButtonDenideResolutionOfGuardianship_Click"
                        EnableViewState="False"
                        ViewStateMode="Disabled">Отклонить</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonSaveResolutionOfGuardianship"
                        runat="server"
                        ForeColor="Black"
                        OnClick="LinkButtonSaveResolutionOfGuardianship_Click"
                        EnableViewState="False"
                        ViewStateMode="Disabled">Сохранить</asp:LinkButton>
                    <br />
                    <br />
                    <asp:CheckBox ID="CheckBoxResolutionOfGuardianshipCharacteristic"
                        runat="server"
                        Text="Паспортные данные совпадают"
                        ViewStateMode="Disabled"
                        EnableViewState="False" />
                </asp:Panel>
                <asp:Panel ID="PanelDebtForUtility"
                    runat="server"
                    ViewStateMode="Enabled"
                    Visible="False">
                    <asp:LinkButton ID="LinkButtonDenideDebtForUtility"
                        runat="server"
                        BorderColor="Black"
                        ForeColor="Black"
                        OnClick="LinkButtonDenideDebtForUtility_Click"
                        EnableViewState="False"
                        ViewStateMode="Disabled">Отклонить</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonSaveDebtForUtility"
                        runat="server"
                        ForeColor="Black"
                        OnClick="LinkButtonSaveDebtForUtility_Click"
                        EnableViewState="False"
                        ViewStateMode="Disabled">Сохранить</asp:LinkButton>
                    <br />
                    <br />
                    <asp:CheckBox ID="CheckBoxDebtForUtilityCharacteristic"
                        runat="server"
                        Text="Задолженность по коммунальным платежам отсутствует"
                        ViewStateMode="Disabled"
                        EnableViewState="False" />
                </asp:Panel>
                <asp:Panel ID="PanelCertificateFromTaxInspection"
                    runat="server"
                    ViewStateMode="Enabled"
                    Visible="False">
                    <asp:LinkButton ID="LinkButtonDenideCertificateFromTaxInspection"
                        runat="server"
                        BorderColor="Black"
                        ForeColor="Black"
                        OnClick="LinkButtonDenideCertificateFromTaxInspection_Click"
                        EnableViewState="False"
                        ViewStateMode="Disabled">Отклонить</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonSaveCertificateFromTaxInspection"
                        runat="server"
                        ForeColor="Black"
                        OnClick="LinkButtonSaveCertificateFromTaxInspection_Click"
                        EnableViewState="False"
                        ViewStateMode="Disabled">Сохранить</asp:LinkButton>
                    <br />
                    <br />
                    <asp:CheckBox ID="CheckBoxCertificateFromTaxInspectionCharacteristic"
                        runat="server"
                        Text="Задолженность по налогам отсутствует"
                        ViewStateMode="Disabled"
                        EnableViewState="False" />
                </asp:Panel>
                <asp:Panel ID="PanelDenide"
                    runat="server"
                    ViewStateMode="Disabled"
                    Visible="False"
                    EnableViewState="False">
                    Вы отклонили документ, для решения вопроса свяжитесь с продавцом и попросите прислать корректный документ
                </asp:Panel>
                <asp:Panel ID="PanelWaiting"
                    runat="server"
                    ViewStateMode="Disabled"
                    Visible="False"
                    EnableViewState="False">
                    Документ ожидается от продавца
                </asp:Panel>
                <asp:Panel ID="PanelDenideClose"
                    runat="server"
                    ViewStateMode="Disabled"
                    Visible="False"
                    EnableViewState="False">
                    Вы отклонили документ
                </asp:Panel>
                <asp:Panel ID="PanelWaitingClose"
                    runat="server"
                    ViewStateMode="Disabled"
                    Visible="False"
                    EnableViewState="False">
                    Документ не был прислан продавцом
                </asp:Panel>
            </div>
        </div>
        <div style="position: absolute; top: 812px; background-color: #FFFFFF; height: 60px; width: 100%; font-size: small;">
            <div style="position: absolute; right: 20px; top: 10px; width: 520px;">
                <asp:Panel ID="PanelReminder"
                    runat="server"
                    EnableViewState="False"
                    ForeColor="#62BB47"
                    Height="40px"
                    ViewStateMode="Disabled"
                    Visible="False"
                    Font-Size="Medium"
                    Width="700px">
                    <strong>Выполнены не все проверки!&nbsp;&nbsp;
                            <asp:Button ID="ButtonToBack"
                                runat="server"
                                Text="&lt; Назад"
                                Style="background-color: white;"
                                BorderColor="#0078D7"
                                BorderWidth="1px"
                                Font-Bold="True"
                                Font-Italic="False"
                                Font-Overline="False"
                                Font-Underline="False"
                                ForeColor="#0078D7"
                                Height="40px"
                                OnClick="ButtonToBack_Click" Width="140px" />
                    </strong>
                    &nbsp;&nbsp;
         <asp:Button ID="ButtonForce"
             runat="server"
             BorderColor="#0078D7"
             BorderWidth="1px"
             Font-Bold="True"
             Font-Italic="False"
             Font-Overline="False"
             Font-Underline="False"
             ForeColor="#0078D7"
             Height="40px"
             OnClick="ButtonForce_Click"
             Style="background-color: white;"
             Text="Продолжить >"
             Width="140px" />
                </asp:Panel>
            </div>
            <div style="position: absolute; right: 20px; top: 10px; width: 145px;">
                <asp:Panel ID="PanelForward"
                    runat="server"
                    EnableViewState="False"
                    ForeColor="#7DC243"
                    Height="40px"
                    ViewStateMode="Disabled"
                    Visible="True"
                    Width="600px">
                    <strong>&nbsp;
                                <asp:Button ID="ButtonForward"
                                    runat="server"
                                    BorderColor="#0078D7"
                                    BorderWidth="1px"
                                    Font-Bold="True"
                                    Font-Italic="False"
                                    Font-Overline="False"
                                    Font-Underline="False"
                                    ForeColor="#0078D7"
                                    Height="40px"
                                    OnClick="ButtonForward_Click"
                                    Style="background-color: white;"
                                    Text="Продолжить &gt;"
                                    Width="140px" />
                    </strong>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
