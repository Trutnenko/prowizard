﻿<%@ Page Title="" Language="C#"
    MasterPageFile="~/WebFormMain.Master"
    AutoEventWireup="true"
    CodeBehind="WebFormPreliminaryContractSeller.aspx.cs"
    Inherits="WebApplicationProWizard.WebForms.PreliminaryContract.WebFormPreliminaryContractSeller"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div style="position: relative; margin: 0 auto; width: 1100px;">
        <div style="position: absolute; top: 80px; background-color: #FFFFFF; width: 100%; height: 100px;">
            <div style="position: relative; left: 20px; top: 20px;">
                <div style="position: absolute; width: 900px; left: 0px;">
                    <asp:Panel ID="PanelCompleted"
                        runat="server"
                        Visible="False"
                        EnableViewState="False"
                        ViewStateMode="Disabled"
                        ForeColor="#525252">
                        Панель для отображения информации: Этап завершен
                    </asp:Panel>
                    <asp:Panel ID="PanelPrepare"
                        runat="server"
                        Visible="False"
                        EnableViewState="False"
                        ViewStateMode="Disabled"
                        ForeColor="#525252">
                        Панель для отображения информации: Подготовка документа
                    </asp:Panel>
                    <asp:Panel ID="PanelAccept"
                        runat="server"
                        Visible="False"
                        EnableViewState="False"
                        ViewStateMode="Disabled"
                        ForeColor="#525252">
                        Панель для отображения информации: Документ согласован
                    </asp:Panel>
                    <asp:Panel ID="PanelDenide"
                        runat="server"
                        Visible="False"
                        EnableViewState="False"
                        ViewStateMode="Disabled"
                        ForeColor="#525252">
                        Панель для отображения информации: Документ отклонен
                    </asp:Panel>
                    <asp:Panel ID="PanelUponAgreement"
                        runat="server"
                        Visible="False"
                        EnableViewState="False"
                        ViewStateMode="Disabled"
                        ForeColor="#525252">
                        Панель для отображения информации: Документ находится на согласовании
                    </asp:Panel>
                </div>
            </div>
        </div>
        <div style="position: absolute; top: 181px; background-color: #FFFFFF; width: 100%; height: 80px;">
            <div style="position: relative; left: 20px; top: 20px;">
                <asp:Label ID="LabelStatusPreliminaryContract"
                    runat="server"
                    Text="LabelStatusPreliminaryContract"
                    Visible="False"
                    ForeColor="#525252"></asp:Label>
                <asp:LinkButton ID="LinkButtonDowloadFilesBuyer"
                    runat="server"
                    OnClick="LinkButtonDowloadFiles_Click"
                    Visible="False"
                    EnableViewState="False"
                    ViewStateMode="Disabled"
                    ForeColor="#0078D7">Предварительный договор (покупатель)</asp:LinkButton>
                <asp:LinkButton ID="LinkButtonDowloadFilesSeller"
                    runat="server"
                    OnClick="LinkButtonDowloadFilesSeller_Click"
                    Visible="False"
                    EnableViewState="False"
                    ForeColor="#0078D7">Предварительный договор (мой)</asp:LinkButton>
                <asp:LinkButton ID="LinkButtonApprovePreliminaryContract"
                    runat="server"
                    OnClick="LinkButtonApprovePreliminaryContract_Click"
                    Visible="False"
                    EnableViewState="False"
                    ForeColor="#0078D7">Подтвердить</asp:LinkButton>
                <asp:LinkButton ID="LinkButtonDeclinePreliminaryContract"
                    runat="server"
                    OnClick="LinkButtonDeclinePreliminaryContract_Click"
                    Visible="False"
                    EnableViewState="False"
                    ForeColor="#0078D7">Отправить замечания</asp:LinkButton>
                <asp:FileUpload ID="FileUploadDeclinedFiles"
                    runat="server"
                    AllowMultiple="true"
                    Visible="False"
                    EnableViewState="False" />
                <asp:Label ID="LabelError"
                    runat="server"
                    Text="LabelError"
                    Visible="False"
                    ForeColor="#525252"></asp:Label>
            </div>
        </div>
        <div style="position: absolute; top: 262px; background-color: #FFFFFF; height: 60px; width: 100%; font-size: small;">
            <div style="position: absolute; right: 20px; top: 10px; width: 300px;">
                <asp:Panel ID="PanelForward"
                    runat="server"
                    EnableViewState="False"
                    ForeColor="#7DC243"
                    Height="40px"
                    ViewStateMode="Disabled"
                    Visible="True"
                    Width="600px">
                    <strong>&nbsp;
                Для продолжении - сделает покупатель
                    </strong>
                </asp:Panel>
            </div>
        </div>
    </div>

</asp:Content>
