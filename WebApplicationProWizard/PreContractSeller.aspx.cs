﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;
using System.Net;
using System.Web;

namespace WebApplicationProWizard.WebForms
{
    public partial class WebFormPreliminaryContractSeller : System.Web.UI.Page
    {
        protected void Page_PreRender(object sender, EventArgs e)
        {
            string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();

            if (File.Exists(Server.MapPath("/DealDocuments/" + IdDeal
                + "/PreContract.docx")))
            {
                LinkButtonDowloadFiles.Visible = true;
            }

            string PassKey = Request.Cookies["CookieProWizard"]["PassKey"].ToString();
            string IdUserValue;

            ClassFindIdUserInCookie.FindIdUser(PassKey, out IdUserValue);

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandStatusPreliminaryContract = Conn.CreateCommand();
            CommandStatusPreliminaryContract.CommandText = "SELECT StatusDeal"
                + " FROM dbo.TableDeal WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandStatusPreliminaryContract.Parameters.Add(ParamIdDeal);

            SqlDataReader dReader;

            Conn.Open();

            dReader = CommandStatusPreliminaryContract.ExecuteReader();

            if (dReader.Read())

            {
                string StatusDeal = dReader["StatusDeal"].ToString();

                if (StatusDeal == "Предварительный договор")

                {
                    PanelPrepare.Visible = true;
                }

                else
                {
                    PanelPrepare.Visible = true;
                    LabelMain.Text = "Этап завершен";

                    LinkButtonForwardToContract.Visible = false;

                    if (File.Exists(Server.MapPath("/DealDocuments/" + IdDeal
                + "/PreContract.docx")))
                    {
                        LinkButtonDowloadFiles.Visible = true;
                        LabelAbout.Text = "Предварительный договор сформирован";
                        LinkButtonDowloadFiles.Visible = true;
                    }

                    else
                    {
                        LinkButtonDowloadFiles.Visible = true;
                        LabelAbout.Text = "Предварительный договор не сформирован";
                        LinkButtonDowloadFiles.Visible = false;
                    }
                }
            }
            Conn.Close();
        }

        protected void LinkButtonDowloadFiles_Click(object sender, EventArgs e)
        {
            string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();
            string FileName = "PreContract.docx";
            string Ftp = "ftp://waws-prod-db3-051.ftp.azurewebsites.windows.net/site/wwwroot/DealDocuments/" + IdDeal + "/";

            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(Ftp + FileName);
            request.Method = WebRequestMethods.Ftp.DownloadFile;

            request.Credentials = new NetworkCredential(@"ProWizard\ProWizard", "100Million");
            request.UsePassive = true;
            request.UseBinary = true;
            request.EnableSsl = false;

            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            using (MemoryStream stream = new MemoryStream())
            {
                response.GetResponseStream().CopyTo(stream);
                Response.AddHeader("content-disposition", "attachment;filename=" + FileName);
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
        }

        protected void LinkButtonForwardToContract_Click(object sender, EventArgs e)
        {
            Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormPreliminaryContractDocument"));
        }

    }
}