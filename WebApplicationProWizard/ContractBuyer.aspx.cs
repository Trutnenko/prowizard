﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using System.Configuration;
using System.Net;

namespace WebApplicationProWizard.WebForms
{
    public partial class WebFormContractBuyer : System.Web.UI.Page
    {
        protected void Page_PreRender(object sender, EventArgs e)
        {
            string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();
            string PassKey = Request.Cookies["CookieProWizard"]["PassKey"].ToString();
            string IdUserValue;

            ClassFindIdUserInCookie.FindIdUser(PassKey, out IdUserValue);

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandStatusPreliminaryContract = Conn.CreateCommand();
            CommandStatusPreliminaryContract.CommandText = "SELECT StatusDeal"
                + " From dbo.TableDeal WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = IdDeal;
            CommandStatusPreliminaryContract.Parameters.Add(ParamIdDeal);

            SqlDataReader dReader;

            Conn.Open();

            dReader = CommandStatusPreliminaryContract.ExecuteReader();

            if (dReader.Read())

            {
                string StatusDeal = dReader["StatusDeal"].ToString();

                if (StatusDeal == "Подготовка к сделке")
                {
                    LinkButtonForwardToContract.Visible = true;
                    PanelPrepare.Visible = true;

                    if (File.Exists(Server.MapPath("/DealDocuments/" + IdDeal
                                + "/Contract.docx")))
                    {
                        LinkButtonDowloadFiles.Visible = true;
                    }
                }

                else
                {
                    PanelForward.Visible = false;
                    PanelReminder.Visible = false;
                    LabelMain.Text = "Этап завершен";

                    if (File.Exists(Server.MapPath("/DealDocuments/" + IdDeal
                                + "/Contract.docx")))
                    {
                        LinkButtonDowloadFiles.Visible = true;
                        LabelAbout.Visible = true;
                        LabelAbout.Text = "Договор сформирован";
                    }

                    else
                    {
                        LabelAbout.Visible = true;
                        LabelAbout.Text = "Договор не сформирован";
                    }
                }
            }
            Conn.Close();
        }

        protected void LinkButtonDowloadFiles_Click(object sender, EventArgs e)
        {
            string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();
            string FileName = "Contract.docx";
            string Ftp = "ftp://waws-prod-db3-051.ftp.azurewebsites.windows.net/site/wwwroot/DealDocuments/" + IdDeal + "/";

            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(Ftp + FileName);
            request.Method = WebRequestMethods.Ftp.DownloadFile;

            request.Credentials = new NetworkCredential(@"ProWizard\ProWizard", "100Million");
            request.UsePassive = true;
            request.UseBinary = true;
            request.EnableSsl = false;

            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            using (MemoryStream stream = new MemoryStream())
            {
                response.GetResponseStream().CopyTo(stream);
                Response.AddHeader("content-disposition", "attachment;filename=" + FileName);
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
        }

        protected void ButtonForward_Click(object sender, EventArgs e)
        {

            if (File.Exists(Server.MapPath("output.docx")))

            {
                SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

                SqlCommand CommandSetStatusInitial = new SqlCommand
                ("Update dbo.TableDeal Set StatusDeal = @ParamStatusDeal"
                + " Where IdDeal = @ParamIdDeal", Conn);

                SqlParameter ParamIdDealTWO = new SqlParameter();
                ParamIdDealTWO.ParameterName = "@ParamIdDeal";
                ParamIdDealTWO.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandSetStatusInitial.Parameters.Add(ParamIdDealTWO);

                SqlParameter ParamStatusDeal = new SqlParameter();
                ParamStatusDeal.ParameterName = "@ParamStatusDeal";
                ParamStatusDeal.Value = "Сделка";
                CommandSetStatusInitial.Parameters.Add(ParamStatusDeal);

                Conn.Open();
                CommandSetStatusInitial.ExecuteNonQuery();
                Conn.Close();

                Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormDealBuyer"));
            }

            else
            {
                PanelForward.Visible = false;
                PanelReminder.Visible = true;
            }

        }
        protected void ButtonForce_Click(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

            SqlCommand CommandSetStatusInitial = new SqlCommand
            ("UPDATE dbo.TableDeal SET StatusDeal = @ParamStatusDeal"
            + " WHERE IdDeal = @ParamIdDeal", Conn);

            SqlParameter ParamIdDealTWO = new SqlParameter();
            ParamIdDealTWO.ParameterName = "@ParamIdDeal";
            ParamIdDealTWO.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandSetStatusInitial.Parameters.Add(ParamIdDealTWO);

            SqlParameter ParamStatusDeal = new SqlParameter();
            ParamStatusDeal.ParameterName = "@ParamStatusDeal";
            ParamStatusDeal.Value = "Сделка";
            CommandSetStatusInitial.Parameters.Add(ParamStatusDeal);

            Conn.Open();
            CommandSetStatusInitial.ExecuteNonQuery();
            Conn.Close();

            Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormDealBuyer"));
        }

        protected void ButtonToBack_Click(object sender, EventArgs e)
        {
            PanelReminder.Visible = false;
            PanelForward.Visible = true;
        }

        protected void LinkButtonForwardToContract_Click(object sender, EventArgs e)
        {
            Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormContractDocument"));
        }
    }
}

