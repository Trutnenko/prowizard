﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Net;
using System.Net.Mail;

namespace WebApplicationProWizard.WebForms
{
    public partial class WebForm : System.Web.UI.Page
    {
        protected void Page_PreRender(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandIdDeal = Conn.CreateCommand();
            CommandIdDeal.CommandText = "SELECT StatusDeal FROM dbo.TableDeal WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandIdDeal.Parameters.Add(ParamIdDeal);

            Conn.Open();
            string StatusDeal = CommandIdDeal.ExecuteScalar().ToString();
            Conn.Close();

            if (StatusDeal == "Подключение продавца")
            {
                TextBoxEmailSeller.Visible = true;
                LinkButtonFindSeller.Visible = true;
            }

            if (StatusDeal != "Подключение продавца")
            {
                LabelMain.Visible = true;
                LabelMain.Text = "Этап завершен";
                LabelAbout.Visible = true;
                LabelAbout.Text = "Продавец подключен к сделке";
            }
        }

        protected void LinkButtonFindSeller_Click(object sender, EventArgs e)
        {
            string EmailValue = TextBoxEmailSeller.Text.ToString();

            if ((EmailValue.Contains("@") == false)
                | (EmailValue.Contains(".") == false)
                | (EmailValue.Length < 5))

            {
                LabelInformation.Text = "Введите Email";
                LabelInformation.Visible = true;
                ButtonForward.Visible = false;
                ButtonSendInvitation.Visible = false;
            }

            else

            {
                SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
                SqlCommand CommandFindEmail = Conn.CreateCommand();
                CommandFindEmail.CommandText = "SELECT Email From dbo.TableUser Where Email = @ParamEmail";

                SqlParameter ParamFindEmail = new SqlParameter();
                ParamFindEmail.ParameterName = "@ParamEmail";
                ParamFindEmail.Value = TextBoxEmailSeller.Text.ToString();
                CommandFindEmail.Parameters.Add(ParamFindEmail);

                Conn.Open();
                string UserExist = Convert.ToString(CommandFindEmail.ExecuteScalar());
                Conn.Close();

                if (UserExist != "")
                {
                    ButtonForward.Visible = true;
                    ButtonSendInvitation.Visible = false;
                    LabelInformation.Text = "Продавец найден";
                    LabelInformation.Visible = true;
                }

                else
                {
                    ButtonSendInvitation.Visible = true;
                    ButtonForward.Visible = false;
                    LabelInformation.Text = "Продавец не найден, отправьте продавцу приглашение для регистрации";
                    LabelInformation.Visible = true;
                }
            }
        }
        protected void ButtonForward_Click(object sender, EventArgs e)
        {
            string EmailValue = TextBoxEmailSeller.Text.ToString();

            if ((EmailValue.Contains("@") == false)
                | (EmailValue.Contains(".") == false)
                | (EmailValue.Length < 5))

            {
                LabelInformation.Text = "Введите Email";
                LabelInformation.Visible = true;
            }

            else

            {
                SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
                SqlCommand CommandFindIdUserSeller = Conn.CreateCommand();
                CommandFindIdUserSeller.CommandText = "SELECT IdUser From dbo.TableUser Where Email = @ParamEmail";
                SqlParameter ParamFindUserSeller = new SqlParameter();
                ParamFindUserSeller.ParameterName = "@ParamEmail";
                ParamFindUserSeller.Value = TextBoxEmailSeller.Text.ToString();
                CommandFindIdUserSeller.Parameters.Add(ParamFindUserSeller);

                Conn.Open();
                string IdUserSeller = Convert.ToString(CommandFindIdUserSeller.ExecuteScalar());

                SqlCommand CommandForUpdateIdUserSellerInTableObjectForDeal = new SqlCommand("UPDATE dbo.TableDeal SET"
                    + " IdUserSeller = @ParamIdUserSeller, " 
                    + " StatusDeal = @ParamStatusDeal"
                    + " Where IdDeal = @ParamIdDeal", Conn);

                SqlParameter ParamIdUserSeller = new SqlParameter();
                ParamIdUserSeller.ParameterName = "@ParamIdUserSeller";
                ParamIdUserSeller.Value = IdUserSeller;
                CommandForUpdateIdUserSellerInTableObjectForDeal.Parameters.Add(ParamIdUserSeller);

                SqlParameter ParamIdDeal = new SqlParameter();
                ParamIdDeal.ParameterName = "@ParamIdDeal";
                ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString(); 
                CommandForUpdateIdUserSellerInTableObjectForDeal.Parameters.Add(ParamIdDeal);

                SqlParameter ParamStatusDeal = new SqlParameter();
                ParamStatusDeal.ParameterName = "@ParamStatusDeal";
                ParamStatusDeal.Value = "Предварительный договор";
                CommandForUpdateIdUserSellerInTableObjectForDeal.Parameters.Add(ParamStatusDeal);

                CommandForUpdateIdUserSellerInTableObjectForDeal.ExecuteNonQuery();
                Conn.Close();

                Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormPreliminaryContractBuyer"));
            }
        }
        protected void ButtonSendInvitation_Click(object sender, EventArgs e)
        {
            string EmailValue = TextBoxEmailSeller.Text.ToString();

            if ((EmailValue.Contains("@") == false)
                | (EmailValue.Contains(".") == false)
                | (EmailValue.Length < 5))

            {
                LabelInformation.Text = "Введите Email";
                LabelInformation.Visible = true;
            }

            else

            {
                try
                {
                    string from = "sergey.trutnenko@yandex.ru";
                    string to = TextBoxEmailSeller.Text.ToString();
                    string subject = "Новый пароль от ProWizard";
                    string body = "Добрый день, это ProWizard. Для совершения сделки по объекту"
                        + " недвижимости пройдите, пожалуйста регистрацию. Необходимо пройти по ссылке"
                        + " http://localhost:49584/WebForms/WebFormRegistration.aspx"
                        + ". После регистрации в ProWizard вы сможете приступить к оформление сделки.";

                    string login = "sergey.trutnenko";
                    string pass = "100Million";

                    var msg = new MailMessage(from, to, subject, body);

                    var smtpClient = new SmtpClient("smtp.yandex.ru", 25);
                    smtpClient.Credentials = new NetworkCredential(login, pass);
                    smtpClient.EnableSsl = true;
                    smtpClient.Send(msg);

                    LabelInformation.Visible = true;
                    LabelInformation.Text = "Приглашение отправлено пользователю."
                        + "Для ускорения процесса свяжитесь с продавцом и попросите как можно быстрее пройти регистрацию."
                        + " После того продавец пройдет регистрацию вы приступите к оформлению сделки.";

                    TextBoxEmailSeller.Text = "";
                    ButtonSendInvitation.Visible = false;
                }

                catch (ArgumentException)

                {
                    LabelInformation.Visible = true;
                    LabelInformation.Text = "Укажите Email";
                    ButtonSendInvitation.Visible = false;
                }

                catch (FormatException)
                {
                    LabelInformation.Visible = true;
                    LabelInformation.Text = "Неверный формат Email";
                    ButtonSendInvitation.Visible = false;
                }
            }
        }
    }
}