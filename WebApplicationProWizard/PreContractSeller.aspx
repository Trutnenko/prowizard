﻿<%@ Page
    Title=""
    Language="C#"
    MasterPageFile="~/Main.Master"
    AutoEventWireup="true"
    CodeBehind="PreContractSeller.aspx.cs"
    Inherits="WebApplicationProWizard.WebForms.WebFormPreliminaryContractSeller"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PreContractSeller" runat="server">
    <link rel="stylesheet" href="style.css" type="text/css" />
    <div style="position: absolute; height: 800px; width: 100%; top: 72px; background-color: #FFFFFF; z-index: 0;">
    </div>
    <div style="position: relative; margin: 0 auto; width: 1000px; background-color: #FFFFFF;">
        <div style="position: absolute; top: 80px; width: 100%; height: 100px;">
            <asp:Panel ID="PanelPrepare"
                runat="server"
                Visible="False">
                <asp:Label runat="server" ID="LabelMain" CssClass="Header0">
                    Предварительный договор
                </asp:Label>
                <asp:LinkButton ID="LinkButtonDowloadFiles"
                    runat="server"
                    OnClick="LinkButtonDowloadFiles_Click"
                    Visible="False"
                    CssClass="ButtonContract">Скачать</asp:LinkButton>
                <br />
                <br />
                <br />
                <br />
                <asp:Label runat="server" ID="LabelAbout" CssClass="Header1">
                    Сформируйте предварительный договор и подпишите с продавцом
                </asp:Label>
            </asp:Panel>
        </div>
        <div style="position: absolute; top: 220px; width: 100%; height: 80px;">
            <asp:LinkButton ID="LinkButtonForwardToContract"
                runat="server"
                OnClick="LinkButtonForwardToContract_Click"
                Visible="True"
                CssClass="ButtonContract">Сформировать</asp:LinkButton>
        </div>      
    </div>
</asp:Content>
