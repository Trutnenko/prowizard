﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Web;

namespace WebApplicationProWizard
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LinkButtonPanelRegistration.Font.Underline = true;
                LinkButtonOpenStepBuyer.Font.Underline = true;
                LinkButtonOpenStepSeller.Font.Underline = false;
            }

            string Question;
            string Answer;
            WebApplicationProWizard.Сlasses.ClassQuestionsForSignIn.QuestionMethod(out Question, out Answer);
            LabelQuestion.Text = Question;
        }

        protected void LinkButtonRegistration_Click(object sender, EventArgs e)
        {
            if ((string.IsNullOrEmpty(TextBoxPasswortRegistration.Text.ToString()) == true)
                | (string.IsNullOrEmpty(TextBoxEmailRegistration.Text.ToString()) == true)
                | (TextBoxEmailRegistration.Text.ToString().Contains("@") == false))

            {
                LabelInfoRegistration.Visible = true;
                LabelInfoRegistration.Text = "Неправильный формат E-mail";
            }

            else
            {
                SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
                Conn.Open();

                SqlCommand CommandEmail = Conn.CreateCommand();
                CommandEmail.CommandText = "SELECT Count (*) From dbo.TableUser Where Email = @ParamEmail";
                SqlParameter ParamEmail = new SqlParameter();
                ParamEmail.ParameterName = "@ParamEmail";
                ParamEmail.Value = TextBoxEmailRegistration.Text.ToString();
                CommandEmail.Parameters.Add(ParamEmail);

                string CountEmail = Convert.ToString(CommandEmail.ExecuteScalar());

                SqlCommand CommandCorrectAnswer = Conn.CreateCommand();
                CommandCorrectAnswer.CommandText = "SELECT Answer From dbo.TableQuestions Where Question = @ParamQuestion";
                SqlParameter ParamQuestion = new SqlParameter();
                ParamQuestion.ParameterName = "@ParamQuestion";
                ParamQuestion.Value = LabelQuestion.Text.ToString();
                CommandCorrectAnswer.Parameters.Add(ParamQuestion);

                string CorrectAnswer = Convert.ToString(CommandCorrectAnswer.ExecuteScalar());

                if (CountEmail != "0")
                {
                    LabelInfoRegistration.Visible = true;
                    LabelInfoRegistration.Text = "Такой E-mail уже зарегистрирован";

                    string Question;
                    string Answer;
                    WebApplicationProWizard.Сlasses.ClassQuestionsForSignIn.QuestionMethod(out Question, out Answer);
                    LabelQuestion.Text = Question;
                }

                if (CountEmail == "0" & TextBoxAnswer.Text.ToString() == CorrectAnswer.ToString())

                {
                    Random RandomObjectForPassKey = new Random();
                    var RandomValueForPassKey = RandomObjectForPassKey.Next();

                    SqlCommand Cmd = new SqlCommand("Insert into dbo.TableUser" +
                        "(Email, Passwort, PassKey) Values (@Email, @Passwort, @PassKey)", Conn);

                    SqlParameter Param = new SqlParameter();
                    Param.ParameterName = "@Email";
                    Param.Value = TextBoxEmailRegistration.Text.ToString();
                    Cmd.Parameters.Add(Param);

                    Param = new SqlParameter();
                    Param.ParameterName = "@Passwort";
                    Param.Value = TextBoxPasswortRegistration.Text.ToString();
                    Cmd.Parameters.Add(Param);

                    Param = new SqlParameter();
                    Param.ParameterName = "@PassKey";
                    Param.Value = RandomValueForPassKey;
                    Cmd.Parameters.Add(Param);
                    Cmd.ExecuteNonQuery();

                    PanelRegistration.Visible = false;
                    LinkButtonPanelSign.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078D7");
                    LinkButtonPanelRegistration.ForeColor = System.Drawing.ColorTranslator.FromHtml("#525252");
                }

                else
                {
                    LabelInfoRegistration.Visible = true;
                    LabelInfoRegistration.Text = "Что-то не так";

                    string Question;
                    string Answer;
                    WebApplicationProWizard.Сlasses.ClassQuestionsForSignIn.QuestionMethod(out Question, out Answer);
                    LabelQuestion.Text = Question;
                }
                Conn.Close();
            }
        }

        protected void LinkButtonPanelRegistration_Click(object sender, EventArgs e)
        {
            TextBoxEmailRegistration.Text = "";
            TextBoxPasswortRegistration.Text = "";

            LinkButtonPanelRegistration.Font.Underline = true;
            LinkButtonPanelSign.Font.Underline = false;

            string Question;
            string Answer;
            WebApplicationProWizard.Сlasses.ClassQuestionsForSignIn.QuestionMethod(out Question, out Answer);
            LabelQuestion.Text = Question;

            TextBoxEmailRegistration.Text = null;
            TextBoxPasswortRegistration.Text = null;
        }

        protected void ButtonInfoProduct_Click(object sender, EventArgs e)
        {
            Response.Redirect("http://prowizard.azurewebsites.net/WebFormAbout.aspx");
        }

        protected void LinkButtonOpenStepBuyer_Click(object sender, EventArgs e)
        {
            LinkButtonOpenStepBuyer.Font.Underline = true;
            LinkButtonOpenStepSeller.Font.Underline = false;

            PanelBuyerAboutStep.Visible = true;
            PanelSellerAboutStep.Visible = false;
        }

        protected void LinkButtonOpenStepSeller_Click(object sender, EventArgs e)
        {
            LinkButtonOpenStepBuyer.Font.Underline = false;
            LinkButtonOpenStepSeller.Font.Underline = true;

            PanelBuyerAboutStep.Visible = false;
            PanelSellerAboutStep.Visible = true;
        }

        protected void LinkButtonCompanyAbout_Click(object sender, EventArgs e)
        {
            HttpCookie CookieProWizard = new HttpCookie("CookieProWizard");
            CookieProWizard["AboutPage"] = "Company";
            Response.Cookies.Add(CookieProWizard);

            Response.Redirect("http://homga.ru/company.aspx");
        }

        protected void LinkButtonVacancy_Click(object sender, EventArgs e)
        {
            HttpCookie CookieProWizard = new HttpCookie("CookieProWizard");
            CookieProWizard["AboutPage"] = "Vacancy";
            Response.Cookies.Add(CookieProWizard);

            Response.Redirect("http://homga.ru/company.aspx");
        }

        protected void LinkButtonPanelSign_Click(object sender, EventArgs e)
        {
            Response.Redirect("http://homga.ru/signin.aspx");
        }
    }
}