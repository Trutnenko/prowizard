﻿using System;
using System.Configuration;
using System.Data.SqlClient;

namespace WebApplicationProWizard
{
    public partial class WebFormMenu : System.Web.UI.Page
    {
        protected void LinkButtonPrivate_Click(object sender, EventArgs e)
        {
            PanelPrivate.Visible = true;
            PanelQuestion.Visible = false;

            LinkButtonPrivate.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078D7");
            LinkButtonQuestion.ForeColor = System.Drawing.ColorTranslator.FromHtml("#525252");
        }
        protected void LinkButtonQuestion_Click(object sender, EventArgs e)
        {
            PanelPrivate.Visible = false;
            PanelQuestion.Visible = true;

            LinkButtonPrivate.ForeColor = System.Drawing.ColorTranslator.FromHtml("#525252");
            LinkButtonQuestion.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078D7");
        }

        protected void LinkButtonRemoveAccaunt_Click(object sender, EventArgs e)
        {
            LabelReminderRemoveAccaunt.Visible = true;
            LinkButtonRemoveAccaunt.Visible = false;
            LinkButtonRemoveAccauntForce.Visible = true;
            LinkButtonCancelRemoveAccaunt.Visible = true;
        }

        protected void SetNewPassword_Click(object sender, EventArgs e)
        {
            if (TextBoxNewPassword.Text.Length >= 1)
            {
                SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

                SqlCommand Cmd = new SqlCommand("Update dbo.TableUser Set Passwort = @Passwort " +
                    " Where PassKey = @ParamPassKey", Conn);

                SqlParameter ParamPasswort = new SqlParameter();
                ParamPasswort.ParameterName = "@Passwort";
                ParamPasswort.Value = TextBoxNewPassword.Text.ToString();
                Cmd.Parameters.Add(ParamPasswort);

                SqlParameter ParamPassKey = new SqlParameter();
                ParamPassKey.ParameterName = "@ParamPassKey";
                ParamPassKey.Value = Request.Cookies["CookieProWizard"]["PassKey"].ToString();
                Cmd.Parameters.Add(ParamPassKey);

                Conn.Open();
                Cmd.ExecuteNonQuery();
                Conn.Close();

                LabelReminderNewPassword.Visible = true;
                LabelReminderNewPassword.Text = "Пароль изменен";
            }

            else
            {
                LabelReminderNewPassword.Visible = true;
            }
        }

        protected void LinkButtonRemoveAccauntForce_Click(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandDELETEUser = new SqlCommand("DELETE FROM dbo.TableUser" +
                " WHERE PassKey = @ParamPassKey", Conn);

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamPassKey";
            ParamIdDeal.Value = Request.Cookies["CookieProWizard"]["PassKey"].ToString();
            CommandDELETEUser.Parameters.Add(ParamIdDeal);

            Conn.Open();
            CommandDELETEUser.ExecuteNonQuery();
            Conn.Close();

            Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormMainPage"));
        }

        protected void LinkButtonСancelRemoveAccaunt_Click(object sender, EventArgs e)
        {
            LabelReminderRemoveAccaunt.Visible = false;
            LinkButtonRemoveAccauntForce.Visible = false;
            LinkButtonCancelRemoveAccaunt.Visible = false;
            LinkButtonRemoveAccaunt.Visible = true;
        }
    }
}