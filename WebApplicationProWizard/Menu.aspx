﻿<%@ Page
    Title=""
    Language="C#"
    MasterPageFile="~/Main.Master"
    AutoEventWireup="true"
    MaintainScrollPositionOnPostback="true"
    CodeBehind="Menu.aspx.cs"
    Inherits="WebApplicationProWizard.WebFormMenu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Menu" runat="server">
    <link rel="stylesheet" href="style.css" type="text/css" />
    <div style="position: absolute; height: 400px; width: 100%; top: 72px; background-color: #FFFFFF; z-index: 0;">
    </div>
    <div style="position: relative; margin: 0 auto; width: 1000px; background-color: #FFFFFF;">
        <div style="position: absolute; top: 50px; height: 600px;">
            <asp:LinkButton
                runat="server"
                ID="LinkButtonPrivate"
                OnClick="LinkButtonPrivate_Click"
                CssClass="SectionBuyerSeller"
                Style="text-decoration: none;" ForeColor="#0078D7">
                Аккаунт
            </asp:LinkButton>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <asp:LinkButton
                runat="server"
                ID="LinkButtonQuestion"
                OnClick="LinkButtonQuestion_Click"
                CssClass="SectionBuyerSeller"
                Style="text-decoration: none;">
                Написать
            </asp:LinkButton>
            <asp:Panel
                runat="server"
                ID="PanelPrivate"
                Style="position: absolute; top: 70px; left: 0px; width: 700px; height: 300px;">
                <asp:Label
                    runat="server"
                    ID="LabelNewPassword"
                    CssClass="Header2">Сменя пароля</asp:Label>
                <br />
                <br />
                <asp:TextBox
                    runat="server"
                    ID="TextBoxNewPassword"
                    placeholder="   Новый пароль"
                    OnClick="TextBoxNewPassord_Click"
                    CssClass="InputMain"></asp:TextBox>
                <asp:Label
                    runat="server"
                    ID="LabelReminderNewPassword"
                    Visible="false"
                    CssClass="HeaderReminder">
                    Введить пароль
                </asp:Label>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:LinkButton
                    runat="server"
                    ID="SetNewPassword"
                    OnClick="SetNewPassword_Click"
                    CssClass="ButtonMain">
                    Ок
                </asp:LinkButton>
                <br />
                <br />
                <br />
                <asp:Label
                    runat="server"
                    ID="LabelRemoveAccaunt"
                    CssClass="Header2">Удаление аккаунта</asp:Label>
                <br />
                <br />
                <asp:LinkButton
                    runat="server"
                    ID="LinkButtonRemoveAccaunt"
                    OnClick="LinkButtonRemoveAccaunt_Click"
                    CssClass="ButtonMain">Удалить аккаунт</asp:LinkButton>
                <br />
                <asp:Label
                    runat="server"
                    ID="LabelReminderRemoveAccaunt"
                    Visible="false"
                    CssClass="HeaderReminder">Удаленный аккаунт нельзя восстановить. Удалить аккаунт?</asp:Label>
                <asp:LinkButton ID="LinkButtonRemoveAccauntForce"
                    runat="server"
                    CssClass="ButtonMain"
                    OnClick="LinkButtonRemoveAccauntForce_Click"
                    Visible="False">Удалить аккаунт
                </asp:LinkButton>
                &nbsp;&nbsp;
                <asp:LinkButton ID="LinkButtonCancelRemoveAccaunt"
                    runat="server"
                    CssClass="ButtonMain"
                    OnClick="LinkButtonСancelRemoveAccaunt_Click"
                    Visible="False">Отменить</asp:LinkButton>
            </asp:Panel>
            <asp:Panel
                runat="server"
                ID="PanelQuestion"
                Style="position: absolute; top: 70px; left: 0px; width: 700px; height: 300px;"
                Visible="false">
            </asp:Panel>
        </div>
    </div>
</asp:Content>

