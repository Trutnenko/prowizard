﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using EasyDox;
using System.Net;
using System.IO;
using System.Web;

namespace WebApplicationProWizard
{
    public partial class WebFormPreContract : System.Web.UI.Page
    {
        protected void Page_PreRender(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

            SqlCommand CommandCountContract = Conn.CreateCommand();
            CommandCountContract.CommandText = "SELECT Count (*) FROM dbo.TablePreContract"
              + " WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandCountContract.Parameters.Add(ParamIdDeal);

            Conn.Open();
            string CountPreContract = Convert.ToString(CommandCountContract.ExecuteScalar());
            Conn.Close();

            if (CountPreContract == "1")
            {
                SqlCommand CommandContract = Conn.CreateCommand();
                CommandContract.CommandText = "SELECT" +
                    " ContractCity, " +
                    " ContractDate," +
                    " ContractCost," +
                    " ContractDeposit," +
                    " GeneralContractDate," +

                    " Buyer," +
                    " BuyerBirthDate," +

                    " Seller," +
                    " SellerBirthDate," +

                    " Address," +
                    " Floor," +
                    " Room," +
                    " Area" +

                    " FROM dbo.TablePreContract WHERE IdDeal = @ParamIdDealTWO";

                SqlParameter ParamIdDealTWO = new SqlParameter();
                ParamIdDealTWO.ParameterName = "@ParamIdDealTWO";
                ParamIdDealTWO.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandContract.Parameters.Add(ParamIdDealTWO);

                SqlDataReader dReader;

                Conn.Open();
                dReader = CommandContract.ExecuteReader();

                if (dReader.Read())

                {
                    TextBoxContractCity.Text = dReader["ContractCity"].ToString();
                    TextBoxContractDate.Text = dReader["ContractDate"].ToString();
                    TextBoxContractCost.Text = dReader["ContractCost"].ToString();
                    TextBoxGeneralContractDate.Text = dReader["GeneralContractDate"].ToString();
                    TextBoxContractDeposit.Text = dReader["ContractDeposit"].ToString();
                    TextBoxContractBuyer.Text = dReader["Buyer"].ToString();
                    TextBoxContractBuyerBirthDate.Text = dReader["BuyerBirthDate"].ToString();
                    TextBoxContractSeller.Text = dReader["Seller"].ToString();
                    TextBoxContractSellerBirthDate.Text = dReader["SellerBirthDate"].ToString();
                    TextBoxContractAddress.Text = dReader["Address"].ToString();
                    TextBoxContractFloor.Text = dReader["Floor"].ToString();
                    TextBoxContractRoom.Text = dReader["Room"].ToString();
                    TextBoxContractArea.Text = dReader["Area"].ToString();

                    dReader.Close();
                }
            }
        }

        protected void ButtonPreContract_Click(object sender, EventArgs e)
        {
            string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

            SqlCommand CommandCountContract = Conn.CreateCommand();
            CommandCountContract.CommandText = "SELECT Count (*) FROM dbo.TablePreContract"
              + " WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = IdDeal;
            CommandCountContract.Parameters.Add(ParamIdDeal);

            Conn.Open();
            string CountContract = Convert.ToString(CommandCountContract.ExecuteScalar());
            Conn.Close();

            if (CountContract == "0")
            {
                var FieldValues = new Dictionary<string, string>
            {
                { "Город заключения договора", TextBoxContractCity.Text },
                { "Дата заключения договора", TextBoxContractDate.Text },
                { "Дата заключения договора купли-продажи", TextBoxGeneralContractDate.Text },
                { "Стоимость квартиры", TextBoxContractCost.Text },
                { "Размер задатка", TextBoxContractDeposit.Text },

                { "Адрес квартиры", TextBoxContractAddress.Text },
                { "Общая площадь", TextBoxContractArea.Text },
                { "Этаж", TextBoxContractFloor.Text },
                { "Количество комнат", TextBoxContractRoom.Text },

                { "ФИО покупателя", TextBoxContractBuyer.Text },
                { "Дата рождения покупателя", TextBoxContractBuyerBirthDate.Text },

                { "ФИО продавца", TextBoxContractSeller.Text },
                { "Дата рождения продавца", TextBoxContractSellerBirthDate.Text },

            };

                var Engine = new Engine();

                Engine.Merge(Server.MapPath("Content/PreliminaryContract.docx"),
                    FieldValues,
                    Server.MapPath("DealDocuments/"
                    + Request.Cookies["Deal"]["IdDeal"].ToString() +
                    "/PreContract.docx"));

                SqlCommand CommandCreateContract = new SqlCommand("INSERT into dbo.TablePreContract" +
                    " (IdDeal," +

                    " ContractCity," +
                    " ContractDate," +
                    " GeneralContractDate," +
                    " ContractCost," +
                    " ContractDeposit," +

                    " Buyer," +
                    " BuyerBirthDate," +

                    " Seller," +
                    " SellerBirthDate," +

                    " Room," +
                    " Area," +
                    " Floor," +
                    " Address)" +

                    " Values" +

                    " (@ParamIdDealTWO," +
                    " @ParamContractCity," +
                    " @ParamContractDate," +
                    " @ParamGeneralContractDate," +
                    " @ParamContractCost," +
                    " @ParamContractDeposit," +

                    " @ParamBuyer," +
                    " @ParamBuyerBirthDate," +

                    " @ParamSeller," +
                    " @ParamSellerBirthDate," +

                    " @ParamRoom," +
                    " @ParamArea," +
                    " @ParamFloor," +
                    " @ParamAddress)"

                    , Conn);

                SqlParameter ParamRoom = new SqlParameter();
                ParamRoom.ParameterName = "@ParamRoom";
                ParamRoom.Value = TextBoxContractRoom.Text;
                CommandCreateContract.Parameters.Add(ParamRoom);

                SqlParameter ParamArea = new SqlParameter();
                ParamArea.ParameterName = "@ParamArea";
                ParamArea.Value = TextBoxContractArea.Text;
                CommandCreateContract.Parameters.Add(ParamArea);

                SqlParameter ParamFloor = new SqlParameter();
                ParamFloor.ParameterName = "@ParamFloor";
                ParamFloor.Value = TextBoxContractFloor.Text;
                CommandCreateContract.Parameters.Add(ParamFloor);

                SqlParameter ParamAddress = new SqlParameter();
                ParamAddress.ParameterName = "@ParamAddress";
                ParamAddress.Value = TextBoxContractAddress.Text;
                CommandCreateContract.Parameters.Add(ParamAddress);

                SqlParameter ParamContractDeposit = new SqlParameter();
                ParamContractDeposit.ParameterName = "@ParamContractDeposit";
                ParamContractDeposit.Value = TextBoxContractDeposit.Text;
                CommandCreateContract.Parameters.Add(ParamContractDeposit);

                SqlParameter ParamGeneralContractDate = new SqlParameter();
                ParamGeneralContractDate.ParameterName = "@ParamGeneralContractDate";
                ParamGeneralContractDate.Value = TextBoxGeneralContractDate.Text;
                CommandCreateContract.Parameters.Add(ParamGeneralContractDate);

                SqlParameter ParamIdDealTWOTWO = new SqlParameter();
                ParamIdDealTWOTWO.ParameterName = "@ParamIdDealTWO";
                ParamIdDealTWOTWO.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandCreateContract.Parameters.Add(ParamIdDealTWOTWO);

                SqlParameter ParamContractCity = new SqlParameter();
                ParamContractCity.ParameterName = "@ParamContractCity";
                ParamContractCity.Value = TextBoxContractCity.Text;
                CommandCreateContract.Parameters.Add(ParamContractCity);

                SqlParameter ParamContractDate = new SqlParameter();
                ParamContractDate.ParameterName = "@ParamContractDate";
                ParamContractDate.Value = TextBoxContractDate.Text;
                CommandCreateContract.Parameters.Add(ParamContractDate);

                SqlParameter ParamContractCost = new SqlParameter();
                ParamContractCost.ParameterName = "@ParamContractCost";
                ParamContractCost.Value = TextBoxContractCost.Text;
                CommandCreateContract.Parameters.Add(ParamContractCost);

                SqlParameter ParamBuyer = new SqlParameter();
                ParamBuyer.ParameterName = "@ParamBuyer";
                ParamBuyer.Value = TextBoxContractBuyer.Text;
                CommandCreateContract.Parameters.Add(ParamBuyer);

                SqlParameter ParamBuyerBirthDate = new SqlParameter();
                ParamBuyerBirthDate.ParameterName = "@ParamBuyerBirthDate";
                ParamBuyerBirthDate.Value = TextBoxContractBuyerBirthDate.Text;
                CommandCreateContract.Parameters.Add(ParamBuyerBirthDate);

                SqlParameter ParamSeller = new SqlParameter();
                ParamSeller.ParameterName = "@ParamSeller";
                ParamSeller.Value = TextBoxContractSeller.Text;
                CommandCreateContract.Parameters.Add(ParamSeller);

                SqlParameter ParamSellerBirthDate = new SqlParameter();
                ParamSellerBirthDate.ParameterName = "@ParamSellerBirthDate";
                ParamSellerBirthDate.Value = TextBoxContractSellerBirthDate.Text;
                CommandCreateContract.Parameters.Add(ParamSellerBirthDate);

                Conn.Open();
                CommandCreateContract.ExecuteNonQuery();
                Conn.Close();
            }

            else
            {
                var FieldValues = new Dictionary<string, string>
            {
                { "Город заключения договора", TextBoxContractCity.Text },
                { "Дата заключения договора", TextBoxContractDate.Text },
                { "Дата заключения договора купли-продажи", TextBoxGeneralContractDate.Text },
                { "Стоимость квартиры", TextBoxContractCost.Text },
                { "Размер задатка", TextBoxContractDeposit.Text },

                { "Адрес квартиры", TextBoxContractAddress.Text },
                { "Общая площадь", TextBoxContractArea.Text },
                { "Этаж", TextBoxContractFloor.Text },
                { "Количество комнат", TextBoxContractRoom.Text },

                { "ФИО покупателя", TextBoxContractBuyer.Text },
                { "Дата рождения покупателя", TextBoxContractBuyerBirthDate.Text },

                { "ФИО продавца", TextBoxContractSeller.Text },
                { "Дата рождения продавца", TextBoxContractSellerBirthDate.Text },

            };

                var Engine = new Engine();

                Engine.Merge(Server.MapPath("Content/PreliminaryContract.docx"),
                    FieldValues,
                    Server.MapPath("DealDocuments/"
                    + Request.Cookies["Deal"]["IdDeal"].ToString() +
                    "/PreContract.docx"));

                SqlCommand CommandUpdateContract = new SqlCommand("UPDATE dbo.TablePreContract" +
                    " Set ContractCity = @ParamContractCity," +
                    " ContractDate = @ParamContractDate," +
                    " GeneralContractDate = @ParamGeneralContractDate," +
                    " ContractCost = @ParamContractCost," +
                    " ContractDeposit = @ParamContractDeposit," +

                    " Buyer = @ParamBuyer," +
                    " BuyerBirthDate = @ParamBuyerBirthDate," +

                    " Seller = @ParamSeller," +
                    " SellerBirthDate = @ParamSellerBirthDate," +

                    " Room = @ParamRoom," +
                    " Area = @ParamArea," +
                    " Floor = @ParamFloor," +
                    " Address = @ParamAddress" +

                    " WHERE IdDeal = @ParamIdDealTWO", Conn);

                SqlParameter ParamRoom = new SqlParameter();
                ParamRoom.ParameterName = "@ParamRoom";
                ParamRoom.Value = TextBoxContractRoom.Text;
                CommandUpdateContract.Parameters.Add(ParamRoom);

                SqlParameter ParamArea = new SqlParameter();
                ParamArea.ParameterName = "@ParamArea";
                ParamArea.Value = TextBoxContractArea.Text;
                CommandUpdateContract.Parameters.Add(ParamArea);

                SqlParameter ParamFloor = new SqlParameter();
                ParamFloor.ParameterName = "@ParamFloor";
                ParamFloor.Value = TextBoxContractFloor.Text;
                CommandUpdateContract.Parameters.Add(ParamFloor);

                SqlParameter ParamAddress = new SqlParameter();
                ParamAddress.ParameterName = "@ParamAddress";
                ParamAddress.Value = TextBoxContractAddress.Text;
                CommandUpdateContract.Parameters.Add(ParamAddress);

                SqlParameter ParamGeneralContractDate = new SqlParameter();
                ParamGeneralContractDate.ParameterName = "@ParamGeneralContractDate";
                ParamGeneralContractDate.Value = TextBoxGeneralContractDate.Text;
                CommandUpdateContract.Parameters.Add(ParamGeneralContractDate);

                SqlParameter ParamContractDeposit = new SqlParameter();
                ParamContractDeposit.ParameterName = "@ParamContractDeposit";
                ParamContractDeposit.Value = TextBoxContractDeposit.Text;
                CommandUpdateContract.Parameters.Add(ParamContractDeposit);

                SqlParameter ParamIdDealELSEELSE = new SqlParameter();
                ParamIdDealELSEELSE.ParameterName = "@ParamIdDealTWO";
                ParamIdDealELSEELSE.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandUpdateContract.Parameters.Add(ParamIdDealELSEELSE);

                SqlParameter ParamContractCity = new SqlParameter();
                ParamContractCity.ParameterName = "@ParamContractCity";
                ParamContractCity.Value = TextBoxContractCity.Text;
                CommandUpdateContract.Parameters.Add(ParamContractCity);

                SqlParameter ParamContractDate = new SqlParameter();
                ParamContractDate.ParameterName = "@ParamContractDate";
                ParamContractDate.Value = TextBoxContractDate.Text;
                CommandUpdateContract.Parameters.Add(ParamContractDate);

                SqlParameter ParamContractCost = new SqlParameter();
                ParamContractCost.ParameterName = "@ParamContractCost";
                ParamContractCost.Value = TextBoxContractCost.Text;
                CommandUpdateContract.Parameters.Add(ParamContractCost);

                SqlParameter ParamBuyer = new SqlParameter();
                ParamBuyer.ParameterName = "@ParamBuyer";
                ParamBuyer.Value = TextBoxContractBuyer.Text;
                CommandUpdateContract.Parameters.Add(ParamBuyer);

                SqlParameter ParamBuyerBirthDate = new SqlParameter();
                ParamBuyerBirthDate.ParameterName = "@ParamBuyerBirthDate";
                ParamBuyerBirthDate.Value = TextBoxContractBuyerBirthDate.Text;
                CommandUpdateContract.Parameters.Add(ParamBuyerBirthDate);

                SqlParameter ParamSeller = new SqlParameter();
                ParamSeller.ParameterName = "@ParamSeller";
                ParamSeller.Value = TextBoxContractSeller.Text;
                CommandUpdateContract.Parameters.Add(ParamSeller);

                SqlParameter ParamSellerBirthDate = new SqlParameter();
                ParamSellerBirthDate.ParameterName = "@ParamSellerBirthDate";
                ParamSellerBirthDate.Value = TextBoxContractSellerBirthDate.Text;
                CommandUpdateContract.Parameters.Add(ParamSellerBirthDate);

                Conn.Open();
                CommandUpdateContract.ExecuteNonQuery();
                Conn.Close();
            }

            string FileName = "PreContract.docx";
            string Ftp = "ftp://waws-prod-db3-051.ftp.azurewebsites.windows.net/site/wwwroot/DealDocuments/" + IdDeal + "/";

            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(Ftp + FileName);
            request.Method = WebRequestMethods.Ftp.DownloadFile;

            request.Credentials = new NetworkCredential(@"ProWizard\ProWizard", "100Million");
            request.UsePassive = true;
            request.UseBinary = true;
            request.EnableSsl = false;

            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            using (MemoryStream stream = new MemoryStream())
            {
                response.GetResponseStream().CopyTo(stream);
                Response.AddHeader("content-disposition", "attachment;filename=" + FileName);
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
        }

        protected void ButtonSave_Click(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

            SqlCommand CommandCountContract = Conn.CreateCommand();
            CommandCountContract.CommandText = "SELECT Count (*) FROM dbo.TablePreContract"
              + " WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandCountContract.Parameters.Add(ParamIdDeal);

            Conn.Open();
            string CountContract = Convert.ToString(CommandCountContract.ExecuteScalar());
            Conn.Close();

            if (CountContract == "0")
            {
                SqlCommand CommandCreateContract = new SqlCommand("INSERT into dbo.TablePreContract" +
                    " (IdDeal," +

                    " ContractCity," +
                    " ContractDate," +
                    " GeneralContractDate," +
                    " ContractCost," +
                    " ContractDeposit," +

                    " Buyer," +
                    " BuyerBirthDate," +

                    " Seller," +
                    " SellerBirthDate," +

                    " Room," +
                    " Area," +
                    " Floor," +
                    " Address)" +

                    " Values" +

                    " (@ParamIdDealTWO," +
                    " @ParamContractCity," +
                    " @ParamContractDate," +
                    " @ParamGeneralContractDate," +
                    " @ParamContractCost," +
                    " @ParamContractDeposit," +

                    " @ParamBuyer," +
                    " @ParamBuyerBirthDate," +

                    " @ParamSeller," +
                    " @ParamSellerBirthDate," +

                    " @ParamRoom," +
                    " @ParamArea," +
                    " @ParamFloor," +
                    " @ParamAddress)"

                    , Conn);

                SqlParameter ParamRoom = new SqlParameter();
                ParamRoom.ParameterName = "@ParamRoom";
                ParamRoom.Value = TextBoxContractRoom.Text;
                CommandCreateContract.Parameters.Add(ParamRoom);

                SqlParameter ParamArea = new SqlParameter();
                ParamArea.ParameterName = "@ParamArea";
                ParamArea.Value = TextBoxContractArea.Text;
                CommandCreateContract.Parameters.Add(ParamArea);

                SqlParameter ParamFloor = new SqlParameter();
                ParamFloor.ParameterName = "@ParamFloor";
                ParamFloor.Value = TextBoxContractFloor.Text;
                CommandCreateContract.Parameters.Add(ParamFloor);

                SqlParameter ParamAddress = new SqlParameter();
                ParamAddress.ParameterName = "@ParamAddress";
                ParamAddress.Value = TextBoxContractAddress.Text;
                CommandCreateContract.Parameters.Add(ParamAddress);

                SqlParameter ParamContractDeposit = new SqlParameter();
                ParamContractDeposit.ParameterName = "@ParamContractDeposit";
                ParamContractDeposit.Value = TextBoxContractDeposit.Text;
                CommandCreateContract.Parameters.Add(ParamContractDeposit);

                SqlParameter ParamGeneralContractDate = new SqlParameter();
                ParamGeneralContractDate.ParameterName = "@ParamGeneralContractDate";
                ParamGeneralContractDate.Value = TextBoxGeneralContractDate.Text;
                CommandCreateContract.Parameters.Add(ParamGeneralContractDate);

                SqlParameter ParamIdDealTWO = new SqlParameter();
                ParamIdDealTWO.ParameterName = "@ParamIdDealTWO";
                ParamIdDealTWO.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandCreateContract.Parameters.Add(ParamIdDealTWO);

                SqlParameter ParamContractCity = new SqlParameter();
                ParamContractCity.ParameterName = "@ParamContractCity";
                ParamContractCity.Value = TextBoxContractCity.Text;
                CommandCreateContract.Parameters.Add(ParamContractCity);

                SqlParameter ParamContractDate = new SqlParameter();
                ParamContractDate.ParameterName = "@ParamContractDate";
                ParamContractDate.Value = TextBoxContractDate.Text;
                CommandCreateContract.Parameters.Add(ParamContractDate);

                SqlParameter ParamContractCost = new SqlParameter();
                ParamContractCost.ParameterName = "@ParamContractCost";
                ParamContractCost.Value = TextBoxContractCost.Text;
                CommandCreateContract.Parameters.Add(ParamContractCost);

                SqlParameter ParamBuyer = new SqlParameter();
                ParamBuyer.ParameterName = "@ParamBuyer";
                ParamBuyer.Value = TextBoxContractBuyer.Text;
                CommandCreateContract.Parameters.Add(ParamBuyer);

                SqlParameter ParamBuyerBirthDate = new SqlParameter();
                ParamBuyerBirthDate.ParameterName = "@ParamBuyerBirthDate";
                ParamBuyerBirthDate.Value = TextBoxContractBuyerBirthDate.Text;
                CommandCreateContract.Parameters.Add(ParamBuyerBirthDate);

                SqlParameter ParamSeller = new SqlParameter();
                ParamSeller.ParameterName = "@ParamSeller";
                ParamSeller.Value = TextBoxContractSeller.Text;
                CommandCreateContract.Parameters.Add(ParamSeller);

                SqlParameter ParamSellerBirthDate = new SqlParameter();
                ParamSellerBirthDate.ParameterName = "@ParamSellerBirthDate";
                ParamSellerBirthDate.Value = TextBoxContractSellerBirthDate.Text;
                CommandCreateContract.Parameters.Add(ParamSellerBirthDate);

                Conn.Open();
                CommandCreateContract.ExecuteNonQuery();
                Conn.Close();
            }

            else
            {
                SqlCommand CommandUpdateContract = new SqlCommand("UPDATE dbo.TablePreContract" +
                    " Set ContractCity = @ParamContractCity," +
                    " ContractDate = @ParamContractDate," +
                    " GeneralContractDate = @ParamGeneralContractDate," +
                    " ContractCost = @ParamContractCost," +
                    " ContractDeposit = @ParamContractDeposit," +

                    " Buyer = @ParamBuyer," +
                    " BuyerBirthDate = @ParamBuyerBirthDate," +

                    " Seller = @ParamSeller," +
                    " SellerBirthDate = @ParamSellerBirthDate," +

                    " Room = @ParamRoom," +
                    " Area = @ParamArea," +
                    " Floor = @ParamFloor," +
                    " Address = @ParamAddress" +

                    " WHERE IdDeal = @ParamIdDealTWO", Conn);

                SqlParameter ParamRoom = new SqlParameter();
                ParamRoom.ParameterName = "@ParamRoom";
                ParamRoom.Value = TextBoxContractRoom.Text;
                CommandUpdateContract.Parameters.Add(ParamRoom);

                SqlParameter ParamArea = new SqlParameter();
                ParamArea.ParameterName = "@ParamArea";
                ParamArea.Value = TextBoxContractArea.Text;
                CommandUpdateContract.Parameters.Add(ParamArea);

                SqlParameter ParamFloor = new SqlParameter();
                ParamFloor.ParameterName = "@ParamFloor";
                ParamFloor.Value = TextBoxContractFloor.Text;
                CommandUpdateContract.Parameters.Add(ParamFloor);

                SqlParameter ParamAddress = new SqlParameter();
                ParamAddress.ParameterName = "@ParamAddress";
                ParamAddress.Value = TextBoxContractAddress.Text;
                CommandUpdateContract.Parameters.Add(ParamAddress);

                SqlParameter ParamGeneralContractDate = new SqlParameter();
                ParamGeneralContractDate.ParameterName = "@ParamGeneralContractDate";
                ParamGeneralContractDate.Value = TextBoxGeneralContractDate.Text;
                CommandUpdateContract.Parameters.Add(ParamGeneralContractDate);

                SqlParameter ParamContractDeposit = new SqlParameter();
                ParamContractDeposit.ParameterName = "@ParamContractDeposit";
                ParamContractDeposit.Value = TextBoxContractDeposit.Text;
                CommandUpdateContract.Parameters.Add(ParamContractDeposit);

                SqlParameter ParamIdDealTWO = new SqlParameter();
                ParamIdDealTWO.ParameterName = "@ParamIdDealTWO";
                ParamIdDealTWO.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                CommandUpdateContract.Parameters.Add(ParamIdDealTWO);

                SqlParameter ParamContractCity = new SqlParameter();
                ParamContractCity.ParameterName = "@ParamContractCity";
                ParamContractCity.Value = TextBoxContractCity.Text;
                CommandUpdateContract.Parameters.Add(ParamContractCity);

                SqlParameter ParamContractDate = new SqlParameter();
                ParamContractDate.ParameterName = "@ParamContractDate";
                ParamContractDate.Value = TextBoxContractDate.Text;
                CommandUpdateContract.Parameters.Add(ParamContractDate);

                SqlParameter ParamContractCost = new SqlParameter();
                ParamContractCost.ParameterName = "@ParamContractCost";
                ParamContractCost.Value = TextBoxContractCost.Text;
                CommandUpdateContract.Parameters.Add(ParamContractCost);

                SqlParameter ParamBuyer = new SqlParameter();
                ParamBuyer.ParameterName = "@ParamBuyer";
                ParamBuyer.Value = TextBoxContractBuyer.Text;
                CommandUpdateContract.Parameters.Add(ParamBuyer);

                SqlParameter ParamBuyerBirthDate = new SqlParameter();
                ParamBuyerBirthDate.ParameterName = "@ParamBuyerBirthDate";
                ParamBuyerBirthDate.Value = TextBoxContractBuyerBirthDate.Text;
                CommandUpdateContract.Parameters.Add(ParamBuyerBirthDate);

                SqlParameter ParamSeller = new SqlParameter();
                ParamSeller.ParameterName = "@ParamSeller";
                ParamSeller.Value = TextBoxContractSeller.Text;
                CommandUpdateContract.Parameters.Add(ParamSeller);

                SqlParameter ParamSellerBirthDate = new SqlParameter();
                ParamSellerBirthDate.ParameterName = "@ParamSellerBirthDate";
                ParamSellerBirthDate.Value = TextBoxContractSellerBirthDate.Text;
                CommandUpdateContract.Parameters.Add(ParamSellerBirthDate);

                Conn.Open();
                CommandUpdateContract.ExecuteNonQuery();
                Conn.Close();
            }
        }

        protected void LinkButtonToHome_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            string PassKey = Request.Cookies["CookieProWizard"]["PassKey"].ToString();
            string IdCurrentUser;
            string IdUserSeller;
            string IdUserBuyer;
            string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

            SqlCommand CommandFindIdUser = Conn.CreateCommand();
            CommandFindIdUser.CommandText = "SELECT" +
                " IdUser FROM dbo.TableUser" +
                " WHERE PassKey = @ParamPassKey";

            SqlParameter ParamPassKey = new SqlParameter();
            ParamPassKey.ParameterName = "@ParamPassKey";
            ParamPassKey.Value = PassKey;
            CommandFindIdUser.Parameters.Add(ParamPassKey);

            Conn.Open();
            IdCurrentUser = CommandFindIdUser.ExecuteScalar().ToString();

            SqlCommand CommandDefineSeller = Conn.CreateCommand();
            CommandDefineSeller.CommandText = "SELECT"
            + " IdUserSeller FROM dbo.TableDeal"
            + " WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDealELSE = new SqlParameter();
            ParamIdDealELSE.ParameterName = "@ParamIdDeal";
            ParamIdDealELSE.Value = IdDeal;
            CommandDefineSeller.Parameters.Add(ParamIdDealELSE);

            IdUserSeller = CommandDefineSeller.ExecuteScalar().ToString();

            SqlCommand CommandDefineBuyer = Conn.CreateCommand();
            CommandDefineBuyer.CommandText = "SELECT"
            + " IdUserBuyer FROM dbo.TableDeal"
            + " WHERE IdDeal = @ParamIdDealTWO";

            SqlParameter ParamIdDealTWO = new SqlParameter();
            ParamIdDealTWO.ParameterName = "@ParamIdDealTWO";
            ParamIdDealTWO.Value = IdDeal;
            CommandDefineBuyer.Parameters.Add(ParamIdDealTWO);

            IdUserBuyer = CommandDefineBuyer.ExecuteScalar().ToString();

            Conn.Close();

            if (IdCurrentUser == IdUserBuyer)
            {
                Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormPreliminaryContractBuyer"));
            }

            if (IdCurrentUser == IdUserSeller)
            {
                Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormPreliminaryContractSeller"));
            }
        }
    }
}