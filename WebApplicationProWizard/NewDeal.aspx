﻿<%@ Page
    Title=""
    Language="C#"
    MasterPageFile="~/Main.Master"
    AutoEventWireup="true"
    MaintainScrollPositionOnPostback="true"
    CodeBehind="NewDeal.aspx.cs"
    Inherits="WebApplicationProWizard.WebForms.WebFormNewDeal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="FindObject" runat="server">
    <link rel="stylesheet" href="style.css" type="text/css" />
    <div style="position: absolute; height: 950px; width: 100%; top: 72px; background-color: #FFFFFF; z-index: 0;">
    </div>
    <div style="position: relative; margin: 0 auto; top: 80px; width: 1000px; height: 800px;">
        <div style="position: absolute; width: 900px; height: 180px;">
            <asp:TextBox ID="TextBoxLinkObjectONE" placeholder=" Чтобы не потерять объявление вставьте сюда ссылку"
                runat="server"
                TextMode="Url"
                Width="700px"
                Visible="False"
                ViewStateMode="Enabled"
                CssClass="InputMain"></asp:TextBox>
            &nbsp;&nbsp;
            <asp:LinkButton ID="LinkButtonRedirectObjectONE"
                runat="server"
                OnClick="LinkButtonRedirectObjectOne_Click"
                Visible="False"
                ViewStateMode="Enabled"
                ForeColor="#525252"
                Style="text-decoration: none">Открыть</asp:LinkButton>
            <br />
            <br />
            <br />
            <asp:Label ID="Label2"
                runat="server"
                Text="Подберите недвижимость и проверьте"
                ForeColor="#525252"
                CssClass="Header1"> </asp:Label>
            <br />
        </div>

        <div style="position: absolute; top: 170px; width: 900px; height: 30px;">
            <asp:CheckBox ID="CheckBox3YearOwner"
                runat="server"
                EnableTheming="True"
                Text=" Более 3-х в собственности"
                CssClass="Header2" />
        </div>
        <div class="Header2" style="position: absolute; left: 80px; top: 210px; width: 900px; height: 50px;">
            Уточните у продавцов сколько лет они владеют собственностью. 
            Мы не рекомендуем объекты недвижимости со сроком владения менее 3х лет.         
        </div>

        <div style="position: absolute; top: 280px; width: 900px; height: 30px;">
            <asp:CheckBox ID="CheckBoxPriceContract"
                runat="server"
                Text=" Полная стоимость"
                ViewStateMode="Enabled"
                Visible="False"
                CssClass="Header2" />
        </div>
        <div class="Header2" style="position: absolute; left: 80px; top: 320px; width: 900px; height: 50px;">
            Уточните у продавцов готовы ли они зафиксировать полную стоимость недвижимости в договоре. 
            Полная стоимость – возможность вернуть все средства в случае конфликтов.           
            <br />
        </div>

        <div style="position: absolute; top: 390px; width: 900px; height: 30px;">
            <asp:CheckBox ID="CheckBoxContractForOriginal"
                runat="server"
                Text=" Сделка без доверенностей"
                ViewStateMode="Enabled"
                Visible="False"
                CssClass="Header2" />
        </div>
        <div class="Header2" style="position: absolute; left: 80px; top: 430px; width: 900px; height: 50px;">
            Уточните у продавцов смогут ли они обеспечить личное присутствие при заключении сделки – без представителей. 
            Доверенности на представителей могут быть отклонены продавцом и сделка будет признана ничтожной.      
        </div>

        <div style="position: absolute; top: 500px; width: 900px; height: 30px;">
            <asp:CheckBox ID="CheckBoxExtractOwner"
                runat="server"
                Text=" Выписка до сделки"
                ViewStateMode="Enabled"
                Visible="False"
                CssClass="Header2" />
        </div>
        <div class="Header2" style="position: absolute; left: 80px; top: 540px; width: 900px; height: 50px;">
            Уточните у продавцов смогут ли они выписаться до совершения сделки – 
            чтобы на момент подписания договора в квартире никто не был прописан.            
            <br />
            <br />
        </div>

        <div style="position: absolute; top: 690px; height: 60px; width: 350px; left: 350px;">
            <asp:Panel ID="PanelReminder"
                runat="server"
                EnableViewState="False"
                ViewStateMode="Disabled"
                Visible="False"
                Width="400px"
                CssClass="HeaderReminder">
                Выполнены не все проверки!
                <br />
                <br />
                <asp:Button ID="Button1"
                    runat="server"
                    Text="&lt; Назад"
                    OnClick="ButtonToBack_Click"
                    CssClass="ButtonElse"
                    Width="140px" />
                &nbsp;&nbsp;
         <asp:Button ID="Button2"
             runat="server"
             OnClick="ButtonForce_Click"
             CssClass="ButtonMain"
             Text="Продолжить >"
             Width="140px" />
            </asp:Panel>
        </div>
        <div style="position: absolute; top: 690px; height: 60px; width: 350px; left: 350px;">
            <asp:Panel ID="PanelForward"
                runat="server"
                EnableViewState="False"
                ViewStateMode="Disabled"
                Visible="True"
                CssClass="HeaderReminder"
                Width="600px">
                <asp:Button ID="ButtonSaveDeal"
                    runat="server"
                    OnClick="ButtonSaveDeal_Click"
                    Text="Сохранить"
                    Width="140px"
                    CssClass="ButtonElse" />
                &nbsp;
                                <asp:Button ID="ButtonForward"
                                    runat="server"
                                    OnClick="ButtonForward_Click"
                                    Style="background-color: white;"
                                    Text="Продолжить &gt;"
                                    CssClass="ButtonMain"
                                    Width="140px" />
            </asp:Panel>
        </div>
    </div>
</asp:Content>

