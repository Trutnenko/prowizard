﻿<%@ Page
    Language="C#"
    AutoEventWireup="true"
    CodeBehind="Company.aspx.cs"
    Inherits="WebApplicationProWizard.Company" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link rel="stylesheet" href="style.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server" style="left: 0px; right: 0px; position: absolute; top: 0px; background-color: #4D1884; height: 800px">
        <div style="position: absolute; top: 0px; height: 500px; width: 100%;">
            <img src="content/Company.jpg" style="z-index: 0; width: 100%; height: 100%; object-fit: cover;" />
        </div>
        <div style="position: absolute; top: 0px; width: 500px; height: 800px; background-color: #FFFFFF; left: 0px; z-index: 10;">
        </div>
        <div style="position: relative; width: 500px; margin: 0 auto; top: 50px; z-index: 100;">
            <asp:LinkButton ID="LinkButtonAbout"
                runat="server"
                OnClick="LinkButtonAbout_Click"
                CssClass="Header1"
                ViewStateMode="Enabled"
                ForeColor="#000000"
                Font-Size="32px">О компании</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="LinkButtonVacancy"
                runat="server"
                OnClick="LinkButtonVacancy_Click"
                CssClass="Header1"
                ViewStateMode="Enabled"
                ForeColor="#000000"
                Font-Size="32px">Вакансии</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <asp:ImageButton ID="ImageButtonClose"
                runat="server" ImageUrl="~/Content/Close.png"
                Style="position: absolute; right: 0px;"
                OnClick="LinkButtonToHome_Click"
                Height="40px" />
            <div style="position: absolute; top: 80px; width: 900px; left: 0px;">
                <asp:Panel ID="PanelCompany"
                    runat="server"
                    Visible="True">
                    <asp:Label ID="LabelCompany"
                        runat="server"
                        CssClass="Header2"
                        Style="color: #000000"
                        Text="ХОМГА создает решения для упрощения сделок на рынке недвижимости.<br> Наше понимание простоты и надежности выражено в новом решении Хомга <br> чтобы сделки с недвимимостью стали проще!">
                    </asp:Label>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <asp:Label ID="Label16"
                        runat="server"
                        CssClass="Header1"
                        Style="color: #000000"
                        Text="Генеральный директор">
                    </asp:Label>
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label17"
                        runat="server"
                        CssClass="Header2"
                        Style="color: #000000"
                        Text="Сергей Ермаков">
                    </asp:Label>
                    <br />
                    <br />
                    <asp:Label ID="Label18"
                        runat="server"
                        CssClass="Header1"
                        Style="color: #000000"
                        Text="Технический руководитель">
                    </asp:Label>
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label19"
                        runat="server"
                        CssClass="Header2"
                        Style="color: #000000"
                        Text="Антон Морозов">
                    </asp:Label>
                    <br />
                    <br />
                    <asp:Label ID="Label20"
                        runat="server"
                        CssClass="Header1"
                        Style="color: #000000"
                        Text="Команда разработки">
                    </asp:Label>
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label21"
                        runat="server"
                        CssClass="Header2"
                        Style="color: #000000"
                        Text="Антон 1">
                    </asp:Label>
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label22"
                        runat="server"
                        CssClass="Header2"
                        Style="color: #000000"
                        Text="Антон 2">
                    </asp:Label>
                    <br />
                    <br />
                    <asp:Label ID="Label23"
                        runat="server"
                        CssClass="Header1"
                        Style="color: #000000"
                        Text="Консультант по сделкам с недвижимостью">
                    </asp:Label>
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label24"
                        runat="server"
                        CssClass="Header2"
                        Style="color: #000000"
                        Text="Дарина Преображенская">
                    </asp:Label>
                    <br />
                    <br />
                    <br />
                    <asp:Label ID="LabelContact"
                        runat="server"
                        CssClass="Header2"
                        Style="color: #000000"
                        Text="109544, г. Москва, б-р Энтузиастов, д. 2">
                    </asp:Label>
                </asp:Panel>
                <asp:Panel ID="PanelVacancy"
                    runat="server"
                    Visible="False">
                    <asp:Label ID="LabelCity"
                        runat="server"
                        CssClass="Header0"
                        Style="color: #000000"
                        Text="Москва"></asp:Label>
                    <br />
                    <br />
                    <asp:Label ID="Label0"
                        runat="server"
                        CssClass="Header1"
                        Style="color: #000000"
                        Text=".Net разработчик">
                    </asp:Label>
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label1"
                        runat="server"
                        CssClass="Header2"
                        Style="color: #000000"
                        Text="C#, ASP.NET, LINQ, HTML5/CSS3/JavaScript/Ang1/Ang2">
                    </asp:Label>
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label2"
                        runat="server"
                        CssClass="Header2"
                        Style="color: #000000"
                        Text="Желателен опыт работы c Microsoft Azure">
                    </asp:Label>
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label3"
                        runat="server"
                        CssClass="Header2"
                        Style="color: #FFFFFF"
                        Text="Опыт работы от 3х лет">
                    </asp:Label>
                    <br />
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label4"
                        runat="server"
                        CssClass="Header2"
                        Style="color: #000000"
                        Text="Современный комфортный офис">
                    </asp:Label>
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label6"
                        runat="server"
                        CssClass="Header2"
                        Style="color: #000000"
                        Text="Гибкий график и удаленная работа">
                    </asp:Label>
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label7"
                        runat="server"
                        CssClass="Header2"
                        Style="color: #000000"
                        Text="Доход от 160 тыс. рублей">
                    </asp:Label>
                    <br />
                    <br />
                    <br />
                    <asp:Label ID="Label5"
                        runat="server"
                        CssClass="Header0"
                        Style="color: #000000"
                        Text="Ярославль">
                    </asp:Label>
                    <br />
                    <br />
                    <asp:Label ID="Label8"
                        runat="server"
                        CssClass="Header1"
                        Style="color: #000000"
                        Text=".Net разработчик">
                    </asp:Label>
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label9"
                        runat="server"
                        CssClass="Header2"
                        Style="color: #000000"
                        Text="C#, ASP.NET, LINQ, HTML5/CSS3/JavaScript/Ang1/Ang2">
                    </asp:Label>
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label10"
                        runat="server"
                        CssClass="Header2"
                        Style="color: #000000"
                        Text="Желателен опыт работы c Microsoft Azure">
                    </asp:Label>
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label11"
                        runat="server"
                        CssClass="Header2"
                        Style="color: #000000"
                        Text="Опыт работы от 3х лет">
                    </asp:Label>
                    <br />
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label12"
                        runat="server"
                        CssClass="Header2"
                        Style="color: #000000"
                        Text="Современный комфортный офис">
                    </asp:Label>
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label13"
                        runat="server"
                        CssClass="Header2"
                        Style="color: #000000"
                        Text="Гибкий график и удаленная работа">
                    </asp:Label>
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label14"
                        runat="server"
                        CssClass="Header2"
                        Style="color: #000000"
                        Text="Доход от 120 тыс. рублей">
                    </asp:Label>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <asp:Label ID="Label15"
                        runat="server"
                        CssClass="Header1"
                        Style="color: #000000"
                        Text="Направляйте, пожалуйста, резюме по адресу Homga@Homga.ru">
                    </asp:Label>
                </asp:Panel>
            </div>
        </div>
    </form>
</body>
</html>
