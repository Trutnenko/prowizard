﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Web;

namespace WebApplicationProWizard
{
    public partial class WebFormPrivate : System.Web.UI.Page
    {
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

                SqlCommand CommandFindIdUser = Conn.CreateCommand();
                CommandFindIdUser.CommandText = "SELECT IdUser From dbo.TableUser" 
                    + " WHERE PassKey = @PassKey";
                SqlParameter ParamFindUser = new SqlParameter();
                ParamFindUser.ParameterName = "@PassKey";
                HttpCookie CookieProWizardResult = new HttpCookie("CookieProWizard");
                ParamFindUser.Value = Request.Cookies["CookieProWizard"]["PassKey"].ToString();
                CommandFindIdUser.Parameters.Add(ParamFindUser);

                Conn.Open();
                string IdUserValue = Convert.ToString(CommandFindIdUser.ExecuteScalar());
                LabelIdUserBuyer.Text = IdUserValue;

                SqlCommand CommandCountDealBuyer = Conn.CreateCommand();
                CommandCountDealBuyer.CommandText = "SELECT COUNT (*) FROM dbo.TableDeal" 
                    + " WHERE IdUserBuyer = @ParamIdUserBuyer";
                SqlParameter ParamCountDealBuyer = new SqlParameter();
                ParamCountDealBuyer.ParameterName = "@ParamIdUserBuyer";
                ParamCountDealBuyer.Value = IdUserValue;
                CommandCountDealBuyer.Parameters.Add(ParamCountDealBuyer);
                string CountDealBuyer = Convert.ToString(CommandCountDealBuyer.ExecuteScalar());

                SqlCommand CommandCountDealSeller = Conn.CreateCommand();
                CommandCountDealSeller.CommandText = "SELECT COUNT (*) FROM dbo.TableDeal" 
                    + " WHERE IdUserSeller = @ParamIdUserSeller";
                SqlParameter ParamCountDealSeller = new SqlParameter();
                ParamCountDealSeller.ParameterName = "@ParamIdUserSeller";
                ParamCountDealSeller.Value = IdUserValue;
                CommandCountDealSeller.Parameters.Add(ParamCountDealSeller);

                string CountDealSeller = Convert.ToString(CommandCountDealSeller.ExecuteScalar());
                Conn.Close();

                if (CountDealBuyer == "0" & CountDealSeller != "0")
                {
                    LinkButtonSectionBuyer.ForeColor = System.Drawing.ColorTranslator.FromHtml("#525252");
                    LinkButtonSectionSeller.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078D7");

                    ButtonCreateDeal.Visible = false;
                    GridViewCurrentDealSell.Visible = true;
                    GridViewCurrentDealBuy.Visible = false;
                    LabelStatusBuyer.Visible = false;
                }

                if (CountDealBuyer == "0" & CountDealSeller == "0")
                {
                    LinkButtonSectionBuyer.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078D7");
                    LinkButtonSectionSeller.ForeColor = System.Drawing.ColorTranslator.FromHtml("#525252");

                    ButtonCreateDeal.Visible = true;
                    GridViewCurrentDealSell.Visible = false;
                    GridViewCurrentDealBuy.Visible = true;
                    LabelStatusBuyer.Visible = true;
                }

                if (CountDealBuyer != "0" & CountDealSeller != "0")
                {
                    LinkButtonSectionBuyer.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078D7");
                    LinkButtonSectionSeller.ForeColor = System.Drawing.ColorTranslator.FromHtml("#525252");

                    ButtonCreateDeal.Visible = true;
                    LabelStatusSell.Visible = false;
                    GridViewCurrentDealSell.Visible = false;
                    GridViewCurrentDealBuy.Visible = true;
                }
            }
        }

        protected void GridViewCurrentDealBuy_SelectedIndexChanged(object sender, EventArgs e)
        {
            HttpCookie CookieProWizard = new HttpCookie("Deal");
            CookieProWizard["IdDeal"] = GridViewCurrentDealBuy.SelectedRow.Cells[1].Text.ToString();
            Response.Cookies.Add(CookieProWizard);

            string StatusDeal = GridViewCurrentDealBuy.SelectedRow.Cells[2].Text;

            if (StatusDeal == "Подбор")
            { Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormNewDeal")); }

            if (StatusDeal == "Подключение продавца")
            { Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormCallSeller")); }

            if (StatusDeal == "Предварительный договор")
            { Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormPreliminaryContractBuyer")); }

            if (StatusDeal == "Проверка документов")
            { Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormMainDocumentsBuyer")); }

            if (StatusDeal == "Подготовка к сделке")
            { Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormContractBuyer")); }

            if (StatusDeal == "Сделка")
            { Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormDealBuyer")); }

            if (StatusDeal == "Передача недвижимости")
            { Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormGettingBuyer")); }
        }

        protected void GridViewCurrentDealSell_SelectedIndexChanged(object sender, EventArgs e)
        {
            HttpCookie CookieProWizard = new HttpCookie("Deal");
            CookieProWizard["IdDeal"] = GridViewCurrentDealSell.SelectedRow.Cells[1].Text.ToString();
            Response.Cookies.Add(CookieProWizard);

            string StatusDeal = GridViewCurrentDealSell.SelectedRow.Cells[2].Text;

            if (StatusDeal == "Предварительный договор")
            { Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormPreliminaryContractSeller")); }

            if (StatusDeal == "Проверка документов")
            { Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormMainDocumentsSeller")); }

            if (StatusDeal == "Подготовка к сделке")
            { Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormContractSeller")); }

            if (StatusDeal == "Сделка")
            { Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormDealSeller")); }

            if (StatusDeal == "Передача недвижимости")
            { Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormGettingSeller")); }
        }

        protected void ButtonCreateDeal_Click(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandFindIdUser = Conn.CreateCommand();
            CommandFindIdUser.CommandText = "SELECT IdUser FROM dbo.TableUser WHERE PassKey = @PassKey";

            SqlParameter ParamFindUser = new SqlParameter();
            ParamFindUser.ParameterName = "@PassKey";
            HttpCookie CookieProWizardResult = new HttpCookie("CookieProWizard");
            ParamFindUser.Value = Request.Cookies["CookieProWizard"]["PassKey"].ToString();
            CommandFindIdUser.Parameters.Add(ParamFindUser);

            Conn.Open();

            string IdUserValue = Convert.ToString(CommandFindIdUser.ExecuteScalar());

            Random RandomObjectForIdDeal = new Random();
            var RandomValueForIdDeal = RandomObjectForIdDeal.Next();

            SqlCommand CommandCreateNewDealONE = new SqlCommand("INSERT INTO dbo.TableDeal (IdDeal, StatusDeal, IdUserBuyer)"
                + " VALUES (@ParamIdDeal, @ParamStatusDeal, @ParamIdUserBuyer)", Conn);

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = RandomValueForIdDeal.ToString();
            CommandCreateNewDealONE.Parameters.Add(ParamIdDeal);

            SqlParameter ParamStatusDeal = new SqlParameter();
            ParamStatusDeal.ParameterName = "@ParamStatusDeal";
            ParamStatusDeal.Value = "Подбор";
            CommandCreateNewDealONE.Parameters.Add(ParamStatusDeal);

            SqlParameter ParamIdUserBuyer = new SqlParameter();
            ParamIdUserBuyer.ParameterName = "@ParamIdUserBuyer";
            ParamIdUserBuyer.Value = IdUserValue;
            CommandCreateNewDealONE.Parameters.Add(ParamIdUserBuyer);

            CommandCreateNewDealONE.ExecuteNonQuery();
            Conn.Close();

            HttpCookie CookieProWizard = new HttpCookie("Deal");
            CookieProWizard["IdDeal"] = RandomValueForIdDeal.ToString();
            Response.Cookies.Add(CookieProWizard);

            DirectoryInfo di = Directory.CreateDirectory(Server.MapPath("DealDocuments/" + RandomValueForIdDeal.ToString()));

            Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormNewDeal"));
        }

        protected void LinkButtonSectionSeller_Click(object sender, EventArgs e)
        {
            LinkButtonSectionBuyer.ForeColor = System.Drawing.ColorTranslator.FromHtml("#525252");
            LinkButtonSectionSeller.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078D7");

            ButtonCreateDeal.Visible = false;
            GridViewCurrentDealSell.Visible = true;
            GridViewCurrentDealBuy.Visible = false;
            LabelStatusBuyer.Visible = false;

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

            SqlCommand CommandFindIdUser = Conn.CreateCommand();
            CommandFindIdUser.CommandText = "SELECT IdUser From dbo.TableUser WHERE PassKey = @PassKey";
            SqlParameter ParamFindUser = new SqlParameter();
            ParamFindUser.ParameterName = "@PassKey";
            HttpCookie CookieProWizardResult = new HttpCookie("CookieProWizard");
            ParamFindUser.Value = Request.Cookies["CookieProWizard"]["PassKey"].ToString();
            CommandFindIdUser.Parameters.Add(ParamFindUser);

            Conn.Open();
            string IdUserValue = Convert.ToString(CommandFindIdUser.ExecuteScalar());

            SqlCommand CommandCountDealSeller = Conn.CreateCommand();
            CommandCountDealSeller.CommandText = "SELECT COUNT (*) FROM dbo.TableDeal WHERE IdUserSeller = @ParamIdUserSeller";
            SqlParameter ParamCountDealSeller = new SqlParameter();
            ParamCountDealSeller.ParameterName = "@ParamIdUserSeller";
            ParamCountDealSeller.Value = IdUserValue;
            CommandCountDealSeller.Parameters.Add(ParamCountDealSeller);

            string CountDealSeller = Convert.ToString(CommandCountDealSeller.ExecuteScalar());
            Conn.Close();

            if (CountDealSeller == "0")
            {
                LabelStatusSell.Visible = true;
            }
        }

        protected void LinkButtonSectionBuyer_Click(object sender, EventArgs e)
        {
            LinkButtonSectionBuyer.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078D7");
            LinkButtonSectionSeller.ForeColor = System.Drawing.ColorTranslator.FromHtml("#525252");

            ButtonCreateDeal.Visible = true;
            LabelStatusSell.Visible = false;
            GridViewCurrentDealSell.Visible = false;
            GridViewCurrentDealBuy.Visible = true;

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

            SqlCommand CommandFindIdUser = Conn.CreateCommand();
            CommandFindIdUser.CommandText = "SELECT IdUser From dbo.TableUser WHERE PassKey = @PassKey";
            SqlParameter ParamFindUser = new SqlParameter();
            ParamFindUser.ParameterName = "@PassKey";
            HttpCookie CookieProWizardResult = new HttpCookie("CookieProWizard");
            ParamFindUser.Value = Request.Cookies["CookieProWizard"]["PassKey"].ToString();
            CommandFindIdUser.Parameters.Add(ParamFindUser);

            Conn.Open();
            string IdUserValue = Convert.ToString(CommandFindIdUser.ExecuteScalar());

            SqlCommand CommandCountDealBuyer = Conn.CreateCommand();
            CommandCountDealBuyer.CommandText = "SELECT COUNT (*) FROM dbo.TableDeal WHERE IdUserBuyer = @ParamIdUserBuyer";
            SqlParameter ParamCountDealBuyer = new SqlParameter();
            ParamCountDealBuyer.ParameterName = "@ParamIdUserBuyer";
            ParamCountDealBuyer.Value = IdUserValue;
            CommandCountDealBuyer.Parameters.Add(ParamCountDealBuyer);

            string CountDealBuyer = Convert.ToString(CommandCountDealBuyer.ExecuteScalar());
            Conn.Close();

            if (CountDealBuyer == "0")
            {
                LabelStatusBuyer.Visible = true;
            }
        }
    }
}