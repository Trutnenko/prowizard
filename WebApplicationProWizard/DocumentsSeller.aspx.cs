﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Web;

namespace WebApplicationProWizard.WebForms.MainDocuments
{
    public partial class WebFormMainDocumentsSeller : System.Web.UI.Page
    {
        protected void Page_PreRender(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandDefineStatusMainDocuments = Conn.CreateCommand();
            CommandDefineStatusMainDocuments.CommandText = "SELECT StatusDeal,"
                + " StatusPassport,"
                + " StatusSertificateOfOwnerShip,"
                + " StatusDocumentFoundation,"
                + " StatusExtractFromRegister,"
                + " StatusExtendedExtractFromRegister,"
                + " StatusTechnicalPassport,"
                + " StatusConsentOfSpouse,"
                + " StatusResolutionOfGuardianship,"
                + " StatusDebtForUtility,"
                + " StatusCertificateFromTaxInspection"
                + " FROM dbo.TableDeal WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandDefineStatusMainDocuments.Parameters.Add(ParamIdDeal);

            SqlDataReader dReader;

            Conn.Open();

            dReader = CommandDefineStatusMainDocuments.ExecuteReader();

            if (dReader.Read())

            {
                string StatusDeal = dReader["StatusDeal"].ToString();
                string StatusPassport = dReader["StatusPassport"].ToString();
                string StatusSertificateOfOwnerShip = dReader["StatusSertificateOfOwnerShip"].ToString();
                string StatusDocumentFoundation = dReader["StatusDocumentFoundation"].ToString();
                string StatusExtractFromRegister = dReader["StatusExtractFromRegister"].ToString();
                string StatusExtendedExtractFromRegister = dReader["StatusExtendedExtractFromRegister"].ToString();
                string StatusTechnicalPassport = dReader["StatusTechnicalPassport"].ToString();
                string StatusConsentOfSpouse = dReader["StatusConsentOfSpouse"].ToString();
                string StatusResolutionOfGuardianship = dReader["StatusResolutionOfGuardianship"].ToString();
                string StatusDebtForUtility = dReader["StatusDebtForUtility"].ToString();
                string StatusCertificateFromTaxInspection = dReader["StatusCertificateFromTaxInspection"].ToString();

                if (StatusDeal == "Проверка документов")
                {
                    PanelCommonInformation.Visible = true;

                    if (StatusPassport == "Отклонен")
                    {
                        LabelPassport.Text = "Отклонен";
                        LinkButtonSendPassport.Visible = true;
                        FileUploadPassport.Visible = true;
                    }

                    if (StatusPassport == "Проверен")
                    {
                        LabelPassport.Text = "Проверен";
                        LinkButtonDenidePassport.Visible = true;
                    }

                    if (StatusPassport == "Подготовка")
                    {
                        LabelPassport.Text = "Отправьте";
                        LinkButtonSendPassport.Visible = true;
                        FileUploadPassport.Visible = true;
                    }

                    if (StatusPassport == "На согласовании")
                    {
                        LabelPassport.Text = "Отправлен";
                        LinkButtonDenidePassport.Visible = true;
                    }

                    if (StatusSertificateOfOwnerShip == "Отклонен")
                    {
                        LabelSertificateOfOwnerShip.Text = "Отклонен";
                        LinkButtonSendSertificateOfOwnerShip.Visible = true;
                        FileUploadSertificateOfOwnerShip.Visible = true;
                    }

                    if (StatusSertificateOfOwnerShip == "Проверен")
                    {
                        LabelSertificateOfOwnerShip.Text = "Проверен";
                        LinkButtonDenideSertificateOfOwnerShip.Visible = true;
                    }

                    if (StatusSertificateOfOwnerShip == "Подготовка")
                    {
                        LabelSertificateOfOwnerShip.Text = "Отправьте";
                        LinkButtonSendSertificateOfOwnerShip.Visible = true;
                        FileUploadSertificateOfOwnerShip.Visible = true;
                    }

                    if (StatusSertificateOfOwnerShip == "На согласовании")
                    {
                        LabelSertificateOfOwnerShip.Text = "Отправлен";
                        LinkButtonDenideSertificateOfOwnerShip.Visible = true;
                        LinkButtonSendSertificateOfOwnerShip.Visible = false;
                    }

                    if (StatusDocumentFoundation == "Отклонен")
                    {
                        LabelDocumentFoundation.Text = "Отклонен";
                        LinkButtonSendDocumentFoundation.Visible = true;
                        FileUploadDocumentFoundation.Visible = true;
                    }

                    if (StatusDocumentFoundation == "Проверен")
                    {
                        LabelDocumentFoundation.Text = "Проверен";
                        LinkButtonDenideDocumentFoundation.Visible = true;
                    }

                    if (StatusDocumentFoundation == "Подготовка")
                    {
                        LabelDocumentFoundation.Text = "Отправьте";
                        LinkButtonSendDocumentFoundation.Visible = true;
                        FileUploadDocumentFoundation.Visible = true;
                    }

                    if (StatusDocumentFoundation == "На согласовании")
                    {
                        LabelDocumentFoundation.Text = "Отправлен";
                        LinkButtonDenideDocumentFoundation.Visible = true;
                    }

                    if (StatusExtractFromRegister == "Отклонен")
                    {
                        LabelExtractFromRegister.Text = "Отклонен";
                        LinkButtonSendExtractFromRegister.Visible = true;
                        FileUploadExtractFromRegister.Visible = true;
                    }

                    if (StatusExtractFromRegister == "Проверен")
                    {
                        LabelExtractFromRegister.Text = "Проверен";
                        LinkButtonDenideExtractFromRegister.Visible = true;
                    }

                    if (StatusExtractFromRegister == "Подготовка")
                    {
                        LabelExtractFromRegister.Text = "Отправьте";
                        LinkButtonSendExtractFromRegister.Visible = true;
                        FileUploadExtractFromRegister.Visible = true;
                    }

                    if (StatusExtractFromRegister == "На согласовании")
                    {
                        LabelExtractFromRegister.Text = "Отправлен";
                        LinkButtonDenideExtractFromRegister.Visible = true;
                    }

                    if (StatusExtendedExtractFromRegister == "Отклонен")
                    {
                        LabelExtendedExtractFromRegister.Text = "Отклонен";
                        LinkButtonSendExtendedExtractFromRegister.Visible = true;
                        FileUploadExtendedExtractFromRegister.Visible = true;
                    }

                    if (StatusExtendedExtractFromRegister == "Проверен")
                    {
                        LabelExtendedExtractFromRegister.Text = "Проверен";
                        LinkButtonDenideExtendedExtractFromRegister.Visible = true;
                    }

                    if (StatusExtendedExtractFromRegister == "Подготовка")
                    {
                        LabelExtendedExtractFromRegister.Text = "Отправьте";
                        LinkButtonSendExtendedExtractFromRegister.Visible = true;
                        FileUploadExtendedExtractFromRegister.Visible = true;
                    }

                    if (StatusExtendedExtractFromRegister == "На согласовании")
                    {
                        LabelExtendedExtractFromRegister.Text = "Отправлен";
                        LinkButtonDenideExtendedExtractFromRegister.Visible = true;
                    }

                    if (StatusTechnicalPassport == "Отклонен")
                    {
                        LabelTechnicalPassport.Text = "Отклонен";
                        LinkButtonSendTechnicalPassport.Visible = true;
                        FileUploadTechnicalPassport.Visible = true;
                    }

                    if (StatusTechnicalPassport == "Проверен")
                    {
                        LabelTechnicalPassport.Text = "Проверен";
                        LinkButtonDenideTechnicalPassport.Visible = true;
                    }

                    if (StatusTechnicalPassport == "Подготовка")
                    {
                        LabelTechnicalPassport.Text = "Отправьте";
                        LinkButtonSendTechnicalPassport.Visible = true;
                        FileUploadTechnicalPassport.Visible = true;
                    }

                    if (StatusTechnicalPassport == "На согласовании")
                    {
                        LabelTechnicalPassport.Text = "Отправлен";
                        LinkButtonDenideTechnicalPassport.Visible = true;
                    }

                    if (StatusConsentOfSpouse == "Отклонен")
                    {
                        LabelConsentOfSpouse.Text = "Отклонен";
                        LinkButtonSendConsentOfSpouse.Visible = true;
                        FileUploadConsentOfSpouse.Visible = true;
                    }

                    if (StatusConsentOfSpouse == "Проверен")
                    {
                        LabelConsentOfSpouse.Text = "Проверен";
                        LinkButtonDenideConsentOfSpouse.Visible = true;
                    }

                    if (StatusConsentOfSpouse == "Подготовка")
                    {
                        LabelConsentOfSpouse.Text = "Отправьте";
                        LinkButtonSendConsentOfSpouse.Visible = true;
                        FileUploadConsentOfSpouse.Visible = true;
                    }

                    if (StatusConsentOfSpouse == "На согласовании")
                    {
                        LabelConsentOfSpouse.Text = "Отправлен";
                        LinkButtonDenideConsentOfSpouse.Visible = true;
                    }

                    if (StatusResolutionOfGuardianship == "Отклонен")
                    {
                        LabelResolutionOfGuardianship.Text = "Отклонен";
                        LinkButtonSendResolutionOfGuardianship.Visible = true;
                        FileUploadResolutionOfGuardianship.Visible = true;
                    }

                    if (StatusResolutionOfGuardianship == "Проверен")
                    {
                        LabelResolutionOfGuardianship.Text = "Проверен";
                        LinkButtonDenideResolutionOfGuardianship.Visible = true;
                    }

                    if (StatusResolutionOfGuardianship == "Подготовка")
                    {
                        LabelResolutionOfGuardianship.Text = "Отправьте";
                        LinkButtonSendResolutionOfGuardianship.Visible = true;
                        FileUploadResolutionOfGuardianship.Visible = true;
                    }

                    if (StatusResolutionOfGuardianship == "На согласовании")
                    {
                        LabelResolutionOfGuardianship.Text = "Отправлен";
                        LinkButtonDenideResolutionOfGuardianship.Visible = true;
                    }

                    if (StatusDebtForUtility == "Отклонен")
                    {
                        LabelDebtForUtility.Text = "Отклонен";
                        LinkButtonSendDebtForUtility.Visible = true;
                        FileUploadDebtForUtility.Visible = true;
                    }

                    if (StatusDebtForUtility == "Проверен")
                    {
                        LabelDebtForUtility.Text = "Проверен";
                        LinkButtonDenideDebtForUtility.Visible = true;
                    }

                    if (StatusDebtForUtility == "Подготовка")
                    {
                        LabelDebtForUtility.Text = "Отправьте";
                        LinkButtonSendDebtForUtility.Visible = true;
                        FileUploadDebtForUtility.Visible = true;
                    }

                    if (StatusDebtForUtility == "На согласовании")
                    {
                        LabelDebtForUtility.Text = "Отправлен";
                        LinkButtonDenideDebtForUtility.Visible = true;
                    }

                    if (StatusCertificateFromTaxInspection == "Отклонен")
                    {
                        LabelCertificateFromTaxInspection.Text = "Отклонен";
                        LinkButtonSendCertificateFromTaxInspection.Visible = true;
                        FileUploadCertificateFromTaxInspection.Visible = true;
                    }

                    if (StatusCertificateFromTaxInspection == "Проверен")
                    {
                        LabelCertificateFromTaxInspection.Text = "Проверен";
                        LinkButtonDenideCertificateFromTaxInspection.Visible = true;
                    }

                    if (StatusCertificateFromTaxInspection == "Подготовка")
                    {
                        LabelCertificateFromTaxInspection.Text = "Отправьте";
                        LinkButtonSendCertificateFromTaxInspection.Visible = true;
                        FileUploadCertificateFromTaxInspection.Visible = true;
                    }

                    if (StatusCertificateFromTaxInspection == "На согласовании")
                    {
                        LabelCertificateFromTaxInspection.Text = "Отправлен";
                        LinkButtonDenideCertificateFromTaxInspection.Visible = true;
                    }
                }

                if (StatusDeal != "Проверка документов")
                {
                    PanelCommonInformation.Visible = false;
                    PanelCloseForm.Visible = true;

                    if (StatusPassport == "Отклонен")
                    {
                        LabelPassport.Text = "Документ был отклонен";
                    }

                    if (StatusPassport == "Проверен")
                    {
                        LabelPassport.Text = "Документ был проверен";
                    }

                    if (StatusSertificateOfOwnerShip == "Отклонен")
                    {
                        LabelSertificateOfOwnerShip.Text = "Документ был отклонен";
                    }

                    if (StatusSertificateOfOwnerShip == "Проверен")
                    {
                        LabelSertificateOfOwnerShip.Text = "Документ был проверен";
                    }

                    if (StatusDocumentFoundation == "Отклонен")
                    {
                        LabelDocumentFoundation.Text = "Документ был отклонен";
                    }

                    if (StatusDocumentFoundation == "Проверен")
                    {
                        LabelDocumentFoundation.Text = "Документ был проверен";
                    }

                    if (StatusExtractFromRegister == "Отклонен")
                    {
                        LabelExtractFromRegister.Text = "Документ был отклонен";
                    }

                    if (StatusExtractFromRegister == "Проверен")
                    {
                        LabelExtractFromRegister.Text = "Документ был проверен";
                    }

                    if (StatusExtendedExtractFromRegister == "Отклонен")
                    {
                        LabelExtendedExtractFromRegister.Text = "Документ был отклонен";
                    }

                    if (StatusExtendedExtractFromRegister == "Проверен")
                    {
                        LabelExtendedExtractFromRegister.Text = "Документ был проверен";
                    }

                    if (StatusTechnicalPassport == "Отклонен")
                    {
                        LabelTechnicalPassport.Text = "Документ был отклонен";
                    }

                    if (StatusTechnicalPassport == "Проверен")
                    {
                        LabelTechnicalPassport.Text = "Документ был проверен";
                    }

                    if (StatusConsentOfSpouse == "Отклонен")
                    {
                        LabelConsentOfSpouse.Text = "Документ был отклонен";
                    }

                    if (StatusConsentOfSpouse == "Проверен")
                    {
                        LabelConsentOfSpouse.Text = "Документ был проверен";
                    }

                    if (StatusResolutionOfGuardianship == "Отклонен")
                    {
                        LabelResolutionOfGuardianship.Text = "Документ был отклонен";
                    }

                    if (StatusResolutionOfGuardianship == "Проверен")
                    {
                        LabelResolutionOfGuardianship.Text = "Документ был проверен";
                    }

                    if (StatusDebtForUtility == "Отклонен")
                    {
                        LabelDebtForUtility.Text = "Документ был отклонен";
                    }

                    if (StatusDebtForUtility == "Проверен")
                    {
                        LabelDebtForUtility.Text = "Документ был проверен";
                    }

                    if (StatusCertificateFromTaxInspection == "Отклонен")
                    {
                        LabelCertificateFromTaxInspection.Text = "Документ был отклонен";
                    }

                    if (StatusCertificateFromTaxInspection == "Проверен")
                    {
                        LabelCertificateFromTaxInspection.Text = "Документ был проверен";
                    }

                    LinkButtonPassport.Enabled = false;
                    LinkButtonSertificateOfOwnerShip.Enabled = false;
                    LinkButtonDocumentFoundation.Enabled = false;
                    LinkButtonExtractFromRegister.Enabled = false;
                    LinkButtonExtendedExtractFromRegister.Enabled = false;
                    LinkButtonTechnicalPassport.Enabled = false;
                    LinkButtonConsentOfSpouse.Enabled = false;
                    LinkButtonResolutionOfGuardianship.Enabled = false;
                    LinkButtonDebtForUtility.Enabled = false;
                    LinkButtonCertificateFromTaxInspection.Enabled = false;

                    LinkButtonSendPassport.Enabled = false;
                    FileUploadPassport.Enabled = false;
                    LinkButtonDenidePassport.Enabled = false;

                    if (StatusPassport == "Подготовка")
                    {
                        LabelPassport.Text = "Документ не был отправлен";
                    }

                    if (StatusPassport == "На согласовании")
                    {
                        LabelPassport.Text = "Документ не был согласован";
                    }

                    LinkButtonSendSertificateOfOwnerShip.Enabled = false;
                    FileUploadSertificateOfOwnerShip.Enabled = false;
                    LinkButtonDenideSertificateOfOwnerShip.Enabled = false;

                    if (StatusSertificateOfOwnerShip == "Подготовка")
                    {
                        LabelSertificateOfOwnerShip.Text = "Документ не был отправлен";
                    }

                    if (StatusSertificateOfOwnerShip == "На согласовании")
                    {
                        LabelSertificateOfOwnerShip.Text = "Документ не был согласован";
                    }

                    LinkButtonSendDocumentFoundation.Enabled = false;
                    FileUploadDocumentFoundation.Enabled = false;
                    LinkButtonDenideDocumentFoundation.Enabled = false;

                    if (StatusDocumentFoundation == "Подготовка")
                    {
                        LabelDocumentFoundation.Text = "Документ не был отправлен";
                    }

                    if (StatusDocumentFoundation == "На согласовании")
                    {
                        LabelDocumentFoundation.Text = "Документ не был согласован";
                    }

                    LinkButtonSendExtractFromRegister.Enabled = false;
                    FileUploadExtractFromRegister.Enabled = false;
                    LinkButtonDenideExtractFromRegister.Enabled = false;

                    if (StatusExtractFromRegister == "Подготовка")
                    {
                        LabelExtractFromRegister.Text = "Документ не был отправлен";
                    }

                    if (StatusExtractFromRegister == "На согласовании")
                    {
                        LabelExtractFromRegister.Text = "Документ не был согласован";
                    }

                    LinkButtonSendExtendedExtractFromRegister.Enabled = false;
                    FileUploadExtendedExtractFromRegister.Enabled = false;
                    LinkButtonDenideExtendedExtractFromRegister.Enabled = false;

                    if (StatusExtendedExtractFromRegister == "Подготовка")
                    {
                        LabelExtendedExtractFromRegister.Text = "Документ не был отправлен";
                    }

                    if (StatusExtendedExtractFromRegister == "На согласовании")
                    {
                        LabelExtendedExtractFromRegister.Text = "Документ не был согласован";
                    }

                    LinkButtonSendTechnicalPassport.Enabled = false;
                    FileUploadTechnicalPassport.Enabled = false;
                    LinkButtonDenideTechnicalPassport.Enabled = false;

                    if (StatusTechnicalPassport == "Подготовка")
                    {
                        LabelTechnicalPassport.Text = "Документ не был отправлен";
                    }

                    if (StatusTechnicalPassport == "На согласовании")
                    {
                        LabelTechnicalPassport.Text = "Документ не был согласован";
                    }

                    LinkButtonSendConsentOfSpouse.Enabled = false;
                    FileUploadConsentOfSpouse.Enabled = false;
                    LinkButtonDenideConsentOfSpouse.Enabled = false;

                    if (StatusConsentOfSpouse == "Подготовка")
                    {
                        LabelConsentOfSpouse.Text = "Документ не был отправлен";
                    }

                    if (StatusConsentOfSpouse == "На согласовании")
                    {
                        LabelConsentOfSpouse.Text = "Документ не был согласован";
                    }

                    LinkButtonSendResolutionOfGuardianship.Enabled = false;
                    FileUploadResolutionOfGuardianship.Enabled = false;
                    LinkButtonDenideResolutionOfGuardianship.Enabled = false;

                    if (StatusResolutionOfGuardianship == "Подготовка")
                    {
                        LabelResolutionOfGuardianship.Text = "Документ не был отправлен";
                    }

                    if (StatusResolutionOfGuardianship == "На согласовании")
                    {
                        LabelResolutionOfGuardianship.Text = "Документ не был согласован";
                    }

                    LinkButtonSendDebtForUtility.Enabled = false;
                    FileUploadDebtForUtility.Enabled = false;
                    LinkButtonDenideDebtForUtility.Enabled = false;

                    if (StatusDebtForUtility == "Подготовка")
                    {
                        LabelDebtForUtility.Text = "Документ не был отправлен";
                    }

                    if (StatusDebtForUtility == "На согласовании")
                    {
                        LabelDebtForUtility.Text = "Документ не был согласован";
                    }

                    LinkButtonSendCertificateFromTaxInspection.Enabled = false;
                    FileUploadCertificateFromTaxInspection.Enabled = false;
                    LinkButtonDenideCertificateFromTaxInspection.Enabled = false;

                    if (StatusCertificateFromTaxInspection == "Подготовка")
                    {
                        LabelCertificateFromTaxInspection.Text = "Документ не был отправлен";
                    }

                    if (StatusCertificateFromTaxInspection == "На согласовании")
                    {
                        LabelCertificateFromTaxInspection.Text = "Документ не был согласован";
                    }
                }
            }
            Conn.Close();
        }

        protected void LinkPassport_Click(object sender, EventArgs e)
        {
            PanelPassportMain.Visible = true;
            PanelSertificateOfOwnerShipMain.Visible = false;
            PanelDocumentFoundationMain.Visible = false;
            PanelExtractFromRegisterMain.Visible = false;
            PanelExtendedExtractFromRegisterMain.Visible = false;
            PanelTechnicalPassportMain.Visible = false;
            PanelConsentOfSpouseMain.Visible = false;
            PanelResolutionOfGuardianshipMain.Visible = false;
            PanelDebtForUtilityMain.Visible = false;
            PanelCertificateFromTaxInspectionMain.Visible = false;

            LinkButtonPassport.ForeColor = System.Drawing.ColorTranslator.FromHtml("#AA4825");
            LinkButtonSertificateOfOwnerShip.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonDocumentFoundation.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonExtractFromRegister.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonExtendedExtractFromRegister.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonTechnicalPassport.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonConsentOfSpouse.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonResolutionOfGuardianship.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonDebtForUtility.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonCertificateFromTaxInspection.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandStatusPasport = Conn.CreateCommand();
            CommandStatusPasport.CommandText = "SELECT StatusDeal, StatusPassport"
                + " FROM dbo.TableDeal WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandStatusPasport.Parameters.Add(ParamIdDeal);

            SqlDataReader dReader;

            Conn.Open();

            dReader = CommandStatusPasport.ExecuteReader();

            if (dReader.Read())

            {
                string StatusDeal = dReader["StatusDeal"].ToString();
                string StatusPassport = dReader["StatusPassport"].ToString();

                if (StatusDeal == "Проверка документов")
                {
                    if (StatusPassport == "Подготовка")
                    {
                        PanelPassportPrepare.Visible = true;
                    }

                    if (StatusPassport == "На согласовании")
                    {
                        PanelCommonUponAccepting.Visible = true;
                    }

                    if (StatusPassport == "Проверен")
                    {
                        PanelCommonCheck.Visible = true;
                    }

                    if (StatusPassport == "Отклонен")
                    {
                        PanelCommonDenide.Visible = true;
                    }
                }
            }
            Conn.Close();
        }
        protected void LinkButtonSendPassport_Click(object sender, EventArgs e)
        {
            string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();

            try
            {
                if (FileUploadPassport.HasFile)
                {
                    if (FileUploadPassport.PostedFile.ContentLength <= 6291456)
                    {
                        foreach (string f in Request.Files.AllKeys)
                        {
                            HttpPostedFile File = Request.Files[f];
                            File.SaveAs(Server.MapPath("DealDocuments/" + IdDeal + "/")
                                + "Passport" + Path.GetExtension(File.FileName));
                        }

                        SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
                        SqlCommand CommandSetStatusMainDocuments = new SqlCommand("UPDATE dbo.TableDeal SET"
                            + " StatusPassport = @ParamStatusPassport  WHERE IdDeal = @ParamIdDeal", Conn);

                        SqlParameter ParamIdDealDouble = new SqlParameter();
                        ParamIdDealDouble.ParameterName = "@ParamIdDeal";
                        ParamIdDealDouble.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                        CommandSetStatusMainDocuments.Parameters.Add(ParamIdDealDouble);

                        SqlParameter ParamStatusPassport = new SqlParameter();
                        ParamStatusPassport.ParameterName = "@ParamStatusPassport";
                        ParamStatusPassport.Value = "На согласовании";
                        CommandSetStatusMainDocuments.Parameters.Add(ParamStatusPassport);

                        Conn.Open();
                        CommandSetStatusMainDocuments.ExecuteNonQuery();
                        Conn.Close();

                        PanelCommonUponAccepting.Visible = true;
                    }

                    else
                    {
                        LabelPassportInformation.Visible = true;
                        LabelPassportInformation.Text = "Не более 6 Мб";

                        PanelPassportPrepare.Visible = true;
                    }
                }

                else
                {
                    LabelPassportInformation.Visible = true;
                    LabelPassportInformation.Text = "Требуется приложить документ";

                    PanelPassportPrepare.Visible = true;
                }
            }

            catch (HttpException)
            {
                LabelPassportInformation.Visible = true;
                LabelPassportInformation.Text = "Не более 6 Мб";

                PanelPassportPrepare.Visible = true;
            }
        }
        protected void LinkButtonDenidePassport_Click(object sender, EventArgs e)
        {
            string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();
            string NameDirectory = Server.MapPath("DealDocuments/"
                 + Request.Cookies["Deal"]["IdDeal"].ToString());
            string[] FileName = Directory.GetFiles(NameDirectory, "Passport*");
            string FileNameWithoutPath = System.IO.Path.GetFileName(FileName[0]);

            File.Delete(Server.MapPath("DealDocuments/" + IdDeal + "/") + FileNameWithoutPath);

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandSetStatusMainDocuments = new SqlCommand("UPDATE dbo.TableDeal SET"
                + " StatusPassport = @ParamStatusPassport WHERE IdDeal = @ParamIdDeal", Conn);

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandSetStatusMainDocuments.Parameters.Add(ParamIdDeal);

            SqlParameter ParamStatusPassport = new SqlParameter();
            ParamStatusPassport.ParameterName = "@ParamStatusPassport";
            ParamStatusPassport.Value = "Подготовка";
            CommandSetStatusMainDocuments.Parameters.Add(ParamStatusPassport);

            Conn.Open();
            CommandSetStatusMainDocuments.ExecuteNonQuery();
            Conn.Close();

            PanelPassportPrepare.Visible = true;
        }

        protected void LinkButtonSertificateOfOwnerShip_Click(object sender, EventArgs e)
        {
            PanelPassportMain.Visible = false;
            PanelSertificateOfOwnerShipMain.Visible = true;
            PanelDocumentFoundationMain.Visible = false;
            PanelExtractFromRegisterMain.Visible = false;
            PanelExtendedExtractFromRegisterMain.Visible = false;
            PanelTechnicalPassportMain.Visible = false;
            PanelConsentOfSpouseMain.Visible = false;
            PanelResolutionOfGuardianshipMain.Visible = false;
            PanelDebtForUtilityMain.Visible = false;
            PanelCertificateFromTaxInspectionMain.Visible = false;

            LinkButtonPassport.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonSertificateOfOwnerShip.ForeColor = System.Drawing.ColorTranslator.FromHtml("#AA4825");
            LinkButtonDocumentFoundation.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonExtractFromRegister.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonExtendedExtractFromRegister.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonTechnicalPassport.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonConsentOfSpouse.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonResolutionOfGuardianship.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonDebtForUtility.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonCertificateFromTaxInspection.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandStatusSertificateOfOwnerShip = Conn.CreateCommand();
            CommandStatusSertificateOfOwnerShip.CommandText = "SELECT StatusDeal,"
                + " StatusSertificateOfOwnerShip FROM dbo.TableDeal WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandStatusSertificateOfOwnerShip.Parameters.Add(ParamIdDeal);

            SqlDataReader dReader;

            Conn.Open();

            dReader = CommandStatusSertificateOfOwnerShip.ExecuteReader();

            if (dReader.Read())

            {
                string StatusDeal = dReader["StatusDeal"].ToString();
                string StatusSertificateOfOwnerShip = dReader["StatusSertificateOfOwnerShip"].ToString();

                if (StatusDeal == "Проверка документов")

                {
                    PanelSertificateOfOwnerShipMain.Visible = true;

                    if (StatusSertificateOfOwnerShip == "Подготовка")
                    {
                        PanelSertificateOfOwnerShipPrepare.Visible = true;
                    }

                    if (StatusSertificateOfOwnerShip == "На согласовании")
                    {
                        PanelCommonUponAccepting.Visible = true;
                    }

                    if (StatusSertificateOfOwnerShip == "Проверен")
                    {
                        PanelCommonCheck.Visible = true;
                    }

                    if (StatusSertificateOfOwnerShip == "Отклонен")
                    {
                        PanelCommonDenide.Visible = true;
                    }
                }
            }
            Conn.Close();
        }
        protected void LinkButtonSendSertificateOfOwnerShip_Click(object sender, EventArgs e)
        {
            try
            {
                if (FileUploadSertificateOfOwnerShip.HasFile)
                {
                    if (FileUploadSertificateOfOwnerShip.PostedFile.ContentLength <= 6291456)
                    {
                        string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();

                        foreach (string f in Request.Files.AllKeys)
                        {
                            HttpPostedFile File = Request.Files[f];
                            File.SaveAs(Server.MapPath("DealDocuments/" + IdDeal + "/")
                                + "SertificateOfOwnerShip" + Path.GetExtension(File.FileName));

                        }

                        SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
                        SqlCommand CommandSetStatusMainDocuments = new SqlCommand("UPDATE dbo.TableDeal SET"
                            + " StatusSertificateOfOwnerShip = @ParamStatusSertificateOfOwnerShip"
                            + " WHERE IdDeal = @ParamIdDeal", Conn);

                        SqlParameter ParamIdDealDouble = new SqlParameter();
                        ParamIdDealDouble.ParameterName = "@ParamIdDeal";
                        ParamIdDealDouble.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                        CommandSetStatusMainDocuments.Parameters.Add(ParamIdDealDouble);

                        SqlParameter ParamStatusSertificateOfOwnerShip = new SqlParameter();
                        ParamStatusSertificateOfOwnerShip.ParameterName = "@ParamStatusSertificateOfOwnerShip";
                        ParamStatusSertificateOfOwnerShip.Value = "На согласовании";
                        CommandSetStatusMainDocuments.Parameters.Add(ParamStatusSertificateOfOwnerShip);

                        Conn.Open();
                        CommandSetStatusMainDocuments.ExecuteNonQuery();
                        Conn.Close();

                        PanelCommonUponAccepting.Visible = true;
                    }
                    else
                    {
                        LabelSertificateOfOwnerShipInformation.Visible = true;
                        LabelSertificateOfOwnerShipInformation.Text = "Не более 6 Мб";

                        PanelSertificateOfOwnerShipPrepare.Visible = true;
                    }
                }

                else
                {
                    LabelSertificateOfOwnerShip.Text = "Требуется приложить документ";

                    PanelSertificateOfOwnerShipPrepare.Visible = true;
                }
            }

            catch (HttpException)
            {
                LabelSertificateOfOwnerShipInformation.Visible = true;
                LabelSertificateOfOwnerShipInformation.Text = "Не более 6 Мб";

                PanelSertificateOfOwnerShipPrepare.Visible = true;
            }
        }
        protected void LinkButtonDenideSertificateOfOwnerShip_Click(object sender, EventArgs e)
        {
            string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();
            string NameDirectory = Server.MapPath("DealDocuments/"
                 + Request.Cookies["Deal"]["IdDeal"].ToString());
            string[] FileName = Directory.GetFiles(NameDirectory, "SertificateOfOwnerShip*");
            string FileNameWithoutPath = System.IO.Path.GetFileName(FileName[0]);

            File.Delete(Server.MapPath("DealDocuments/" + IdDeal + "/") + FileNameWithoutPath);

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandSetStatusMainDocuments = new SqlCommand("UPDATE dbo.TableDeal"
                + " SET StatusSertificateOfOwnerShip = @ParamStatusSertificateOfOwnerShip"
                + " WHERE IdDeal = @ParamIdDeal", Conn);

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandSetStatusMainDocuments.Parameters.Add(ParamIdDeal);

            SqlParameter ParamStatusSertificateOfOwnerShip = new SqlParameter();
            ParamStatusSertificateOfOwnerShip.ParameterName = "@ParamStatusSertificateOfOwnerShip";
            ParamStatusSertificateOfOwnerShip.Value = "Подготовка";
            CommandSetStatusMainDocuments.Parameters.Add(ParamStatusSertificateOfOwnerShip);

            Conn.Open();
            CommandSetStatusMainDocuments.ExecuteNonQuery();
            Conn.Close();

            PanelPassportPrepare.Visible = true;
        }

        protected void LinkButtonDocumentFoundation_Click(object sender, EventArgs e)
        {
            PanelPassportMain.Visible = false;
            PanelSertificateOfOwnerShipMain.Visible = false;
            PanelDocumentFoundationMain.Visible = true;
            PanelExtractFromRegisterMain.Visible = false;
            PanelExtendedExtractFromRegisterMain.Visible = false;
            PanelTechnicalPassportMain.Visible = false;
            PanelConsentOfSpouseMain.Visible = false;
            PanelResolutionOfGuardianshipMain.Visible = false;
            PanelDebtForUtilityMain.Visible = false;
            PanelCertificateFromTaxInspectionMain.Visible = false;

            LinkButtonPassport.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonSertificateOfOwnerShip.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonDocumentFoundation.ForeColor = System.Drawing.ColorTranslator.FromHtml("#AA4825");
            LinkButtonExtractFromRegister.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonExtendedExtractFromRegister.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonTechnicalPassport.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonConsentOfSpouse.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonResolutionOfGuardianship.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonDebtForUtility.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonCertificateFromTaxInspection.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandStatusDocumentFoundation = Conn.CreateCommand();
            CommandStatusDocumentFoundation.CommandText = "SELECT StatusDeal, StatusDocumentFoundation"
                + " FROM dbo.TableDeal WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandStatusDocumentFoundation.Parameters.Add(ParamIdDeal);

            SqlDataReader dReader;

            Conn.Open();

            dReader = CommandStatusDocumentFoundation.ExecuteReader();

            if (dReader.Read())

            {
                string StatusDeal = dReader["StatusDeal"].ToString();
                string StatusDocumentFoundation = dReader["StatusDocumentFoundation"].ToString();

                if (StatusDeal == "Проверка документов")

                {
                    PanelDocumentFoundationMain.Visible = true;

                    if (StatusDocumentFoundation == "Подготовка")
                    {
                        PanelDocumentFoundationPrepare.Visible = true;
                    }

                    if (StatusDocumentFoundation == "На согласовании")
                    {
                        PanelCommonUponAccepting.Visible = true;
                    }

                    if (StatusDocumentFoundation == "Проверен")
                    {
                        PanelCommonCheck.Visible = true;
                    }

                    if (StatusDocumentFoundation == "Отклонен")
                    {
                        PanelCommonDenide.Visible = true;
                    }
                }
            }
            Conn.Close();
        }
        protected void LinkButtonSendDocumentFoundation_Click(object sender, EventArgs e)
        {
            try
            {
                if (FileUploadDocumentFoundation.HasFile)
                {
                    if (FileUploadDocumentFoundation.PostedFile.ContentLength <= 6291456)
                    {
                        string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();

                        foreach (string f in Request.Files.AllKeys)
                        {
                            HttpPostedFile File = Request.Files[f];
                            File.SaveAs(Server.MapPath("DealDocuments/" + IdDeal + "/")
                                + "DocumentFoundation" + Path.GetExtension(File.FileName));
                        }

                        SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

                        SqlCommand CommandSetStatusMainDocuments = new SqlCommand("UPDATE dbo.TableDeal SET"
                            + " StatusDocumentFoundation = @ParamStatusDocumentFoundation"
                            + " WHERE IdDeal = @ParamIdDeal", Conn);

                        SqlParameter ParamIdDealDouble = new SqlParameter();
                        ParamIdDealDouble.ParameterName = "@ParamIdDeal";
                        ParamIdDealDouble.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                        CommandSetStatusMainDocuments.Parameters.Add(ParamIdDealDouble);

                        SqlParameter ParamStatusDocumentFoundation = new SqlParameter();
                        ParamStatusDocumentFoundation.ParameterName = "@ParamStatusDocumentFoundation";
                        ParamStatusDocumentFoundation.Value = "На согласовании";
                        CommandSetStatusMainDocuments.Parameters.Add(ParamStatusDocumentFoundation);

                        Conn.Open();
                        CommandSetStatusMainDocuments.ExecuteNonQuery();
                        Conn.Close();

                        PanelCommonUponAccepting.Visible = true;
                    }
                    else
                    {
                        LabelDocumentFoundationtInformation.Visible = true;
                        LabelDocumentFoundationtInformation.Text = "Не более 6 Мб";

                        PanelDocumentFoundationPrepare.Visible = true;
                    }
                }

                else
                {
                    LabelDocumentFoundation.Text = "Требуется приложить документ";

                    PanelDocumentFoundationPrepare.Visible = true;
                }
            }

            catch (HttpException)
            {
                LabelDocumentFoundationtInformation.Visible = true;
                LabelDocumentFoundationtInformation.Text = "Не более 6 Мб";

                PanelDocumentFoundationPrepare.Visible = true;
            }
        }
        protected void LinkButtonDenideDocumentFoundation_Click(object sender, EventArgs e)
        {
            string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();
            string NameDirectory = Server.MapPath("DealDocuments/"
                 + Request.Cookies["Deal"]["IdDeal"].ToString());
            string[] FileName = Directory.GetFiles(NameDirectory, "DocumentFoundation*");
            string FileNameWithoutPath = System.IO.Path.GetFileName(FileName[0]);

            File.Delete(Server.MapPath("DealDocuments/" + IdDeal + "/") + FileNameWithoutPath);

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandSetStatusMainDocuments = new SqlCommand("UPDATE dbo.TableDeal SET"
               + " StatusDocumentFoundation = @ParamStatusDocumentFoundation"
               + " WHERE IdDeal = @ParamIdDeal", Conn);

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandSetStatusMainDocuments.Parameters.Add(ParamIdDeal);

            SqlParameter ParamStatusDocumentFoundation = new SqlParameter();
            ParamStatusDocumentFoundation.ParameterName = "@ParamStatusDocumentFoundation";
            ParamStatusDocumentFoundation.Value = "Подготовка";
            CommandSetStatusMainDocuments.Parameters.Add(ParamStatusDocumentFoundation);

            Conn.Open();
            CommandSetStatusMainDocuments.ExecuteNonQuery();
            Conn.Close();

            PanelPassportPrepare.Visible = true;
        }

        protected void LinkButtonExtractFromRegister_Click(object sender, EventArgs e)
        {
            PanelPassportMain.Visible = false;
            PanelSertificateOfOwnerShipMain.Visible = false;
            PanelDocumentFoundationMain.Visible = false;
            PanelExtractFromRegisterMain.Visible = true;
            PanelExtendedExtractFromRegisterMain.Visible = false;
            PanelTechnicalPassportMain.Visible = false;
            PanelConsentOfSpouseMain.Visible = false;
            PanelResolutionOfGuardianshipMain.Visible = false;
            PanelDebtForUtilityMain.Visible = false;
            PanelCertificateFromTaxInspectionMain.Visible = false;

            LinkButtonPassport.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonSertificateOfOwnerShip.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonDocumentFoundation.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonExtractFromRegister.ForeColor = System.Drawing.ColorTranslator.FromHtml("#AA4825");
            LinkButtonExtendedExtractFromRegister.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonTechnicalPassport.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonConsentOfSpouse.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonResolutionOfGuardianship.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonDebtForUtility.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonCertificateFromTaxInspection.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandStatusExtractFromRegister = Conn.CreateCommand();
            CommandStatusExtractFromRegister.CommandText = "SELECT StatusDeal, StatusExtractFromRegister"
                + " FROM dbo.TableDeal WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandStatusExtractFromRegister.Parameters.Add(ParamIdDeal);

            SqlDataReader dReader;

            Conn.Open();

            dReader = CommandStatusExtractFromRegister.ExecuteReader();

            if (dReader.Read())

            {
                string StatusDeal = dReader["StatusDeal"].ToString();
                string StatusExtractFromRegister = dReader["StatusExtractFromRegister"].ToString();

                if (StatusDeal == "Проверка документов")

                {
                    PanelExtractFromRegisterMain.Visible = true;

                    if (StatusExtractFromRegister == "Подготовка")
                    {
                        PanelExtractFromRegisterPrepare.Visible = true;
                    }

                    if (StatusExtractFromRegister == "На согласовании")
                    {
                        PanelCommonUponAccepting.Visible = true;
                    }

                    if (StatusExtractFromRegister == "Проверен")
                    {
                        PanelCommonCheck.Visible = true;
                    }

                    if (StatusExtractFromRegister == "Отклонен")
                    {
                        PanelCommonDenide.Visible = true;
                    }
                }
            }
            Conn.Close();
        }
        protected void LinkButtonSendExtractFromRegister_Click(object sender, EventArgs e)
        {
            try
            {
                if (FileUploadExtractFromRegister.HasFile)
                {
                    if (FileUploadExtractFromRegister.PostedFile.ContentLength <= 6291456)
                    {
                        string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();

                        foreach (string f in Request.Files.AllKeys)
                        {
                            HttpPostedFile File = Request.Files[f];
                            File.SaveAs(Server.MapPath("DealDocuments/" + IdDeal + "/")
                                + "ExtractFromRegister" + Path.GetExtension(File.FileName));
                        }

                        SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
                        SqlCommand CommandSetStatusMainDocuments = new SqlCommand("UPDATE dbo.TableDeal SET"
                            + " StatusExtractFromRegister = @ParamStatusExtractFromRegister"
                            + " WHERE IdDeal = @ParamIdDeal", Conn);

                        SqlParameter ParamIdDealDouble = new SqlParameter();
                        ParamIdDealDouble.ParameterName = "@ParamIdDeal";
                        ParamIdDealDouble.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                        CommandSetStatusMainDocuments.Parameters.Add(ParamIdDealDouble);

                        SqlParameter ParamStatusExtractFromRegister = new SqlParameter();
                        ParamStatusExtractFromRegister.ParameterName = "@ParamStatusExtractFromRegister";
                        ParamStatusExtractFromRegister.Value = "На согласовании";
                        CommandSetStatusMainDocuments.Parameters.Add(ParamStatusExtractFromRegister);

                        Conn.Open();
                        CommandSetStatusMainDocuments.ExecuteNonQuery();
                        Conn.Close();

                        PanelCommonUponAccepting.Visible = true;
                    }
                    else
                    {
                        LabelExtractFromRegisterInformation.Visible = true;
                        LabelExtractFromRegisterInformation.Text = "Не более 6 Мб";

                        PanelExtractFromRegisterPrepare.Visible = true;
                    }
                }
                else
                {
                    LabelExtractFromRegister.Text = "Требуется приложить документ";

                    PanelExtractFromRegisterPrepare.Visible = true;
                }
            }

            catch (HttpException)
            {
                LabelExtractFromRegisterInformation.Visible = true;
                LabelExtractFromRegisterInformation.Text = "Не более 6 Мб";

                PanelExtractFromRegisterPrepare.Visible = true;
            }
        }
        protected void LinkButtonDenideExtractFromRegister_Click(object sender, EventArgs e)
        {
            string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();
            string NameDirectory = Server.MapPath("DealDocuments/"
                 + Request.Cookies["Deal"]["IdDeal"].ToString());
            string[] FileName = Directory.GetFiles(NameDirectory, "ExtractFromRegister*");
            string FileNameWithoutPath = System.IO.Path.GetFileName(FileName[0]);

            File.Delete(Server.MapPath("DealDocuments/" + IdDeal + "/") + FileNameWithoutPath);

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandSetStatusMainDocuments = new SqlCommand("UPDATE dbo.TableDeal SET"
               + " StatusExtractFromRegister = @ParamStatusExtractFromRegister"
               + " WHERE IdDeal = @ParamIdDeal", Conn);

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandSetStatusMainDocuments.Parameters.Add(ParamIdDeal);

            SqlParameter ParamStatusExtractFromRegister = new SqlParameter();
            ParamStatusExtractFromRegister.ParameterName = "@ParamStatusExtractFromRegister";
            ParamStatusExtractFromRegister.Value = "Подготовка";
            CommandSetStatusMainDocuments.Parameters.Add(ParamStatusExtractFromRegister);

            Conn.Open();
            CommandSetStatusMainDocuments.ExecuteNonQuery();
            Conn.Close();

            PanelPassportPrepare.Visible = true;
        }

        protected void LinkButtonExtendedExtractFromRegister_Click(object sender, EventArgs e)
        {
            PanelPassportMain.Visible = false;
            PanelSertificateOfOwnerShipMain.Visible = false;
            PanelDocumentFoundationMain.Visible = false;
            PanelExtractFromRegisterMain.Visible = false;
            PanelExtendedExtractFromRegisterMain.Visible = true;
            PanelTechnicalPassportMain.Visible = false;
            PanelConsentOfSpouseMain.Visible = false;
            PanelResolutionOfGuardianshipMain.Visible = false;
            PanelDebtForUtilityMain.Visible = false;
            PanelCertificateFromTaxInspectionMain.Visible = false;

            LinkButtonPassport.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonSertificateOfOwnerShip.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonDocumentFoundation.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonExtractFromRegister.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonExtendedExtractFromRegister.ForeColor = System.Drawing.ColorTranslator.FromHtml("#AA4825");
            LinkButtonTechnicalPassport.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonConsentOfSpouse.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonResolutionOfGuardianship.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonDebtForUtility.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonCertificateFromTaxInspection.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandStatusExtendedExtractFromRegister = Conn.CreateCommand();
            CommandStatusExtendedExtractFromRegister.CommandText = "SELECT StatusDeal, StatusExtendedExtractFromRegister"
                + " FROM dbo.TableDeal WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandStatusExtendedExtractFromRegister.Parameters.Add(ParamIdDeal);

            SqlDataReader dReader;

            Conn.Open();

            dReader = CommandStatusExtendedExtractFromRegister.ExecuteReader();

            if (dReader.Read())

            {
                string StatusDeal = dReader["StatusDeal"].ToString();
                string StatusExtendedExtractFromRegister = dReader["StatusExtendedExtractFromRegister"].ToString();

                if (StatusDeal == "Проверка документов")

                {
                    PanelExtendedExtractFromRegisterMain.Visible = true;

                    if (StatusExtendedExtractFromRegister == "Подготовка")
                    {
                        PanelExtendedExtractFromRegisterPrepare.Visible = true;
                    }

                    if (StatusExtendedExtractFromRegister == "На согласовании")
                    {
                        PanelCommonUponAccepting.Visible = true;
                    }

                    if (StatusExtendedExtractFromRegister == "Проверен")
                    {
                        PanelCommonCheck.Visible = true;
                    }

                    if (StatusExtendedExtractFromRegister == "Отклонен")
                    {
                        PanelCommonDenide.Visible = true;
                    }
                }
            }
            Conn.Close();
        }
        protected void LinkButtonSendExtendedExtractFromRegister_Click(object sender, EventArgs e)
        {
            try
            {
                if (FileUploadExtendedExtractFromRegister.HasFile)
                {
                    if (FileUploadExtendedExtractFromRegister.PostedFile.ContentLength <= 6291456)
                    {
                        string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();

                        foreach (string f in Request.Files.AllKeys)
                        {
                            HttpPostedFile File = Request.Files[f];
                            File.SaveAs(Server.MapPath("DealDocuments/" + IdDeal + "/")
                                + "ExtendedExtractFromRegister" + Path.GetExtension(File.FileName));
                        }

                        SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
                        SqlCommand CommandSetStatusMainDocuments = new SqlCommand("UPDATE dbo.TableDeal SET"
                            + " StatusExtendedExtractFromRegister = @ParamStatusExtendedExtractFromRegister"
                            + " WHERE IdDeal = @ParamIdDeal", Conn);

                        SqlParameter ParamIdDealDouble = new SqlParameter();
                        ParamIdDealDouble.ParameterName = "@ParamIdDeal";
                        ParamIdDealDouble.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                        CommandSetStatusMainDocuments.Parameters.Add(ParamIdDealDouble);

                        SqlParameter ParamStatusExtendedExtractFromRegister = new SqlParameter();
                        ParamStatusExtendedExtractFromRegister.ParameterName = "@ParamStatusExtendedExtractFromRegister";
                        ParamStatusExtendedExtractFromRegister.Value = "На согласовании";
                        CommandSetStatusMainDocuments.Parameters.Add(ParamStatusExtendedExtractFromRegister);

                        Conn.Open();
                        CommandSetStatusMainDocuments.ExecuteNonQuery();
                        Conn.Close();

                        PanelCommonUponAccepting.Visible = true;
                    }
                    else
                    {
                        LabelExtendedExtractFromRegisterInformation.Visible = true;
                        LabelExtendedExtractFromRegisterInformation.Text = "Не более 6 Мб";

                        PanelExtendedExtractFromRegisterPrepare.Visible = true;
                    }
                }

                else
                {
                    LabelExtendedExtractFromRegister.Text = "Требуется приложить документ";

                    PanelExtendedExtractFromRegisterPrepare.Visible = true;
                }
            }

            catch (HttpException)
            {
                LabelExtendedExtractFromRegisterInformation.Visible = true;
                LabelExtendedExtractFromRegisterInformation.Text = "Не более 6 Мб";

                PanelExtendedExtractFromRegisterPrepare.Visible = true;
            }
        }
        protected void LinkButtonDenideExtendedExtractFromRegister_Click(object sender, EventArgs e)
        {
            string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();
            string NameDirectory = Server.MapPath("DealDocuments/"
                 + Request.Cookies["Deal"]["IdDeal"].ToString());
            string[] FileName = Directory.GetFiles(NameDirectory, "ExtendedExtractFromRegister*");
            string FileNameWithoutPath = System.IO.Path.GetFileName(FileName[0]);

            File.Delete(Server.MapPath("DealDocuments/" + IdDeal + "/") + FileNameWithoutPath);

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandSetStatusMainDocuments = new SqlCommand("Update dbo.TableDeal Set"
               + " StatusExtendedExtractFromRegister = @ParamStatusExtendedExtractFromRegister  Where IdDeal = @ParamIdDeal", Conn);

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandSetStatusMainDocuments.Parameters.Add(ParamIdDeal);

            SqlParameter ParamStatusExtendedExtractFromRegister = new SqlParameter();
            ParamStatusExtendedExtractFromRegister.ParameterName = "@ParamStatusExtendedExtractFromRegister";
            ParamStatusExtendedExtractFromRegister.Value = "Подготовка";
            CommandSetStatusMainDocuments.Parameters.Add(ParamStatusExtendedExtractFromRegister);

            Conn.Open();
            CommandSetStatusMainDocuments.ExecuteNonQuery();
            Conn.Close();

            PanelPassportPrepare.Visible = true;
        }

        protected void LinkButtonTechnicalPassport_Click(object sender, EventArgs e)
        {
            PanelPassportMain.Visible = false;
            PanelSertificateOfOwnerShipMain.Visible = false;
            PanelDocumentFoundationMain.Visible = false;
            PanelExtractFromRegisterMain.Visible = false;
            PanelExtendedExtractFromRegisterMain.Visible = false;
            PanelTechnicalPassportMain.Visible = true;
            PanelConsentOfSpouseMain.Visible = false;
            PanelResolutionOfGuardianshipMain.Visible = false;
            PanelDebtForUtilityMain.Visible = false;
            PanelCertificateFromTaxInspectionMain.Visible = false;

            LinkButtonPassport.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonSertificateOfOwnerShip.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonDocumentFoundation.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonExtractFromRegister.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonExtendedExtractFromRegister.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonTechnicalPassport.ForeColor = System.Drawing.ColorTranslator.FromHtml("#AA4825");
            LinkButtonConsentOfSpouse.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonResolutionOfGuardianship.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonDebtForUtility.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonCertificateFromTaxInspection.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandStatusTechnicalPassport = Conn.CreateCommand();
            CommandStatusTechnicalPassport.CommandText = "SELECT StatusDeal, StatusTechnicalPassport"
                + " FROM dbo.TableDeal WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandStatusTechnicalPassport.Parameters.Add(ParamIdDeal);

            SqlDataReader dReader;

            Conn.Open();

            dReader = CommandStatusTechnicalPassport.ExecuteReader();

            if (dReader.Read())

            {
                string StatusDeal = dReader["StatusDeal"].ToString();
                string StatusTechnicalPassport = dReader["StatusTechnicalPassport"].ToString();

                if (StatusDeal == "Проверка документов")

                {
                    PanelTechnicalPassportMain.Visible = true;

                    if (StatusTechnicalPassport == "Подготовка")
                    {
                        PanelTechnicalPassportPrepare.Visible = true;
                    }

                    if (StatusTechnicalPassport == "На согласовании")
                    {
                        PanelCommonUponAccepting.Visible = true;
                    }

                    if (StatusTechnicalPassport == "Проверен")
                    {
                        PanelCommonCheck.Visible = true;
                    }

                    if (StatusTechnicalPassport == "Отклонен")
                    {
                        PanelCommonDenide.Visible = true;
                    }
                }
            }
            Conn.Close();
        }
        protected void LinkButtonSendTechnicalPassport_Click(object sender, EventArgs e)
        {
            try
            {
                if (FileUploadTechnicalPassport.HasFile)
                {
                    if (FileUploadTechnicalPassport.PostedFile.ContentLength <= 6291456)
                    {
                        string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();

                        foreach (string f in Request.Files.AllKeys)
                        {
                            HttpPostedFile File = Request.Files[f];
                            File.SaveAs(Server.MapPath("DealDocuments/" + IdDeal + "/")
                                + "TechnicalPassport" + Path.GetExtension(File.FileName));
                        }

                        SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
                        SqlCommand CommandSetStatusMainDocuments = new SqlCommand("UPDATE dbo.TableDeal SET"
                            + " StatusTechnicalPassport = @ParamStatusTechnicalPassport"
                            + " WHERE IdDeal = @ParamIdDeal", Conn);

                        SqlParameter ParamIdDealDouble = new SqlParameter();
                        ParamIdDealDouble.ParameterName = "@ParamIdDeal";
                        ParamIdDealDouble.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                        CommandSetStatusMainDocuments.Parameters.Add(ParamIdDealDouble);

                        SqlParameter ParamStatusTechnicalPassport = new SqlParameter();
                        ParamStatusTechnicalPassport.ParameterName = "@ParamStatusTechnicalPassport";
                        ParamStatusTechnicalPassport.Value = "На согласовании";
                        CommandSetStatusMainDocuments.Parameters.Add(ParamStatusTechnicalPassport);

                        Conn.Open();
                        CommandSetStatusMainDocuments.ExecuteNonQuery();
                        Conn.Close();

                        PanelCommonUponAccepting.Visible = true;
                    }
                    else
                    {
                        LabelTechnicalPassportInformation.Visible = true;
                        LabelTechnicalPassportInformation.Text = "Не более 6 Мб";

                        PanelTechnicalPassportPrepare.Visible = true;
                    }
                }
                else
                {
                    LabelTechnicalPassport.Text = "Требуется приложить документ";

                    PanelTechnicalPassportPrepare.Visible = true;
                }
            }

            catch (HttpException)
            {
                LabelTechnicalPassportInformation.Visible = true;
                LabelTechnicalPassportInformation.Text = "Не более 6 Мб";

                PanelTechnicalPassportPrepare.Visible = true;
            }
        }
        protected void LinkButtonDenideTechnicalPassport_Click(object sender, EventArgs e)
        {
            string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();
            string NameDirectory = Server.MapPath("DealDocuments/"
                 + Request.Cookies["Deal"]["IdDeal"].ToString());
            string[] FileName = Directory.GetFiles(NameDirectory, "TechnicalPassport*");
            string FileNameWithoutPath = System.IO.Path.GetFileName(FileName[0]);

            File.Delete(Server.MapPath("DealDocuments/" + IdDeal + "/") + FileNameWithoutPath);

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandSetStatusMainDocuments = new SqlCommand("UPDATE dbo.TableDeal SET"
               + " StatusTechnicalPassport = @ParamStatusTechnicalPassport"
               + " WHERE IdDeal = @ParamIdDeal", Conn);

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandSetStatusMainDocuments.Parameters.Add(ParamIdDeal);

            SqlParameter ParamStatusTechnicalPassport = new SqlParameter();
            ParamStatusTechnicalPassport.ParameterName = "@ParamStatusTechnicalPassport";
            ParamStatusTechnicalPassport.Value = "Подготовка";
            CommandSetStatusMainDocuments.Parameters.Add(ParamStatusTechnicalPassport);

            Conn.Open();
            CommandSetStatusMainDocuments.ExecuteNonQuery();
            Conn.Close();

            PanelPassportPrepare.Visible = true;
        }

        protected void LinkButtonConsentOfSpouse_Click(object sender, EventArgs e)
        {
            PanelPassportMain.Visible = false;
            PanelSertificateOfOwnerShipMain.Visible = false;
            PanelDocumentFoundationMain.Visible = false;
            PanelExtractFromRegisterMain.Visible = false;
            PanelExtendedExtractFromRegisterMain.Visible = false;
            PanelTechnicalPassportMain.Visible = false;
            PanelConsentOfSpouseMain.Visible = true;
            PanelResolutionOfGuardianshipMain.Visible = false;
            PanelDebtForUtilityMain.Visible = false;
            PanelCertificateFromTaxInspectionMain.Visible = false;

            LinkButtonPassport.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonSertificateOfOwnerShip.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonDocumentFoundation.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonExtractFromRegister.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonExtendedExtractFromRegister.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonTechnicalPassport.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonConsentOfSpouse.ForeColor = System.Drawing.ColorTranslator.FromHtml("#AA4825");
            LinkButtonResolutionOfGuardianship.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonDebtForUtility.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonCertificateFromTaxInspection.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandStatusConsentOfSpouse = Conn.CreateCommand();
            CommandStatusConsentOfSpouse.CommandText = "SELECT StatusDeal, StatusConsentOfSpouse"
                + " FROM dbo.TableDeal WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandStatusConsentOfSpouse.Parameters.Add(ParamIdDeal);

            SqlDataReader dReader;

            Conn.Open();

            dReader = CommandStatusConsentOfSpouse.ExecuteReader();

            if (dReader.Read())

            {
                string StatusDeal = dReader["StatusDeal"].ToString();
                string StatusConsentOfSpouse = dReader["StatusConsentOfSpouse"].ToString();

                if (StatusDeal == "Проверка документов")

                {
                    PanelConsentOfSpouseMain.Visible = true;

                    if (StatusConsentOfSpouse == "Подготовка")
                    {
                        PanelConsentOfSpousePrepare.Visible = true;
                    }

                    if (StatusConsentOfSpouse == "На согласовании")
                    {
                        PanelCommonUponAccepting.Visible = true;
                    }

                    if (StatusConsentOfSpouse == "Проверен")
                    {
                        PanelCommonCheck.Visible = true;
                    }

                    if (StatusConsentOfSpouse == "Отклонен")
                    {
                        PanelCommonDenide.Visible = true;
                    }
                }
            }
            Conn.Close();
        }
        protected void LinkButtonSendConsentOfSpouse_Click(object sender, EventArgs e)
        {
            try
            {
                if (FileUploadConsentOfSpouse.HasFile)
                {
                    if (FileUploadConsentOfSpouse.PostedFile.ContentLength <= 6291456)
                    {
                        string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();

                        foreach (string f in Request.Files.AllKeys)
                        {
                            HttpPostedFile File = Request.Files[f];
                            File.SaveAs(Server.MapPath("DealDocuments/" + IdDeal + "/")
                                + "ConsentOfSpouse" + Path.GetExtension(File.FileName));
                        }

                        SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
                        SqlCommand CommandSetStatusMainDocuments = new SqlCommand("UPDATE dbo.TableDeal SET"
                            + " StatusConsentOfSpouse = @ParamStatusConsentOfSpouse"
                            + " WHERE IdDeal = @ParamIdDeal", Conn);

                        SqlParameter ParamIdDealDouble = new SqlParameter();
                        ParamIdDealDouble.ParameterName = "@ParamIdDeal";
                        ParamIdDealDouble.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                        CommandSetStatusMainDocuments.Parameters.Add(ParamIdDealDouble);

                        SqlParameter ParamStatusConsentOfSpouse = new SqlParameter();
                        ParamStatusConsentOfSpouse.ParameterName = "@ParamStatusConsentOfSpouse";
                        ParamStatusConsentOfSpouse.Value = "На согласовании";
                        CommandSetStatusMainDocuments.Parameters.Add(ParamStatusConsentOfSpouse);

                        Conn.Open();
                        CommandSetStatusMainDocuments.ExecuteNonQuery();
                        Conn.Close();

                        PanelCommonUponAccepting.Visible = true;
                    }
                    else
                    {
                        LabelConsentOfSpouseInformation.Visible = true;
                        LabelConsentOfSpouseInformation.Text = "Не более 6 Мб";

                        PanelConsentOfSpousePrepare.Visible = true;
                    }
                }
                else
                {
                    LabelConsentOfSpouse.Text = "Требуется приложить документ";

                    PanelConsentOfSpousePrepare.Visible = true;
                }
            }

            catch (HttpException)
            {
                LabelConsentOfSpouseInformation.Visible = true;
                LabelConsentOfSpouseInformation.Text = "Не более 6 Мб";

                PanelConsentOfSpousePrepare.Visible = true;
            }
        }
        protected void LinkButtonDenideConsentOfSpouse_Click(object sender, EventArgs e)
        {
            string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();
            string NameDirectory = Server.MapPath("DealDocuments/"
                 + Request.Cookies["Deal"]["IdDeal"].ToString());
            string[] FileName = Directory.GetFiles(NameDirectory, "ConsentOfSpouse*");
            string FileNameWithoutPath = System.IO.Path.GetFileName(FileName[0]);

            File.Delete(Server.MapPath("DealDocuments/" + IdDeal + "/") + FileNameWithoutPath);

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandSetStatusMainDocuments = new SqlCommand("UPDATE dbo.TableDeal SET"
               + " StatusConsentOfSpouse = @ParamStatusConsentOfSpouse"
               + " WHERE IdDeal = @ParamIdDeal", Conn);

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandSetStatusMainDocuments.Parameters.Add(ParamIdDeal);

            SqlParameter ParamStatusConsentOfSpouse = new SqlParameter();
            ParamStatusConsentOfSpouse.ParameterName = "@ParamStatusConsentOfSpouse";
            ParamStatusConsentOfSpouse.Value = "Подготовка";
            CommandSetStatusMainDocuments.Parameters.Add(ParamStatusConsentOfSpouse);

            Conn.Open();
            CommandSetStatusMainDocuments.ExecuteNonQuery();
            Conn.Close();

            PanelPassportPrepare.Visible = true;
        }

        protected void LinkButtonResolutionOfGuardianship_Click(object sender, EventArgs e)
        {
            PanelPassportMain.Visible = false;
            PanelSertificateOfOwnerShipMain.Visible = false;
            PanelDocumentFoundationMain.Visible = false;
            PanelExtractFromRegisterMain.Visible = false;
            PanelExtendedExtractFromRegisterMain.Visible = false;
            PanelTechnicalPassportMain.Visible = false;
            PanelConsentOfSpouseMain.Visible = false;
            PanelResolutionOfGuardianshipMain.Visible = true;
            PanelDebtForUtilityMain.Visible = false;
            PanelCertificateFromTaxInspectionMain.Visible = false;

            LinkButtonPassport.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonSertificateOfOwnerShip.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonDocumentFoundation.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonExtractFromRegister.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonExtendedExtractFromRegister.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonTechnicalPassport.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonConsentOfSpouse.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonResolutionOfGuardianship.ForeColor = System.Drawing.ColorTranslator.FromHtml("#AA4825");
            LinkButtonDebtForUtility.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonCertificateFromTaxInspection.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandStatusResolutionOfGuardianship = Conn.CreateCommand();
            CommandStatusResolutionOfGuardianship.CommandText = "SELECT StatusDeal, StatusResolutionOfGuardianship"
                + " FROM dbo.TableDeal WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandStatusResolutionOfGuardianship.Parameters.Add(ParamIdDeal);

            SqlDataReader dReader;

            Conn.Open();

            dReader = CommandStatusResolutionOfGuardianship.ExecuteReader();

            if (dReader.Read())

            {
                string StatusDeal = dReader["StatusDeal"].ToString();
                string StatusResolutionOfGuardianship = dReader["StatusResolutionOfGuardianship"].ToString();

                if (StatusDeal == "Проверка документов")

                {
                    PanelResolutionOfGuardianshipMain.Visible = true;

                    if (StatusResolutionOfGuardianship == "Подготовка")
                    {
                        PanelResolutionOfGuardianshipPrepare.Visible = true;
                    }

                    if (StatusResolutionOfGuardianship == "На согласовании")
                    {
                        PanelCommonUponAccepting.Visible = true;
                    }

                    if (StatusResolutionOfGuardianship == "Проверен")
                    {
                        PanelCommonCheck.Visible = true;
                    }

                    if (StatusResolutionOfGuardianship == "Отклонен")
                    {
                        PanelCommonDenide.Visible = true;
                    }
                }
            }
            Conn.Close();
        }
        protected void LinkButtonSendResolutionOfGuardianship_Click(object sender, EventArgs e)
        {
            try
            {
                if (FileUploadResolutionOfGuardianship.HasFile)
                {
                    if (FileUploadResolutionOfGuardianship.PostedFile.ContentLength <= 6291456)
                    {
                        string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();

                        foreach (string f in Request.Files.AllKeys)
                        {
                            HttpPostedFile File = Request.Files[f];
                            File.SaveAs(Server.MapPath("DealDocuments/" + IdDeal + "/")
                                + "ResolutionOfGuardianship" + Path.GetExtension(File.FileName));
                        }

                        SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
                        SqlCommand CommandSetStatusMainDocuments = new SqlCommand("UPDATE dbo.TableDeal SET"
                            + " StatusResolutionOfGuardianship = @ParamStatusResolutionOfGuardianship"
                            + " WHERE IdDeal = @ParamIdDeal", Conn);

                        SqlParameter ParamIdDealDouble = new SqlParameter();
                        ParamIdDealDouble.ParameterName = "@ParamIdDeal";
                        ParamIdDealDouble.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                        CommandSetStatusMainDocuments.Parameters.Add(ParamIdDealDouble);

                        SqlParameter ParamStatusResolutionOfGuardianship = new SqlParameter();
                        ParamStatusResolutionOfGuardianship.ParameterName = "@ParamStatusResolutionOfGuardianship";
                        ParamStatusResolutionOfGuardianship.Value = "На согласовании";
                        CommandSetStatusMainDocuments.Parameters.Add(ParamStatusResolutionOfGuardianship);

                        Conn.Open();
                        CommandSetStatusMainDocuments.ExecuteNonQuery();
                        Conn.Close();

                        PanelCommonUponAccepting.Visible = true;
                    }
                    else
                    {
                        LabelResolutionOfGuardianshipInformation.Visible = true;
                        LabelResolutionOfGuardianshipInformation.Text = "Не более 6 Мб";

                        PanelResolutionOfGuardianshipPrepare.Visible = true;
                    }
                }
                else
                {
                    LabelResolutionOfGuardianship.Text = "Требуется приложить документ";

                    PanelResolutionOfGuardianshipPrepare.Visible = true;
                }
            }

            catch (HttpException)
            {
                LabelResolutionOfGuardianshipInformation.Visible = true;
                LabelResolutionOfGuardianshipInformation.Text = "Не более 6 Мб";

                PanelResolutionOfGuardianshipPrepare.Visible = true;
            }
        }
        protected void LinkButtonDenideResolutionOfGuardianship_Click(object sender, EventArgs e)
        {
            string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();
            string NameDirectory = Server.MapPath("DealDocuments/"
                 + Request.Cookies["Deal"]["IdDeal"].ToString());
            string[] FileName = Directory.GetFiles(NameDirectory, "ResolutionOfGuardianship*");
            string FileNameWithoutPath = System.IO.Path.GetFileName(FileName[0]);

            File.Delete(Server.MapPath("DealDocuments/" + IdDeal + "/") + FileNameWithoutPath);

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandSetStatusMainDocuments = new SqlCommand("UPDATE dbo.TableDeal SET"
               + " StatusResolutionOfGuardianship = @ParamStatusResolutionOfGuardianship"
               + " WHERE IdDeal = @ParamIdDeal", Conn);

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandSetStatusMainDocuments.Parameters.Add(ParamIdDeal);

            SqlParameter ParamStatusResolutionOfGuardianship = new SqlParameter();
            ParamStatusResolutionOfGuardianship.ParameterName = "@ParamStatusResolutionOfGuardianship";
            ParamStatusResolutionOfGuardianship.Value = "Подготовка";
            CommandSetStatusMainDocuments.Parameters.Add(ParamStatusResolutionOfGuardianship);

            Conn.Open();
            CommandSetStatusMainDocuments.ExecuteNonQuery();
            Conn.Close();

            PanelPassportPrepare.Visible = true;
        }

        protected void LinkButtonDebtForUtility_Click(object sender, EventArgs e)
        {
            PanelPassportMain.Visible = false;
            PanelSertificateOfOwnerShipMain.Visible = false;
            PanelDocumentFoundationMain.Visible = false;
            PanelExtractFromRegisterMain.Visible = false;
            PanelExtendedExtractFromRegisterMain.Visible = false;
            PanelTechnicalPassportMain.Visible = false;
            PanelConsentOfSpouseMain.Visible = false;
            PanelResolutionOfGuardianshipMain.Visible = false;
            PanelDebtForUtilityMain.Visible = true;
            PanelCertificateFromTaxInspectionMain.Visible = false;

            LinkButtonPassport.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonSertificateOfOwnerShip.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonDocumentFoundation.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonExtractFromRegister.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonExtendedExtractFromRegister.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonTechnicalPassport.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonConsentOfSpouse.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonResolutionOfGuardianship.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonDebtForUtility.ForeColor = System.Drawing.ColorTranslator.FromHtml("#AA4825");
            LinkButtonCertificateFromTaxInspection.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandStatusDebtForUtility = Conn.CreateCommand();
            CommandStatusDebtForUtility.CommandText = "SELECT StatusDeal," +
                " StatusDebtForUtility FROM dbo.TableDeal WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandStatusDebtForUtility.Parameters.Add(ParamIdDeal);

            SqlDataReader dReader;

            Conn.Open();

            dReader = CommandStatusDebtForUtility.ExecuteReader();

            if (dReader.Read())

            {
                string StatusDeal = dReader["StatusDeal"].ToString();
                string StatusDebtForUtility = dReader["StatusDebtForUtility"].ToString();

                if (StatusDeal == "Проверка документов")

                {
                    PanelDebtForUtilityMain.Visible = true;

                    if (StatusDebtForUtility == "Подготовка")
                    {
                        PanelDebtForUtilityPrepare.Visible = true;
                    }

                    if (StatusDebtForUtility == "На согласовании")
                    {
                        PanelCommonUponAccepting.Visible = true;
                    }

                    if (StatusDebtForUtility == "Проверен")
                    {
                        PanelCommonCheck.Visible = true;
                    }

                    if (StatusDebtForUtility == "Отклонен")
                    {
                        PanelCommonDenide.Visible = true;
                    }
                }
            }
            Conn.Close();
        }
        protected void LinkButtonSendDebtForUtility_Click(object sender, EventArgs e)
        {
            try
            {
                if (FileUploadDebtForUtility.HasFile)
                {
                    if (FileUploadDebtForUtility.PostedFile.ContentLength <= 6291456)
                    {
                        string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();

                        foreach (string f in Request.Files.AllKeys)
                        {
                            HttpPostedFile File = Request.Files[f];
                            File.SaveAs(Server.MapPath("DealDocuments/" + IdDeal + "/")
                                + "DebtForUtility" + Path.GetExtension(File.FileName));
                        }

                        SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
                        SqlCommand CommandSetStatusMainDocuments = new SqlCommand("UPDATE dbo.TableDeal SET"
                            + " StatusDebtForUtility = @ParamStatusDebtForUtility"
                            + " WHERE IdDeal = @ParamIdDeal", Conn);

                        SqlParameter ParamIdDealDouble = new SqlParameter();
                        ParamIdDealDouble.ParameterName = "@ParamIdDeal";
                        ParamIdDealDouble.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                        CommandSetStatusMainDocuments.Parameters.Add(ParamIdDealDouble);

                        SqlParameter ParamStatusDebtForUtility = new SqlParameter();
                        ParamStatusDebtForUtility.ParameterName = "@ParamStatusDebtForUtility";
                        ParamStatusDebtForUtility.Value = "На согласовании";
                        CommandSetStatusMainDocuments.Parameters.Add(ParamStatusDebtForUtility);

                        Conn.Open();
                        CommandSetStatusMainDocuments.ExecuteNonQuery();
                        Conn.Close();

                        PanelCommonUponAccepting.Visible = true;
                    }
                    else
                    {
                        LabelDebtForUtilityInformation.Visible = true;
                        LabelDebtForUtilityInformation.Text = "Не более 6 Мб";

                        PanelDebtForUtilityPrepare.Visible = true;
                    }
                }

                else
                {
                    LabelDebtForUtility.Text = "Требуется приложить документ";

                    PanelDebtForUtilityPrepare.Visible = true;
                }
            }


            catch (HttpException)
            {
                LabelDebtForUtilityInformation.Visible = true;
                LabelDebtForUtilityInformation.Text = "Не более 6 Мб";

                PanelDebtForUtilityPrepare.Visible = true;
            }
        }
        protected void LinkButtonDenideDebtForUtility_Click(object sender, EventArgs e)
        {
            string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();
            string NameDirectory = Server.MapPath("DealDocuments/"
                 + Request.Cookies["Deal"]["IdDeal"].ToString());
            string[] FileName = Directory.GetFiles(NameDirectory, "DebtForUtility*");
            string FileNameWithoutPath = System.IO.Path.GetFileName(FileName[0]);

            File.Delete(Server.MapPath("DealDocuments/" + IdDeal + "/") + FileNameWithoutPath);

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandSetStatusMainDocuments = new SqlCommand("UPDATE dbo.TableDeal SET"
               + " StatusDebtForUtility = @ParamStatusDebtForUtility"
               + " WHERE IdDeal = @ParamIdDeal", Conn);

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandSetStatusMainDocuments.Parameters.Add(ParamIdDeal);

            SqlParameter ParamStatusDebtForUtility = new SqlParameter();
            ParamStatusDebtForUtility.ParameterName = "@ParamStatusDebtForUtility";
            ParamStatusDebtForUtility.Value = "Подготовка";
            CommandSetStatusMainDocuments.Parameters.Add(ParamStatusDebtForUtility);

            Conn.Open();
            CommandSetStatusMainDocuments.ExecuteNonQuery();
            Conn.Close();

            PanelPassportPrepare.Visible = true;
        }

        protected void LinkButtonCertificateFromTaxInspection_Click(object sender, EventArgs e)
        {
            PanelPassportMain.Visible = false;
            PanelSertificateOfOwnerShipMain.Visible = false;
            PanelDocumentFoundationMain.Visible = false;
            PanelExtractFromRegisterMain.Visible = false;
            PanelExtendedExtractFromRegisterMain.Visible = false;
            PanelTechnicalPassportMain.Visible = false;
            PanelConsentOfSpouseMain.Visible = false;
            PanelResolutionOfGuardianshipMain.Visible = false;
            PanelDebtForUtilityMain.Visible = false;
            PanelCertificateFromTaxInspectionMain.Visible = true;

            LinkButtonPassport.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonSertificateOfOwnerShip.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonDocumentFoundation.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonExtractFromRegister.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonExtendedExtractFromRegister.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonTechnicalPassport.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonConsentOfSpouse.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonResolutionOfGuardianship.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonDebtForUtility.ForeColor = System.Drawing.ColorTranslator.FromHtml("#007EE5");
            LinkButtonCertificateFromTaxInspection.ForeColor = System.Drawing.ColorTranslator.FromHtml("#AA4825");

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandStatusCertificateFromTaxInspection = Conn.CreateCommand();
            CommandStatusCertificateFromTaxInspection.CommandText = "SELECT StatusDeal, StatusCertificateFromTaxInspection"
                + " FROM dbo.TableDeal WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandStatusCertificateFromTaxInspection.Parameters.Add(ParamIdDeal);

            SqlDataReader dReader;

            Conn.Open();

            dReader = CommandStatusCertificateFromTaxInspection.ExecuteReader();

            if (dReader.Read())

            {
                string StatusDeal = dReader["StatusDeal"].ToString();
                string StatusCertificateFromTaxInspection = dReader["StatusCertificateFromTaxInspection"].ToString();

                if (StatusDeal == "Проверка документов")

                {
                    PanelCertificateFromTaxInspectionMain.Visible = true;

                    if (StatusCertificateFromTaxInspection == "Подготовка")
                    {
                        PanelCertificateFromTaxInspectionPrepare.Visible = true;
                    }

                    if (StatusCertificateFromTaxInspection == "На согласовании")
                    {
                        PanelCommonUponAccepting.Visible = true;
                    }

                    if (StatusCertificateFromTaxInspection == "Проверен")
                    {
                        PanelCommonCheck.Visible = true;
                    }

                    if (StatusCertificateFromTaxInspection == "Отклонен")
                    {
                        PanelCommonDenide.Visible = true;
                    }
                }
            }
            Conn.Close();
        }
        protected void LinkButtonSendCertificateFromTaxInspection_Click(object sender, EventArgs e)
        {
            try
            {
                if (FileUploadCertificateFromTaxInspection.HasFile)
                {
                    if (FileUploadCertificateFromTaxInspection.PostedFile.ContentLength <= 6291456)
                    {
                        string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();

                        foreach (string f in Request.Files.AllKeys)
                        {
                            HttpPostedFile File = Request.Files[f];
                            File.SaveAs(Server.MapPath("DealDocuments/" + IdDeal + "/")
                                + "CertificateFromTaxInspection" + Path.GetExtension(File.FileName));
                        }

                        SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
                        SqlCommand CommandSetStatusMainDocuments = new SqlCommand("UPDATE dbo.TableDeal SET"
                            + " StatusCertificateFromTaxInspection = @ParamStatusCertificateFromTaxInspection"
                            + " WHERE IdDeal = @ParamIdDeal", Conn);

                        SqlParameter ParamIdDealDouble = new SqlParameter();
                        ParamIdDealDouble.ParameterName = "@ParamIdDeal";
                        ParamIdDealDouble.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                        CommandSetStatusMainDocuments.Parameters.Add(ParamIdDealDouble);

                        SqlParameter ParamStatusCertificateFromTaxInspection = new SqlParameter();
                        ParamStatusCertificateFromTaxInspection.ParameterName = "@ParamStatusCertificateFromTaxInspection";
                        ParamStatusCertificateFromTaxInspection.Value = "На согласовании";
                        CommandSetStatusMainDocuments.Parameters.Add(ParamStatusCertificateFromTaxInspection);

                        Conn.Open();
                        CommandSetStatusMainDocuments.ExecuteNonQuery();
                        Conn.Close();

                        PanelCommonUponAccepting.Visible = true;
                    }
                    else
                    {
                        LabelCertificateFromTaxInspectionInformation.Visible = true;
                        LabelCertificateFromTaxInspectionInformation.Text = "Не более 6 Мб";

                        PanelCertificateFromTaxInspectionPrepare.Visible = true;
                    }
                }
                else
                {
                    LabelCertificateFromTaxInspection.Text = "Требуется приложить документ";

                    PanelCertificateFromTaxInspectionPrepare.Visible = true;
                }
            }

            catch (HttpException)
            {
                LabelCertificateFromTaxInspectionInformation.Visible = true;
                LabelCertificateFromTaxInspectionInformation.Text = "Не более 6 Мб";

                PanelCertificateFromTaxInspectionPrepare.Visible = true;
            }
        }
        protected void LinkButtonDenideCertificateFromTaxInspection_Click(object sender, EventArgs e)
        {
            string IdDeal = Request.Cookies["Deal"]["IdDeal"].ToString();
            string NameDirectory = Server.MapPath("DealDocuments/"
                 + Request.Cookies["Deal"]["IdDeal"].ToString());
            string[] FileName = Directory.GetFiles(NameDirectory, "CertificateFromTaxInspection*");
            string FileNameWithoutPath = System.IO.Path.GetFileName(FileName[0]);

            File.Delete(Server.MapPath("DealDocuments/" + IdDeal + "/") + FileNameWithoutPath);

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandSetStatusMainDocuments = new SqlCommand("UPDATE dbo.TableDeal SET"
               + " StatusCertificateFromTaxInspection = @ParamStatusCertificateFromTaxInspection"
               + " WHERE IdDeal = @ParamIdDeal", Conn);

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandSetStatusMainDocuments.Parameters.Add(ParamIdDeal);

            SqlParameter ParamStatusCertificateFromTaxInspection = new SqlParameter();
            ParamStatusCertificateFromTaxInspection.ParameterName = "@ParamStatusCertificateFromTaxInspection";
            ParamStatusCertificateFromTaxInspection.Value = "Подготовка";
            CommandSetStatusMainDocuments.Parameters.Add(ParamStatusCertificateFromTaxInspection);

            Conn.Open();
            CommandSetStatusMainDocuments.ExecuteNonQuery();
            Conn.Close();

            PanelPassportPrepare.Visible = true;
        }
    }
}
