﻿using System;
using System.Web;

namespace WebApplicationProWizard
{
    public partial class Company : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
            HttpCookie CookieProWizardResult = new HttpCookie("CookieProWizard");
            string Page = Request.Cookies["CookieProWizard"]["AboutPage"].ToString();

                if (Page == "Company")
                {
                    PanelCompany.Visible = true;
                    PanelVacancy.Visible = false;

                    LinkButtonAbout.Font.Underline = true;
                    LinkButtonVacancy.Font.Underline = false;
                }

                if (Page == "Vacancy")
                {
                    PanelCompany.Visible = false;
                    PanelVacancy.Visible = true;

                    LinkButtonAbout.Font.Underline = false;
                    LinkButtonVacancy.Font.Underline = true;
                }

                if (Page == "Contact")
                {
                    PanelCompany.Visible = false;
                    PanelVacancy.Visible = false;

                    LinkButtonAbout.Font.Underline = false;
                    LinkButtonVacancy.Font.Underline = false;
                }
            }      
        }

        protected void LinkButtonAbout_Click(object sender, EventArgs e)
        {
            LinkButtonAbout.Font.Underline = true;
            LinkButtonVacancy.Font.Underline = false;

            PanelCompany.Visible = true;
            PanelVacancy.Visible = false;
        }

        protected void LinkButtonVacancy_Click(object sender, EventArgs e)
        {
            LinkButtonAbout.Font.Underline = false;
            LinkButtonVacancy.Font.Underline = true;

            PanelCompany.Visible = false;
            PanelVacancy.Visible = true;
        }

        protected void LinkButtonContact_Click(object sender, EventArgs e)
        {
            LinkButtonAbout.Font.Underline = false;
            LinkButtonVacancy.Font.Underline = false;

            PanelCompany.Visible = false;
            PanelVacancy.Visible = false;
        }

        protected void LinkButtonToHome_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            Response.Redirect("http://homga.ru/index.aspx");
        }
    }
}