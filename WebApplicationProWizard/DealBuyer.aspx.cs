﻿using System;
using System.Configuration;
using System.Data.SqlClient;

namespace WebApplicationProWizard.WebForms.Deal
{
    public partial class WebFormDealBuyer : System.Web.UI.Page
    {
        protected void Page_PreRender(object sender, EventArgs e)
        {
            string FileName = Request.Cookies["Deal"]["IdDeal"].ToString() + "_PreContract.docx";

            string PassKey = Request.Cookies["CookieProWizard"]["PassKey"].ToString();
            string IdUserValue;

            ClassFindIdUserInCookie.FindIdUser(PassKey, out IdUserValue);

            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandStatusPreliminaryContract = Conn.CreateCommand();
            CommandStatusPreliminaryContract.CommandText = "SELECT StatusDeal"
                + " FROM dbo.TableDeal WHERE IdDeal = @ParamIdDeal";

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandStatusPreliminaryContract.Parameters.Add(ParamIdDeal);

            SqlDataReader dReader;

            Conn.Open();

            dReader = CommandStatusPreliminaryContract.ExecuteReader();

            if (dReader.Read())

            {
                string StatusDeal = dReader["StatusDeal"].ToString();

                if (StatusDeal == "Сделка")
                {
                    if (!IsPostBack)
                    {
                        LinkButtonPanel1.Font.Underline = true;
                        LinkButtonPanel2.Font.Underline = false;
                    }
                }

                else
                {
                    PanelForward.Visible = false;
                    PanelReminder.Visible = false;
                    Panel1.Visible = false;
                    Panel1.Visible = false;
                    LinkButtonPanel1.Visible = false;
                    LinkButtonPanel2.Visible = false;
                    LabelStageClose.Visible = true;
                }
            }
            Conn.Close();
        }

        protected void ButtonForward_Click(object sender, EventArgs e)
        {
            PanelForward.Visible = false;
            PanelReminder.Visible = true;
        }
        protected void ButtonForce_Click(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            SqlCommand CommandSetStatusGetting = new SqlCommand("UPDATE dbo.TableDeal"
                + " SET StatusDeal = @ParamStatusDeal"
                + " WHERE IdDeal = @ParamIdDeal", Conn);

            SqlParameter ParamIdDeal = new SqlParameter();
            ParamIdDeal.ParameterName = "@ParamIdDeal";
            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
            CommandSetStatusGetting.Parameters.Add(ParamIdDeal);

            SqlParameter ParamStatusDeal = new SqlParameter();
            ParamStatusDeal.ParameterName = "@ParamStatusDeal";
            ParamStatusDeal.Value = "Передача недвижимости";
            CommandSetStatusGetting.Parameters.Add(ParamStatusDeal);

            Conn.Open();
            CommandSetStatusGetting.ExecuteNonQuery();
            Conn.Close();

            Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormGettingBuyer"));
        }
        protected void ButtonToBack_Click(object sender, EventArgs e)
        {
            PanelForward.Visible = true;
            PanelReminder.Visible = false;
        }

        protected void LinkButtonPanel1_Click(object sender, EventArgs e)
        {
            LinkButtonPanel1.Font.Underline = true;
            LinkButtonPanel2.Font.Underline = false;

            Panel1.Visible = true;
            Panel2.Visible = false;
        }

        protected void LinkButtonPanel2_Click(object sender, EventArgs e)
        {
            LinkButtonPanel1.Font.Underline = false;
            LinkButtonPanel2.Font.Underline = true;

            Panel1.Visible = false;
            Panel2.Visible = true;
        }
    }
}