﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;

namespace WebApplicationProWizard
{
    public partial class WebFormMain : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));

                SqlCommand CommandFindEmailUser = Conn.CreateCommand();
                CommandFindEmailUser.CommandText = "SELECT Email, IdUser FROM dbo.TableUser WHERE PassKey = @ParamPassKeyEmail";

                SqlParameter ParamPassKeyEmail = new SqlParameter();
                ParamPassKeyEmail.ParameterName = "@ParamPassKeyEmail";
                ParamPassKeyEmail.Value = Request.Cookies["CookieProWizard"]["PassKey"].ToString();
                CommandFindEmailUser.Parameters.Add(ParamPassKeyEmail);

                SqlDataReader dReader;

                Conn.Open();
                dReader = CommandFindEmailUser.ExecuteReader();

                if (dReader.Read())

                {
                    string Email = dReader["Email"].ToString();
                    string IdUser = dReader["IdUser"].ToString();

                    dReader.Close();

                    if (Email.Length != 0)

                    {
                        LabelEmail.Visible = true;
                        LabelEmail.Text = Email;
                        LinkButtonSignOut.Visible = true;

                        if (Request.Cookies["CookieProWizard"]["PassKey"].ToString() != null)
                        {
                            SqlCommand CommandDeal = Conn.CreateCommand();
                            CommandDeal.CommandText = "SELECT StatusDeal, IdUserBuyer,"
                            + " IdUserSeller From dbo.TableDeal Where IdDeal = @ParamIdDeal";

                            SqlParameter ParamIdDeal = new SqlParameter();
                            ParamIdDeal.ParameterName = "@ParamIdDeal";
                            ParamIdDeal.Value = Request.Cookies["Deal"]["IdDeal"].ToString();
                            CommandDeal.Parameters.Add(ParamIdDeal);

                            SqlDataReader dReaderTWO;
                            dReaderTWO = CommandDeal.ExecuteReader();

                            if (dReaderTWO.Read())
                            {
                                string StatusDeal = dReaderTWO["StatusDeal"].ToString();
                                string IdUserBuyer = dReaderTWO["IdUserBuyer"].ToString();
                                string IdUserSeller = dReaderTWO["IdUserSeller"].ToString();

                                if (Request.Url.ToString() != "http://homga.ru/Private.aspx")
                                {
                                    if (IdUserSeller == IdUser)
                                    {
                                        LinkButtonWizardPreliminaryContractSeller.Visible = true;
                                        LinkButtonWizardMainDocumentSeller.Visible = true;
                                        LinkButtonWizardPreparationDealSeller.Visible = true;
                                        LinkButtonWizardDealSeller.Visible = true;
                                        LinkButtonWizardTransferPropertySeller.Visible = true;

                                        if (StatusDeal == "Предварительный договор")
                                        {
                                            LinkButtonWizardPreliminaryContractSeller.Enabled = true;
                                        }

                                        if (StatusDeal == "Проверка документов")
                                        {
                                            LinkButtonWizardPreliminaryContractSeller.Enabled = true;
                                            LinkButtonWizardMainDocumentSeller.Enabled = true;
                                        }

                                        if (StatusDeal == "Подготовка к сделке")
                                        {
                                            LinkButtonWizardPreliminaryContractSeller.Enabled = true;
                                            LinkButtonWizardMainDocumentSeller.Enabled = true;
                                            LinkButtonWizardPreparationDealSeller.Enabled = true;
                                        }

                                        if (StatusDeal == "Сделка")
                                        {
                                            LinkButtonWizardPreliminaryContractSeller.Enabled = true;
                                            LinkButtonWizardMainDocumentSeller.Enabled = true;
                                            LinkButtonWizardPreparationDealSeller.Enabled = true;
                                            LinkButtonWizardDealSeller.Enabled = true;
                                        }

                                        if (StatusDeal == "Передача недвижимости")
                                        {
                                            LinkButtonWizardPreliminaryContractSeller.Enabled = true;
                                            LinkButtonWizardMainDocumentSeller.Enabled = true;
                                            LinkButtonWizardPreparationDealSeller.Enabled = true;
                                            LinkButtonWizardDealSeller.Enabled = true;
                                            LinkButtonWizardTransferPropertySeller.Enabled = true;
                                        }
                                    }

                                    if (IdUserBuyer == IdUser)
                                    {
                                        LinkButtonWizardFindObject.Visible = true;
                                        LinkButtonWizardCallSeller.Visible = true;
                                        LinkButtonWizardPreliminaryContractBuyer.Visible = true;
                                        LinkButtonWizardMainDocumentBuyer.Visible = true;
                                        LinkButtonWizardPreparationDealBuyer.Visible = true;
                                        LinkButtonWizardDealBuyer.Visible = true;
                                        LinkButtonWizardTransferPropertyBuyer.Visible = true;

                                        if (StatusDeal == "Подбор")
                                        {
                                            LinkButtonWizardFindObject.Enabled = true;
                                        }

                                        if (StatusDeal == "Подключение продавца")
                                        {
                                            LinkButtonWizardFindObject.Enabled = true;
                                            LinkButtonWizardCallSeller.Enabled = true;
                                        }

                                        if (StatusDeal == "Предварительный договор")
                                        {
                                            LinkButtonWizardFindObject.Enabled = true;
                                            LinkButtonWizardCallSeller.Enabled = true;
                                            LinkButtonWizardPreliminaryContractBuyer.Enabled = true;
                                        }

                                        if (StatusDeal == "Проверка документов")
                                        {
                                            LinkButtonWizardFindObject.Enabled = true;
                                            LinkButtonWizardCallSeller.Enabled = true;
                                            LinkButtonWizardPreliminaryContractBuyer.Enabled = true;
                                            LinkButtonWizardMainDocumentBuyer.Enabled = true;
                                        }

                                        if (StatusDeal == "Подготовка к сделке")
                                        {
                                            LinkButtonWizardFindObject.Enabled = true;
                                            LinkButtonWizardCallSeller.Enabled = true;
                                            LinkButtonWizardPreliminaryContractBuyer.Enabled = true;
                                            LinkButtonWizardMainDocumentBuyer.Enabled = true;
                                            LinkButtonWizardPreparationDealBuyer.Enabled = true;
                                        }

                                        if (StatusDeal == "Сделка")
                                        {
                                            LinkButtonWizardFindObject.Enabled = true;
                                            LinkButtonWizardCallSeller.Enabled = true;
                                            LinkButtonWizardPreliminaryContractBuyer.Enabled = true;
                                            LinkButtonWizardMainDocumentBuyer.Enabled = true;
                                            LinkButtonWizardPreparationDealBuyer.Enabled = true;
                                            LinkButtonWizardDealBuyer.Enabled = true;
                                        }

                                        if (StatusDeal == "Передача недвижимости")
                                        {
                                            LinkButtonWizardFindObject.Enabled = true;
                                            LinkButtonWizardCallSeller.Enabled = true;
                                            LinkButtonWizardPreliminaryContractBuyer.Enabled = true;
                                            LinkButtonWizardMainDocumentBuyer.Enabled = true;
                                            LinkButtonWizardPreparationDealBuyer.Enabled = true;
                                            LinkButtonWizardDealBuyer.Enabled = true;
                                            LinkButtonWizardTransferPropertyBuyer.Enabled = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    Conn.Close();
                }
            }
            catch { }

            if (Request.Url.ToString() == "http://homga.ru/Menu.aspx")
            {
                PanelMenuClose.Visible = true;
                PanelMenuOpen.Visible = false;

                LinkButtonWizardPreliminaryContractSeller.Visible = false;
                LinkButtonWizardMainDocumentSeller.Visible = false;
                LinkButtonWizardPreparationDealSeller.Visible = false;
                LinkButtonWizardDealSeller.Visible = false;
                LinkButtonWizardTransferPropertySeller.Visible = false;

                LinkButtonWizardFindObject.Visible = false;
                LinkButtonWizardCallSeller.Visible = false;
                LinkButtonWizardPreliminaryContractBuyer.Visible = false;
                LinkButtonWizardMainDocumentBuyer.Visible = false;
                LinkButtonWizardPreparationDealBuyer.Visible = false;
                LinkButtonWizardDealBuyer.Visible = false;
                LinkButtonWizardTransferPropertyBuyer.Visible = false;
            }
            if (Request.Url.ToString() == "http://homga.ru/Private.aspx")
            {
                ImageButtonClose.Visible = false;
            }
            if (Request.Url.ToString() == "http://homga.ru/NewDeal.aspx")
            {
                LinkButtonWizardFindObject.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
            }
            if (Request.Url.ToString() == "http://homga.ru/Call.aspx")
            {
                LinkButtonWizardCallSeller.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
            }

            if (Request.Url.ToString() == "http://homga.ru/PreContractBuyer.aspx")
            {
                LinkButtonWizardPreliminaryContractBuyer.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
            }
            if (Request.Url.ToString() == "http://homga.ru/DocumentsBuyer.aspx")
            {
                LinkButtonWizardMainDocumentBuyer.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
            }
            if (Request.Url.ToString() == "http://homga.ru/ContractBuyer.aspx")
            {
                LinkButtonWizardPreparationDealBuyer.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
            }
            if (Request.Url.ToString() == "http://homga.ru/DealBuyer.aspx")
            {
                LinkButtonWizardDealBuyer.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
            }
            if (Request.Url.ToString() == "http://homga.ru/GettingBuyer.aspx")
            {
                LinkButtonWizardTransferPropertyBuyer.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
            }

            if (Request.Url.ToString() == "http://homga.ru/PreContractSeller.aspx")
            {
                LinkButtonWizardPreliminaryContractSeller.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
            }
            if (Request.Url.ToString() == "http://homga.ru/DocumentsSeller.aspx")
            {
                LinkButtonWizardMainDocumentSeller.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
            }
            if (Request.Url.ToString() == "http://homga.ru/ContractSeller.aspx")
            {
                LinkButtonWizardPreparationDealSeller.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
            }
            if (Request.Url.ToString() == "http://homga.ru/DealSeller.aspx")
            {
                LinkButtonWizardDealSeller.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
            }
            if (Request.Url.ToString() == "http://homga.ru/GettingSeller.aspx")
            {
                LinkButtonWizardTransferPropertySeller.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
            }
        }

        protected void LinkButtonWizardPreliminaryContractSeller_Click(object sender, EventArgs e)
        {
            Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormPreliminaryContractSeller"));
        }
        protected void LinkButtonWizardMainDocumentSeller_Click(object sender, EventArgs e)
        {
            Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormMainDocumentsSeller"));
        }
        protected void LinkButtonWizardPreparationDealSeller_Click(object sender, EventArgs e)
        {
            Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormContractSeller"));
        }
        protected void LinkButtonWizardDealSeller_Click(object sender, EventArgs e)
        {
            Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormDealSeller"));
        }
        protected void LinkButtonWizardTransferPropertySeller_Click(object sender, EventArgs e)
        {
            Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormGettingSeller"));
        }

        protected void LinkButtonWizardFindObjectBuyer_Click(object sender, EventArgs e)
        {
            Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormNewDeal"));
        }
        protected void LinkButtonWizardCallSellerBuyer_Click(object sender, EventArgs e)
        {
            Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormCallSeller"));
        }
        protected void LinkButtonWizardPreliminaryContractBuyer_Click(object sender, EventArgs e)
        {
            Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormPreliminaryContractBuyer"));
        }
        protected void LinkButtonWizardMainDocumentBuyer_Click(object sender, EventArgs e)
        {
            Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormMainDocumentsBuyer"));
        }
        protected void LinkButtonWizardPreparationDealBuyer_Click(object sender, EventArgs e)
        {
            Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormContractBuyer"));
        }
        protected void LinkButtonWizardDealBuyer_Click(object sender, EventArgs e)
        {
            Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormDealBuyer"));
        }
        protected void LinkButtonWizardTransferPropertyBuyer_Click(object sender, EventArgs e)
        {
            Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormGettingBuyer"));
        }

        protected void LinkButtonSignOut_Click(object sender, EventArgs e)
        {
            HttpCookie CookieProWizard = new HttpCookie("CookieProWizard");
            Response.Cookies["CookieProWizard"].Expires = DateTime.Now.AddDays(-1);
            Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormMainPage"));
            Response.Redirect(Request.RawUrl);
        }

        protected void LinkButtonToHome_Click(object sender, EventArgs e)
        {
            Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormPrivate"));
        }

        protected void ImageOpenMenu_Click(object sender, ImageClickEventArgs e)
        {
            HttpCookie CookieProWizardMenu = new HttpCookie("CookieProWizardMenu");
            Uri СausedUrl = Request.Url;
            CookieProWizardMenu["Url"] = СausedUrl.ToString();
            Response.Cookies.Add(CookieProWizardMenu);

            Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormMenu"));
        }

        protected void ImageCloseMenu_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect(Request.Cookies["CookieProWizardMenu"]["Url"].ToString());
        }
    }
}