﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace WebApplicationProWizard
{
    public partial class SignIn : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void LinkButtonSign_Click(object sender, EventArgs e)
        {
            SqlConnection Conn = new
                SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            Conn.Open();

            SqlCommand CommandFindAccaunt = Conn.CreateCommand();
            CommandFindAccaunt.CommandText = "SELECT COUNT(*)"
                + " FROM dbo.TableUser"
                + " WHERE Email = @ParamEmail"
                + " AND Passwort = @ParamPasswort";

            SqlParameter ParamEmail = new SqlParameter();
            ParamEmail.ParameterName = "@ParamEmail";
            ParamEmail.Value = TextBoxEmail.Text.ToString();
            CommandFindAccaunt.Parameters.Add(ParamEmail);

            SqlParameter ParamPasswort = new SqlParameter();
            ParamPasswort.ParameterName = "@ParamPasswort";
            ParamPasswort.Value = TextBoxPasswort.Text.ToString();
            CommandFindAccaunt.Parameters.Add(ParamPasswort);

            string CountAccaunt =
                Convert.ToString(CommandFindAccaunt.ExecuteScalar());

            if (CountAccaunt == "1")
            {
                Random RandomObjectForPassKey = new Random();
                var PassKey = RandomObjectForPassKey.Next();

                SqlCommand CommandGetIdUser = new
                SqlCommand("SELECT IdUser FROM dbo.TableUser"
                        + " WHERE Email = @ParamEmail2", Conn);
                SqlParameter ParamEmail2 = new SqlParameter();
                ParamEmail2.ParameterName = "@ParamEmail2";
                ParamEmail2.Value = TextBoxEmail.Text.ToString(); ;
                CommandGetIdUser.Parameters.Add(ParamEmail2);

                string IdUser =
                    Convert.ToString(CommandGetIdUser.ExecuteScalar());

                SqlCommand CommandUpdatePassKey = new
                   SqlCommand("UPDATE dbo.TableUser"
                    + " SET PassKey = @PassKey"
                    + " WHERE IdUser = @ParamIdUser", Conn);
                SqlParameter ParamPassKey = new SqlParameter();
                ParamPassKey.ParameterName = "@PassKey";
                ParamPassKey.Value = PassKey;
                CommandUpdatePassKey.Parameters.Add(ParamPassKey);

                SqlParameter ParamIdUser = new SqlParameter();
                ParamIdUser.ParameterName = "@ParamIdUser";
                ParamIdUser.Value = IdUser;
                CommandUpdatePassKey.Parameters.Add(ParamIdUser);

                CommandUpdatePassKey.ExecuteNonQuery();

                HttpCookie CookieProWizard = new HttpCookie("CookieProWizard");
                CookieProWizard["PassKey"] = PassKey.ToString();
                Response.Cookies.Add(CookieProWizard);

                Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormPrivate"));
            }

            else
            {
                LabelInfoSignIn.Visible = true;
                LabelInfoSignIn.Text = "Что-то не так";
            }

            Conn.Close();

        }

        protected void LinkButtonSentNewPassport_Click(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnString"));
            Conn.Open();

            SqlCommand CommandCheckEmail = Conn.CreateCommand();
            CommandCheckEmail.CommandText = "SELECT COUNT (*)"
                + " FROM dbo.TableUser WHERE Email = @Email";

            SqlParameter ParamCheckEmail = new SqlParameter();
            ParamCheckEmail.ParameterName = "@Email";
            ParamCheckEmail.Value = TextBoxEmailForRestore.Text.ToString();
            CommandCheckEmail.Parameters.Add(ParamCheckEmail);

            string CountEmail = Convert.ToString(CommandCheckEmail.ExecuteScalar());

            if (CountEmail == "1")
            {
                SqlCommand CommandUpdatePasswort = new SqlCommand("UPDATE dbo.TableUser SET"
                    + " Passwort = @ParamPasswort WHERE Email = @ParamEmail", Conn);

                Random RandomValuePasswort = new Random();
                var RandomValuePasswortResult = RandomValuePasswort.Next().ToString();

                SqlParameter ParamPasswort = new SqlParameter();
                ParamPasswort.ParameterName = "@ParamPasswort";
                ParamPasswort.Value = RandomValuePasswortResult;
                CommandUpdatePasswort.Parameters.Add(ParamPasswort);

                SqlParameter ParamEmail = new SqlParameter();
                ParamEmail.ParameterName = "@ParamEmail";
                ParamEmail.Value = TextBoxEmailForRestore.Text.ToString();
                CommandUpdatePasswort.Parameters.Add(ParamEmail);

                CommandUpdatePasswort.ExecuteNonQuery();

                string from = "Homga@Homga.ru";
                string to = TextBoxEmailForRestore.Text.ToString();
                string subject = "Новый пароль";
                string body = "Новый пароль: " + RandomValuePasswortResult;
                string login = "Homga@Homga.ru";
                string pass = "100Million";
                var msg = new MailMessage(from, to, subject, body);
                var smtpClient = new SmtpClient("mail.nic.ru", 25);

                smtpClient.Credentials = new NetworkCredential(login, pass);
                smtpClient.EnableSsl = true;
                smtpClient.Send(msg);

                LabelInfoSignIn.Visible = true;
                LabelInfoSignIn.Text = "Пароль отправлен на Email";

                PanelSignIn.Visible = true;
                PanelForRestore.Visible = false;

            }

            if (CountEmail == "0")
            {
                LinkButtonForNewPasswort.Visible = false;
            }
            Conn.Close();
        }

        protected void LinkButtonForNewPasswort_Click(object sender, EventArgs e)
        {
            PanelForRestore.Visible = true;
            PanelSignIn.Visible = false;
            LinkButtonForNewPasswort.Visible = true;
        }

        protected void LinkButtonRestore_Click(object sender, EventArgs e)
        {
            PanelForRestore.Visible = true;
            PanelSignIn.Visible = false;      
        }

        protected void LinkButtonBackToMain_Click(object sender, EventArgs e)
        {
            PanelForRestore.Visible = false;
            PanelSignIn.Visible = true;
        }

        protected void LinkButtonToHome_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            Response.Redirect("http://homga.ru/index.aspx");
        }
    }
}