﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplicationProWizard
{
    public partial class _404 : System.Web.UI.Page
    {
        protected void LinkButtonInMain_Click(object sender, EventArgs e)
        {
            Response.Redirect(ConfigurationManager.AppSettings.Get("WebFormMainPage"));
        }
    }
}