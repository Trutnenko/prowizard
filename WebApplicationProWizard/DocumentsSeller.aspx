﻿<%@ Page
    Title=""
    Language="C#"
    MasterPageFile="~/Main.Master"
    AutoEventWireup="true"
    CodeBehind="DocumentsSeller.aspx.cs"
    Inherits="WebApplicationProWizard.WebForms.MainDocuments.WebFormMainDocumentsSeller"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainDocumentSeller" runat="server">
    <link rel="stylesheet" href="style.css" type="text/css" />
    <div style="position: absolute; height: 850px; width: 100%; top: 72px; background-color: #FFFFFF; z-index: 0;">
    </div>
    <div style="position: relative; margin: 0 auto; width: 1000px; background-color: #FFFFFF;">
        <div style="position: absolute; top: 80px; width: 100%; height: 130px;">
            <div style="position: relative; top: 20px;">
                <div style="position: absolute; width: 900px; left: 0px;">
                    <asp:Panel ID="PanelCommonInformation"
                        runat="server"
                        EnableViewState="False"
                        ViewStateMode="Disabled">
                        <asp:Label runat="server" ID="LabelAbout" CssClass="Header1">
                        Направьте покупателю документы на проверку<br /><br />               
                        Если продавцов несколько, то отправьте документы по всем продавцам.
                        Например, в разделе Паспорт должны быть все паспорта продавцов.<br /><br /> 
                        Несколько файлов архивируйте в один файл, перед тем как отправить.
                        </asp:Label>
                    </asp:Panel>
                    <asp:Panel ID="PanelCloseForm"
                        runat="server"
                        Visible="False">
                        <asp:Label ID="Label2"
                            runat="server"
                            Text="Этап завершен"
                            ForeColor="#525252"
                            CssClass="Header0"> </asp:Label>
                    </asp:Panel>
                </div>
            </div>
        </div>
        <div style="position: absolute; top: 300px; width: 500px; height: 600px;">
            <asp:LinkButton ID="LinkButtonPassport"
                runat="server"
                OnClick="LinkPassport_Click"
                CssClass="Document">Паспорт</asp:LinkButton>
            &nbsp;&nbsp;
                    <asp:Label ID="LabelPassport"
                        runat="server"
                        CssClass="Status"
                        Text="LabelPassport"></asp:Label>
            <br />
            <br />
            <asp:LinkButton ID="LinkButtonSertificateOfOwnerShip"
                runat="server"
                OnClick="LinkButtonSertificateOfOwnerShip_Click"
                CssClass="Document">Свидетельство</asp:LinkButton>
            &nbsp;&nbsp;
                    <asp:Label ID="LabelSertificateOfOwnerShip"
                        runat="server"
                        CssClass="Status"
                        Text="LabelSertificateOfOwnerShip"></asp:Label>
            <br />
            <br />
            <asp:LinkButton ID="LinkButtonDocumentFoundation"
                runat="server"
                OnClick="LinkButtonDocumentFoundation_Click"
                CssClass="Document">Документ основание</asp:LinkButton>
            &nbsp;&nbsp;
                    <asp:Label ID="LabelDocumentFoundation"
                        runat="server"
                        CssClass="Status"
                        Text="LabelDocumentFoundation"></asp:Label>
            <br />
            <br />
            <asp:LinkButton ID="LinkButtonExtractFromRegister"
                runat="server"
                OnClick="LinkButtonExtractFromRegister_Click"
                CssClass="Document">Выписка ЕГРП</asp:LinkButton>
            &nbsp;&nbsp;
                    <asp:Label ID="LabelExtractFromRegister"
                        runat="server"
                        CssClass="Status"
                        Text="LabelExtractFromRegister"></asp:Label>
            <br />
            <br />
            <asp:LinkButton ID="LinkButtonExtendedExtractFromRegister"
                runat="server"
                OnClick="LinkButtonExtendedExtractFromRegister_Click"
                CssClass="Document">Расширенная выписка</asp:LinkButton>
            &nbsp;&nbsp;
                    <asp:Label ID="LabelExtendedExtractFromRegister"
                        runat="server"
                        CssClass="Status"
                        Text="LabelExtendedExtractFromRegister"></asp:Label>
            <br />
            <br />
            <asp:LinkButton ID="LinkButtonTechnicalPassport"
                runat="server"
                OnClick="LinkButtonTechnicalPassport_Click"
                CssClass="Document">Технический паспорт</asp:LinkButton>
            &nbsp;&nbsp;
                    <asp:Label ID="LabelTechnicalPassport"
                        runat="server"
                        CssClass="Status"
                        Text="LabelTechnicalPassport"></asp:Label>
            <br />
            <br />
            <asp:LinkButton ID="LinkButtonConsentOfSpouse"
                runat="server"
                OnClick="LinkButtonConsentOfSpouse_Click"
                CssClass="Document">Согласие супруга</asp:LinkButton>
            &nbsp;&nbsp;
                    <asp:Label ID="LabelConsentOfSpouse"
                        runat="server"
                        CssClass="Status"
                        Text="LabelConsentOfSpouse"></asp:Label>
            <br />
            <br />
            <asp:LinkButton ID="LinkButtonResolutionOfGuardianship"
                runat="server"
                OnClick="LinkButtonResolutionOfGuardianship_Click"
                CssClass="Document">Разрешение опеки</asp:LinkButton>
            &nbsp;&nbsp;
                    <asp:Label ID="LabelResolutionOfGuardianship"
                        runat="server"
                        CssClass="Status"
                        Text="LabelResolutionOfGuardianship"></asp:Label>
            <br />
            <br />
            <asp:LinkButton ID="LinkButtonDebtForUtility"
                runat="server"
                OnClick="LinkButtonDebtForUtility_Click"
                CssClass="Document">Справка по КУ</asp:LinkButton>
            &nbsp;&nbsp;
                    <asp:Label ID="LabelDebtForUtility"
                        runat="server"
                        CssClass="Status"
                        Text="LabelDebtForUtility"></asp:Label>
            <br />
            <br />
            <asp:LinkButton ID="LinkButtonCertificateFromTaxInspection"
                runat="server"
                OnClick="LinkButtonCertificateFromTaxInspection_Click"
                CssClass="Document">Справка по налогам</asp:LinkButton>
            &nbsp;&nbsp;
                    <asp:Label ID="LabelCertificateFromTaxInspection"
                        runat="server"
                        CssClass="Status"
                        Text="LabelCertificateFromTaxInspection"></asp:Label>
        </div>
        <div style="position: absolute; top: 300px; left: 500px; width: 450px; height: 600px;">
            <asp:Panel ID="PanelCommonUponAccepting"
                runat="server"
                ViewStateMode="Disabled"
                CssClass="Header2"
                Visible="False">
                Документ находится на согласовании
                <br />
                <br />
            </asp:Panel>
            <asp:Panel ID="PanelCommonDenide"
                runat="server"
                ViewStateMode="Disabled"
                CssClass="Header2"
                Visible="False">
                Документ отклонен
                <br />
                <br />
            </asp:Panel>
            <asp:Panel ID="PanelCommonCheck"
                runat="server"
                ViewStateMode="Disabled"
                CssClass="Header2"
                Visible="False">
                Документ проверен
                <br />
                <br />
            </asp:Panel>
            <asp:Panel ID="PanelPassportPrepare"
                runat="server"
                ViewStateMode="Disabled"
                CssClass="Header2"
                Visible="False">
                Здесь должна быть информация про подготовку паспорта и отправку его на согласование
                <br />
                <br />
            </asp:Panel>
            <asp:Panel ID="PanelPassportMain"
                runat="server"
                ViewStateMode="Enabled"
                Visible="False">
                <asp:FileUpload ID="FileUploadPassport"
                    runat="server"
                    Visible="False"
                    AllowMultiple="False"
                    ViewStateMode="Disabled" />
                <asp:LinkButton ID="LinkButtonSendPassport"
                    runat="server"
                    OnClick="LinkButtonSendPassport_Click"
                    Visible="False"
                    CssClass="ButtonInDocument"
                    ViewStateMode="Disabled">Отправить</asp:LinkButton>
                <asp:LinkButton ID="LinkButtonDenidePassport"
                    runat="server"
                    OnClick="LinkButtonDenidePassport_Click"
                    Visible="False"
                    CssClass="ButtonInDocument"
                    ViewStateMode="Disabled">Отменить</asp:LinkButton>
                <br />
                <br />
                <asp:Label ID="LabelPassportInformation"
                    runat="server"
                    Text="LabelPassportInformation"
                    Visible="False"
                    ViewStateMode="Disabled"></asp:Label>
            </asp:Panel>

            <asp:Panel ID="PanelSertificateOfOwnerShipPrepare"
                runat="server"
                ViewStateMode="Disabled"
                CssClass="Header2"
                Visible="False">
                Здесь должна быть информация про подготовку свидетельства и отправку его на согласование
                <br />
                <br />
            </asp:Panel>
            <asp:Panel ID="PanelSertificateOfOwnerShipMain"
                runat="server"
                ViewStateMode="Enabled"
                Visible="False">
                <asp:FileUpload ID="FileUploadSertificateOfOwnerShip"
                    runat="server"
                    Visible="False"
                    AllowMultiple="False"
                    ViewStateMode="Disabled" />
                <asp:LinkButton ID="LinkButtonSendSertificateOfOwnerShip"
                    runat="server"
                    OnClick="LinkButtonSendSertificateOfOwnerShip_Click"
                    Visible="False"
                    CssClass="ButtonInDocument">Отправить</asp:LinkButton>
                <asp:LinkButton ID="LinkButtonDenideSertificateOfOwnerShip"
                    runat="server"
                    OnClick="LinkButtonDenideSertificateOfOwnerShip_Click"
                    Visible="False"
                    CssClass="ButtonInDocument"
                    ViewStateMode="Disabled">Отменить</asp:LinkButton>
                <br />
                <br />
                <asp:Label ID="LabelSertificateOfOwnerShipInformation"
                    runat="server"
                    Text="LabelSertificateOfOwnerShipInformation"
                    Visible="False"
                    ViewStateMode="Disabled"></asp:Label>
            </asp:Panel>

            <asp:Panel ID="PanelDocumentFoundationPrepare"
                runat="server"
                ViewStateMode="Disabled"
                CssClass="Header2"
                Visible="False">
                Здесь должна быть информация про подготовку документа основания и отправку его на согласование
                <br />
                <br />
            </asp:Panel>
            <asp:Panel ID="PanelDocumentFoundationMain"
                runat="server"
                ViewStateMode="Enabled"
                Visible="False">
                <asp:FileUpload ID="FileUploadDocumentFoundation"
                    runat="server"
                    Visible="False"
                    AllowMultiple="False"
                    ViewStateMode="Disabled" />
                <asp:LinkButton ID="LinkButtonSendDocumentFoundation"
                    runat="server"
                    OnClick="LinkButtonSendDocumentFoundation_Click"
                    Visible="False"
                    CssClass="ButtonInDocument"
                    ViewStateMode="Disabled">Отправить</asp:LinkButton>
                <asp:LinkButton ID="LinkButtonDenideDocumentFoundation"
                    runat="server"
                    OnClick="LinkButtonDenideDocumentFoundation_Click"
                    Visible="False"
                    CssClass="ButtonInDocument"
                    ViewStateMode="Disabled">Отменить</asp:LinkButton>
                <br />
                <br />
                <asp:Label ID="LabelDocumentFoundationtInformation"
                    runat="server"
                    Text="LabelDocumentFoundationtInformation"
                    Visible="False"
                    ViewStateMode="Disabled"></asp:Label>
            </asp:Panel>

            <asp:Panel ID="PanelExtractFromRegisterPrepare"
                runat="server"
                ViewStateMode="Disabled"
                CssClass="Header2"
                Visible="False">
                Здесь должна быть информация про подготовку выписки из ЕГРП и отправку его на согласование
                <br />
                <br />
            </asp:Panel>
            <asp:Panel ID="PanelExtractFromRegisterMain"
                runat="server"
                ViewStateMode="Enabled"
                Visible="False">
                <asp:FileUpload ID="FileUploadExtractFromRegister"
                    runat="server"
                    Visible="False"
                    AllowMultiple="False"
                    ViewStateMode="Disabled" />
                <asp:LinkButton ID="LinkButtonSendExtractFromRegister"
                    runat="server"
                    OnClick="LinkButtonSendExtractFromRegister_Click"
                    Visible="False"
                    CssClass="ButtonInDocument"
                    ViewStateMode="Disabled">Отправить</asp:LinkButton>
                <asp:LinkButton ID="LinkButtonDenideExtractFromRegister"
                    runat="server"
                    OnClick="LinkButtonDenideExtractFromRegister_Click"
                    Visible="False"
                    CssClass="ButtonInDocument"
                    ViewStateMode="Disabled">Отменить</asp:LinkButton>
                <br />
                <br />
                <asp:Label ID="LabelExtractFromRegisterInformation"
                    runat="server"
                    Text="LabelExtractFromRegisterInformation"
                    Visible="False"
                    ViewStateMode="Disabled"></asp:Label>
            </asp:Panel>

            <asp:Panel ID="PanelExtendedExtractFromRegisterPrepare"
                runat="server"
                ViewStateMode="Disabled"
                CssClass="Header2"
                Visible="False">
                Здесь должна быть информация про подготовку выписки из домовой книги и отправку его на согласование
                <br />
                <br />
            </asp:Panel>
            <asp:Panel ID="PanelExtendedExtractFromRegisterMain"
                runat="server"
                ViewStateMode="Enabled"
                Visible="False">
                <asp:FileUpload ID="FileUploadExtendedExtractFromRegister"
                    runat="server"
                    Visible="False"
                    AllowMultiple="False"
                    ViewStateMode="Disabled" />
                <asp:LinkButton ID="LinkButtonSendExtendedExtractFromRegister"
                    runat="server"
                    OnClick="LinkButtonSendExtendedExtractFromRegister_Click"
                    Visible="False"
                    CssClass="ButtonInDocument"
                    ViewStateMode="Disabled">Отправить</asp:LinkButton>
                <asp:LinkButton ID="LinkButtonDenideExtendedExtractFromRegister"
                    runat="server"
                    OnClick="LinkButtonDenideExtendedExtractFromRegister_Click"
                    Visible="False"
                    CssClass="ButtonInDocument"
                    ViewStateMode="Disabled">Отменить</asp:LinkButton>
                <br />
                <br />
                <asp:Label ID="LabelExtendedExtractFromRegisterInformation"
                    runat="server"
                    Text="LabelExtendedExtractFromRegisterInformation"
                    Visible="False"
                    ViewStateMode="Enabled"></asp:Label>
            </asp:Panel>

            <asp:Panel ID="PanelTechnicalPassportPrepare"
                runat="server"
                ViewStateMode="Disabled"
                CssClass="Header2"
                Visible="False">
                Здесь должна быть информация про технического паспорта и отправку его на согласование
                <br />
                <br />
            </asp:Panel>
            <asp:Panel ID="PanelTechnicalPassportMain"
                runat="server"
                ViewStateMode="Enabled"
                Visible="False">
                <asp:FileUpload ID="FileUploadTechnicalPassport"
                    runat="server"
                    Visible="False"
                    AllowMultiple="False"
                    ViewStateMode="Disabled" />
                <asp:LinkButton ID="LinkButtonSendTechnicalPassport"
                    runat="server"
                    OnClick="LinkButtonSendTechnicalPassport_Click"
                    Visible="False"
                    CssClass="ButtonInDocument"
                    ViewStateMode="Disabled">Отправить</asp:LinkButton>
                <asp:LinkButton ID="LinkButtonDenideTechnicalPassport"
                    runat="server"
                    OnClick="LinkButtonDenideTechnicalPassport_Click"
                    Visible="False"
                    CssClass="ButtonInDocument"
                    ViewStateMode="Disabled">Отменить</asp:LinkButton>
                <br />
                <br />
                <asp:Label ID="LabelTechnicalPassportInformation"
                    runat="server"
                    Text="LabelTechnicalPassportInformation"
                    Visible="False"
                    ViewStateMode="Disabled"></asp:Label>
            </asp:Panel>

            <asp:Panel ID="PanelConsentOfSpousePrepare"
                runat="server"
                ViewStateMode="Disabled"
                CssClass="Header2"
                Visible="False">
                Здесь должна быть информация про согласие супруги и отправку его на согласование
                <br />
                <br />
            </asp:Panel>
            <asp:Panel ID="PanelConsentOfSpouseMain"
                runat="server"
                ViewStateMode="Enabled"
                Visible="False">
                <asp:FileUpload ID="FileUploadConsentOfSpouse"
                    runat="server"
                    Visible="False"
                    AllowMultiple="False"
                    ViewStateMode="Disabled" />
                <asp:LinkButton ID="LinkButtonSendConsentOfSpouse"
                    runat="server"
                    OnClick="LinkButtonSendConsentOfSpouse_Click"
                    Visible="False"
                    CssClass="ButtonInDocument"
                    ViewStateMode="Disabled">Отправить</asp:LinkButton>
                <asp:LinkButton ID="LinkButtonDenideConsentOfSpouse"
                    runat="server"
                    OnClick="LinkButtonDenideConsentOfSpouse_Click"
                    Visible="False"
                    CssClass="ButtonInDocument"
                    ViewStateMode="Disabled">Отменить</asp:LinkButton>
                <br />
                <br />
                <asp:Label ID="LabelConsentOfSpouseInformation"
                    runat="server"
                    Text="LabelConsentOfSpouseInformation"
                    Visible="False"
                    ViewStateMode="Disabled"></asp:Label>
            </asp:Panel>

            <asp:Panel ID="PanelResolutionOfGuardianshipPrepare"
                runat="server"
                ViewStateMode="Disabled"
                CssClass="Header2"
                Visible="False">
                Здесь должна быть информация про разрешение органов опеки и отправку его на согласование
                <br />
                <br />
            </asp:Panel>
            <asp:Panel ID="PanelResolutionOfGuardianshipMain"
                runat="server"
                ViewStateMode="Enabled"
                Visible="False">
                <asp:FileUpload ID="FileUploadResolutionOfGuardianship"
                    runat="server"
                    Visible="False"
                    AllowMultiple="False"
                    ViewStateMode="Disabled" />
                <asp:LinkButton ID="LinkButtonSendResolutionOfGuardianship"
                    runat="server"
                    OnClick="LinkButtonSendResolutionOfGuardianship_Click"
                    Visible="False"
                    CssClass="ButtonInDocument"
                    ViewStateMode="Disabled">Отправить</asp:LinkButton>
                <asp:LinkButton ID="LinkButtonDenideResolutionOfGuardianship"
                    runat="server"
                    OnClick="LinkButtonDenideResolutionOfGuardianship_Click"
                    Visible="False"
                    CssClass="ButtonInDocument"
                    ViewStateMode="Disabled">Отменить</asp:LinkButton>
                <br />
                <br />
                <asp:Label ID="LabelResolutionOfGuardianshipInformation"
                    runat="server"
                    Text="LabelResolutionOfGuardianshipInformation"
                    Visible="False"
                    ViewStateMode="Disabled"></asp:Label>
            </asp:Panel>

            <asp:Panel ID="PanelDebtForUtilityPrepare"
                runat="server"
                ViewStateMode="Disabled"
                CssClass="Header2"
                Visible="False">
                Здесь должна быть информация по коммуналке и отправку его на согласование
                <br />
                <br />
            </asp:Panel>
            <asp:Panel ID="PanelDebtForUtilityMain"
                runat="server"
                ViewStateMode="Enabled"
                Visible="False">
                <asp:FileUpload ID="FileUploadDebtForUtility"
                    runat="server"
                    Visible="False"
                    AllowMultiple="False"
                    ViewStateMode="Disabled" />
                <asp:LinkButton ID="LinkButtonSendDebtForUtility"
                    runat="server"
                    OnClick="LinkButtonSendDebtForUtility_Click"
                    Visible="False"
                    CssClass="ButtonInDocument"
                    ViewStateMode="Disabled">Отправить</asp:LinkButton>
                <asp:LinkButton ID="LinkButtonDenideDebtForUtility"
                    runat="server"
                    OnClick="LinkButtonDenideDebtForUtility_Click"
                    Visible="False"
                    CssClass="ButtonInDocument"
                    ViewStateMode="Disabled">Отменить</asp:LinkButton>
                <br />
                <br />
                <asp:Label ID="LabelDebtForUtilityInformation"
                    runat="server"
                    Text="LabelDebtForUtilityInformation"
                    Visible="False"
                    ViewStateMode="Disabled"></asp:Label>
            </asp:Panel>

            <asp:Panel ID="PanelCertificateFromTaxInspectionPrepare"
                runat="server"
                ViewStateMode="Disabled"
                CssClass="Header2"
                Visible="False">
                Здесь должна быть информация по налоговой задолженности и отправку его на согласование
                <br />
                <br />
            </asp:Panel>
            <asp:Panel ID="PanelCertificateFromTaxInspectionMain"
                runat="server"
                ViewStateMode="Enabled"
                Visible="False">
                <asp:FileUpload ID="FileUploadCertificateFromTaxInspection"
                    runat="server"
                    Visible="False"
                    AllowMultiple="False"
                    ViewStateMode="Disabled" />
                <asp:LinkButton ID="LinkButtonSendCertificateFromTaxInspection"
                    runat="server"
                    OnClick="LinkButtonSendCertificateFromTaxInspection_Click"
                    Visible="False"
                    CssClass="ButtonInDocument"
                    ViewStateMode="Disabled">Отправить</asp:LinkButton>
                <asp:LinkButton ID="LinkButtonDenideCertificateFromTaxInspection"
                    runat="server"
                    OnClick="LinkButtonDenideCertificateFromTaxInspection_Click"
                    Visible="False"
                    CssClass="ButtonInDocument"
                    ViewStateMode="Disabled">Отменить</asp:LinkButton>
                <br />
                <br />
                <asp:Label ID="LabelCertificateFromTaxInspectionInformation"
                    runat="server"
                    Text="LabelCertificateFromTaxInspectionInformation"
                    Visible="False"
                    ViewStateMode="Disabled"></asp:Label>
            </asp:Panel>

        </div>
    </div>
</asp:Content>
